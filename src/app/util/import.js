//サイト共通js

////////////////////////////////////////////////////////////////////////
// ● 基本JSセット


//読み込み項目
// shortcutmenu --------------------------------------------------------
function writeLeftMenu(rootDir){
	$.ajax({
		url: rootDir + "app/pages/wa0Common/wa0000/wa0000.shortcutmenu.html",
		cache: false,
		async: false,
		success: function(html){

			html = html.replace(/xlink:href="/g, 'xlink:href="' + rootDir);
			html = html.replace(/\{\$root\}/g, rootDir);
			document.write(html);
		}
	});
}

// commonhead ----------------------------------------------------------
function writeHeader(rootDir){
	$.ajax({
		url: rootDir + "app/pages/wa0Common/wa0000/wa0000.commonhead.html",
		cache: false,
		async: false,
		success: function(html){

			html = html.replace(/xlink:href="/g, 'xlink:href="' + rootDir);
			html = html.replace(/\{\$root\}/g, rootDir);
			document.write(html);
		}
	});
}

// sidemenu ----------------------------------------------------------
function writeSideMenu(rootDir){
	$.ajax({
		url: rootDir + "app/pages/wa0Common/wa0000/wa0000.sidemenu.html",
		cache: false,
		async: false,
		success: function(html){

			html = html.replace(/xlink:href="/g, 'xlink:href="' + rootDir);
			html = html.replace(/\{\$root\}/g, rootDir);
			document.write(html);
		}
	});
}


// Web Font-------------------------------------------------------------
