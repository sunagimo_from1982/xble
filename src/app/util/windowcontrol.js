$(function(){
	//modal閉じるボタン
	$("i.close").click(function(){
		closeModalWindow();
	});
	
	//overlayをクリックしたら閉じる
	$(".overlay").click(function(ev){
		var modal = $(".modal");
		
		if($(this).prop("id").length > 0){
			modal =  $("#" + $(this).prop("id")).find(".modal");
		}
		
		var clickpoint_Y = ev.pageY;
		var clickpoint_X = ev.pageX;
		
		var modalleft = modal.offset().left;
		var modaltop = modal.offset().top;
		var modalright = modal.offset().left +  modal.width();
		var modalbottom = modal.offset().top +  modal.height();
		if(clickpoint_Y != 0 && clickpoint_X != 0){
			if(!((clickpoint_Y > modaltop && clickpoint_Y < modalbottom ) &&
			   (clickpoint_X > modalleft && clickpoint_X < modalright))){
				closeModalWindow();
			}
		}
	});
	
	
	//ag-grid 検索メニューがあったらSELECTサイズを変更
	//当初クリックイベントだったが、なぜか最終行とその手前の行でクリックイベントが取れない
	//そのためインターバルで継続監視（もっと良い方法があればそちらに変更）
	setInterval(function(){
		if($(".ag-menu")[0]){
			if($(".ag-filter-select")[0] && $(".ag-filter-select").attr("size") != 6){
			   $(".ag-filter-select").attr("size","6");
			}
		}
	},100);
	
	//select制御
	$("select.content").each(function(){
		if($(this).val().length > 0){
			$(this).addClass("normal");
		}
		$(this).change(function(){
			if($(this).val().length == 0){
				$(this).removeClass("active");
				$(this).removeClass("normal");
				$(this).addClass("default");
			}else{
				$(this).removeClass("default");
				$(this).removeClass("normal");
				$(this).addClass("active");
			}
		});   
		   
	});
	
	$("input[type='text'],input[type='time'],input[type='password'],input[type='date'],input[type='number']").change(function(){
		if($(this).val().length == 0){
			$(this).removeClass("active");
		}else{
			$(this).addClass("active");
		}
	});
	
	//autoloader
	var loaderItem = ["0000111 荷主ダミーテキストA","0000112 荷主ダミーテキストB","0000113 荷主ダミーテキストC","0000114 荷主ダミーテキストD","0000115 荷主ダミーテキストE","0000116 荷主ダミーテキストF","0000117 荷主ダミーテキストG","0000118 荷主ダミーテキストH","ALL"];
		
	
	//フォーカスで選択領域を展開
	//フォーカスアウト、アイテムクリックで領域を閉じる
	$(".headerAutoload").focusin(function(){
		var h ="";
		//すでに入力値があるか判定
		//あったらその分を引いたデータを表示
		if($(this).val().length == 0){
			for(var i = 0; i < loaderItem.length;i++){
				h += "<li class='headerAutoloader-list'>" + loaderItem[i] + "</li>";
			}
		}else{
			for(var i = 0; i < loaderItem.length;i++){
				if(loaderItem[i].indexOf($(this).val()) != -1){
					h += "<li class='headerAutoloader-list'>" + loaderItem[i] + "</li>";
				}
			}

		}
		
		$(this).next(".headerAutoloader").html(h);
		
		$(this).next(".headerAutoloader").removeClass("dspnone");
		setLoaderFunction_header();
		

	}).focusout(function(){
		var obj = $(this);
		setTimeout(function(){
			obj.next(".headerAutoloader").addClass("dspnone");
		},200);
	});	
	//入力があったら非対象のデータを間引く
	$(".headerAutoload").keyup(function(){
		var h ="";
		for(var i = 0; i < loaderItem.length;i++){
			if(loaderItem[i].indexOf($(this).val()) != -1){
				h += "<li class='headerAutoloader-list'>" + loaderItem[i] + "</li>";
			}
		}
		$(this).next(".headerAutoloader").html(h);
		setLoaderFunction_header();
	});
	
	$("#headerAutoloaderReset").click(function(){
		$("#ninushi").val("");
		$("#headerAutoloaderReset").addClass("dspnone");
	});
	
	$("#ninushi").change(function(){
		if($(".headerAutoload").val().length == 0){
			$("#headerAutoloaderReset").addClass("dspnone");
		}else{
			$("#headerAutoloaderReset").removeClass("dspnone");

		}
	});
});

function setLoaderFunction_header(){
	$(".headerAutoloader .headerAutoloader-list").click(function(){
		$("#ninushi").val($(this).html());
		$(this).parent().addClass("dspnone");

		
		$(this).parent().prev("input").change();
	});

}
function getLocaleText(){
	return localeText = {noRowsToShow: ' ',equals:"等しい",notEqual:"等しくない",startsWith:"～で始まる",endsWith:"～で終わる",contains:"含む",notContains:"含まない"};
	
}
//ag-gridの仕様電話再描画されるためソートするとチェックボックスがデフォルトデザインに戻ってしまうためそれを防ぐ
function headerCheckboxChange(){
	$(".ag-header .ag-checkbox-unchecked img").css("display","none");
	$(".ag-header .ag-checkbox-checked img").css("display","none");
	$(".ag-header .ag-checkbox-indeterminate img").css("display","none");
}
//ag-gridの仕様電話再描画されるためソートするとチェックボックスがデフォルトデザインに戻ってしまうためそれを防ぐ
function checkboxChange(){
	$(".ag-row .ag-selection-checkbox img:nth-child(1)").attr("src","../../../../assets/images/checkbox_checked.png");
	$(".ag-row .ag-selection-checkbox img:nth-child(2)").attr("src","../../../../assets/images/checkbox_body.png");

}
//modalのopen,close
function openModalWindow(){
	$(".overlay").removeClass("dspnone");

	/*if($("#myGrid6")[0]){
		$("#myGrid6 div.ag-body-viewport").css("height",$("#myGrid6 div.ag-body-container").height() + 5 + "px");
	}*/
}
function closeModalWindow(){
	$(".overlay").addClass("dspnone");
}
//入荷実績登録のopen、close
function openOtherWindow(){
	$("div.otherwindow").removeClass("dspnone");
	$("div.otherwindow").addClass("openotherwindow");
}
function closeOtherWindow(){
	$("div.otherwindow").removeClass("openotherwindow");
	//display:noneでアニメーションやtransitionを使うと機能しないため
	//○○秒後にdisplay:noneを発動させる
	setTimeout(function(){
		$("div.otherwindow").addClass("dspnone");		
	}, 300);
}

//tab切替
function changeTab(tnum){
	var cnt = 1;
	$("section.tabs > ul.tabs-nav > li.tabs-nav__item").each(function() {
		$("#tab"+ cnt + "-item").addClass("dspnone");
		if($(this)[0].id == "t" + tnum){
			$("#t" + cnt).addClass("tabs-nav__item--active");
			$("#tab"+ cnt + "-item").removeClass("dspnone");
			
			var dt = 0;
			if(tnum == 4){
				dt = 30;
			}
			setGridData1("myGrid" + tnum,dt);

		}else{
			$("#t" + cnt).removeClass("tabs-nav__item--active");
		}
		cnt++;
	});

	
}
//上部からメッセージボックスがおりてくる
function showMessageBox(msg){
	//表示メッセージセット
	$(".messageBox--msg").html(msg);
	$(".messageBox").addClass("messageBox-open");
	//3秒たったら隠す
	setTimeout(function(){
		$(".messageBox").removeClass("messageBox-open");		
	}, 3000);
}
function showMessageBox_error(msg){
	//表示メッセージセット
	$(".messageBox-error--msg").html(msg);
	$(".messageBox-error").addClass("messageBox-open");
	//3秒たったら隠す
	setTimeout(function(){
		$(".messageBox-error").removeClass("messageBox-open");		
	}, 3000);
}

function gridWidth(owname,coldef,gridname,tablename){
	var ow = $(owname).innerWidth() - 40;
	//Gridの横幅が画面幅以下なら調整する
	var w = 20;
	for(var i = 0 ; i < coldef.length ;i++){
		w += coldef[i].width;
	}

	if(ow > w){
		$(gridname ).css("width","calc(100% - 30px)");
		$(tablename).css("width","calc(100% - 30px)");
		
		
	}
	
}
function gridHeight(owname,rowdata,gridname,tablename,flg1 = 0,flg2 = 0){
	var oh = $(owname).innerHeight();
	//Gridの横幅が画面幅以下なら調整する
	var h = 95;

	
	if ($(gridname + " .ag-paging-panel").css('display') == 'none' || flg1 == 1) {
		h = 55;
	}	
	for(var i = 0 ; i < rowdata.length ;i++){
		h += 40;
	}

	//初期表示しているすべて（TAB4）のみロード時に横スクロールの表示設定を行う
	var parentArea = $(gridname + " div.ag-body-container").width();

	var tbarea = $(gridname + " div.ag-body-viewport").scrollTop() + $(gridname + " div.ag-body-viewport").width();

	if(parentArea - tbarea > 0 && rowdata.length != 0){

		h += 17;
	}

	if(oh > h){
		if(rowdata.length != 0){

			h += 7;
		}else{
			h += 5;
		}
		$(gridname ).css("height",h + "px");
		$(tablename).css("height",h + "px");
	}else{
		$(gridname ).css("height","");
		$(tablename).css("height","");
	}
}
function hiddenPagerControl(divid,type){
	if(type == 0){
		if($("#myGrid" + divid + " .ag-paging-panel")[0]){
			$("#myGrid" + divid + " .ag-paging-panel").hide();
		}
		if($("#pagerAllCount" + divid)[0]){
			$("#pagerAllCount" + divid).hide();
		}
		if($("#pagerItem" + divid)[0]){
			$("#pagerItem" + divid).hide();
		}
		if($("#pagercounter" + divid)[0]){
			$("#pagercounter" + divid).hide();
		}
	}else{
		if($("#myGrid" + divid + " .ag-paging-panel")[0]){
			$("#myGrid" + divid + " .ag-paging-panel").show();
		}
		if($("#pagerAllCount" + divid)[0]){
			$("#pagerAllCount" + divid).show();
		}
		if($("#pagerItem" + divid)[0]){
			$("#pagerItem" + divid).show();
		}
		if($("#pagercounter" + divid)[0]){
			$("#pagercounter" + divid).show();
		}
	}

}

function dateZeroAd(data){

	if(data.toString().length == 1){
		data = "0" + data.toString();
		console.log(data)
	}
	return data;
}