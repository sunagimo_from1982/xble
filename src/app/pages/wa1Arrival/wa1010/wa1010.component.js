$(function(){

	//各tab内のモーダル生成
	setGridData1("myGrid4",30);
	gridHeight("#tab4-item",rowData,"#myGrid4","#table4",1);
	//入荷次席登録
	setGridData2("myGrid5",30);
	//モーダル
	setGridData3("myGrid6",2);
	
	



	//初期表示しているすべて（TAB4）のみロード時に横スクロールの場所設定を行う
	var parentArea = $("#myGrid4 div.ag-body-container").height();

	var tbarea = $("#myGrid4 div.ag-body-viewport").scrollTop() + $("#myGrid4 div.ag-body-viewport").height();
	if(parentArea - tbarea > 0){
		//スクロールバーがある場合の処理
		$("#table4 div.scrollArrow").css("right","16px");
	}else{
		//スクロールバーがない場合の処理
		$("#table4 div.scrollArrow").css("right","0px");

	}

	//初期表示しているすべて（TAB4）のみロード時に横スクロールの表示設定を行う
	var parentArea = $("#myGrid4 div.ag-body-container").width();

	var tbarea = $("#myGrid4 div.ag-body-viewport").scrollTop() + $("#myGrid4 div.ag-body-viewport").width();

	if(parentArea - tbarea > 0){
		//スクロールバーがある場合の処理
		$("#table4 div.scrollArrow").removeClass("dspnone");
		var height = $("#myGrid4").innerHeight() - 76;
		var dspheight = $("#myGrid4 .ag-body-container").innerHeight() ;
		
		var gh = 0;
		if(height < dspheight){
			gh = height;
		}else{
			gh = dspheight;
		}
		
		$("#table4 div.scrollArrow").css("height",gh + "px");

	}else{
		//スクロールバーがない場合の処理
		$("#table4 div.scrollArrow").addClass("dspnone");

	}



	/*TAB4のテーブル内の→マウスオーバー挙動*/
	var tm1 = 0;
	var tm2 = 0;
	var scrollMax = 0;
	var dspflg1 = true;
	//スクロールイベント
	$("#table4 div.scrollArrow").hover(function(){
		tm2 = setTimeout(function(){
			tm1 = setInterval(function(){
				//スクロールがMAXになったら表示を消すためにここで値を取得
				scrollMax = $("#myGrid4 div.ag-body-container").width() - $("#myGrid4 div.ag-body-viewport").width();

				$("#myGrid4 div.ag-body-viewport").scrollLeft( $("#myGrid4 div.ag-body-viewport").scrollLeft() + 2);

				if(scrollMax <= $("#myGrid4 div.ag-body-viewport").scrollLeft() && dspflg1){
					$("#table1 div.scrollArrow").addClass("dspnone");
					dspflg1 = false;
					clearTimeout(tm2);
					clearInterval(tm1);
				}
			},0);
		},300);
	},function(){
		clearTimeout(tm2);
		clearInterval(tm1);
	});
	//scrollArrowが消えているときにscrollMAXと値が変わったら再表示
	$("#myGrid4 div.ag-body-viewport").scroll(function(){
		
		scrollMax = $("#myGrid4 div.ag-body-container").width() - $("#myGrid4 div.ag-body-viewport").width();
		
		if(scrollMax <= $("#myGrid4 div.ag-body-viewport").scrollLeft()){
			$("#table4 div.scrollArrow").addClass("dspnone");
			dspflg4 = false;
		}
		
		if(scrollMax > $("#myGrid4 div.ag-body-viewport").scrollLeft() && !dspflg4){
			$("#table4 div.scrollArrow").removeClass("dspnone");
			dspflg4 = true;
		}
	});




});
//テーブル作成用ファンクション
var columnDefs1;
var gridOptions1;
var rowData;
function setGridData1(divid,data){

	// ヘッダー

	//headerName=列の題名,field=列のID,width=幅、cellClas=各セルに適用するクラス
	//headerCheckboxSelection=ヘッダーにチェックボックスを付けるか？
	//checkboxSelection=各行にチェックボックスを表示するか？
	//pinned=列固定,suppressMenu=ヘッダー一列目に検索フィールドを出すか？
	//hide=表示、非表示、sortable=ソートができるかできないか
	//filter=ヘッダー2列目の検索フィールドを何にするか？
	
	/*ヘッダーデータをタイプで入れ替える*/
	columnDefs1 = [
		{headerName: "", field: "col0", width: 30,cellClass: ['tc','cl_w','noCellFocus'],
		 headerCheckboxSelection: true,checkboxSelection: true,suppressMenu: true,
		 lockPosition:true,pinned: 'left'
		},
		/*スクロール時にステータスフィールドが消えたら表示するフィールド*/
		{headerName: "", field: "col99",suppressMenu: true,width:8,
		 cellClass: ['tc','cellclass'],hide:true,pinned: 'left',
			cellStyle: function(params) {
				//ステータス色変化
				if (params.value=='0') {
					return {backgroundColor: 'rgba(235,153,175,0.2)',color: '#eb99af'};
				}else if (params.value=='1') {
					return {backgroundColor: 'rgba(227,230,87,0.2)',color: 'rgba(227,230,87,0.2)'};
				}else if (params.value=='2') {
					return {backgroundColor: 'rgba(201,202,202,0.2)',color: 'rgba(201,202,202,0.2)'};
				} else {
					return null;
				}
			}
		},
		{headerName: "入荷予定ID", field: "col1", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tr','cellclass']},
		{headerName: "入荷ステータス", field: "col2", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tc','cellclass'],
			cellStyle: function(params) {
				//ステータス色変化
				if (params.value=='未入荷') {
					return {backgroundColor: 'rgba(235,153,175,0.2)'};
				}else if (params.value=='受付中') {
					return {backgroundColor: 'rgba(227,230,87,0.2)'};
				}else if (params.value=='入荷完了') {
					return {backgroundColor: 'rgba(201,202,202,0.2)',color:'#b5b4b3'};
				} else {
					return null;
				}
			}
		},
		{headerName: "入荷予定日", field: "col3", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tc','cellclass']},
		{headerName: "発注ID", field: "col4", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:80,cellClass: ['tc','cellclass']},
		{headerName: "荷主コード", field: "col5", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tc','cellclass']},
		{headerName: "荷主名", field: "col6", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:300,cellClass: ['tl','cellclass']},
		{headerName: "商品コード", field: "col7", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:150,cellClass: ['tc','cellclass']},
		{headerName: "商品名", field: "col8", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:300,cellClass: ['tl','cellclass']},
		{headerName: "入荷番号", field: "col9", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:150,cellClass: ['tc','cellclass']},
		{headerName: "ロット", field: "col10", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:80,cellClass: ['tr','cellclass']},
		{headerName: "単位名", field: "col11", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:80,cellClass: ['tl','cellclass']},
		{headerName: "人数", field: "col12", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:80,cellClass: ['tr','cellclass']},
		{headerName: "入荷予定数", field: "col13", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tr','cellclass']},
		{headerName: "入荷予定残数", field: "col14", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tr','cellclass']},
		{headerName: "仕入れ先", field: "col15", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tc','cellclass']}
	];	

	// 行のテストデータ

	rowData = [];

	for(var i = 0; i < data;i++){
		rowData.push({col0: i,col99:0,col1: '1544' + i, col2: '未入荷', col3: '2019-02-28', col4: 'C-6', col5: '00001', col6: 'SESOS株式会社', col7: '17391104-043', col8: '紙BD・こまﾍﾟｰﾊﾟﾚｽﾛｰﾙｷｬpｶﾗｰ', col9: '20180228-0001', col10: '12', col11: 'カートン', col12: '1', col13: '200', col14: '100', col15: ''});

		rowData.push({col0: i,col99:2,col1: '1544', col2: '入荷完了', col3: '2019-02-28', col4: 'C-6', col5: '00001', col6: 'SESOS株式会社', col7: '17391104-043', col8: '紙BD・こまﾍﾟｰﾊﾟﾚｽﾛｰﾙｷｬpｶﾗｰ', col9: '20180228-0001', col10: '12', col11: 'カートン', col12: '1', col13: '200', col14: '100', col15: ''});

		rowData.push({col0: i,col99:1,col1: '1544', col2: '受付中', col3: '2019-02-28', col4: 'C-6', col5: '00001', col6: 'SESOS株式会社', col7: '17391104-043', col8: '紙BD・こまﾍﾟｰﾊﾟﾚｽﾛｰﾙｷｬpｶﾗｰ', col9: '20180228-0001', col10: '12', col11: 'カートン', col12: '1', col13: '200', col14: '100', col15: ''});
	}
			

	// オプションの設定
	gridOptions1 = {
		enableColResize:true,
		enableSorting: true,
		enableFilter: true,
		headerHeight: 30,
		floatingFiltersHeight:30,
		columnDefs: columnDefs1,/*headedrデータ*/
		rowData: rowData,/*行データ*/
		floatingFilter: true,/*2列目のフィルターのon/off*/
		suppressRowClickSelection:true,/*チェックボックス以外のクリックを認めるか？*/
		rowSelection :'multiple',/*複数選択できるように*/
		localeText: getLocaleText(),/*データ０の時のメッセージ*/
		rowHeight : 40
	};
	


	//   対象となる要素の指定
	var eGridDiv = document.querySelector('#' + divid);

	//   グリッドの生成
	new agGrid.Grid(eGridDiv, gridOptions1);

	//セルクリック時に行を選択状態にする
	gridOptions1.onCellClicked = function(param){
		/*クリックされたセルの取得*/
		var cols = param.colDef.field;
		var index = param.node.childIndex;
		//セルがチェックボックスセルだったら
		if(cols == "col0"){
			//状況により行のセレクトをon/offする
			var node = gridOptions1.api.getRowNode(index);
			if(node.isSelected()){
				node.setSelected(false);

			}else{
				node.setSelected(true);
			}
		}
	}
	//ag-gridの仕様電話再描画されるためソートするとチェックボックスがデフォルトデザインに戻ってしまうためそれを防ぐ
	gridOptions1.onSortChanged = function(param){
		checkboxChange();
	}

	gridOptions1.onFilterChanged = function(param){
		//モーダル内の検索インプットへのアクティブクラス付与
		$(".ag-floating-filter-input").each(function(){
			if($(this).val().length == 0){
				$(this).removeClass("active");
			}else{
				$(this).addClass("active");
			}
		});
		checkboxChange();
		
	}
	
	/*今回の場合はテーブル4*/
	var chkflg4 = false;
	//横スクロールでステータス領域が消えた時に左はじにカラー領域を表示
	$("#myGrid4 div.ag-body-viewport").scroll(function(e){
		//今回のステータス位置が185付近なので今回はそこに設定
		if($("#myGrid4 div.ag-body-viewport").scrollLeft() > 185 && !chkflg4 ){
			chkflg4 = true;

			showcol(true);
		}else if($("#myGrid4 div.ag-body-viewport").scrollLeft() <= 185 && chkflg4){
			chkflg4 = false;
			showcol(false);

		}
		checkboxChange();
	});
	//列の表示非表示切り替え
	function showcol(show) {
		gridOptions1.columnApi.setColumnVisible('col99', show);
	}
	
	var index =[];
	$("#myGrid4 .ag-body-viewport-wrapper .ag-body-container .ag-row").each(function(){
		var row = $(this);

		$(this).find(".ag-cell").each(function(){

			if($(this).attr("colid") == "col2"){
				if($(this).html() == "入荷完了"){
					index.push(row.attr("row"));
					row.addClass("status3_rowstyle");
				}
			}
		});
	});
	
	$("#myGrid4 .ag-pinned-left-cols-viewport .ag-pinned-left-cols-container .ag-row").each(function(){
		var row = $(this);
		
		for(var i = 0; i < index.length;i++){
			if($(this).attr("row") == index[i]){
				row.addClass("status3_rowstyle");
		   }
		}
	});
	headerCheckboxChange();
	checkboxChange();
	gridWidth(".tableitem",columnDefs1,"#myGrid1", "#table1");

}

//テーブル作成用ファンクション-入荷実績登録用
var columnDefs2;
var gridOptions2;
var rowData2;
function setGridData2(divid,data){

	// ヘッダー

	//headerName=列の題名,field=列のID,width=幅、cellClas=各セルに適用するクラス
	//headerCheckboxSelection=ヘッダーにチェックボックスを付けるか？
	//checkboxSelection=各行にチェックボックスを表示するか？
	//pinned=列固定,suppressMenu=ヘッダー一列目に検索フィールドを出すか？
	//hide=表示、非表示、sortable=ソートができるかできないか
	//filter=ヘッダー2列目の検索フィールドを何にするか？
	
	/*ヘッダーデータをタイプで入れ替える*/
	columnDefs2 = [
		{headerName: "入荷予定ID", field: "col1",
		 suppressMenu: true,sortable:true,width:100,cellClass: ['tr','cellclass']},
		{headerName: "入荷予定日", field: "col2",
		 suppressMenu: true,sortable:true,width:100,cellClass: ['tr','cellclass']},
		{headerName: "商品名", field: "col3",
		 suppressMenu: true,sortable:true,width:200,cellClass: ['tl','cellclass']},
		{headerName: "商品コード", field: "col4",
		 suppressMenu: true,sortable:true,width:150,cellClass: ['tc','cellclass']},
		{headerName: "単位名", field: "col5",
		 suppressMenu: true,sortable:true,width:100,cellClass: ['tc','cellclass']},
		{headerName: "入荷予定数", field: "col6",
		 suppressMenu: true,sortable:true,width:80,cellClass: ['tr','cellclass']},
		{headerName: "入荷予定残数", field: "col7",
		 suppressMenu: true,sortable:true,width:80,cellClass: ['tr','cellclass']},
		{headerName: "格納ロケーション", field: "col8",editable: true,
		 suppressMenu: true,sortable:true,width:100,cellClass: 
		function(params) {
			//値によって追加するクラスを可変させる
			if(params.value.length != 0){
				return ['tl','cellclass','editClass','noCellFocus',"txtactive"];
			}
			return ['tl','cellclass','editClass','noCellFocus'];
		}},
		{headerName: "入荷数", field: "col9",editable: true,
		 suppressMenu: true,sortable:true,width:100,cellClass:  
		function(params) {
			//値によって追加するクラスを可変させる
			if(params.value.length != 0){
				return ['tr','cellclass','editClass','noCellFocus',"txterror"];
			}
			return ['tr','cellclass','editClass','noCellFocus'];
		}},
		{headerName: "", field: "col10",
		 suppressMenu: true,sortable:true,width:135,cellClass: ['tc','cellclass'],
		 cellRenderer: (params) => {
				const element = document.createElement('div');
				element.innerHTML = "<a class='btn_secondary' href='#'>強制入荷完了</a>";

				return element;
			}
		}
	];			
		

	// 行のテストデータ

	rowData2 = [];

	for(var i = 0; i < data;i++){
		var txt = "";
		//testデータ
		if(i == 0){
			txt = "test";
		}
		
		rowData2.push({col1: 1544 + i , col2: '2019-01-25', col3: '紙BD・こまﾍﾟｰﾊﾟﾚｽﾛｰﾙｷｬpｶﾗｰ', 
					  col4: '17391104-043', col5: 'カートン', col6: '200', 
					  col7: '100', col8: txt, 
					  col9: txt, col10: ''});
	}
			

	// オプションの設定
	gridOptions2 = {
		enableColResize:true,
		enableSorting: true,
		enableFilter: true,
		headerHeight: 30,
		floatingFiltersHeight:30,
		columnDefs: columnDefs2,/*headedrデータ*/
		rowData: rowData2,/*行データ*/
		floatingFilter: false,/*2列目のフィルターのon/off*/
		suppressRowClickSelection:true,/*チェックボックス以外のクリックを認めるか？*/
		rowSelection :'multiple',/*複数選択できるように*/
		localeText: getLocaleText(),/*データ０の時のメッセージ*/
		rowHeight : 40
	};

	//   対象となる要素の指定
	var eGridDiv = document.querySelector('#' + divid);

	//   グリッドの生成
	new agGrid.Grid(eGridDiv, gridOptions2);



}//テーブル作成用ファンクション-入荷実績送信用（モーダル）
var columnDefs3;
var gridOption3s;
var rowData3;
function setGridData3(divid,data){

	// ヘッダー

	//headerName=列の題名,field=列のID,width=幅、cellClas=各セルに適用するクラス
	//headerCheckboxSelection=ヘッダーにチェックボックスを付けるか？
	//checkboxSelection=各行にチェックボックスを表示するか？
	//pinned=列固定,suppressMenu=ヘッダー一列目に検索フィールドを出すか？
	//hide=表示、非表示、sortable=ソートができるかできないか
	//filter=ヘッダー2列目の検索フィールドを何にするか？
	
	/*ヘッダーデータをタイプで入れ替える*/
	columnDefs3 = [
		{headerName: "", field: "col0", width: 30,cellClass: ['tc','noCellFocus'],
		 headerCheckboxSelection: true,checkboxSelection: true,suppressMenu: true,
		 lockPosition:true,pinned: 'left'
		},
		{headerName: "荷主コード", field: "col1", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tr','cellclass']},
		{headerName: "荷主名", field: "col2", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:610,cellClass: ['tl','cellclass']}
	];			
		

	// 行のテストデータ

	rowData3 = [];

	for(var i = 0; i < data;i++){
		rowData3.push({col0: i,col1: '0000' + i, col2: 'コカ・コーラボトラーズジャパン株式会社'});
	}
			

	// オプションの設定
	gridOptions3 = {
		enableColResize:true,
		enableSorting: true,
		enableFilter: true,
		headerHeight: 30,
		floatingFiltersHeight:30,
		columnDefs: columnDefs3,/*headedrデータ*/
		rowData: rowData3,/*行データ*/
		floatingFilter: true,/*2列目のフィルターのon/off*/
		suppressRowClickSelection:true,/*チェックボックス以外のクリックを認めるか？*/
		rowSelection :'multiple',/*複数選択できるように*/
		localeText: getLocaleText(),/*データ０の時のメッセージ*/
		rowHeight : 40,
		domLayout: 'autoHeight'

	};

	//   対象となる要素の指定
	var eGridDiv = document.querySelector('#' + divid);

	//   グリッドの生成
	new agGrid.Grid(eGridDiv, gridOptions3);


	//セルクリック時に行を選択状態にする
	gridOptions3.onCellClicked = function(param){
		/*クリックされたセルの取得*/
		var cols = param.colDef.field;
		var index = param.rowIndex;
		//セルがチェックボックスセルだったら
		if(cols == "col0"){
			//状況により行のセレクトをon/offする
			var node = gridOptions3.api.getRowNode(index);
			if(node.isSelected()){
				node.setSelected(false);

			}else{
				node.setSelected(true);
			}
		}
	}
	//ag-gridの仕様電話再描画されるためソートするとチェックボックスがデフォルトデザインに戻ってしまうためそれを防ぐ
	gridOptions3.onSortChanged = function(param){
		checkboxChange();
	}
	gridOptions3.onFilterChanged = function(param){
		//モーダル内の検索インプットへのアクティブクラス付与
		$(".ag-floating-filter-input").each(function(){
			if($(this).val().length == 0){
				$(this).removeClass("active");
			}else{
				$(this).addClass("active");
			}
		});
		checkboxChange();
	}
	/*今回の場合はテーブル4*/
	var chkflg4 = false;
	//横スクロールでステータス領域が消えた時に左はじにカラー領域を表示
	$("#myGrid6 div.ag-body-viewport").scroll(function(e){
		checkboxChange();
	});
	headerCheckboxChange();
	checkboxChange();
	gridWidth(".tableitem",columnDefs3,"#myGrid3", "#table3");
}

//tab切替
function changeTab(tnum){
	var cnt = 1;
	$("section.tabs > ul.tabs-nav > li.tabs-nav__item").each(function() {
		$("#tab"+ cnt + "-item").addClass("dspnone");
		if($(this)[0].id == "t" + tnum){
			$("#t" + cnt).addClass("tabs-nav__item--active");
			$("#tab"+ cnt + "-item").removeClass("dspnone");
			
			var dt = 0;
			if(tnum == 4){
				dt = 30;
			}
			setGridData1("myGrid" + tnum,dt);
			gridHeight("#tab" + tnum + "-item",rowData,"#myGrid" + tnum,"#table" + tnum,1);

		}else{
			$("#t" + cnt).removeClass("tabs-nav__item--active");
		}
		cnt++;
	});

	
}
//modal内のspredに謎のスクロールが発生
//親要素を少し大きくするスクリプト
function modalSpredHeightControl(){
	if($("#myGrid6")[0]){
		$("#myGrid6 div.ag-body-viewport").css("height",$("#myGrid6 div.ag-body-container").height() + 5 + "px");
	}
}
function openModalWindow_wa1010(){
	openModalWindow();
	//gridWidth(".modal",columnDefs3,"#myGrid6", ".modal-content__tablearea");
}
function openOtherWindow_wa1010(){
	openOtherWindow();
	gridWidth(".otherwindow",columnDefs2,"#myGrid5", ".otherwindow-tablearea");
	gridHeight_wa1010(".otherwindow",rowData2,"#myGrid5",".otherwindow-tablearea",1);
	
}

function gridHeight_wa1010(owname,rowdata,gridname,tablename,flg1 = 0,flg2 = 0){
	var oh = $(owname).innerHeight();
	//Gridの横幅が画面幅以下なら調整する
	var h = 25;

	
	for(var i = 0 ; i < rowdata.length ;i++){
		h += 40;
	}

	//初期表示しているすべて（TAB4）のみロード時に横スクロールの表示設定を行う
	var parentArea = $(gridname + " div.ag-body-container").width();

	var tbarea = $(gridname + " div.ag-body-viewport").scrollTop() + $(gridname + " div.ag-body-viewport").width();

	if(parentArea - tbarea > 0){
		
		h += 17;
	}
	console.log(oh);
	console.log(h);
	if(oh > h){
		h += 7;
		$(gridname ).css("height",h + "px");
		$(tablename).css("height",h + "px");
	}else{
		$(gridname ).css("height","");
		$(tablename).css("height","");
	}
}