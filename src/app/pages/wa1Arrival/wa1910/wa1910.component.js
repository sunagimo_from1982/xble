$(function(){
	//アップロード（input=file）クリックし、ファイルを選択したときにファイル名を表示するスクリプト
	if($("#upload")){
		setInterval(function(){

			if($("#upload").val().length == 0){
				$("#filename").html("ファイルが選択されていません。");
			}else{
				$("#filename").html("<span><img class='deletebtn' src='../../../../assets/images/delete.png'></span>ファイル：" + $("#upload")[0].files[0].name);
				
				$("#upload_btn").removeClass("disabled");
			}
		},1000);
	}

	
});



// JavaScript Document
function selectSelected(id){
	//未選択
	if($("#" + id).val() == "0"){
		$("#" + id).removeClass("active");
		$("#" + id).removeClass("error");
		$("#" + id).removeClass("disabled");
	//active
	}else if($("#" + id).val() == "1"){
		$("#" + id).addClass("active");
		$("#" + id).removeClass("error");
		$("#" + id).removeClass("disabled");
	//error
	}else if($("#" + id).val() == "2"){
		$("#" + id).removeClass("active");
		$("#" + id).addClass("error");
		$("#" + id).removeClass("disabled");
	//disabled
	}else if($("#" + id).val() == "3"){
		$("#" + id).removeClass("active");
		$("#" + id).removeClass("error");
		$("#" + id).addClass("disabled");
	}
}
