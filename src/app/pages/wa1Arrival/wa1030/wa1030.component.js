$(function(){

	//各tab内のモーダル生成
	setGridData1("myGrid1",30);
	
	



	//初期表示しているすべて（TAB4）のみロード時に横スクロールの場所設定を行う
	var parentArea = $("#myGrid1 div.ag-body-container").height();

	var tbarea = $("#myGrid1 div.ag-body-viewport").scrollTop() + $("#myGrid1 div.ag-body-viewport").height();
	if(parentArea - tbarea > 0){
		//スクロールバーがある場合の処理
		$("#table1 div.scrollArrow").css("right","16px");
	}else{
		//スクロールバーがない場合の処理
		$("#table1 div.scrollArrow").css("right","0px");

	}

	//初期表示しているすべて（TAB4）のみロード時に横スクロールの表示設定を行う
	var parentArea = $("#myGrid1 div.ag-body-container").width();

	var tbarea = $("#myGrid1 div.ag-body-viewport").scrollTop() + $("#myGrid1 div.ag-body-viewport").width();

	if(parentArea - tbarea > 0){
		//スクロールバーがある場合の処理
		$("#table1 div.scrollArrow").removeClass("dspnone");
		var height = $("#myGrid1").innerHeight() - 76;
		var dspheight = $("#myGrid1 .ag-body-container").innerHeight() ;
		
		var gh = 0;
		if(height < dspheight){
			gh = height;
		}else{
			gh = dspheight;
		}
		
		$("#table1 div.scrollArrow").css("height",gh + "px");

	}else{
		//スクロールバーがない場合の処理
		$("#table1 div.scrollArrow").addClass("dspnone");

	}



	/*TAB4のテーブル内の→マウスオーバー挙動*/
	var tm1 = 0;
	var tm2 = 0;
	var scrollMax = 0;
	var dspflg1 = true;
	//スクロールイベント
	$("#table1 div.scrollArrow").hover(function(){
		tm2 = setTimeout(function(){
			tm1 = setInterval(function(){
				//スクロールがMAXになったら表示を消すためにここで値を取得
				scrollMax = $("#myGrid1 div.ag-body-container").width() - $("#myGrid1 div.ag-body-viewport").width();

				$("#myGrid1 div.ag-body-viewport").scrollLeft( $("#myGrid1 div.ag-body-viewport").scrollLeft() + 2);

				if(scrollMax <= $("#myGrid1 div.ag-body-viewport").scrollLeft() && dspflg1){
					$("#table1 div.scrollArrow").addClass("dspnone");
					dspflg1 = false;
					clearTimeout(tm2);
					clearInterval(tm1);
				}
			},0);
		},300);
	},function(){
		clearTimeout(tm2);
		clearInterval(tm1);
	});
	
	
	
	
	
	
	//scrollArrowが消えているときにscrollMAXと値が変わったら再表示
	$("#myGrid1 div.ag-body-viewport").scroll(function(){
		
		//スクロールがMAXになったら表示を消すためにここで値を取得

		scrollMax = $("#myGrid1 div.ag-body-container").width() - $("#myGrid1 div.ag-body-viewport").width();
		if(scrollMax <= $("#myGrid1 div.ag-body-viewport").scrollLeft()){
			$("#table1 div.scrollArrow").addClass("dspnone");
			dspflg1 = false;
		}
		
		
		
		if(scrollMax > $("#myGrid1 div.ag-body-viewport").scrollLeft() && !dspflg1){
			$("#table1 div.scrollArrow").removeClass("dspnone");
			dspflg1 = true;
		}
	});



});
//テーブル作成用ファンクション
var columnDefs1;
var gridOptions1;
function setGridData1(divid,data){

	// ヘッダー

	//headerName=列の題名,field=列のID,width=幅、cellClas=各セルに適用するクラス
	//headerCheckboxSelection=ヘッダーにチェックボックスを付けるか？
	//checkboxSelection=各行にチェックボックスを表示するか？
	//pinned=列固定,suppressMenu=ヘッダー一列目に検索フィールドを出すか？
	//hide=表示、非表示、sortable=ソートができるかできないか
	//filter=ヘッダー2列目の検索フィールドを何にするか？
	
	/*ヘッダーデータをタイプで入れ替える*/
	columnDefs1 = [
		{headerName: "", field: "col0", width: 30,cellClass: ['tc','cl_w','noCellFocus'],
		 headerCheckboxSelection: true,checkboxSelection: true,suppressMenu: true,
		 lockPosition:true,pinned: 'left'
		},
		/*スクロール時にステータスフィールドが消えたら表示するフィールド*/
		{headerName: "", field: "col99",suppressMenu: true,width:8,
		 cellClass: ['tc','cellclass'],hide:true,pinned: 'left',
			cellStyle: function(params) {
				//ステータス色変化
				if (params.value=='0') {
					return {backgroundColor: 'rgba(235,153,175,0.2)',color: '#eb99af'};
				}else if (params.value=='1') {
					return {backgroundColor: 'rgba(201,202,202,0.2)',color: 'rgba(201,202,202,0.2)'};
				} else {
					return null;
				}
			}
		},
		{headerName: "荷主コード", field: "col1", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "荷主名", field: "col2", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "入庫ステータス", field: "col3", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tc','cellclass'],
			cellStyle: function(params) {
				//ステータス色変化
				if (params.value=='未入庫') {
					return {backgroundColor: 'rgba(235,153,175,0.2)'};
				}else if (params.value=='入庫完了') {
					return {backgroundColor: 'rgba(201,202,202,0.2)',color:'#b5b4b3'};
				} else {
					return null;
				}
			}
		},
		{headerName: "入庫指示No.", field: "col4", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:80,cellClass: ['tr','cellclass']},
		{headerName: "商品コード", field: "col5", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "商品名", field: "col6", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:300,cellClass: ['tl','cellclass']},
		{headerName: "ロット", field: "col7", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:80,cellClass: ['tl','cellclass']},
		{headerName: "使用期限", field: "col8", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "入庫日", field: "col9", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "人数", field: "col10", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:80,cellClass: ['tr','cellclass']},
		{headerName: "入庫数", field: "col11", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:80,cellClass: ['tr','cellclass']},
		{headerName: "入荷番号", field: "col12", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
		{headerName: "発注番号", field: "col13", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
		{headerName: "発注行番号", field: "col14", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:80,cellClass: ['tl','cellclass']},
		{headerName: "入荷予定日", field: "col15", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "入荷予定数", field: "col16", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:80,cellClass: ['tr','cellclass']},
		{headerName: "計上・日時", field: "col17", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
		{headerName: "計上・ユーザ名", field: "col18", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "先ロケーションコード", field: "col19", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
		{headerName: "先ロケーション名", field: "col20", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
		{headerName: "先ロケ・在庫区分", field: "col21", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:80,cellClass: ['tl','cellclass']},
		{headerName: "先ロケ・開始日時", field: "col22", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:180,cellClass: ['tl','cellclass']},
		{headerName: "先ロケ・終了日時", field: "col23", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:180,cellClass: ['tl','cellclass']},
		{headerName: "先ロケ・ユーザ名", field: "col24", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "更新日時", field: "col25", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:180,cellClass: ['tl','cellclass']},
		{headerName: "更新者名", field: "col26", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "登録日時", field: "col27", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:180,cellClass: ['tl','cellclass']},
		{headerName: "登録者名", field: "col28", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tl','cellclass']}
	];	

	// 行のテストデータ

	rowData = [];

	for(var i = 0; i < data;i++){
		rowData.push({col0: i,col99:0,col1: '0000' + i, col2: 'システムサービス', col3: '未入庫', col4: '99', col5: 'SS9749', col6: 'TED2 ﾊﾞﾝﾀﾞﾅうつぶせぬいぐるみ', col7: '1', col8: '2018-12-21', col9: '2018-12-10', col10: '10', col11: '600', col12: '20180927-001518', col13: 'AR-0927-464', col14: '1', col15: '2018-09-27', col16: '600', col17: '2018-09-20', col18: '山本', col19: 'PFTEST0927-107', col20: 'PFTEST0927-107', col21: '良品', col22: '2018-09-28 16:07:32', col23: '2018-09-28 16:07:32', col24: '山本', col25: '2018-09-28 16:07:32', col26: '山本', col27: '2018-09-28 16:07:32', col28: '山本'});

		rowData.push({col0: i,col99:1,col1: '0000' + i, col2: 'システムサービス', col3: '入庫完了', col4: '99', col5: 'SS9749', col6: 'TED2 ﾊﾞﾝﾀﾞﾅうつぶせぬいぐるみ', col7: '1', col8: '2018-12-21', col9: '2018-12-10', col10: '10', col11: '600', col12: '20180927-001518', col13: 'AR-0927-464', col14: '1', col15: '2018-09-27', col16: '600', col17: '2018-09-20', col18: '山本', col19: 'PFTEST0927-107', col20: 'PFTEST0927-107', col21: '良品', col22: '2018-09-28 16:07:32', col23: '2018-09-28 16:07:32', col24: '山本', col25: '2018-09-28 16:07:32', col26: '山本', col27: '2018-09-28 16:07:32', col28: '山本'});

	}
			

	// オプションの設定
	gridOptions1 = {
		enableColResize:true,
		enableSorting: true,
		enableFilter: true,
		headerHeight: 30,
		floatingFiltersHeight:30,
		columnDefs: columnDefs1,/*headedrデータ*/
		rowData: rowData,/*行データ*/
		floatingFilter: true,/*2列目のフィルターのon/off*/
		suppressRowClickSelection:true,/*チェックボックス以外のクリックを認めるか？*/
		rowSelection :'multiple',/*複数選択できるように*/
		localeText: getLocaleText(),/*データ０の時のメッセージ*/
		rowHeight : 40
	};
	


	//   対象となる要素の指定
	var eGridDiv = document.querySelector('#' + divid);

	//   グリッドの生成
	new agGrid.Grid(eGridDiv, gridOptions1);

	//セルクリック時に行を選択状態にする
	gridOptions1.onCellClicked = function(param){
		/*クリックされたセルの取得*/
		var cols = param.colDef.field;
		var index = param.node.childIndex;
		//セルがチェックボックスセルだったら
		if(cols == "col0"){
			//状況により行のセレクトをon/offする
			var node = gridOptions1.api.getRowNode(index);
			if(node.isSelected()){
				node.setSelected(false);

			}else{
				node.setSelected(true);
			}
		}
	}
	//ag-gridの仕様電話再描画されるためソートするとチェックボックスがデフォルトデザインに戻ってしまうためそれを防ぐ
	gridOptions1.onSortChanged = function(param){
		checkboxChange();
	}

	gridOptions1.onFilterChanged = function(param){
		//モーダル内の検索インプットへのアクティブクラス付与
		$(".ag-floating-filter-input").each(function(){
			if($(this).val().length == 0){
				$(this).removeClass("active");
			}else{
				$(this).addClass("active");
			}
		});
		checkboxChange();
		
	}
	
	/*今回の場合はテーブル1*/
	var chkflg1 = false;
	//横スクロールでステータス領域が消えた時に左はじにカラー領域を表示
	$("#myGrid1 div.ag-body-viewport").scroll(function(e){
		//今回のステータス位置が185付近なので今回はそこに設定
		if($("#myGrid1 div.ag-body-viewport").scrollLeft() > 285 && !chkflg1 ){
			chkflg1 = true;

			showcol(true);
		}else if($("#myGrid1 div.ag-body-viewport").scrollLeft() <= 285 && chkflg1){
			chkflg1 = false;
			showcol(false);

		}
		checkboxChange();
	});
	//列の表示非表示切り替え
	function showcol(show) {
		gridOptions1.columnApi.setColumnVisible('col99', show);
	}
	
	headerCheckboxChange();
	checkboxChange();
	gridWidth(".tableitem",columnDefs1,"#myGrid1","#table1")
	gridHeight(".tableitem",rowData,"#myGrid1","#table1",1);	

}

