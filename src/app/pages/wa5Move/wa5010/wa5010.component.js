$(function(){

	//各tab内のモーダル生成
	setGridData1("1",30);
	//入荷次席登録
	setGridData2("5",30);
	//モーダル
	setGridData3("6",7);
	
	//モーダル
	setGridData4("7",7);
	//ケース、バラ数inputがチェンジされかつ入力があったら移動先入力へ
	/*$(".otherwindow input").change(function(){
		var chk = 0;
		$(".otherwindow input").each(function(){
			var index = $(".otherwindow input").index(this);
			if(index == 0 || index == 1){
				if($(this).val().length > 0) chk++;
			}
		});
		
		if(chk == 2){
			$("#otherwindow-disabled1").removeClass("otherwindow-dropshadow");
			$("#otherwindow-disabled2").removeClass("otherwindow-disabled");
			$("#otherwindow-disabled2").addClass("otherwindow-dropshadow");
		}
	});*/
	
});


var columnDefs1_1;
var gridOptions1_1;
var rowData1_1 = [];
function setGridData1_1(divid,data){

	// ヘッダー

	//headerName=列の題名,field=列のID,width=幅、cellClas=各セルに適用するクラス
	//headerCheckboxSelection=ヘッダーにチェックボックスを付けるか？
	//checkboxSelection=各行にチェックボックスを表示するか？
	//pinned=列固定,suppressMenu=ヘッダー一列目に検索フィールドを出すか？
	//hide=表示、非表示、sortable=ソートができるかできないか
	//filter=ヘッダー2列目の検索フィールドを何にするか？
	
	/*ヘッダーデータをタイプで入れ替える*/
	
	columnDefs1_1 = getColmnDefs(divid);


	// 行のテストデータ

	rowData1_1 = getRowData(divid,data);


			
	// オプションの設定
	gridOptions1_1 = {
		enableColResize:true,
		enableSorting: true,
		enableFilter: true,
		headerHeight: 30,
		floatingFiltersHeight:30,
		columnDefs: columnDefs1_1,/*headedrデータ*/
		rowData: rowData1_1,/*行データ*/
		floatingFilter: true,/*2列目のフィルターのon/off*/
		suppressRowClickSelection:true,/*チェックボックス以外のクリックを認めるか？*/
		rowSelection :'multiple',/*複数選択できるように*/
		localeText: getLocaleText(),/*データ０の時のメッセージ*/
		rowHeight : 40,
		pagination:true,
		paginationPageSize:rowData1_1.length
	};
	


	//   対象となる要素の指定
	var eGridDiv = document.querySelector('#myGrid' + divid);

	//   グリッドの生成
	new agGrid.Grid(eGridDiv, gridOptions1_1);

	setFunctions(gridOptions1_1,divid,rowData1_1);
	
	/*今回の場合はテーブル4*/
	var chkflg4 = false;
	//横スクロールでステータス領域が消えた時に左はじにカラー領域を表示
	$("#myGrid" + divid + " div.ag-body-viewport").scroll(function(e){
		//今回のステータス位置が185付近なので今回はそこに設定
		if($("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() > 395 && !chkflg4 ){
			chkflg4 = true;

			showcol(true);
		}else if($("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() <= 395 && chkflg4){
			chkflg4 = false;
			showcol(false);

		}
		checkboxChange();
	});
	//列の表示非表示切り替え
	function showcol(show) {
		gridOptions1_1.columnApi.setColumnVisible('col99', show);
	}
	
	gridOptions1_1.onRowSelected = function(param){
		$(".tableitem__btnarea--rightarea span a").removeClass("disabled");
	}
	

	getAllCnt(rowData1_1,divid,gridOptions1_1);
	
	
	headerCheckboxChange();
	checkboxChange();
	spredScrollArrowChange(divid);
}

var columnDefs1_2;
var gridOptions1_2;
var rowData1_2 = [];
function setGridData1_2(divid,data){

	// ヘッダー

	//headerName=列の題名,field=列のID,width=幅、cellClas=各セルに適用するクラス
	//headerCheckboxSelection=ヘッダーにチェックボックスを付けるか？
	//checkboxSelection=各行にチェックボックスを表示するか？
	//pinned=列固定,suppressMenu=ヘッダー一列目に検索フィールドを出すか？
	//hide=表示、非表示、sortable=ソートができるかできないか
	//filter=ヘッダー2列目の検索フィールドを何にするか？
	
	/*ヘッダーデータをタイプで入れ替える*/
	
	columnDefs1_2 = getColmnDefs(divid);


	// 行のテストデータ

	rowData1_2 = getRowData(divid,data);


			
	// オプションの設定
	gridOptions1_2 = {
		enableColResize:true,
		enableSorting: true,
		enableFilter: true,
		headerHeight: 30,
		floatingFiltersHeight:30,
		columnDefs: columnDefs1_2,/*headedrデータ*/
		rowData: rowData1_2,/*行データ*/
		floatingFilter: true,/*2列目のフィルターのon/off*/
		suppressRowClickSelection:true,/*チェックボックス以外のクリックを認めるか？*/
		rowSelection :'multiple',/*複数選択できるように*/
		localeText: getLocaleText(),/*データ０の時のメッセージ*/
		rowHeight : 40,
		pagination:true,
		paginationPageSize:rowData1_2.length
	};
	


	//   対象となる要素の指定
	var eGridDiv = document.querySelector('#myGrid' + divid);

	//   グリッドの生成
	new agGrid.Grid(eGridDiv, gridOptions1_2);

	setFunctions(gridOptions1_2,divid,rowData1_2);
	
	/*今回の場合はテーブル4*/
	var chkflg4 = false;
	//横スクロールでステータス領域が消えた時に左はじにカラー領域を表示
	$("#myGrid" + divid + " div.ag-body-viewport").scroll(function(e){
		//今回のステータス位置が185付近なので今回はそこに設定
		if($("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() > 395 && !chkflg4 ){
			chkflg4 = true;

			showcol(true);
		}else if($("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() <= 395 && chkflg4){
			chkflg4 = false;
			showcol(false);

		}
		checkboxChange();
	});
	//列の表示非表示切り替え
	function showcol(show) {
		gridOptions1_2.columnApi.setColumnVisible('col99', show);
	}
	
	gridOptions1_2.onRowSelected = function(param){
		$(".tableitem__btnarea--rightarea span a").removeClass("disabled");
	}
	

	getAllCnt(rowData1_2,divid,gridOptions1_2);
	
	
	headerCheckboxChange();
	checkboxChange();
	spredScrollArrowChange(divid);
}


var columnDefs1_3;
var gridOptions1_3;
var rowData1_3 = [];
function setGridData1_3(divid,data){

	// ヘッダー

	//headerName=列の題名,field=列のID,width=幅、cellClas=各セルに適用するクラス
	//headerCheckboxSelection=ヘッダーにチェックボックスを付けるか？
	//checkboxSelection=各行にチェックボックスを表示するか？
	//pinned=列固定,suppressMenu=ヘッダー一列目に検索フィールドを出すか？
	//hide=表示、非表示、sortable=ソートができるかできないか
	//filter=ヘッダー2列目の検索フィールドを何にするか？
	
	/*ヘッダーデータをタイプで入れ替える*/
	
	columnDefs1_3 = getColmnDefs(divid);


	// 行のテストデータ

	rowData1_3 = getRowData(divid,data);


			
	// オプションの設定
	gridOptions1_3 = {
		enableColResize:true,
		enableSorting: true,
		enableFilter: true,
		headerHeight: 30,
		floatingFiltersHeight:30,
		columnDefs: columnDefs1_3,/*headedrデータ*/
		rowData: rowData1_3,/*行データ*/
		floatingFilter: true,/*2列目のフィルターのon/off*/
		suppressRowClickSelection:true,/*チェックボックス以外のクリックを認めるか？*/
		rowSelection :'multiple',/*複数選択できるように*/
		localeText: getLocaleText(),/*データ０の時のメッセージ*/
		rowHeight : 40,
		pagination:true,
		paginationPageSize:20
	};
	


	//   対象となる要素の指定
	var eGridDiv = document.querySelector('#myGrid' + divid);

	//   グリッドの生成
	new agGrid.Grid(eGridDiv, gridOptions1_3);

	setFunctions(gridOptions1_3,divid,rowData1_3);
	
	/*今回の場合はテーブル4*/
	var chkflg4 = false;
	//横スクロールでステータス領域が消えた時に左はじにカラー領域を表示
	$("#myGrid" + divid + " div.ag-body-viewport").scroll(function(e){
		//今回のステータス位置が185付近なので今回はそこに設定
		if($("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() > 375 && !chkflg4 ){
			chkflg4 = true;

			showcol(true);
		}else if($("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() <= 375 && chkflg4){
			chkflg4 = false;
			showcol(false);

		}
		checkboxChange();
	});
	//列の表示非表示切り替え
	function showcol(show) {
		gridOptions1_3.columnApi.setColumnVisible('col99', show);
	}
	
	

	getAllCnt(rowData1_3,divid,gridOptions1_3);
	
	
	headerCheckboxChange();
	checkboxChange();
	spredScrollArrowChange(divid);
}


var columnDefs1_4;
var gridOptions1_4;
var rowData1_4 = [];
function setGridData1_4(divid,data){

	// ヘッダー

	//headerName=列の題名,field=列のID,width=幅、cellClas=各セルに適用するクラス
	//headerCheckboxSelection=ヘッダーにチェックボックスを付けるか？
	//checkboxSelection=各行にチェックボックスを表示するか？
	//pinned=列固定,suppressMenu=ヘッダー一列目に検索フィールドを出すか？
	//hide=表示、非表示、sortable=ソートができるかできないか
	//filter=ヘッダー2列目の検索フィールドを何にするか？
	
	/*ヘッダーデータをタイプで入れ替える*/
	
	columnDefs1_4 = getColmnDefs(divid);


	// 行のテストデータ

	rowData1_4 = getRowData(divid,data);


			
	// オプションの設定
	gridOptions1_4 = {
		enableColResize:true,
		enableSorting: true,
		enableFilter: true,
		headerHeight: 30,
		floatingFiltersHeight:30,
		columnDefs: columnDefs1_4,/*headedrデータ*/
		rowData: rowData1_4,/*行データ*/
		floatingFilter: true,/*2列目のフィルターのon/off*/
		suppressRowClickSelection:true,/*チェックボックス以外のクリックを認めるか？*/
		rowSelection :'multiple',/*複数選択できるように*/
		localeText: getLocaleText(),/*データ０の時のメッセージ*/
		rowHeight : 40,
		pagination:true,
		paginationPageSize:20
	};
	


	//   対象となる要素の指定
	var eGridDiv = document.querySelector('#myGrid' + divid);

	//   グリッドの生成
	new agGrid.Grid(eGridDiv, gridOptions1_4);

	setFunctions(gridOptions1_4,divid,rowData1_4);
	
	/*今回の場合はテーブル4*/
	var chkflg4 = false;
	//横スクロールでステータス領域が消えた時に左はじにカラー領域を表示
	$("#myGrid" + divid + " div.ag-body-viewport").scroll(function(e){
		//今回のステータス位置が185付近なので今回はそこに設定
		if($("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() > 375 && !chkflg4 ){
			chkflg4 = true;

			showcol(true);
		}else if($("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() <= 375 && chkflg4){
			chkflg4 = false;
			showcol(false);

		}
		checkboxChange();
	});
	//列の表示非表示切り替え
	function showcol(show) {
		gridOptions1_4.columnApi.setColumnVisible('col99', show);
	}
	
	

	getAllCnt(rowData1_4,divid,gridOptions1_4);
	
	
	headerCheckboxChange();
	checkboxChange();
	spredScrollArrowChange(divid);
}

function setFunctions(gridOptions,divid,rowData){
		//セルクリック時に行を選択状態にする
	gridOptions.onCellClicked = function(param){
		/*クリックされたセルの取得*/
		var cols = param.colDef.field;
		var index = param.node.childIndex;
		//セルがチェックボックスセルだったら
		if(cols == "col0"){
			//状況により行のセレクトをon/offする
			var node = gridOptions.api.getRowNode(index);
			if(node.isSelected()){
				node.setSelected(false);

			}else{
				node.setSelected(true);
			}
		}
	}
	//ag-gridの仕様電話再描画されるためソートするとチェックボックスがデフォルトデザインに戻ってしまうためそれを防ぐ
	gridOptions.onSortChanged = function(param){
		checkboxChange();
	}

	gridOptions.onFilterChanged = function(param){
		//モーダル内の検索インプットへのアクティブクラス付与
		$(".ag-floating-filter-input").each(function(){
			if($(this).val().length == 0){
				$(this).removeClass("active");
			}else{
				$(this).addClass("active");
			}
		});
		checkboxChange();
		
	}
	gridOptions.onSelectionChanged= function(param){
		getAllCnt(rowData,divid,gridOptions);
		
	}
	if(divid <= 4){
		var sort = [
			{colId: 'col1', sort: 'asc'}
		];
		gridOptions.api.setSortModel(sort);
	}
	
	
	if(rowData.length > 0 && (divid == 3 || divid == 4)){
		$("#pagerItem" + divid + ".tableitem__tablearea__pagerItem .disabled").removeClass("disabled");
		//各矢印クリックでページ移動
		$("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i").click(function(){
			if($(this).prop("id") == "first" && !$(this).hasClass("disabled")){
				gridOptions.api.paginationGoToFirstPage();
			}else if($(this).prop("id") == "prev" && !$(this).hasClass("disabled")){
				gridOptions.api.paginationGoToPreviousPage();
			}else if($(this).prop("id") == "next" && !$(this).hasClass("disabled")){
				gridOptions.api.paginationGoToNextPage();
			}else if($(this).prop("id") == "last" && !$(this).hasClass("disabled")){
				gridOptions.api.paginationGoToLastPage()
			}
			setPageSep(gridOptions,divid);
		});
		setPageSep(gridOptions,divid);
	}
	var coldef = columnDefs1_1;
	var rd = rowData1_1;
	if(divid == "2"){
		coldef = columnDefs1_2;
		rd = rowData1_2;
	}else if(divid == "3"){
		coldef = columnDefs1_3;
		rd = rowData1_3;
	}else if(divid == "4"){
		coldef = columnDefs1_4;
		rd = rowData1_4;
	}
	
	gridWidth(".tableitem",coldef,"#myGrid" + divid, "#table" + divid)
	gridHeight(".tableitem",rd,"#myGrid" + divid, "#table" + divid);	
	
}
//pagerの表示件数を変更
function updatePager1(){
	var value = $("#select5").val();
    gridOptions1_3.api.paginationSetPageSize(Number(value));
	checkboxChange();
	setPageSep(gridOptions1_3,3);
}
function updatePager2(){
	var value = $("#select6").val();
    gridOptions1_4.api.paginationSetPageSize(Number(value));
	checkboxChange();
	setPageSep(gridOptions1_4,4);
}
//pagerの真ん中の構築 
function setPageSep(gridOptions,divid){

	var h = "<span class='tableitem__tablearea__pagerItem__nowpage--desc--big'>" + (gridOptions.api.paginationGetCurrentPage() + 1) + "</span> /" + gridOptions.api.paginationGetTotalPages() + " ";
	$("#pagerItem" + divid + " .tableitem__tablearea__pagerItem__nowpage--desc").html(h);
	//1ページ目では前に戻れないようにdisabled
	if(gridOptions.api.paginationGetCurrentPage() + 1 == 1){
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#first").addClass("disabled");
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#prev").addClass("disabled");
	}else{
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#first").removeClass("disabled");
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#prev").removeClass("disabled");
	}
	console.log(divid);
	//LASTページ目では次にいけないようにdisabled
	if(gridOptions.api.paginationGetCurrentPage() + 1 == gridOptions.api.paginationGetTotalPages()){
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#next").addClass("disabled");
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#last").addClass("disabled");
	}else{
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#next").removeClass("disabled");
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#last").removeClass("disabled");
	}
	
}

//スプレッドの矢印の構築・再構築
function spredScrollArrowChange(divid){
	//初期表示しているすべて（TAB4）のみロード時に横スクロールの場所設定を行う
	var parentArea = $("#myGrid" + divid + " div.ag-body-container").height();

	var tbarea = $("#myGrid" + divid + " div.ag-body-viewport").scrollTop() + $("#myGrid" + divid + " div.ag-body-viewport").height();
	if(parentArea - tbarea > 0){
		//スクロールバーがある場合の処理
		$("#table" + divid + " div.scrollArrow").css("right","16px");
	}else{
		//スクロールバーがない場合の処理
		$("#table" + divid + " div.scrollArrow").css("right","0px");

	}

	//初期表示しているすべて（TAB4）のみロード時に横スクロールの表示設定を行う
	var parentArea = $("#myGrid" + divid + " div.ag-body-container").width();

	var tbarea = $("#myGrid" + divid + " div.ag-body-viewport").scrollTop() + $("#myGrid" + divid + " div.ag-body-viewport").width();

	if(parentArea - tbarea > 0){
		//スクロールバーがある場合の処理
		$("#table" + divid + " div.scrollArrow").removeClass("dspnone");
		var height = $("#myGrid" + divid + "").innerHeight() - 121;
		var dspheight = $("#myGrid" + divid + " .ag-body-container").innerHeight() ;

		var gh = 0;
		if(height < dspheight){
			gh = height;
		}else{
			gh = dspheight;
		}

		$("#table" + divid + " div.scrollArrow").css("height",gh + "px");

	}else{
		//スクロールバーがない場合の処理
		$("#table" + divid + " div.scrollArrow").addClass("dspnone");

	}



	/*TAB4のテーブル内の→マウスオーバー挙動*/
	var tm1 = 0;
	var tm2 = 0;
	var scrollMax = 0;
	var dspflg1 = true;
	//スクロールイベント
	$("#table" + divid + " div.scrollArrow").hover(function(){
		tm2 = setTimeout(function(){
			tm1 = setInterval(function(){
				//スクロールがMAXになったら表示を消すためにここで値を取得
				scrollMax = $("#myGrid" + divid + " div.ag-body-container").width() - $("#myGrid" + divid + " div.ag-body-viewport").width();

				$("#myGrid" + divid + " div.ag-body-viewport").scrollLeft( $("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() + 2);

				if(scrollMax <= $("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() && dspflg1){
					$("#table" + divid + " div.scrollArrow").addClass("dspnone");
					dspflg1 = false;
					clearTimeout(tm2);
					clearInterval(tm1);
				}
			},0);
		},300);
	},function(){
		clearTimeout(tm2);
		clearInterval(tm1);
	});

	//scrollArrowが消えているときにscrollMAXと値が変わったら再表示
	$("#myGrid" + divid + " div.ag-body-viewport").scroll(function(){

		scrollMax = $("#myGrid" + divid + " div.ag-body-container").width() - $("#myGrid" + divid + " div.ag-body-viewport").width();

		if(scrollMax <= $("#myGrid" + divid + " div.ag-body-viewport").scrollLeft()){
			$("#table" + divid + " div.scrollArrow").addClass("dspnone");
			dspflg1 = false;
		}

		if(scrollMax > $("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() && !dspflg1){
			$("#table" + divid + " div.scrollArrow").removeClass("dspnone");
			dspflg1 = true;
		}
	});	
}
//テーブル作成用ファンクション-入荷実績登録用
var columnDefs2;
var gridOptions2;
var rowData2;
function setGridData2(divid,data){

	// ヘッダー

	//headerName=列の題名,field=列のID,width=幅、cellClas=各セルに適用するクラス
	//headerCheckboxSelection=ヘッダーにチェックボックスを付けるか？
	//checkboxSelection=各行にチェックボックスを表示するか？
	//pinned=列固定,suppressMenu=ヘッダー一列目に検索フィールドを出すか？
	//hide=表示、非表示、sortable=ソートができるかできないか
	//filter=ヘッダー2列目の検索フィールドを何にするか？
	
	/*ヘッダーデータをタイプで入れ替える*/
	columnDefs2 = [
		{headerName: "", field: "col0", width: 30,cellClass: ['tc','cl_w','noCellFocus'],
		 headerCheckboxSelection: true,checkboxSelection: true,suppressMenu: true,
		 lockPosition:true,pinned: 'left'
		},
		{headerName: "荷主コード", field: "col1",
		 suppressMenu: true,sortable:true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "荷主名", field: "col2",
		 suppressMenu: true,sortable:true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "商品コード", field: "col3",
		 suppressMenu: true,sortable:true,width:200,cellClass: ['tl','cellclass']},
		{headerName: "商品名", field: "col4",
		 suppressMenu: true,sortable:true,width:200,cellClass: ['tl','cellclass']},
		{headerName: "商品コード", field: "col5",
		 suppressMenu: true,sortable:true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "ロット", field: "col6",
		 suppressMenu: true,sortable:true,width:100,cellClass: ['tr','cellclass']},
		{headerName: "仕様期限", field: "col7",
		 suppressMenu: true,sortable:true,width:120,cellClass: ['tr','cellclass']},
		{headerName: "入数", field: "col8",
		 suppressMenu: true,sortable:true,width:80,cellClass: ['tr','cellclass']},
		{headerName: "移動数(ケース）", field: "col9",
		 suppressMenu: true,sortable:true,width:120,cellClass: ['tr','cellclass']},
		{headerName: "移動数（バラ）", field: "col10",
		 suppressMenu: true,sortable:true,width:120,cellClass: ['tr','cellclass']},
		{headerName: "移動数", field: "col11",
		 suppressMenu: true,sortable:true,width:120,cellClass: ['tr','cellclass']},
		{headerName: "元ロケ・ロケーションコード", field: "col12",
		 suppressMenu: true,sortable:true,width:200,cellClass: ['tr','cellclass']},
		{headerName: "元ロケ", field: "col13",
		 suppressMenu: true,sortable:true,width:120,cellClass: ['tr','cellclass']},
		{headerName: "元ロケ・在庫区分", field: "col14",
		 suppressMenu: true,sortable:true,width:140,cellClass: ['tr','cellclass']},
		{headerName: "先ロケ・ロケーションコード", field: "col9",
		 suppressMenu: true,sortable:true,width:200,cellClass: ['tr','cellclass']},
		{headerName: "先ロケ", field: "col15",
		 suppressMenu: true,sortable:true,width:120,cellClass: ['tr','cellclass']},
		{headerName: "先ロケ・在庫区分", field: "col16",
		 suppressMenu: true,sortable:true,width:140,cellClass: ['tr','cellclass']}	];			
		

	// 行のテストデータ

	rowData2 = [];

	/*for(var i = 0; i < data;i++){
		
		rowData.push({col1: 1544 + i , col2: '2019-01-25', col3: '紙BD・こまﾍﾟｰﾊﾟﾚｽﾛｰﾙｷｬpｶﾗｰ', 
					  col4: '17391104-043', col5: 'カートン', col6: '200', 
					  col7: '100', col8: txt, 
					  col9: txt, col10: ''});
	}*/
			

	// オプションの設定
	gridOptions2 = {
		enableColResize:true,
		enableSorting: true,
		enableFilter: true,
		headerHeight: 30,
		floatingFiltersHeight:30,
		columnDefs: columnDefs2,/*headedrデータ*/
		rowData: rowData2,/*行データ*/
		floatingFilter: true,/*2列目のフィルターのon/off*/
		suppressRowClickSelection:true,/*チェックボックス以外のクリックを認めるか？*/
		rowSelection :'multiple',/*複数選択できるように*/
		localeText: getLocaleText(),/*データ０の時のメッセージ*/
		rowHeight : 40,
		domLayout: 'autoHeight'
	};

	//   対象となる要素の指定
	var eGridDiv = document.querySelector('#myGrid' + divid);

	//   グリッドの生成
	new agGrid.Grid(eGridDiv, gridOptions2);
	gridOptions2.onRowSelected = function(param){
		$(".otherwindow .otherwindow-contents__location--btncenter").each(function(){
			var index = $(".otherwindow .otherwindow-contents__location--btncenter").index(this);
			if(index == 1){
				$(this).find("a").removeClass("disabled");
			}
		});
	}

	headerCheckboxChange();
	checkboxChange();


}
function setGrid5(){
	
	
	rowData2 = [];

	for(var i = 0; i < 1;i++){
		rowData2.push({col0: i,col1: '0000111', col2: '荷主A', col3: '商品1',col4:'1234567890', col5: '2019-04-20', col6: '10', col7: '1', col8: '2', col9: '12', col10: '1', col11: '3', col12: '2', col13: '4', col14: '5', col15: '6', col16: '7'});
	}	
	gridOptions2.api.updateRowData({add: rowData2});
	headerCheckboxChange();
	checkboxChange();

	$("#otherwindow-disabled2").removeClass("otherwindow-dropshadow");
	$("#otherwindow-disabled2").removeClass("otherwindow-dropshadow");
	
	$("#otherwindow-disabled1").addClass("otherwindow-disabled");
	$("#otherwindow-disabled2").addClass("otherwindow-disabled");
	$("#otherwindow-disabled3").removeClass("otherwindow-disabled");
	
	$(".otherwindow input").each(function(){
		var index = $(".otherwindow input").index(this);
		if(index == 0 || index == 1){
			$(this).addClass("disabled");
			$(this).prop("disabled",true);
		}
	});

	$(".otherwindow .btn_primary,.otherwindow .btn_secondary").each(function(){
		var index = $(".otherwindow .btn_primary,.otherwindow .btn_secondary").index(this);
		if(index >= 1 && index <= 4){
			$(this).addClass("disabled");
		}else if(index == 5){
			$(this).removeClass("disabled");
		}
	});
	
	
}


//テーブル作成用ファンクション-入荷実績送信用（モーダル）
var columnDefs3;
var gridOption3;
var rowData3;
function setGridData3(divid,data){

	// ヘッダー

	//headerName=列の題名,field=列のID,width=幅、cellClas=各セルに適用するクラス
	//headerCheckboxSelection=ヘッダーにチェックボックスを付けるか？
	//checkboxSelection=各行にチェックボックスを表示するか？
	//pinned=列固定,suppressMenu=ヘッダー一列目に検索フィールドを出すか？
	//hide=表示、非表示、sortable=ソートができるかできないか
	//filter=ヘッダー2列目の検索フィールドを何にするか？
	
	/*ヘッダーデータをタイプで入れ替える*/
	columnDefs3 = [
		{headerName: "", field: "col0", width: 30,cellClass: ['tc','noCellFocus'],
		 checkboxSelection: true,suppressMenu: true,
		 lockPosition:true,pinned: 'left'
		},
		{headerName: "ロケーション名", field: "col1", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:150,cellClass: ['tl','cellclass']},
		{headerName: "ロケブロック名", field: "col2", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:150,cellClass: ['tl','cellclass']},
		{headerName: "在庫区分", field: "col3", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:80,cellClass: ['tl','cellclass']},
		{headerName: "温度帯", field: "col4", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:80,cellClass: ['tl','cellclass']},
		{headerName: "商品コード", field: "col5", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:150,cellClass: ['tl','cellclass']},
		{headerName: "商品名", field: "col6", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:150,cellClass: ['tl','cellclass']},
		{headerName: "ピースバーコード", field: "col7", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:150,cellClass: ['tl','cellclass']},
		{headerName: "ケースバーコード", field: "col8", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:150,cellClass: ['tl','cellclass']},
		{headerName: "ロット", field: "col9", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "使用期限", field: "col10", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
		{headerName: "入庫日", field: "col11", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
		{headerName: "入数", field: "col12", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:80,cellClass: ['tl','cellclass']},
		{headerName: "在庫数（ケース）", field: "col13", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:150,cellClass: ['tl','cellclass']},
		{headerName: "在庫数（バラ）", field: "col14", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:150,cellClass: ['tl','cellclass']},
		{headerName: "在庫数", field: "col15", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
		{headerName: "引当可能数（ケース）", field: "col16", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:150,cellClass: ['tl','cellclass']},
		{headerName: "引当可能数（バラ）", field: "col17", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:150,cellClass: ['tl','cellclass']},
		{headerName: "引当可能数", field: "col18", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:150,cellClass: ['tl','cellclass']},
		{headerName: "引当済数（ケース）", field: "col19", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:150,cellClass: ['tl','cellclass']},
		{headerName: "引当済数（バラ）", field: "col20", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:150,cellClass: ['tl','cellclass']},
		{headerName: "引当済数", field: "col21", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:150,cellClass: ['tl','cellclass']},
		{headerName: "荷主コード", field: "col22", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "荷主名", field: "col23", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
		{headerName: "ロケーション", field: "col24", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "引当不可フラグ", field: "col25", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:80,cellClass: ['tl','cellclass']},
		{headerName: "備考", field: "col26", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:150,cellClass: ['tl','cellclass']}
	];			
		

	// 行のテストデータ

	rowData3 = [];

	/*for(var i = 0; i < data;i++){
		rowData.push({col0: i,col1: '0000' + i, col2: 'コカ・コーラボトラーズジャパン株式会社'});
	}*/
			

	// オプションの設定
	gridOptions3 = {
		enableColResize:true,
		enableSorting: true,
		enableFilter: true,
		headerHeight: 30,
		floatingFiltersHeight:30,
		columnDefs: columnDefs3,/*headedrデータ*/
		rowData: rowData3,/*行データ*/
		floatingFilter: true,/*2列目のフィルターのon/off*/
		suppressRowClickSelection:true,/*チェックボックス以外のクリックを認めるか？*/
		localeText: getLocaleText(),/*データ０の時のメッセージ*/
		rowHeight : 40,
		domLayout: 'autoHeight',
		pagination:true,
		paginationPageSize:rowData3.length

	};

	//   対象となる要素の指定
	var eGridDiv = document.querySelector('#myGrid' + divid);

	//   グリッドの生成
	new agGrid.Grid(eGridDiv, gridOptions3);


	//セルクリック時に行を選択状態にする
	gridOptions3.onCellClicked = function(param){
		/*クリックされたセルの取得*/
		var cols = param.colDef.field;
		var index = param.rowIndex;
		//セルがチェックボックスセルだったら
		if(cols == "col0"){
			//状況により行のセレクトをon/offする
			var node = gridOptions3.api.getRowNode(index);
			if(node.isSelected()){
				node.setSelected(false);

			}else{
				node.setSelected(true);
			}
		}
	}
	gridOptions3.onRowSelected = function(param){
		$("#modal1 .modal-header a").removeClass("disabled");
	}
	//ag-gridの仕様電話再描画されるためソートするとチェックボックスがデフォルトデザインに戻ってしまうためそれを防ぐ
	gridOptions3.onSortChanged = function(param){
		checkboxChange_modal();
	}
	gridOptions3.onFilterChanged = function(param){
		//モーダル内の検索インプットへのアクティブクラス付与
		$(".ag-floating-filter-input").each(function(){
			if($(this).val().length == 0){
				$(this).removeClass("active");
			}else{
				$(this).addClass("active");
			}
		});
		checkboxChange_modal();
	}
	/*今回の場合はテーブル4*/
	var chkflg4 = false;
	//横スクロールでステータス領域が消えた時に左はじにカラー領域を表示
	$("#myGrid" + divid + " div.ag-body-viewport").scroll(function(e){
		checkboxChange_modal();
	});
	gridOptions3.onSelectionChanged= function(param){
		getAllCnt(rowData3,divid,gridOptions3);
		
	}
	checkboxChange_modal();
	
	
}
function setGrid6(){
	$("#modal1 p.nodata2").html("");
	
	
	rowData3 = [];

	for(var i = 0; i < 5;i++){
		rowData3.push({col0: i,col1: 'Bセルブロック', col2: 'バラ通常', col3: '良品',col4:'常温', col5: 'ITEM01', col6: 'ダミー商品-01', col7: '1703002-82', col8: 'corporate-01PCS', col9: '1234567890', col10: '2019-04-03', col11: '2019-04-03', col12: 'ｶｰﾄﾝ', col13: '3', col14: '10', col15: '2', col16: '3', col17: '4', col18: '6', col19: '6', col20: '2', col21: '2', col22: '0000111', col23: '荷主・SEAOSS(A)', col24: '50101', col25: '0', col26: 'TEST'});
	}	
	gridOptions3.api.updateRowData({add: rowData3});
	checkboxChange_modal();
	getAllCnt(rowData3,6,gridOptions3);	
	gridWidth(".modal-content__tablearea",columnDefs3,"#myGrid6" ,".modal-content__tablearea");
}
var columnDefs4;
var gridOption4;
var rowData4;
function setGridData4(divid,data){

	// ヘッダー

	//headerName=列の題名,field=列のID,width=幅、cellClas=各セルに適用するクラス
	//headerCheckboxSelection=ヘッダーにチェックボックスを付けるか？
	//checkboxSelection=各行にチェックボックスを表示するか？
	//pinned=列固定,suppressMenu=ヘッダー一列目に検索フィールドを出すか？
	//hide=表示、非表示、sortable=ソートができるかできないか
	//filter=ヘッダー2列目の検索フィールドを何にするか？
	
	/*ヘッダーデータをタイプで入れ替える*/
	columnDefs4 = [
		{headerName: "", field: "col0", width: 30,cellClass: ['tc','noCellFocus'],
		 checkboxSelection: true,suppressMenu: true,
		 lockPosition:true,pinned: 'left'
		},
		{headerName: "ロケーション名", field: "col1", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:150,cellClass: ['tl','cellclass']},
		{headerName: "ロケブロック名", field: "col2", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:150,cellClass: ['tl','cellclass']},
		{headerName: "在庫区分", field: "col3", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:80,cellClass: ['tl','cellclass']},
		{headerName: "温度帯", field: "col4", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:80,cellClass: ['tl','cellclass']},
		{headerName: "商品コード", field: "col5", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:150,cellClass: ['tl','cellclass']},
		{headerName: "商品名", field: "col6", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:150,cellClass: ['tl','cellclass']},
		{headerName: "ピースバーコード", field: "col7", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:150,cellClass: ['tl','cellclass']},
		{headerName: "ケースバーコード", field: "col8", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:150,cellClass: ['tl','cellclass']},
		{headerName: "ロット", field: "col9", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "使用期限", field: "col10", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
		{headerName: "入庫日", field: "col11", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
		{headerName: "入数", field: "col12", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:80,cellClass: ['tl','cellclass']},
		{headerName: "在庫数（ケース）", field: "col13", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:150,cellClass: ['tl','cellclass']},
		{headerName: "在庫数（バラ）", field: "col14", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:150,cellClass: ['tl','cellclass']},
		{headerName: "在庫数", field: "col15", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:150,cellClass: ['tl','cellclass']},
		{headerName: "引当可能数（ケース）", field: "col16", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:150,cellClass: ['tl','cellclass']},
		{headerName: "引当可能数（バラ）", field: "col17", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:150,cellClass: ['tl','cellclass']},
		{headerName: "引当可能数", field: "col18", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:150,cellClass: ['tl','cellclass']},
		{headerName: "引当済数（ケース）", field: "col19", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:150,cellClass: ['tl','cellclass']},
		{headerName: "引当済数（バラ）", field: "col20", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:150,cellClass: ['tl','cellclass']},
		{headerName: "引当済数", field: "col21", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:150,cellClass: ['tl','cellclass']},
		{headerName: "荷主コード", field: "col22", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "荷主名", field: "col23", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
		{headerName: "ロケーション", field: "col24", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "引当不可フラグ", field: "col25", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:80,cellClass: ['tl','cellclass']},
		{headerName: "備考", field: "col26", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:150,cellClass: ['tl','cellclass']}
	];			

	// 行のテストデータ

	rowData4 = [];

	/*for(var i = 0; i < data;i++){
		rowData.push({col0: i,col1: '0000' + i, col2: 'コカ・コーラボトラーズジャパン株式会社'});
	}*/
			

	// オプションの設定
	gridOptions4 = {
		enableColResize:true,
		enableSorting: true,
		enableFilter: true,
		headerHeight: 30,
		floatingFiltersHeight:30,
		columnDefs: columnDefs4,/*headedrデータ*/
		rowData: rowData4,/*行データ*/
		floatingFilter: true,/*2列目のフィルターのon/off*/
		suppressRowClickSelection:true,/*チェックボックス以外のクリックを認めるか？*/
		localeText: getLocaleText(),/*データ０の時のメッセージ*/
		rowHeight : 40,
		domLayout: 'autoHeight',
		pagination:true,
		paginationPageSize:rowData4.length

	};

	//   対象となる要素の指定
	var eGridDiv = document.querySelector('#myGrid' + divid);

	//   グリッドの生成
	new agGrid.Grid(eGridDiv, gridOptions4);


	//セルクリック時に行を選択状態にする
	gridOptions4.onCellClicked = function(param){
		/*クリックされたセルの取得*/
		var cols = param.colDef.field;
		var index = param.rowIndex;
		//セルがチェックボックスセルだったら
		if(cols == "col0"){
			//状況により行のセレクトをon/offする
			var node = gridOptions4.api.getRowNode(index);
			if(node.isSelected()){
				node.setSelected(false);

			}else{
				node.setSelected(true);
			}
		}
	}
	gridOptions4.onRowSelected = function(param){
		$("#modal2 .modal-header a").removeClass("disabled");
	}
	//ag-gridの仕様電話再描画されるためソートするとチェックボックスがデフォルトデザインに戻ってしまうためそれを防ぐ
	gridOptions4.onSortChanged = function(param){
		checkboxChange_modal();
	}
	gridOptions4.onFilterChanged = function(param){
		//モーダル内の検索インプットへのアクティブクラス付与
		$(".ag-floating-filter-input").each(function(){
			if($(this).val().length == 0){
				$(this).removeClass("active");
			}else{
				$(this).addClass("active");
			}
		});
		checkboxChange_modal();
	}
	/*今回の場合はテーブル4*/
	var chkflg4 = false;
	//横スクロールでステータス領域が消えた時に左はじにカラー領域を表示
	$("#myGrid" + divid + " div.ag-body-viewport").scroll(function(e){
		checkboxChange_modal();
	});
	gridOptions4.onSelectionChanged= function(param){
		getAllCnt(rowData4,divid,gridOptions4);
		
	}
	checkboxChange_modal();

}
//データ追加
function setGrid7(){
	$("#modal2 p.nodata2").html("");
	
	
	rowData4 = [];

	for(var i = 0; i < 5;i++){
		rowData4.push({col0: i,col1: 'Bセルブロック', col2: 'バラ通常', col3: '良品',col4:'常温', col5: 'ITEM01', col6: 'ダミー商品-01', col7: '1703002-82', col8: 'corporate-01PCS', col9: '1234567890', col10: '2019-04-03', col11: '2019-04-03', col12: 'ｶｰﾄﾝ', col13: '3', col14: '10', col15: '2', col16: '3', col17: '4', col18: '6', col19: '6', col20: '2', col21: '2', col22: '0000111', col23: '荷主・SEAOSS(A)', col24: '50101', col25: '0', col26: 'TEST'});
	}	
	gridOptions4.api.updateRowData({add: rowData4});
	checkboxChange_modal();
	getAllCnt(rowData4,7,gridOptions4);
	gridWidth(".modal-content__tablearea",columnDefs4,"#myGrid7" ,".modal-content__tablearea");
}

//テーブル作成用ファンクション
function setGridData1(divid,data){
	if(divid == "1" && rowData1_1.length == 0){
		setGridData1_1(divid,data);
	}else if(divid == "2" && rowData1_2.length == 0){
		setGridData1_2(divid,data);
	}else if(divid == "3" && rowData1_3.length == 0){
		setGridData1_3(divid,data);
	}else if(divid == "4" && rowData1_4.length == 0){
		setGridData1_4(divid,data);
	}
	
}
//setGridData1系のヘッダーデータ構築
function getColmnDefs(divid){
	if(divid == "1" || divid =="2"){
		return columnDefs1 = [
			{headerName: "", field: "col0", width: 30,cellClass: ['tc','cl_w','noCellFocus'],
			 headerCheckboxSelection: true,checkboxSelection: true,suppressMenu: true,
			 lockPosition:true,pinned: 'left'
			},
			/*スクロール時にステータスフィールドが消えたら表示するフィールド*/
			{headerName: "", field: "col99",suppressMenu: true,width:8,
			 cellClass: ['tc','cellclass'],hide:true,pinned: 'left',
				cellStyle: function(params) {
					//ステータス色変化
					if (params.value=='0') {
						return {backgroundColor: 'rgba(235,153,175,0.2)',color: '#eb99af'};
					}else if (params.value=='1') {
						return {backgroundColor: 'rgba(227,230,87,0.2)',color: 'rgba(227,230,87,0.2)'};
					}else if (params.value=='2') {
						return {backgroundColor: 'rgba(201,202,202,0.2)',color: 'rgba(201,202,202,0.2)'};
					} else {
						return null;
					}
				}
			},
			{headerName: "移動指示番号", field: "col1", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "荷主コード", field: "col2", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "荷主名", field: "col3", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:130,cellClass: ['tl','cellclass']},
			{headerName: "移動ステータス", field: "col4", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:130,cellClass: ['tc','cellclass'],
				cellStyle: function(params) {
					//ステータス色変化
					if (params.value=='未移動') {
						return {backgroundColor: 'rgba(235,153,175,0.2)'};
					}else if (params.value=='移動元出庫開始' || params.value=='移動元出庫完了') {
						return {backgroundColor: 'rgba(227,230,87,0.2)'};
					}else if (params.value=='移動先入庫完了') {
						return {backgroundColor: 'rgba(201,202,202,0.2)'};
					} else {
						return null;
					}
				}
			},
			{headerName: "商品コード", field: "col5", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "商品名", field: "col6", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:140,cellClass: ['tc','cellclass']},
			{headerName: "ロット", field: "col7", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tc','cellclass']},
			{headerName: "使用期限", field: "col8", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "入数", field: "col9", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:80,cellClass: ['tr','cellclass']},
			{headerName: "移動提示数", field: "col10", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tr','cellclass']},
			{headerName: "元ロケ・ロケーションコード", field: "col11", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:200,cellClass: ['tl','cellclass']},
			{headerName: "元ロケ", field: "col12", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "元ロケ・在庫区分", field: "col13", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
			{headerName: "元ロケ・開始日時", field: "col14", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "元ロケ・終了日時", field: "col15", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "元ロケ・ユーザ名", field: "col16", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
			{headerName: "先ロケ・ロケーションコード", field: "col17", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:200,cellClass: ['tl','cellclass']},
			{headerName: "先ロケ", field: "col18", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "先ロケ・在庫区分", field: "col19", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
			{headerName: "先ロケ・開始日時", field: "col20", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "先ロケ・終了日時", field: "col21", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "先ロケ・ユーザ名", field: "col22", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
			{headerName: "更新日時", field: "col23", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "更新者名", field: "col24", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "登録日時", field: "col25", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "登録者名", field: "col26", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']}
		];
	}else{
		return columnDefs1 = [
			/*スクロール時にステータスフィールドが消えたら表示するフィールド*/
			{headerName: "", field: "col99",suppressMenu: true,width:8,
			 cellClass: ['tc','cellclass'],hide:true,pinned: 'left',
				cellStyle: function(params) {
					//ステータス色変化
					if (params.value=='0') {
						return {backgroundColor: 'rgba(235,153,175,0.2)',color: '#eb99af'};
					}else if (params.value=='1') {
						return {backgroundColor: 'rgba(227,230,87,0.2)',color: 'rgba(227,230,87,0.2)'};
					}else if (params.value=='2') {
						return {backgroundColor: 'rgba(201,202,202,0.2)',color: 'rgba(201,202,202,0.2)'};
					} else {
						return null;
					}
				}
			},
			{headerName: "移動指示番号", field: "col1", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "荷主コード", field: "col2", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "荷主名", field: "col3", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:130,cellClass: ['tl','cellclass']},
			{headerName: "移動ステータス", field: "col4", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:130,cellClass: ['tc','cellclass'],
				cellStyle: function(params) {
					//ステータス色変化
					if (params.value=='未移動') {
						return {backgroundColor: 'rgba(235,153,175,0.2)'};
					}else if (params.value=='移動元出庫開始' || params.value=='移動元出庫完了') {
						return {backgroundColor: 'rgba(227,230,87,0.2)'};
					}else if (params.value=='移動先入庫完了') {
						return {backgroundColor: 'rgba(201,202,202,0.2)'};
					} else {
						return null;
					}
				}
			},
			{headerName: "商品コード", field: "col5", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "商品名", field: "col6", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:140,cellClass: ['tc','cellclass']},
			{headerName: "ロット", field: "col7", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tc','cellclass']},
			{headerName: "使用期限", field: "col8", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "入数", field: "col9", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:80,cellClass: ['tr','cellclass']},
			{headerName: "移動提示数", field: "col10", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tr','cellclass']},
			{headerName: "元ロケ・ロケーションコード", field: "col11", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:200,cellClass: ['tl','cellclass']},
			{headerName: "元ロケ", field: "col12", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "元ロケ・在庫区分", field: "col13", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
			{headerName: "元ロケ・開始日時", field: "col14", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "元ロケ・終了日時", field: "col15", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "元ロケ・ユーザ名", field: "col16", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
			{headerName: "先ロケ・ロケーションコード", field: "col17", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:200,cellClass: ['tl','cellclass']},
			{headerName: "先ロケ", field: "col18", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "先ロケ・在庫区分", field: "col19", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "先ロケ・開始日時", field: "col20", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "先ロケ・終了日時", field: "col21", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
			{headerName: "先ロケ・ユーザ名", field: "col22", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
			{headerName: "更新日時", field: "col23", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "更新者名", field: "col24", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "登録日時", field: "col25", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "登録者名", field: "col26", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']}
		];	}	
}
//setGridData1系のデータ構築
function getRowData(divid,data){
	var rowData = [];
	if(divid == "1"){
		for(var i = 0; i < data;i++){
			rowData.push({col0: i,col99:0,col1: 'M00000002' + i, col2: '0000111', col3: 'ダミー荷主名1', col4: '未移動', col5: 'ITEM00-01', col6: 'テスト商品-01', col7: '1234567890-01', col8: '2019-10-23', col9: '10', col10: '2', col11: '50101', col12: '5-01-01', col13: '良品', col14: '2019-04-01 19:21:56', col15: '2019-04-01 19:21:56', col16: '小林01', col17: '30101', col18: 'ロケS-013', col19: '良品', col20: '2019-04-01 19:21:56', col21: '2019-04-01 19:21:56', col22: '小林01', col23: '2019-04-01 19:21:56', col24: 'hama001', col25: '2019-04-01 19:21:56', col26: 'hama001'});

		}
	}else if(divid == "2"){
		for(var i = 0; i < data;i++){
			rowData.push({col0: i,col99:1,col1: 'M00000002' + i, col2: '0000111', col3: 'ダミー荷主名1', col4: '移動元出庫開始', col5: 'ITEM00-01', col6: 'テスト商品-01', col7: '1234567890-01', col8: '2019-10-23', col9: '10', col10: '2', col11: '50101', col12: '5-01-01', col13: '良品', col14: '2019-04-01 19:21:56', col15: '2019-04-01 19:21:56', col16: '小林01', col17: '30101', col18: 'ロケS-013', col19: '良品', col20: '2019-04-01 19:21:56', col21: '2019-04-01 19:21:56', col22: '小林01', col23: '2019-04-01 19:21:56', col24: 'hama001', col25: '2019-04-01 19:21:56', col26: 'hama001'});
			
			
			rowData.push({col0: i,col99:1,col1: 'M00000002' + i, col2: '0000111', col3: 'ダミー荷主名1', col4: '移動元出庫完了', col5: 'ITEM00-01', col6: 'テスト商品-01', col7: '1234567890-01', col8: '2019-10-23', col9: '10', col10: '2', col11: '50101', col12: '5-01-01', col13: '良品', col14: '2019-04-01 19:21:56', col15: '2019-04-01 19:21:56', col16: '小林01', col17: '30101', col18: 'ロケS-013', col19: '良品', col20: '2019-04-01 19:21:56', col21: '2019-04-01 19:21:56', col22: '小林01', col23: '2019-04-01 19:21:56', col24: 'hama001', col25: '2019-04-01 19:21:56', col26: 'hama001'});
			
		}
	}else if(divid == "3"){
		for(var i = 0; i < data;i++){
			rowData.push({col0: i,col99:2,col1: 'M00000002' + i, col2: '0000111', col3: 'ダミー荷主名1', col4: '移動先入庫完了', col5: 'ITEM00-01', col6: 'テスト商品-01', col7: '1234567890-01', col8: '2019-10-23', col9: '10', col10: '2', col11: '50101', col12: '5-01-01', col13: '良品', col14: '2019-04-01 19:21:56', col15: '2019-04-01 19:21:56', col16: '小林01', col17: '30101', col18: 'ロケS-013', col19: '良品', col20: '2019-04-01 19:21:56', col21: '2019-04-01 19:21:56', col22: '小林01', col23: '2019-04-01 19:21:56', col24: 'hama001', col25: '2019-04-01 19:21:56', col26: 'hama001'});
		}
	}else if(divid == "4"){
		for(var i = 0; i < data;i++){
			rowData.push({col0: i,col99:0,col1: 'M00000002' + i, col2: '0000111', col3: 'ダミー荷主名1', col4: '未移動', col5: 'ITEM00-01', col6: 'テスト商品-01', col7: '1234567890-01', col8: '2019-10-23', col9: '10', col10: '2', col11: '50101', col12: '5-01-01', col13: '良品', col14: '2019-04-01 19:21:56', col15: '2019-04-01 19:21:56', col16: '小林01', col17: '30101', col18: 'ロケS-013', col19: '良品', col20: '2019-04-01 19:21:56', col21: '2019-04-01 19:21:56', col22: '小林01', col23: '2019-04-01 19:21:56', col24: 'hama001', col25: '2019-04-01 19:21:56', col26: 'hama001'});

			rowData.push({col0: i,col99:1,col1: 'M00000002' + i, col2: '0000111', col3: 'ダミー荷主名1', col4: '移動元出庫開始', col5: 'ITEM00-01', col6: 'テスト商品-01', col7: '1234567890-01', col8: '2019-10-23', col9: '10', col10: '2', col11: '50101', col12: '5-01-01', col13: '良品', col14: '2019-04-01 19:21:56', col15: '2019-04-01 19:21:56', col16: '小林01', col17: '30101', col18: 'ロケS-013', col19: '良品', col20: '2019-04-01 19:21:56', col21: '2019-04-01 19:21:56', col22: '小林01', col23: '2019-04-01 19:21:56', col24: 'hama001', col25: '2019-04-01 19:21:56', col26: 'hama001'});
			
			
			rowData.push({col0: i,col99:1,col1: 'M00000002' + i, col2: '0000111', col3: 'ダミー荷主名1', col4: '移動元出庫完了', col5: 'ITEM00-01', col6: 'テスト商品-01', col7: '1234567890-01', col8: '2019-10-23', col9: '10', col10: '2', col11: '50101', col12: '5-01-01', col13: '良品', col14: '2019-04-01 19:21:56', col15: '2019-04-01 19:21:56', col16: '小林01', col17: '30101', col18: 'ロケS-013', col19: '良品', col20: '2019-04-01 19:21:56', col21: '2019-04-01 19:21:56', col22: '小林01', col23: '2019-04-01 19:21:56', col24: 'hama001', col25: '2019-04-01 19:21:56', col26: 'hama001'});

			rowData.push({col0: i,col99:2,col1: 'M00000002' + i, col2: '0000111', col3: 'ダミー荷主名1', col4: '移動先入庫完了', col5: 'ITEM00-01', col6: 'テスト商品-01', col7: '1234567890-01', col8: '2019-10-23', col9: '10', col10: '2', col11: '50101', col12: '5-01-01', col13: '良品', col14: '2019-04-01 19:21:56', col15: '2019-04-01 19:21:56', col16: '小林01', col17: '30101', col18: 'ロケS-013', col19: '良品', col20: '2019-04-01 19:21:56', col21: '2019-04-01 19:21:56', col22: '小林01', col23: '2019-04-01 19:21:56', col24: 'hama001', col25: '2019-04-01 19:21:56', col26: 'hama001'});
		}
	}
	return rowData;
}
//pagerの件数取得
function getAllCnt(rowData,divid,gridOptions){
	$("#allcount" + divid).html(rowData.length);
	if($("#selectcount" + divid)[0]){
		$("#selectcount" + divid).html(gridOptions.api.getSelectedRows().length);
	}
}
//移動指示キャンセル
function showCancelMessage(){
	
	var cnt = gridOptions1_1.api.getSelectedRows().length;
	if(cnt > 0){
		
		
		for(var i = 0;i < rowData1_1.length;i++){
			var index = i;
			//セルがチェックボックスセルだったら
			//状況により行のセレクトをon/offする
			var node = gridOptions1_1.api.getRowNode(index);
			if(node.isSelected()){
				node.setSelected(false);

			}
		}

		
		
		showMessageBox(cnt + "件の移動指示をキャンセルしました。");
	}
	
	
}
//移動指示作成
function setMessage(type){
	
	if(type == "1"){
		clearData();
		 $('.otherwindow-contentsScroll').animate({ scrollTop: 0 }, 500);
	}else{
		var cnt = gridOptions2.api.getSelectedRows().length;
		if(cnt > 0){


			for(var i = 0;i < rowData2.length;i++){
				var index = i;
				//セルがチェックボックスセルだったら
				//状況により行のセレクトをon/offする
				var node = gridOptions2.api.getRowNode(index);
				if(node.isSelected()){
					node.setSelected(false);

				}
			}

			var selectedData = gridOptions2.api.getSelectedRows();
			var res = gridOptions2.api.setRowData([]);


			showMessageBox(cnt + "件の移動指示を作成しました。");
		}
		//なぜかtimeoutをしかけないと最後のボタンまでdisabledされない。
		setTimeout(function(){
			$(".otherwindow .btn_primary,.otherwindow .btn_secondary").each(function(){
				var index = $(".otherwindow .btn_primary,.otherwindow .btn_secondary").index(this);

				if(index == 6){
					$(this).addClass("disabled");
				}
			});

		},100);

	}
	
}
//指示作業完了
function showMoveMessage(){
	var cnt = gridOptions1_2.api.getSelectedRows().length;
	if(cnt > 0){
		for(var i = 0;i < rowData1_2.length;i++){
			var index = i;
			//セルがチェックボックスセルだったら
			//状況により行のセレクトをon/offする
			var node = gridOptions1_2.api.getRowNode(index);
			if(node.isSelected()){
				node.setSelected(false);

			}
		}
		showMessageBox(cnt + "件の移動指示を完了しました。");
	}
}

//tab切替
function changeTab(tnum){
	var cnt = 1;
	$("section.tabs > ul.tabs-nav > li.tabs-nav__item").each(function() {
		$("#tab"+ cnt + "-item").addClass("dspnone");
		if($(this)[0].id == "t" + tnum){
			$("#t" + cnt).addClass("tabs-nav__item--active");
			$("#tab"+ cnt + "-item").removeClass("dspnone");
			
			dt = 30;
			setGridData1(tnum,dt);
			checkboxChange();

		}else{
			$("#t" + cnt).removeClass("tabs-nav__item--active");
		}
		cnt++;
	});

	
}
//modal内のspredに謎のスクロールが発生
//親要素を少し大きくするスクリプト
function modalSpredHeightControl(){
	if($("#myGrid6")[0]){
		$("#myGrid6 div.ag-body-viewport").css("height",$("#myGrid6 div.ag-body-container").height() + 5 + "px");
	}
}

function openModalWindow_wa5010(id){
	$("#modal" + id).removeClass("dspnone");
}

function closeModalWindow_wa5010(id){
	$("#modal" + id).addClass("dspnone");
}
//modalはラジオボタンにする
function checkboxChange_modal(){
	$("#modal1 .ag-row .ag-selection-checkbox img:nth-child(1)").attr("src","../../../../assets/images/radio_checked.png");
	$("#modal1 .ag-row .ag-selection-checkbox img:nth-child(2)").attr("src","../../../../assets/images/radio_body.png");
	$("#modal2 .ag-row .ag-selection-checkbox img:nth-child(1)").attr("src","../../../../assets/images/radio_checked.png");
	$("#modal2 .ag-row .ag-selection-checkbox img:nth-child(2)").attr("src","../../../../assets/images/radio_body.png");

}
//指示作成/引当の商品選択モーダルの決定の際と
//ロケ―ション選択決定の際の挙動
function select_items(id){
	
	if(id == "1"){
		$("#item1").html("荷主A");
		$("#item2").html("17303002-082");
		$("#item3").html("商品1");
		$("#item4").html("1234567890");
		$("#item5").html("2019-04-20");
		$("#item6").html("10");
		$("#item7").html("バラ（通常）");
		$("#item8").html("00-11-22-33-44—55-1");
		$("#item9").html("Bセルブロック");
		
		$(".otherwindow input").each(function(){
			var index = $(".otherwindow input").index(this);
			if(index == 0 || index == 1){
				$(this).removeClass("disabled");
				$(this).prop("disabled",false);
			}
		});
		
		$(".otherwindow .btn_primary").each(function(){
			var index = $(".otherwindow .btn_primary").index(this);
			if(index == 1){
				$(this).removeClass("disabled");
			}
		});
		
		$("#otherwindow-disabled1").removeClass("otherwindow-dropshadow");
		$("#otherwindow-disabled2").removeClass("otherwindow-disabled");
		$("#otherwindow-disabled2").addClass("otherwindow-dropshadow");
		
	}else{
		$("#item10").html("00-11-22-33-44-55-66-1");
		$("#item11").html("Bセルブロック");
		$("#item12").html("バラ（通常）");
		$("#item13").html("良品");
		
		$(".otherwindow .otherwindow-contents__location--btncenter").each(function(){
			var index = $(".otherwindow .otherwindow-contents__location--btncenter").index(this);
			if(index == 0){
				$(this).find("a").removeClass("disabled");
			}
		});
		
	}
	closeModalWindow();
}
//上からせりあがるウィンドウを閉じる
//グリッドに追加トランジションをかける
function closeOtherWindow_wa5010(){
	$("div.otherwindow").removeClass("openotherwindow");
	
	setTimeout(function(){
		$("div.otherwindow").addClass("dspnone");		
	}, 300);
	//登録、修正の場合は下記を実行、グリッドの指定行を青色にし2秒後にトランジションで消す
	$(".ag-pinned-left-cols-container .ag-row").each(function(){
		var index = $(".ag-pinned-left-cols-container .ag-row").index(this)
		if(index == 0 || index == 1){
			$(this).addClass("row-update");

			var obj = $(this);
			setTimeout(function(){
				obj.addClass("row-default");
				obj.removeClass("row-update");
			},2000);
		}
	});
	$(".ag-body-container .ag-row").each(function(){
		var index = $(".ag-body-container .ag-row").index(this)
		if(index == 0 || index == 1){
			$(this).addClass("row-update");
			var obj = $(this);
			setTimeout(function(){
				obj.addClass("row-default");
				obj.removeClass("row-update");
			},2000);
		}
	});
	
}

function clearData(){
		$("#item1").html("");
		$("#item2").html("");
		$("#item3").html("");
		$("#item4").html("");
		$("#item5").html("");
		$("#item6").html("");
		$("#item7").html("");
		$("#item8").html("");
		$("#item9").html("");
		$("#item10").html("");
		$("#item11").html("");
		$("#item12").html("");
		$("#item13").html("");
	
		$(".otherwindow input").each(function(){
			var index = $(".otherwindow input").index(this);
			if(index == 0 || index == 1){
				$(this).addClass("disabled");
				$(this).prop("disabled",true);
				$(this).val("");
			}
		});

		$(".otherwindow input").each(function(){
			var index = $(".otherwindow input").index(this);
			if(index == 0 || index == 1){
				$(this).addClass("disabled");
				$(this).prop("disabled",true);
				$(this).val("");
			}
		});
	
		//なぜかtimeoutをしかけないと最後のボタンまでdisabledされない。
		setTimeout(function(){
			$(".otherwindow .btn_primary,.otherwindow .btn_secondary").each(function(){
				var index = $(".otherwindow .btn_primary,.otherwindow .btn_secondary").index(this);

				if(index >= 2){
					$(this).addClass("disabled");
				}else{
					$(this).removeClass("disabled");
				}
			});

		},100);

		$("#otherwindow-disabled1").addClass("otherwindow-dropshadow");
		$("#otherwindow-disabled2").removeClass("otherwindow-dropshadow");
		$("#otherwindow-disabled1").removeClass("otherwindow-disabled");
		$("#otherwindow-disabled2").addClass("otherwindow-disabled");
		$("#otherwindow-disabled3").addClass("otherwindow-disabled");

	
}
//上にせりあがるwindowのdisabled解除
function openOtherWindow_wa5010(){
	
	$("#otherwindow-disabled1").removeClass("otherwindow-disabled");
	$("#otherwindow-disabled1").addClass("otherwindow-dropshadow");
	openOtherWindow();
	
	gridWidth(".otherwindow",columnDefs2,"#myGrid5" ,".otherwindow-contents__location--grid");

}