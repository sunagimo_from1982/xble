$(function(){

	//各tab内のモーダル生成
	setGridData1("1",30);
	//補充候補リスト
	//setGridData2("5",30);
	
});


var columnDefs1_1;
var gridOptions1_1;
var rowData1_1 = [];
function setGridData1_1(divid,data){

	// ヘッダー

	//headerName=列の題名,field=列のID,width=幅、cellClas=各セルに適用するクラス
	//headerCheckboxSelection=ヘッダーにチェックボックスを付けるか？
	//checkboxSelection=各行にチェックボックスを表示するか？
	//pinned=列固定,suppressMenu=ヘッダー一列目に検索フィールドを出すか？
	//hide=表示、非表示、sortable=ソートができるかできないか
	//filter=ヘッダー2列目の検索フィールドを何にするか？
	
	/*ヘッダーデータをタイプで入れ替える*/
	
	columnDefs1_1 = getColmnDefs(divid);


	// 行のテストデータ

	rowData1_1 = getRowData(divid,data);


			
	// オプションの設定
	gridOptions1_1 = {
		enableColResize:true,
		enableSorting: true,
		enableFilter: true,
		headerHeight: 30,
		floatingFiltersHeight:30,
		columnDefs: columnDefs1_1,/*headedrデータ*/
		rowData: rowData1_1,/*行データ*/
		floatingFilter: true,/*2列目のフィルターのon/off*/
		suppressRowClickSelection:true,/*チェックボックス以外のクリックを認めるか？*/
		rowSelection :'multiple',/*複数選択できるように*/
		localeText: getLocaleText(),/*データ０の時のメッセージ*/
		rowHeight : 40,
		pagination:true,
		paginationPageSize:rowData1_1.length
	};
	


	//   対象となる要素の指定
	var eGridDiv = document.querySelector('#myGrid' + divid);

	//   グリッドの生成
	new agGrid.Grid(eGridDiv, gridOptions1_1);

	setFunctions(gridOptions1_1,divid,rowData1_1);
	
	/*今回の場合はテーブル4*/
	var chkflg4 = false;
	//横スクロールでステータス領域が消えた時に左はじにカラー領域を表示
	$("#myGrid" + divid + " div.ag-body-viewport").scroll(function(e){
		//今回のステータス位置が185付近なので今回はそこに設定
		if($("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() > 235 && !chkflg4 ){
			chkflg4 = true;

			showcol(true);
		}else if($("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() <= 235 && chkflg4){
			chkflg4 = false;
			showcol(false);

		}
		checkboxChange();
	});
	//列の表示非表示切り替え
	function showcol(show) {
		gridOptions1_1.columnApi.setColumnVisible('col99', show);
	}
	
	gridOptions1_1.onRowSelected = function(param){
		$(".tableitem__btnarea--rightarea span a").removeClass("disabled");
	}
	

	getAllCnt(rowData1_1,divid,gridOptions1_1);
	
	
	headerCheckboxChange();
	checkboxChange();
	spredScrollArrowChange(divid);
}

var columnDefs1_2;
var gridOptions1_2;
var rowData1_2 = [];
function setGridData1_2(divid,data){

	// ヘッダー

	//headerName=列の題名,field=列のID,width=幅、cellClas=各セルに適用するクラス
	//headerCheckboxSelection=ヘッダーにチェックボックスを付けるか？
	//checkboxSelection=各行にチェックボックスを表示するか？
	//pinned=列固定,suppressMenu=ヘッダー一列目に検索フィールドを出すか？
	//hide=表示、非表示、sortable=ソートができるかできないか
	//filter=ヘッダー2列目の検索フィールドを何にするか？
	
	/*ヘッダーデータをタイプで入れ替える*/
	
	columnDefs1_2 = getColmnDefs(divid);


	// 行のテストデータ

	rowData1_2 = getRowData(divid,data);


			
	// オプションの設定
	gridOptions1_2 = {
		enableColResize:true,
		enableSorting: true,
		enableFilter: true,
		headerHeight: 30,
		floatingFiltersHeight:30,
		columnDefs: columnDefs1_2,/*headedrデータ*/
		rowData: rowData1_2,/*行データ*/
		floatingFilter: true,/*2列目のフィルターのon/off*/
		suppressRowClickSelection:true,/*チェックボックス以外のクリックを認めるか？*/
		rowSelection :'multiple',/*複数選択できるように*/
		localeText: getLocaleText(),/*データ０の時のメッセージ*/
		rowHeight : 40,
		pagination:true,
		paginationPageSize:rowData1_2.length
	};
	


	//   対象となる要素の指定
	var eGridDiv = document.querySelector('#myGrid' + divid);

	//   グリッドの生成
	new agGrid.Grid(eGridDiv, gridOptions1_2);

	setFunctions(gridOptions1_2,divid,rowData1_2);
	
	/*今回の場合はテーブル4*/
	var chkflg4 = false;
	//横スクロールでステータス領域が消えた時に左はじにカラー領域を表示
	$("#myGrid" + divid + " div.ag-body-viewport").scroll(function(e){
		//今回のステータス位置が185付近なので今回はそこに設定
		if($("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() > 235 && !chkflg4 ){
			chkflg4 = true;

			showcol(true);
		}else if($("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() <= 235 && chkflg4){
			chkflg4 = false;
			showcol(false);

		}
		checkboxChange();
	});
	//列の表示非表示切り替え
	function showcol(show) {
		gridOptions1_2.columnApi.setColumnVisible('col99', show);
	}
	
	gridOptions1_2.onRowSelected = function(param){
		$(".tableitem__btnarea--rightarea span a").removeClass("disabled");
	}
	

	getAllCnt(rowData1_2,divid,gridOptions1_2);
	
	
	headerCheckboxChange();
	checkboxChange();
	spredScrollArrowChange(divid);
}


var columnDefs1_3;
var gridOptions1_3;
var rowData1_3 = [];
function setGridData1_3(divid,data){

	// ヘッダー

	//headerName=列の題名,field=列のID,width=幅、cellClas=各セルに適用するクラス
	//headerCheckboxSelection=ヘッダーにチェックボックスを付けるか？
	//checkboxSelection=各行にチェックボックスを表示するか？
	//pinned=列固定,suppressMenu=ヘッダー一列目に検索フィールドを出すか？
	//hide=表示、非表示、sortable=ソートができるかできないか
	//filter=ヘッダー2列目の検索フィールドを何にするか？
	
	/*ヘッダーデータをタイプで入れ替える*/
	
	columnDefs1_3 = getColmnDefs(divid);


	// 行のテストデータ

	rowData1_3 = getRowData(divid,data);


			
	// オプションの設定
	gridOptions1_3 = {
		enableColResize:true,
		enableSorting: true,
		enableFilter: true,
		headerHeight: 30,
		floatingFiltersHeight:30,
		columnDefs: columnDefs1_3,/*headedrデータ*/
		rowData: rowData1_3,/*行データ*/
		floatingFilter: true,/*2列目のフィルターのon/off*/
		suppressRowClickSelection:true,/*チェックボックス以外のクリックを認めるか？*/
		rowSelection :'multiple',/*複数選択できるように*/
		localeText: getLocaleText(),/*データ０の時のメッセージ*/
		rowHeight : 40,
		pagination:true,
		paginationPageSize:20
	};
	


	//   対象となる要素の指定
	var eGridDiv = document.querySelector('#myGrid' + divid);

	//   グリッドの生成
	new agGrid.Grid(eGridDiv, gridOptions1_3);

	setFunctions(gridOptions1_3,divid,rowData1_3);
	
	/*今回の場合はテーブル4*/
	var chkflg4 = false;
	//横スクロールでステータス領域が消えた時に左はじにカラー領域を表示
	$("#myGrid" + divid + " div.ag-body-viewport").scroll(function(e){
		//今回のステータス位置が185付近なので今回はそこに設定
		if($("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() > 215 && !chkflg4 ){
			chkflg4 = true;

			showcol(true);
		}else if($("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() <= 215 && chkflg4){
			chkflg4 = false;
			showcol(false);

		}
		checkboxChange();
	});
	//列の表示非表示切り替え
	function showcol(show) {
		gridOptions1_3.columnApi.setColumnVisible('col99', show);
	}
	
	

	getAllCnt(rowData1_3,divid,gridOptions1_3);
	
	
	headerCheckboxChange();
	checkboxChange();
	spredScrollArrowChange(divid);
}


var columnDefs1_4;
var gridOptions1_4;
var rowData1_4 = [];
function setGridData1_4(divid,data){

	// ヘッダー

	//headerName=列の題名,field=列のID,width=幅、cellClas=各セルに適用するクラス
	//headerCheckboxSelection=ヘッダーにチェックボックスを付けるか？
	//checkboxSelection=各行にチェックボックスを表示するか？
	//pinned=列固定,suppressMenu=ヘッダー一列目に検索フィールドを出すか？
	//hide=表示、非表示、sortable=ソートができるかできないか
	//filter=ヘッダー2列目の検索フィールドを何にするか？
	
	/*ヘッダーデータをタイプで入れ替える*/
	
	columnDefs1_4 = getColmnDefs(divid);


	// 行のテストデータ

	rowData1_4 = getRowData(divid,data);


			
	// オプションの設定
	gridOptions1_4 = {
		enableColResize:true,
		enableSorting: true,
		enableFilter: true,
		headerHeight: 30,
		floatingFiltersHeight:30,
		columnDefs: columnDefs1_4,/*headedrデータ*/
		rowData: rowData1_4,/*行データ*/
		floatingFilter: true,/*2列目のフィルターのon/off*/
		suppressRowClickSelection:true,/*チェックボックス以外のクリックを認めるか？*/
		rowSelection :'multiple',/*複数選択できるように*/
		localeText: getLocaleText(),/*データ０の時のメッセージ*/
		rowHeight : 40,
		pagination:true,
		paginationPageSize:20
	};
	


	//   対象となる要素の指定
	var eGridDiv = document.querySelector('#myGrid' + divid);

	//   グリッドの生成
	new agGrid.Grid(eGridDiv, gridOptions1_4);

	setFunctions(gridOptions1_4,divid,rowData1_4);
	
	/*今回の場合はテーブル4*/
	var chkflg4 = false;
	//横スクロールでステータス領域が消えた時に左はじにカラー領域を表示
	$("#myGrid" + divid + " div.ag-body-viewport").scroll(function(e){
		//今回のステータス位置が185付近なので今回はそこに設定
		if($("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() > 215 && !chkflg4 ){
			chkflg4 = true;

			showcol(true);
		}else if($("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() <= 215 && chkflg4){
			chkflg4 = false;
			showcol(false);

		}
		checkboxChange();
	});
	//列の表示非表示切り替え
	function showcol(show) {
		gridOptions1_4.columnApi.setColumnVisible('col99', show);
	}
	
	

	getAllCnt(rowData1_4,divid,gridOptions1_4);
	
	
	headerCheckboxChange();
	checkboxChange();
	spredScrollArrowChange(divid);
}

function setFunctions(gridOptions,divid,rowData){
		//セルクリック時に行を選択状態にする
	gridOptions.onCellClicked = function(param){
		/*クリックされたセルの取得*/
		var cols = param.colDef.field;
		var index = param.node.childIndex;
		//セルがチェックボックスセルだったら
		if(cols == "col0"){
			//状況により行のセレクトをon/offする
			var node = gridOptions.api.getRowNode(index);
			if(node.isSelected()){
				node.setSelected(false);

			}else{
				node.setSelected(true);
			}
		}
	}
	//ag-gridの仕様電話再描画されるためソートするとチェックボックスがデフォルトデザインに戻ってしまうためそれを防ぐ
	gridOptions.onSortChanged = function(param){
		checkboxChange();
	}

	gridOptions.onFilterChanged = function(param){
		//モーダル内の検索インプットへのアクティブクラス付与
		$(".ag-floating-filter-input").each(function(){
			if($(this).val().length == 0){
				$(this).removeClass("active");
			}else{
				$(this).addClass("active");
			}
		});
		checkboxChange();
		
	}
	if(divid < 5){
		var sort = [
			{colId: 'col1', sort: 'asc'}
		];
		gridOptions.api.setSortModel(sort);
	}
	gridOptions.onSelectionChanged= function(param){
		getAllCnt(rowData,divid,gridOptions);
		
	}
	if(rowData.length > 0 && (divid == 3 || divid == 4)){
		$("#pagerItem" + divid + ".tableitem__tablearea__pagerItem .disabled").removeClass("disabled");
		//各矢印クリックでページ移動
		$("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i").click(function(){
			if($(this).prop("id") == "first" && !$(this).hasClass("disabled")){
				gridOptions.api.paginationGoToFirstPage();
			}else if($(this).prop("id") == "prev" && !$(this).hasClass("disabled")){
				gridOptions.api.paginationGoToPreviousPage();
			}else if($(this).prop("id") == "next" && !$(this).hasClass("disabled")){
				gridOptions.api.paginationGoToNextPage();
			}else if($(this).prop("id") == "last" && !$(this).hasClass("disabled")){
				gridOptions.api.paginationGoToLastPage()
			}
			setPageSep(gridOptions,divid);
		});
		setPageSep(gridOptions,divid);
	}
	var coldef = columnDefs1_1;
	if(divid == "2"){
		coldef = columnDefs1_2;
	}else if(divid == "3"){
		coldef = columnDefs1_3;
	}else if(divid == "4"){
		coldef = columnDefs1_4;
	}
	
	gridWidth(".tableitem",coldef,"#myGrid" + divid, "#table" + divid)
}

//pagerの表示件数を変更
function updatePager1(){
	var value = $("#select5").val();
    gridOptions1_3.api.paginationSetPageSize(Number(value));
	checkboxChange();
	setPageSep(gridOptions1_3,3);
}
function updatePager2(){
	var value = $("#select6").val();
    gridOptions1_4.api.paginationSetPageSize(Number(value));
	checkboxChange();
	setPageSep(gridOptions1_4,4);
}
//pagerの真ん中の構築 
function setPageSep(gridOptions,divid){

	var h = "<span class='tableitem__tablearea__pagerItem__nowpage--desc--big'>" + (gridOptions.api.paginationGetCurrentPage() + 1) + "</span> /" + gridOptions.api.paginationGetTotalPages() + " ";
	$("#pagerItem" + divid + " .tableitem__tablearea__pagerItem__nowpage--desc").html(h);
	//1ページ目では前に戻れないようにdisabled
	if(gridOptions.api.paginationGetCurrentPage() + 1 == 1){
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#first").addClass("disabled");
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#prev").addClass("disabled");
	}else{
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#first").removeClass("disabled");
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#prev").removeClass("disabled");
	}
	console.log(divid);
	//LASTページ目では次にいけないようにdisabled
	if(gridOptions.api.paginationGetCurrentPage() + 1 == gridOptions.api.paginationGetTotalPages()){
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#next").addClass("disabled");
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#last").addClass("disabled");
	}else{
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#next").removeClass("disabled");
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#last").removeClass("disabled");
	}
	
}
//スプレッドの矢印の構築・再構築
function spredScrollArrowChange(divid){
	//初期表示しているすべて（TAB4）のみロード時に横スクロールの場所設定を行う
	var parentArea = $("#myGrid" + divid + " div.ag-body-container").height();

	var tbarea = $("#myGrid" + divid + " div.ag-body-viewport").scrollTop() + $("#myGrid" + divid + " div.ag-body-viewport").height();
	if(parentArea - tbarea > 0){
		//スクロールバーがある場合の処理
		$("#table" + divid + " div.scrollArrow").css("right","16px");
	}else{
		//スクロールバーがない場合の処理
		$("#table" + divid + " div.scrollArrow").css("right","0px");

	}

	//初期表示しているすべて（TAB4）のみロード時に横スクロールの表示設定を行う
	var parentArea = $("#myGrid" + divid + " div.ag-body-container").width();

	var tbarea = $("#myGrid" + divid + " div.ag-body-viewport").scrollTop() + $("#myGrid" + divid + " div.ag-body-viewport").width();

	if(parentArea - tbarea > 0){
		//スクロールバーがある場合の処理
		$("#table" + divid + " div.scrollArrow").removeClass("dspnone");
		var height = $("#myGrid" + divid + "").innerHeight() - 121;
		var dspheight = $("#myGrid" + divid + " .ag-body-container").innerHeight() ;

		var gh = 0;
		if(height < dspheight){
			gh = height;
		}else{
			gh = dspheight;
		}

		$("#table" + divid + " div.scrollArrow").css("height",gh + "px");

	}else{
		//スクロールバーがない場合の処理
		$("#table" + divid + " div.scrollArrow").addClass("dspnone");

	}



	/*TAB4のテーブル内の→マウスオーバー挙動*/
	var tm1 = 0;
	var tm2 = 0;
	var scrollMax = 0;
	var dspflg1 = true;
	//スクロールイベント
	$("#table" + divid + " div.scrollArrow").hover(function(){
		tm2 = setTimeout(function(){
			tm1 = setInterval(function(){
				//スクロールがMAXになったら表示を消すためにここで値を取得
				scrollMax = $("#myGrid" + divid + " div.ag-body-container").width() - $("#myGrid" + divid + " div.ag-body-viewport").width();

				$("#myGrid" + divid + " div.ag-body-viewport").scrollLeft( $("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() + 2);

				if(scrollMax <= $("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() && dspflg1){
					$("#table" + divid + " div.scrollArrow").addClass("dspnone");
					dspflg1 = false;
					clearTimeout(tm2);
					clearInterval(tm1);
				}
			},0);
		},300);
	},function(){
		clearTimeout(tm2);
		clearInterval(tm1);
	});

	//scrollArrowが消えているときにscrollMAXと値が変わったら再表示
	$("#myGrid" + divid + " div.ag-body-viewport").scroll(function(){

		scrollMax = $("#myGrid" + divid + " div.ag-body-container").width() - $("#myGrid" + divid + " div.ag-body-viewport").width();

		if(scrollMax <= $("#myGrid" + divid + " div.ag-body-viewport").scrollLeft()){
			$("#table" + divid + " div.scrollArrow").addClass("dspnone");
			dspflg1 = false;
		}

		if(scrollMax > $("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() && !dspflg1){
			$("#table" + divid + " div.scrollArrow").removeClass("dspnone");
			dspflg1 = true;
		}
	});	
}
//テーブル作成用ファンクション-入荷実績登録用
var columnDefs2;
var gridOptions2;
var rowData2;
function setGridData2(divid,data){

	// ヘッダー

	//headerName=列の題名,field=列のID,width=幅、cellClas=各セルに適用するクラス
	//headerCheckboxSelection=ヘッダーにチェックボックスを付けるか？
	//checkboxSelection=各行にチェックボックスを表示するか？
	//pinned=列固定,suppressMenu=ヘッダー一列目に検索フィールドを出すか？
	//hide=表示、非表示、sortable=ソートができるかできないか
	//filter=ヘッダー2列目の検索フィールドを何にするか？
	
	/*ヘッダーデータをタイプで入れ替える*/
	columnDefs2 = [
		{headerName: "", field: "col0", width: 30,cellClass: ['tc','cl_w','noCellFocus'],
		 headerCheckboxSelection: true,checkboxSelection: true,suppressMenu: true,
		 lockPosition:true,pinned: 'left'
		},
		{headerName: "荷主コード", field: "col1",
		 suppressMenu: true,sortable:true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "荷主名", field: "col2",
		 suppressMenu: true,sortable:true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "元ロケーション", field: "col3",
		 suppressMenu: true,sortable:true,width:200,cellClass: ['tl','cellclass']},
		{headerName: "先ロケ―ション", field: "col4",
		 suppressMenu: true,sortable:true,width:200,cellClass: ['tl','cellclass']},
		{headerName: "商品コード", field: "col5",
		 suppressMenu: true,sortable:true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "ロット", field: "col6",
		 suppressMenu: true,sortable:true,width:80,cellClass: ['tr','cellclass']},
		{headerName: "単位", field: "col7",
		 suppressMenu: true,sortable:true,width:80,cellClass: ['tr','cellclass']},
		{headerName: "人数", field: "col8",
		 suppressMenu: true,sortable:true,width:80,cellClass: ['tr','cellclass']},
		{headerName: "数量", field: "col9",
		 suppressMenu: true,sortable:true,width:80,cellClass: ['tr','cellclass']},
		{headerName: "作成日時", field: "col10",
		 suppressMenu: true,sortable:true,width:160,cellClass: ['tl','cellclass']}
	];			
		

	// 行のテストデータ

	rowData2 = [];

	for(var i = 0; i < data;i++){
		rowData2.push({col0: i,col1: '0000111', col2: 'ダミー荷主名1', col3: 'Aバックヤード',col4:'Bセルブロック', col5: 'ITEM00-01', col6: '1234567890-01', col7: 'ｶｰﾄﾝ', col8: '10', col9: '30', col10: '2019-03-22 18:00:00'});
	}	
			

	// オプションの設定
	gridOptions2 = {
		enableColResize:true,
		enableSorting: true,
		enableFilter: true,
		headerHeight: 30,
		floatingFiltersHeight:30,
		columnDefs: columnDefs2,/*headedrデータ*/
		rowData: rowData2,/*行データ*/
		floatingFilter: true,/*2列目のフィルターのon/off*/
		suppressRowClickSelection:true,/*チェックボックス以外のクリックを認めるか？*/
		rowSelection :'multiple',/*複数選択できるように*/
		localeText: getLocaleText(),/*データ０の時のメッセージ*/
		rowHeight : 40,
		domLayout: 'autoHeight',
		pagination:true,
		paginationPageSize:rowData2.length
	};

	//   対象となる要素の指定
	var eGridDiv = document.querySelector('#myGrid' + divid);

	//   グリッドの生成
	new agGrid.Grid(eGridDiv, gridOptions2);
	gridOptions2.onRowSelected = function(param){
		$(".otherwindow .otherwindow-contents__location--btncenter").each(function(){
			var index = $(".otherwindow .otherwindow-contents__location--btncenter").index(this);
			if(index == 1){
				$(this).find("a").removeClass("disabled");
			}
		});
	}
	
	//横スクロールでステータス領域が消えた時に左はじにカラー領域を表示
	$("#myGrid" + divid + " div.ag-body-viewport").scroll(function(e){
		checkboxChange();
	});
	
	setFunctions(gridOptions2,divid,rowData2);
	headerCheckboxChange();
	checkboxChange();

	getAllCnt(rowData2,divid,gridOptions2);
	var ow = $(".otherwindow").innerWidth() - 40;

	//Gridの横幅が画面幅以下なら調整する
	var w = 0;
	for(var i = 0 ; i < columnDefs2.length ;i++){
		w += columnDefs2[i].width;
	}

	if(ow > w){
		$("#myGrid" + divid ).css("width",w + "px");
	}
}

//テーブル作成用ファンクション
function setGridData1(divid,data){
	if(divid == "1" && rowData1_1.length == 0){
		setGridData1_1(divid,data);
	}else if(divid == "2" && rowData1_2.length == 0){
		setGridData1_2(divid,data);
	}else if(divid == "3" && rowData1_3.length == 0){
		setGridData1_3(divid,data);
	}else if(divid == "4" && rowData1_4.length == 0){
		setGridData1_4(divid,data);
	}
	
}
//setGridData1系のヘッダーデータ構築
function getColmnDefs(divid){
	if(divid == "1" || divid =="2"){
		return columnDefs1 = [
			{headerName: "", field: "col0", width: 30,cellClass: ['tc','cl_w','noCellFocus'],
			 headerCheckboxSelection: true,checkboxSelection: true,suppressMenu: true,
			 lockPosition:true,pinned: 'left'
			},
			/*スクロール時にステータスフィールドが消えたら表示するフィールド*/
			{headerName: "", field: "col99",suppressMenu: true,width:8,
			 cellClass: ['tc','cellclass'],hide:true,pinned: 'left',
				cellStyle: function(params) {
					//ステータス色変化
					if (params.value=='0') {
						return {backgroundColor: 'rgba(235,153,175,0.2)',color: '#eb99af'};
					}else if (params.value=='1') {
						return {backgroundColor: 'rgba(227,230,87,0.2)',color: 'rgba(227,230,87,0.2)'};
					}else if (params.value=='2') {
						return {backgroundColor: 'rgba(201,202,202,0.2)',color: 'rgba(201,202,202,0.2)'};
					} else {
						return null;
					}
				}
			},
			{headerName: "補充タイプ", field: "col1", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:90,cellClass: ['tc','cellclass'],
				cellStyle: function(params) {
					//ステータス色変化
					if (params.value=='緊急') {
						return {color: 'rgba(217,59,59,1)'};
					}else {
						return null;
					}
				}
			},
			{headerName: "補充ステータス", field: "col2", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:130,cellClass: ['tc','cellclass'],
				cellStyle: function(params) {
					//ステータス色変化
					if (params.value=='未補充') {
						return {backgroundColor: 'rgba(235,153,175,0.2)'};
					}else if (params.value=='補充元出庫完了') {
						return {backgroundColor: 'rgba(227,230,87,0.2)'};
					}else if (params.value=='補充先入庫完了') {
						return {backgroundColor: 'rgba(201,202,202,0.2)'};
					} else {
						return null;
					}
				}
			},
			{headerName: "荷主コード", field: "col3", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "荷主名", field: "col4", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:130,cellClass: ['tl','cellclass']},
			{headerName: "補充番号", field: "col5", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:130,cellClass: ['tl','cellclass']},
			{headerName: "商品コード", field: "col6", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "商品名", field: "col7", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
			{headerName: "ロット", field: "col8", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "単位名", field: "col9", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "入数", field: "col10", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:80,cellClass: ['tr','cellclass']},
			{headerName: "数量", field: "col11", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tr','cellclass']},
			{headerName: "元ロケ・ロケーション名", field: "col12", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:200,cellClass: ['tl','cellclass']},
			{headerName: "先ロケ・ロケーション名", field: "col13", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:200,cellClass: ['tl','cellclass']},
			{headerName: "登録日時", field: "col14", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:170,cellClass: ['tl','cellclass']}
		];
	}else{
		return columnDefs1 = [
			/*スクロール時にステータスフィールドが消えたら表示するフィールド*/
			{headerName: "", field: "col99",suppressMenu: true,width:8,
			 cellClass: ['tc','cellclass'],hide:true,pinned: 'left',
				cellStyle: function(params) {
					//ステータス色変化
					if (params.value=='0') {
						return {backgroundColor: 'rgba(235,153,175,0.2)',color: '#eb99af'};
					}else if (params.value=='1') {
						return {backgroundColor: 'rgba(227,230,87,0.2)',color: 'rgba(227,230,87,0.2)'};
					}else if (params.value=='2') {
						return {backgroundColor: 'rgba(201,202,202,0.2)',color: 'rgba(201,202,202,0.2)'};
					} else {
						return null;
					}
				}
			},
			{headerName: "補充タイプ", field: "col1", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:90,cellClass: ['tc','cellclass'],
				cellStyle: function(params) {
					//ステータス色変化
					if (params.value=='緊急') {
						return {color: 'rgba(217,59,59,1)'};
					}else {
						return null;
					}
				}
			},
			{headerName: "補充ステータス", field: "col2", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:130,cellClass: ['tc','cellclass'],
				cellStyle: function(params) {
					//ステータス色変化
					if (params.value=='未補充') {
						return {backgroundColor: 'rgba(235,153,175,0.2)'};
					}else if (params.value=='補充元出庫完了') {
						return {backgroundColor: 'rgba(227,230,87,0.2)'};
					}else if (params.value=='補充先入庫完了') {
						return {backgroundColor: 'rgba(201,202,202,0.2)'};
					} else {
						return null;
					}
				}
			},
			{headerName: "荷主コード", field: "col3", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "荷主名", field: "col4", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:130,cellClass: ['tl','cellclass']},
			{headerName: "補充番号", field: "col5", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:130,cellClass: ['tl','cellclass']},
			{headerName: "商品コード", field: "col6", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "商品名", field: "col7", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
			{headerName: "ロット", field: "col8", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "単位名", field: "col9", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "入数", field: "col10", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:80,cellClass: ['tr','cellclass']},
			{headerName: "数量", field: "col11", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tr','cellclass']},
			{headerName: "元ロケ・ロケーション名", field: "col12", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:200,cellClass: ['tl','cellclass']},
			{headerName: "先ロケ・ロケーション名", field: "col13", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:200,cellClass: ['tl','cellclass']},
			{headerName: "登録日時", field: "col14", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:170,cellClass: ['tl','cellclass']}
		];	}	
}
//setGridData1系のデータ構築
function getRowData(divid,data){
	var rowData = [];
	if(divid == "1"){
		for(var i = 0; i < data;i++){
			var textval = "通常";
			if(i < 7){
				textval = "緊急";
			}
			rowData.push({col0: i,col99:0,col1: textval, col2: '未補充',col3: '0000222',col4: '荷主・SEAOS (B)', col5: 'S000000003', col6: 'ITEM01', col7: '商品1', col8: 'S001', col9: 'ｶｰﾄﾝ', col10: '10', col11: '1', col12: 'PC001', col13: 'ロケK003', col14: '2019-04-01 20:15:25'});

		}
	}else if(divid == "2"){
		for(var i = 0; i < data;i++){
			var textval = "通常";
			if(i < 7){
				textval = "緊急";
			}
			rowData.push({col0: i,col99:1,col1: textval, col2: '補充元出庫完了',col3: '0000222',col4: '荷主・SEAOS (B)', col5: 'S000000003', col6: 'ITEM01', col7: '商品1', col8: 'S001', col9: 'ｶｰﾄﾝ', col10: '10', col11: '1', col12: 'PC001', col13: 'ロケK003', col14: '2019-04-01 20:15:25'});
			
		}
	}else if(divid == "3"){
		for(var i = 0; i < data;i++){
			var textval = "通常";
			if(i < 7){
				textval = "緊急";
			}
			rowData.push({col0: i,col99:2,col1: textval, col2: '補充先入庫完了',col3: '0000222',col4: '荷主・SEAOS (B)', col5: 'S000000003', col6: 'ITEM01', col7: '商品1', col8: 'S001', col9: 'ｶｰﾄﾝ', col10: '10', col11: '1', col12: 'PC001', col13: 'ロケK003', col14: '2019-04-01 20:15:25'});
		}
	}else if(divid == "4"){
		for(var i = 0; i < data;i++){
			var textval = "通常";
			if(i < 2){
				textval = "緊急";
			}
			rowData.push({col0: i,col99:0,col1: textval, col2: '未補充',col3: '0000222',col4: '荷主・SEAOS (B)', col5: 'S000000003', col6: 'ITEM01', col7: '商品1', col8: 'S001', col9: 'ｶｰﾄﾝ', col10: '10', col11: '1', col12: 'PC001', col13: 'ロケK003', col14: '2019-04-01 20:15:25'});

			rowData.push({col0: i,col99:1,col1: textval, col2: '補充元出庫完了',col3: '0000222',col4: '荷主・SEAOS (B)', col5: 'S000000003', col6: 'ITEM01', col7: '商品1', col8: 'S001', col9: 'ｶｰﾄﾝ', col10: '10', col11: '1', col12: 'PC001', col13: 'ロケK003', col14: '2019-04-01 20:15:25'});

			
			
			rowData.push({col0: i,col99:2,col1: textval, col2: '補充先入庫完了',col3: '0000222',col4: '荷主・SEAOS (B)', col5: 'S000000003', col6: 'ITEM01', col7: '商品1', col8: 'S001', col9: 'ｶｰﾄﾝ', col10: '10', col11: '1', col12: 'PC001', col13: 'ロケK003', col14: '2019-04-01 20:15:25'});


		}
	}
	return rowData;
}
//pagerの件数取得
function getAllCnt(rowData,divid,gridOptions){
	$("#allcount" + divid).html(rowData.length);
	if($("#selectcount" + divid)[0]){
		$("#selectcount" + divid).html(gridOptions.api.getSelectedRows().length);
	}
}
//補充作業完了
function showAddMessage(){
	var cnt = gridOptions1_2.api.getSelectedRows().length;
	if(cnt > 0){
		for(var i = 0;i < rowData1_2.length;i++){
			var index = i;
			//セルがチェックボックスセルだったら
			//状況により行のセレクトをon/offする
			var node = gridOptions1_2.api.getRowNode(index);
			if(node.isSelected()){
				node.setSelected(false);

			}
		}
		showMessageBox(cnt + "件の補充指示を完了しました。");
	}
}

//tab切替
function changeTab(tnum){
	var cnt = 1;
	$("section.tabs > ul.tabs-nav > li.tabs-nav__item").each(function() {
		$("#tab"+ cnt + "-item").addClass("dspnone");
		if($(this)[0].id == "t" + tnum){
			$("#t" + cnt).addClass("tabs-nav__item--active");
			$("#tab"+ cnt + "-item").removeClass("dspnone");
			
			dt = 30;
			setGridData1(tnum,dt);
			checkboxChange();

		}else{
			$("#t" + cnt).removeClass("tabs-nav__item--active");
		}
		cnt++;
	});

	
}

//上からせりあがるウィンドウを閉じる
//グリッドに追加トランジションをかける
function closeOtherWindow_wa5010(){
	if(gridOptions2.api.getSelectedRows().length > 0){
		$("div.otherwindow").removeClass("openotherwindow");

		setTimeout(function(){
			$("div.otherwindow").addClass("dspnone");		
		}, 300);
		//登録、修正の場合は下記を実行、グリッドの指定行を青色にし2秒後にトランジションで消す
		$(".ag-pinned-left-cols-container .ag-row").each(function(){
			var index = $(".ag-pinned-left-cols-container .ag-row").index(this)
			if(index == 7 || index == 8){
				$(this).addClass("row-update");

				var obj = $(this);
				setTimeout(function(){
					obj.addClass("row-default");
					obj.removeClass("row-update");
				},2000);
			}
		});
		$(".ag-body-container .ag-row").each(function(){
			var index = $(".ag-body-container .ag-row").index(this)
			if(index == 7 || index == 8){
				$(this).addClass("row-update");
				var obj = $(this);
				setTimeout(function(){
					obj.addClass("row-default");
					obj.removeClass("row-update");
				},2000);
			}
		});
	}
}

function closeModalWindow_wa5020(){
	closeModalWindow();
	$(".loading_overlay").removeClass("dspnone");
	setGridData2("5",5);
	setTimeout(function(){
		$(".loading_overlay").addClass("dspnone");
		openOtherWindow();
		gridWidth(".otherwindow",columnDefs2,"#myGrid2", ".otherwindow-tablearea__container")

	},3000);
}