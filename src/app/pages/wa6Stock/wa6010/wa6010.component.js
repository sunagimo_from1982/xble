

$(function(){

	setGridData1("myGrid1",0);
	
	var sh = $(".search__searcharea").height();
	$(".search__searcharea--title").click(function(){
		if($(".search__searcharea__items").hasClass("dspnone")){
			$(".tableitem__tablearea").css("height","calc(100% - 295px)");
			$(".search__searcharea__items").removeClass("dspnone");
			$(this).removeClass("closed");
		}else{
			$(".search__searcharea__items").addClass("dspnone");
			$(this).addClass("closed");
			$(".tableitem__tablearea").css("height","calc(100% - 140px)");
		}
		spredScrollArrowChange();
	});
	
	$(".arrowdiv").click(function(e){
		//サイドメニューオープン時にスプレッドの高さを調節
		setTimeout(function(){
			/*サイドメニューオープン時*/
			if($(".arrowdiv").hasClass("openarrow")){
				if(sh != $(".search__searcharea").height()){
					if(!$(".search__searcharea__items").hasClass("dspnone")){
						$(".tableitem__tablearea").css("height","calc(100% - 365px)");
					}			
				}			
			/*サイドメニュークローズ時*/
			}else{
				if(!$(".search__searcharea__items").hasClass("dspnone")){
					$(".tableitem__tablearea").css("height","calc(100% - 295px)");
				}else{
					$(".tableitem__tablearea").css("height","calc(100% - 140px)");
				}
			}
			spredScrollArrowChange();
		},200);
		
	});	

	//上にせりあがってくるメニューのinputをdisabledにする（在庫登録）
	$("#otherwindow_regist .disabled").each(function(){
		$(this).prop("disabled",true);
	});
	//form要素に変更がったら要素をactiveにしルールに従ってdisabledから解放
	$("#otherwindow_regist input,#otherwindow_regist select,#otherwindow_regist textarea").change(function(){

		if($(this).prop("type") == "text" || $(this).prop("type") == "date" || $(this)[0].tagName.toLowerCase() == "textarea"){
			$(this).addClass("active");
		}
		var next = $("#otherwindow_regist input,#otherwindow_regist select,#otherwindow_regist textarea").index(this) + 1;
		if($("#otherwindow_regist input,#otherwindow_regist select,#otherwindow_regist textarea")[next]){
			var cnt = 0;
			
			var hissuCnt = 0;
			$("#otherwindow_regist input,#otherwindow_regist select,#otherwindow_regist textarea").each(function(){
				if(next == 1 && cnt == next){
				   $(this).prop("disabled",false);
				   $(this).removeClass("disabled");
					
				}else{
					if(next == 2 && (cnt != 2 && cnt != 10)){
					   $(this).prop("disabled",false);
					   $(this).removeClass("disabled");

					   if($(this).prop("type") == "checkbox"){
						   $(this).next(".cb1span").removeClass("disabled");
					   }
					}
				}
				if(cnt == 0 || cnt == 2 || cnt == 3 || cnt == 4 || cnt == 6 || cnt == 7){
					if($(this).val().length > 0){

						hissuCnt++;
					}
				}
				cnt++;
			});/**/
			if($("#select3").val() == "1" && hissuCnt == 5){
				$("#regist1").removeClass("disabled");
			}else if($("#select3").val() == "0" && hissuCnt == 6){
				$("#regist1").removeClass("disabled");
			}
		}
		
		//$(this).prop("disabled","false");
		//$(this).removeClass("disabled");
	});
	//form要素に変更がったら要素をactiveにしルールに従ってdisabledから解放
	$("#otherwindow_update input,#otherwindow_update select,#otherwindow_update textarea").change(function(){
		$("#regist2").removeClass("disabled");

	});
	
	//上にせりあがってくるメニューのinputをdisabledにする（在庫修正）
	$("#otherwindow_update .disabled").each(function(){
		$(this).prop("disabled",true);
	});

	//autoloader
	//autooaderに表示させるアイテム
	var loaderItem = ["00130000001 スウェットワーク","00130000002 スウェットワーク","00130000003 スウェットワーク","00130000004 スウェットワーク","00130000005 スウェットワーク","00130000006 スウェットワーク","00130000007 スウェットワーク","00130000008 スウェットワーク","00130000009 スウェットワーク","00130000010 スウェットワーク","00130000011 スウェットワーク","00130000312 スウェットワーク",];
		
	
	//フォーカスで選択領域を展開
	//フォーカスアウト、アイテムクリックで領域を閉じる
	$(".autoload").focusin(function(){
		var h ="";
		//すでに入力値があるか判定
		//あったらその分を引いたデータを表示
		if($(this).val().length == 0){
			for(var i = 0; i < loaderItem.length;i++){
				h += "<li class='autoloader-list'>" + loaderItem[i] + "</li>";
			}
		}else{
			for(var i = 0; i < loaderItem.length;i++){
				if(loaderItem[i].indexOf($(this).val()) != -1){
					h += "<li class='autoloader-list'>" + loaderItem[i] + "</li>";
				}
			}

		}
		
		$(this).next(".autoloader").html(h);
		
		$(this).next(".autoloader").removeClass("dspnone");
		setLoaderFunction();
		

	}).focusout(function(){
		var obj = $(this);
		setTimeout(function(){
			obj.next(".autoloader").addClass("dspnone");
		},200);
	});
	
	//入力があったら非対象のデータを間引く
	$(".autoload").keyup(function(){
		var h ="";
		for(var i = 0; i < loaderItem.length;i++){
			if(loaderItem[i].indexOf($(this).val()) != -1){
				h += "<li class='autoloader-list'>" + loaderItem[i] + "</li>";
			}
		}
		$(this).next(".autoloader").html(h);
		setLoaderFunction();
	});
});

function setLoaderFunction(){
	$(".autoloader .autoloader-list").click(function(){
		var sp = $(this).html().split(" ");
		$(this).parent().prev("input").val(sp[0]);
		$(this).parent().addClass("dspnone");
		$(this).parent().parent().next().find("input").val(sp[1]);
		
		$(this).parent().prev("input").change();
	});

}
//テーブル作成用ファンクション
var columnDefs1;
var gridOptions1;
function setGridData1(divid,data){

	// ヘッダー

	//headerName=列の題名,field=列のID,width=幅、cellClas=各セルに適用するクラス
	//headerCheckboxSelection=ヘッダーにチェックボックスを付けるか？
	//checkboxSelection=各行にチェックボックスを表示するか？
	//pinned=列固定,suppressMenu=ヘッダー一列目に検索フィールドを出すか？
	//hide=表示、非表示、sortable=ソートができるかできないか
	//filter=ヘッダー2列目の検索フィールドを何にするか？
	
	/*ヘッダーデータをタイプで入れ替える*/
	columnDefs1 = [
		{headerName: "", field: "col0", width: 30,cellClass: ['tc','cl_w','noCellFocus'],
		 headerCheckboxSelection: true,checkboxSelection: true,suppressMenu: true,
		 lockPosition:true,pinned: 'left',suppressMovable:true
		},
		{headerName: "", field: "col1",pinned: 'left',lockPosition:true,
		 suppressMenu: true,sortable:true,width:95,cellClass: ['tc','cellclass'],suppressMovable:true,
		 cellRenderer: (params) => {
				const element = document.createElement('div');
				element.innerHTML = "<a class='btn_secondary' href='javascript:openOtherWindow_wa6010(\"otherwindow_regist\");'>在庫登録</a>";

				return element;
			}
		},
		{headerName: "ロケーション名", field: "col2", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:120,cellClass: ['tl','cellclass'],pinned: 'left'},
		{headerName: "ロケーション", field: "col3", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:120,cellClass: ['tc','cellclass'],pinned: 'left'},
		/*スクロール時にステータスフィールドが消えたら表示するフィールド*/
		{headerName: "商品コード", field: "col99", filter:'text',unSortIcon: true,lockPosition:true,
		 suppressMenu: true,width:300,cellClass: ['tl','cellclass'],hide:true,pinned: 'left',
		 suppressMovable:true,
		 cellRenderer: (params) => {
				const element = document.createElement('div');
				element.innerHTML = "<div class='itemcode'><p>" + params.value + "</p><a class='btn_secondary' href='javascript:openOtherWindow_wa6010(\"otherwindow_update\");'>在庫修正</a></div>";

				return element;
			}},
		{headerName: "倉庫コード", field: "col4", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:80,cellClass: ['tl','cellclass']},
		{headerName: "フロアコード", field: "col5", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "エリアコード", field: "col6", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "列コード", field: "col7", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:80,cellClass: ['tl','cellclass']},
		{headerName: "棚コード", field: "col8", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:80,cellClass: ['tl','cellclass']},
		{headerName: "段コード", field: "col9", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:80,cellClass: ['tl','cellclass']},
		{headerName: "番コード", field: "col10", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:80,cellClass: ['tl','cellclass']},
		{headerName: "ロケ荷主コード", field: "col11", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
		{headerName: "ロケ荷主名", field: "col12", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
		{headerName: "格納什器ケース名", field: "col13", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
		{headerName: "ロケーションブロック名", field: "col14", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
		{headerName: "在庫区分", field: "col15", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:80,cellClass: ['tl','cellclass']},
		{headerName: "温度帯", field: "col16", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:80,cellClass: ['tl','cellclass']},
		{headerName: "ロケABC", field: "col17", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:80,cellClass: ['tl','cellclass']},
		{headerName: "標準ケース", field: "col18", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "商品コード", field: "col19", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:300,cellClass: ['tl','cellclass'],
		 cellRenderer: (params) => {
				const element = document.createElement('div');
				element.innerHTML = "<div class='itemcode'><p>" + params.value + "</p><a class='btn_secondary' href='javascript:openOtherWindow_wa6010(\"otherwindow_update\");'>在庫修正</a></div>";

				return element;
			}},
		{headerName: "商品名", field: "col20", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
		{headerName: "ピースバーコード", field: "col21", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
		{headerName: "ケースバーコード", field: "col22", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
		{headerName: "ロット", field: "col23", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:80,cellClass: ['tl','cellclass']},
		{headerName: "使用期限", field: "col24", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "入庫日", field: "col25", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "単位名", field: "col26", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:80,cellClass: ['tl','cellclass']},
		{headerName: "標準ケース入数", field: "col27", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:120,cellClass: ['tr','cellclass']},
		{headerName: "インナーケース数", field: "col28", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:130,cellClass: ['tr','cellclass']},
		{headerName: "インナーケース入数", field: "col29", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:140,cellClass: ['tr','cellclass']},
		{headerName: "バレット段数", field: "col30", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tr','cellclass']},
		{headerName: "バレット巻数", field: "col31", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tr','cellclass']},
		{headerName: "在庫数（ケース）", field: "col32", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:130,cellClass: ['tr','cellclass']},
		{headerName: "在庫数（インナーケース）", field: "col33", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:150,cellClass: ['tr','cellclass']},
		{headerName: "在庫数（バラ）", field: "col34", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:130,cellClass: ['tr','cellclass']},
		{headerName: "在庫数", field: "col35", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tr','cellclass']},
		{headerName: "引当可能数（ケース）", field: "col36", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:130,cellClass: ['tr','cellclass']},
		{headerName: "引当可能数（インナーケース）", field: "col37", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:150,cellClass: ['tr','cellclass']},
		{headerName: "引当可能数（バラ）", field: "col38", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:130,cellClass: ['tr','cellclass']},
		{headerName: "引当可能数", field: "col39", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tr','cellclass']},
		{headerName: "引当済数（ケース）", field: "col40", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tr','cellclass']},
		{headerName: "引当済数（インナーケース）", field: "col14", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:120,cellClass: ['tr','cellclass']},
		{headerName: "引当済数（バラ）", field: "col41", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tr','cellclass']},
		{headerName: "引当済数", field: "col42", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tr','cellclass']},
		{headerName: "荷主コード", field: "col43", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "荷主名", field: "col44", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
		{headerName: "引当設定", field: "col45", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:80,cellClass: ['tl','cellclass']},
		{headerName: "備考", field: "col46", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:200,cellClass: ['tl','cellclass']}
	];	

	// 行のテストデータ

	rowData = [];

	/*for(var i = 0; i < data;i++){
		rowData.push({col0: i,col1: '', col2: 'ロケーションテスト1', col3: '112233445561',col99:'00003801-041-test', col4: '00', col5: '11', col6: '22', col7: '33', col8: '44', col9: '55', col10: '66', col11: 'A123456', col12: '荷主・SEAOS（A）', col13: '軽量棚S箱', col14: 'バラ（通常）', col15: '良品', col16: '温度帯', col17: 'A', col18: 'メーカ箱', col19: '00003801-041-test', col20: 'テスト商品名001A', col21: 'SEAOS01-01CASE', col22: 'SEAOS01-01PCS', col23: '123456', col24: '2019-02-05', col25: '2019-02-05', col26: 'ｶｰﾄﾝ', col27: '10', col28: '10', col29: '3', col30: '3', col31: '1', col32: '1', col33: '1', col34: '1', col35: '1', col36: '1', col37: '1', col38: '1', col39: '1', col40: '1', col41: '1', col42: '1', col43: 'A123456', col44: '荷主・SEAOS(A)', col45: '不可', col46: '○○○の都合で一時引当不可とする'});

	}*/
			

	// オプションの設定
	gridOptions1 = {
		enableColResize:true,
		enableSorting: true,
		enableFilter: true,
		headerHeight: 30,
		floatingFiltersHeight:30,
		columnDefs: columnDefs1,/*headedrデータ*/
		rowData: rowData,/*行データ*/
		floatingFilter: true,/*2列目のフィルターのon/off*/
		suppressRowClickSelection:true,/*チェックボックス以外のクリックを認めるか？*/
		rowSelection :'multiple',/*複数選択できるように*/
		localeText: getLocaleText(),/*データ０の時のメッセージ*/
		rowHeight : 40
	};
	


	//   対象となる要素の指定
	var eGridDiv = document.querySelector('#' + divid);

	//   グリッドの生成
	new agGrid.Grid(eGridDiv, gridOptions1);

	//セルクリック時に行を選択状態にする
	gridOptions1.onCellClicked = function(param){
		/*クリックされたセルの取得*/
		var cols = param.colDef.field;
		var index = param.node.childIndex;
		//セルがチェックボックスセルだったら
		if(cols == "col0"){
			//状況により行のセレクトをon/offする
			var node = gridOptions1.api.getRowNode(index);
			if(node.isSelected()){
				node.setSelected(false);

			}else{
				node.setSelected(true);
			}
		}
	}
	gridOptions1.onRowSelected = function(param){
		$(".tableitem__btnarea--rightarea span a").removeClass("disabled");
	}
	//ag-gridの仕様電話再描画されるためソートするとチェックボックスがデフォルトデザインに戻ってしまうためそれを防ぐ
	gridOptions1.onSortChanged = function(param){
		checkboxChange();
	}

	gridOptions1.onFilterChanged = function(param){
		//モーダル内の検索インプットへのアクティブクラス付与
		$(".ag-floating-filter-input").each(function(){
			if($(this).val().length == 0){
				$(this).removeClass("active");
			}else{
				$(this).addClass("active");
			}
		});
		checkboxChange();
		
	}
	
	/*今回の場合はテーブル4*/
	var chkflg1 = false;
	//横スクロールでステータス領域が消えた時に左はじに商品コード領域を表示
	$("#myGrid1 div.ag-body-viewport").scroll(function(e){
		//今回のステータス位置が185付近なので今回はそこに設定
		if($("#myGrid1 div.ag-body-viewport").scrollLeft() > 1585 && !chkflg1 ){
			chkflg1 = true;

			showcol(true);
		}else if($("#myGrid1 div.ag-body-viewport").scrollLeft() <= 1585 && chkflg1){
			chkflg1 = false;
			showcol(false);

		}
		checkboxChange();
	});
	//列の表示非表示切り替え
	function showcol(show) {
		gridOptions1.columnApi.setColumnVisible('col99', show);
	}
	

	headerCheckboxChange();
	checkboxChange();
	gridWidth(".tableitem",columnDefs1,"#myGrid1","#table1")
	gridHeight(".tableitem",rowData,"#myGrid1","#table1",1);	

}
function setGrid(){
	$("p.nodata").html("");
	
	
	rowData = [];

	for(var i = 0; i < 30;i++){
		rowData.push({col0: i,col1: '', col2: 'ロケーションテスト1', col3: '112233445561',col99:'00003801-041-test', col4: '00', col5: '11', col6: '22', col7: '33', col8: '44', col9: '55', col10: '66', col11: 'A123456', col12: '荷主・SEAOS（A）', col13: '軽量棚S箱', col14: 'バラ（通常）', col15: '良品', col16: '温度帯', col17: 'A', col18: 'メーカ箱', col19: '00003801-041-test', col20: 'テスト商品名001A', col21: 'SEAOS01-01CASE', col22: 'SEAOS01-01PCS', col23: '123456', col24: '2019-02-05', col25: '2019-02-05', col26: 'ｶｰﾄﾝ', col27: '10', col28: '10', col29: '3', col30: '3', col31: '1', col32: '1', col33: '1', col34: '1', col35: '1', col36: '1', col37: '1', col38: '1', col39: '1', col40: '1', col41: '1', col42: '1', col43: 'A123456', col44: '荷主・SEAOS(A)', col45: '不可', col46: '○○○の都合で一時引当不可とする'});
	}	
	gridOptions1.api.updateRowData({add: rowData});
	checkboxChange();

	spredScrollArrowChange();
	
	//buttonのdisabled解放
	$(".tableitem__btnarea--leftarea span:nth-child(2) a").removeClass("disabled");
	gridHeight(".tableitem",rowData,"#myGrid1","#table1",1);	
}
function spredScrollArrowChange(){
	//初期表示しているすべて（TAB4）のみロード時に横スクロールの場所設定を行う
	var parentArea = $("#myGrid1 div.ag-body-container").height();

	var tbarea = $("#myGrid1 div.ag-body-viewport").scrollTop() + $("#myGrid1 div.ag-body-viewport").height();
	if(parentArea - tbarea > 0){
		//スクロールバーがある場合の処理
		$("#table1 div.scrollArrow").css("right","16px");
	}else{
		//スクロールバーがない場合の処理
		$("#table1 div.scrollArrow").css("right","0px");

	}

	//初期表示しているすべて（TAB4）のみロード時に横スクロールの表示設定を行う
	var parentArea = $("#myGrid1 div.ag-body-container").width();

	var tbarea = $("#myGrid1 div.ag-body-viewport").scrollTop() + $("#myGrid1 div.ag-body-viewport").width();

	if(parentArea - tbarea > 0){
		//スクロールバーがある場合の処理
		$("#table1 div.scrollArrow").removeClass("dspnone");
		var height = $("#myGrid1").innerHeight() - 76;
		var dspheight = $("#myGrid1 .ag-body-container").innerHeight() ;
		
		var gh = 0;
		if(height < dspheight){
			gh = height;
		}else{
			gh = dspheight;
		}
		
		$("#table1 div.scrollArrow").css("height",gh + "px");

	}else{
		//スクロールバーがない場合の処理
		$("#table1 div.scrollArrow").addClass("dspnone");

	}



	/*TAB4のテーブル内の→マウスオーバー挙動*/
	var tm1 = 0;
	var tm2 = 0;
	var scrollMax = 0;
	var dspflg1 = true;
	//スクロールイベント
	$("#table1 div.scrollArrow").hover(function(){
		tm2 = setTimeout(function(){
			tm1 = setInterval(function(){
				//スクロールがMAXになったら表示を消すためにここで値を取得
				scrollMax = $("#myGrid1 div.ag-body-container").width() - $("#myGrid1 div.ag-body-viewport").width();

				$("#myGrid1 div.ag-body-viewport").scrollLeft( $("#myGrid1 div.ag-body-viewport").scrollLeft() + 2);

				if(scrollMax <= $("#myGrid1 div.ag-body-viewport").scrollLeft() && dspflg1){
					$("#table1 div.scrollArrow").addClass("dspnone");
					dspflg1 = false;
					clearTimeout(tm2);
					clearInterval(tm1);
				}
			},0);
		},300);
	},function(){
		clearTimeout(tm2);
		clearInterval(tm1);
	});
	
	//scrollArrowが消えているときにscrollMAXと値が変わったら再表示
	$("#myGrid1 div.ag-body-viewport").scroll(function(){
		
		scrollMax = $("#myGrid1 div.ag-body-container").width() - $("#myGrid1 div.ag-body-viewport").width();
		
		if(scrollMax <= $("#myGrid1 div.ag-body-viewport").scrollLeft()){
			$("#table1 div.scrollArrow").addClass("dspnone");
			dspflg1 = false;
		}
		
		if(scrollMax > $("#myGrid1 div.ag-body-viewport").scrollLeft() && !dspflg1){
			$("#table1 div.scrollArrow").removeClass("dspnone");
			dspflg1 = true;
		}
	});	
}

//tab切替
function changeTab(tnum){
	var cnt = 1;
	$("section.tabs > ul.tabs-nav > li.tabs-nav__item").each(function() {
		$("#tab"+ cnt + "-item").addClass("dspnone");
		if($(this)[0].id == "t" + tnum){
			$("#t" + cnt).addClass("tabs-nav__item--active");
			$("#tab"+ cnt + "-item").removeClass("dspnone");
			
			var dt = 0;
			if(tnum == 4){
				dt = 30;
			}
			setGridData1("myGrid" + tnum,dt);

		}else{
			$("#t" + cnt).removeClass("tabs-nav__item--active");
		}
		cnt++;
	});
}


// JavaScript Document
function selectSelected(id){
	//未選択
	if($("#" + id).val() == ""){
		$("#" + id).removeClass("active");
		$("#" + id).removeClass("error");
		$("#" + id).removeClass("disabled");
	//active
	}else if($("#" + id).val() == "1"){
		$("#" + id).addClass("active");
		$("#" + id).removeClass("error");
		$("#" + id).removeClass("disabled");
	//error
	}else if($("#" + id).val() == "2"){
		$("#" + id).removeClass("active");
		$("#" + id).addClass("error");
		$("#" + id).removeClass("disabled");
	//disabled
	}else if($("#" + id).val() == "3"){
		$("#" + id).removeClass("active");
		$("#" + id).removeClass("error");
		$("#" + id).addClass("disabled");
	}
}
//ボタンにdisabled属性がついていたらファンクションを動作させない
function checkModalOpen(obj){
	if(!$(".tableitem__btnarea--rightarea span a").hasClass("disabled")){
		openModalWindow();
	}
}


function checkInput(){
	var radioCheck = $('input[name=modalradio]:eq(0)').prop('checked');
	//var txt = $(".modal-content__contentarea textarea").val();
	if(radioCheck){
		$(".modal-header a").removeClass("disabled");
	}
	
}

//最古登録/修正のopen、close
function openOtherWindow_wa6010(id){
	$("div#" + id).removeClass("dspnone");
	$("div#" + id).addClass("openotherwindow");
}
function closeOtherWindow_wa6010(id,obj,idx){
	if(obj.length > 0 && !$("#" + obj).hasClass("disabled")){
		$("div#" + id).removeClass("openotherwindow");
		//display:noneでアニメーションやtransitionを使うと機能しないため
		//○○秒後にdisplay:noneを発動させる
		setTimeout(function(){
			$("div#" + id).addClass("dspnone");		
		}, 300);
		//登録、修正の場合は下記を実行、グリッドの指定行を青色にし2秒後にトランジションで消す
		$(".ag-pinned-left-cols-container .ag-row").each(function(){
			var index = $(".ag-pinned-left-cols-container .ag-row").index(this)
			if(index == idx){
				$(this).addClass("row-update");

				var obj = $(this);
				setTimeout(function(){
					obj.addClass("row-default");
					obj.removeClass("row-update");
				},2000);
			}
		});
		$(".ag-body-container .ag-row").each(function(){
			var index = $(".ag-body-container .ag-row").index(this)
			if(index == idx){
				$(this).addClass("row-update");
				var obj = $(this);
				setTimeout(function(){
					obj.addClass("row-default");
					obj.removeClass("row-update");
				},2000);
			}
		});
	}else if(obj.length == 0){
		$("div#" + id).removeClass("openotherwindow");
		//display:noneでアニメーションやtransitionを使うと機能しないため
		//○○秒後にdisplay:noneを発動させる
		setTimeout(function(){
			$("div#" + id).addClass("dspnone");		
		}, 300);
	}

}
function nextCheck(obj){
	if(obj.val() == "1"){
		obj.parent().next().find("input").addClass("disabled");
		obj.parent().next().find("input").prop("disabled",true);
	}else{
		obj.parent().next().find("input").removeClass("disabled");
		obj.parent().next().find("input").prop("disabled",false);
	}
}