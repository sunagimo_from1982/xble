

$(function(){

	setGridData1("myGrid1",0);
	
	var sh = $(".search__searcharea").height();
	$(".search__searcharea--title").click(function(){
		if($(".search__searcharea__items").hasClass("dspnone")){
			$(".tableitem__tablearea").css("height","calc(100% - 290px)");
			$(".search__searcharea__items").removeClass("dspnone");
			$(this).removeClass("closed");
		}else{
			$(".search__searcharea__items").addClass("dspnone");
			$(this).addClass("closed");
			$(".tableitem__tablearea").css("height","calc(100% - 135px)");
		}
		spredScrollArrowChange();
	});
	
	$(".arrowdiv").click(function(e){
		//サイドメニューオープン時にスプレッドの高さを調節
		setTimeout(function(){
			/*サイドメニューオープン時*/
			if($(".arrowdiv").hasClass("openarrow")){
				if(sh != $(".search__searcharea").height()){
					if(!$(".search__searcharea__items").hasClass("dspnone")){
						$(".tableitem__tablearea").css("height","calc(100% - 365px)");
					}			
				}			
			/*サイドメニュークローズ時*/
			}else{
				if(!$(".search__searcharea__items").hasClass("dspnone")){
					$(".tableitem__tablearea").css("height","calc(100% - 295px)");
				}else{
					$(".tableitem__tablearea").css("height","calc(100% - 140px)");
				}
			}
			
			spredScrollArrowChange();
		},200);
		
	});	

	//form要素に変更がったらactiveにする
	$("#otherwindow_update input,#otherwindow_update select,#otherwindow_update textarea").change(function(){

		if($(this).prop("type") == "text" || $(this).prop("type") == "date" || $(this)[0].tagName.toLowerCase() == "textarea"){
			$(this).addClass("active");
		}

		$("#regist2").removeClass("disabled");
	});



});


//テーブル作成用ファンクション
var columnDefs1;
var gridOptions1;
var rowData = [];
function setGridData1(divid,data){

	// ヘッダー

	//headerName=列の題名,field=列のID,width=幅、cellClas=各セルに適用するクラス
	//headerCheckboxSelection=ヘッダーにチェックボックスを付けるか？
	//checkboxSelection=各行にチェックボックスを表示するか？
	//pinned=列固定,suppressMenu=ヘッダー一列目に検索フィールドを出すか？
	//hide=表示、非表示、sortable=ソートができるかできないか
	//filter=ヘッダー2列目の検索フィールドを何にするか？
	
	/*ヘッダーデータをタイプで入れ替える*/
	columnDefs1 = [
		{headerName: "", field: "col0", width: 30,cellClass: ['tc','cl_w','noCellFocus'],
		 headerCheckboxSelection: true,checkboxSelection: true,suppressMenu: true,
		 lockPosition:true,pinned: 'left',suppressMovable:true
		},
		{headerName: "", field: "col1",pinned: 'left',lockPosition:true,
		 suppressMenu: true,sortable:true,width:95,cellClass: ['tc','cellclass'],suppressMovable:true,
		 cellRenderer: (params) => {
				const element = document.createElement('div');
				element.innerHTML = "<a class='btn_secondary' href='javascript:openOtherWindow_wa6020(\"otherwindow_update\");'>修正</a>";

				return element;
			}
		},
		{headerName: "受払ID", field: "col2", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tr','cellclass']},
		{headerName: "確定", field: "col3", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:80,cellClass: ['tl','cellclass']},
		{headerName: "荷主名", field: "col4", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "発生日", field: "col5", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "計上日", field: "col6", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "計上年月", field: "col7", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "ロケーション", field: "col8", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
		{headerName: "ロケーション名", field: "col9", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
		{headerName: "ロケーションブロック名", field: "col10", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:150,cellClass: ['tl','cellclass']},
		{headerName: "商品コード", field: "col11", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:150,cellClass: ['tl','cellclass']},
		{headerName: "商品名", field: "col12", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:150,cellClass: ['tl','cellclass']},
		{headerName: "在庫区分", field: "col13", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:80,cellClass: ['tl','cellclass']},
		{headerName: "ロット", field: "col14", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "使用期限", field: "col15", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "単位名", field: "col16", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "入数", field: "col17", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:80,cellClass: ['tr','cellclass']},
		{headerName: "数量", field: "col18", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:80,cellClass: ['tr','cellclass']},
		{headerName: "業務区分名", field: "col19", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "指示番号", field: "col20", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
		{headerName: "トランザクション区分", field: "col21", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
		{headerName: "トランザクション区分枝", field: "col22", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
		{headerName: "備考", field: "col23", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
		{headerName: "請求対象外", field: "col24", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "請求対象外日時", field: "col25", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
		{headerName: "請求対象外者名", field: "col26", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "更新日時", field: "col27", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
		{headerName: "更新者名", field: "col28", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "登録日時", field: "col29", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
		{headerName: "登録者名", field: "col30", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tl','cellclass']}
	];	

	// 行のテストデータ


			

	// オプションの設定
	gridOptions1 = {
		enableColResize:true,
		enableSorting: true,
		enableFilter: true,
		headerHeight: 30,
		floatingFiltersHeight:30,
		columnDefs: columnDefs1,/*headedrデータ*/
		rowData: rowData,/*行データ*/
		floatingFilter: true,/*2列目のフィルターのon/off*/
		suppressRowClickSelection:true,/*チェックボックス以外のクリックを認めるか？*/
		rowSelection :'multiple',/*複数選択できるように*/
		localeText: getLocaleText(),/*データ０の時のメッセージ*/
		rowHeight : 40,
		pagination:true,
		paginationPageSize:20
	};
	


	//   対象となる要素の指定
	var eGridDiv = document.querySelector('#' + divid);

	//   グリッドの生成
	new agGrid.Grid(eGridDiv, gridOptions1);

	//セルクリック時に行を選択状態にする
	gridOptions1.onCellClicked = function(param){
		/*クリックされたセルの取得*/
		var cols = param.colDef.field;
		var index = param.node.childIndex;
		//セルがチェックボックスセルだったら
		if(cols == "col0"){
			//状況により行のセレクトをon/offする
			var node = gridOptions1.api.getRowNode(index);
			if(node.isSelected()){
				node.setSelected(false);

			}else{
				node.setSelected(true);
			}
		}
	}
	//ag-gridの仕様電話再描画されるためソートするとチェックボックスがデフォルトデザインに戻ってしまうためそれを防ぐ
	gridOptions1.onSortChanged = function(param){
		checkboxChange();
	}

	gridOptions1.onFilterChanged = function(param){
		//モーダル内の検索インプットへのアクティブクラス付与
		$(".ag-floating-filter-input").each(function(){
			if($(this).val().length == 0){
				$(this).removeClass("active");
			}else{
				$(this).addClass("active");
			}
		});
		checkboxChange();
		
	}
	gridOptions1.onPaginationChanged = function(param){
		spredScrollArrowChange();
	}
	

	headerCheckboxChange();
	checkboxChange();
	
	gridWidth(".tableitem",columnDefs1,"#myGrid1","#table1")
	
	hiddenPagerControl(1,0);
	gridHeight(".tableitem",rowData,"#myGrid1","#table1");
}
//pagerの表示件数を変更
function updatePager(){
	var value = $("#select2").val();
    gridOptions1.api.paginationSetPageSize(Number(value));
	checkboxChange();
	setPageSep();
}
//pagerの件数取得
function getAllCnt(){
	$("#allcount").html(rowData.length);
}
var nowPage = 0;
function setGrid(){
	$("p.nodata").html("");
	
	

	for(var i = 0; i < 100;i++){
		rowData.push({col0: i,col1: '', col2: 1600 + i, col3: '確定', col4: '荷主・SEAOS(A)',col5:'2019-02-05', col6: '2019-02-05', col7: '2019-02-05', col8: '112233445561', col9: 'ロケテスト01', col10: 'バラ（通常）', col11: '00003801-041-test', col12: 'テスト商品名001A', col13: '良品', col14: '123456', col15: '2019-02-05', col16: 'ｶｰﾄﾝ', col17: '10', col18: '10', col19: '移動（入庫）', col20: '20190320-00001', col21: '入荷予定', col22: 'バラ', col23: 'ダミーテキストダミーテ', col24: '請求対象外', col25: '2019-03-20 10:35:55', col26: '山田太郎', col27: '2019-03-20 10:35:55', col28: '山田太郎', col29: '2019-03-20 10:35:55', col30: '山田太郎'});
	}	
	gridOptions1.api.updateRowData({add: rowData});
	checkboxChange();

	spredScrollArrowChange();
	getAllCnt();
	
	if(rowData.length > 0){
		$(".tableitem__tablearea__pagerItem .disabled").removeClass("disabled");
		//各矢印クリックでページ移動
		$(".tableitem__tablearea__pagerItem i").click(function(){
			if($(this).prop("id") == "first" && !$(this).hasClass("disabled")){
				gridOptions1.api.paginationGoToFirstPage();
			}else if($(this).prop("id") == "prev" && !$(this).hasClass("disabled")){
				gridOptions1.api.paginationGoToPreviousPage();
			}else if($(this).prop("id") == "next" && !$(this).hasClass("disabled")){
				gridOptions1.api.paginationGoToNextPage();
			}else if($(this).prop("id") == "last" && !$(this).hasClass("disabled")){
				gridOptions1.api.paginationGoToLastPage()
			}
			setPageSep();
		});
		setPageSep();
	}
	hiddenPagerControl(1,1);
	gridHeight(".tableitem",rowData,"#myGrid1","#table1");	
}
//pagerの真ん中の構築 
function setPageSep(){
	
	var h = "<span class='tableitem__tablearea__pagerItem__nowpage--desc--big'>" + (gridOptions1.api.paginationGetCurrentPage() + 1) + "</span> /" + gridOptions1.api.paginationGetTotalPages() + " ";
	$(".tableitem__tablearea__pagerItem__nowpage--desc").html(h);
	//1ページ目では前に戻れないようにdisabled
	if(gridOptions1.api.paginationGetCurrentPage() + 1 == 1){
	   $(".tableitem__tablearea__pagerItem i#first").addClass("disabled");
	   $(".tableitem__tablearea__pagerItem i#prev").addClass("disabled");
	}else{
	   $(".tableitem__tablearea__pagerItem i#first").removeClass("disabled");
	   $(".tableitem__tablearea__pagerItem i#prev").removeClass("disabled");
	}
	
	//LASTページ目では次にいけないようにdisabled
	if(gridOptions1.api.paginationGetCurrentPage() + 1 == gridOptions1.api.paginationGetTotalPages()){
	   $(".tableitem__tablearea__pagerItem i#next").addClass("disabled");
	   $(".tableitem__tablearea__pagerItem i#last").addClass("disabled");
	}else{
	   $(".tableitem__tablearea__pagerItem i#next").removeClass("disabled");
	   $(".tableitem__tablearea__pagerItem i#last").removeClass("disabled");
	}
	
}
//スプレッドの矢印の構築・再構築
function spredScrollArrowChange(){
	if(rowData.length > 0){
		//初期表示しているすべて（TAB4）のみロード時に横スクロールの場所設定を行う
		var parentArea = $("#myGrid1 div.ag-body-container").height();

		var tbarea = $("#myGrid1 div.ag-body-viewport").scrollTop() + $("#myGrid1 div.ag-body-viewport").height();
		if(parentArea - tbarea > 0){
			//スクロールバーがある場合の処理
			$("#table1 div.scrollArrow").css("right","16px");
		}else{
			//スクロールバーがない場合の処理
			$("#table1 div.scrollArrow").css("right","0px");

		}

		//初期表示しているすべて（TAB4）のみロード時に横スクロールの表示設定を行う
		var parentArea = $("#myGrid1 div.ag-body-container").width();

		var tbarea = $("#myGrid1 div.ag-body-viewport").scrollTop() + $("#myGrid1 div.ag-body-viewport").width();

		if(parentArea - tbarea > 0){
			//スクロールバーがある場合の処理
			$("#table1 div.scrollArrow").removeClass("dspnone");
			var height = $("#myGrid1").innerHeight() - 121;
			var dspheight = $("#myGrid1 .ag-body-container").innerHeight() ;

			var gh = 0;
			if(height < dspheight){
				gh = height;
			}else{
				gh = dspheight;
			}

			$("#table1 div.scrollArrow").css("height",gh + "px");

		}else{
			//スクロールバーがない場合の処理
			$("#table1 div.scrollArrow").addClass("dspnone");

		}



		/*TAB4のテーブル内の→マウスオーバー挙動*/
		var tm1 = 0;
		var tm2 = 0;
		var scrollMax = 0;
		var dspflg1 = true;
		//スクロールイベント
		$("#table1 div.scrollArrow").hover(function(){
			tm2 = setTimeout(function(){
				tm1 = setInterval(function(){
					//スクロールがMAXになったら表示を消すためにここで値を取得
					scrollMax = $("#myGrid1 div.ag-body-container").width() - $("#myGrid1 div.ag-body-viewport").width();

					$("#myGrid1 div.ag-body-viewport").scrollLeft( $("#myGrid1 div.ag-body-viewport").scrollLeft() + 1);

					if(scrollMax <= $("#myGrid1 div.ag-body-viewport").scrollLeft() && dspflg1){
						$("#table1 div.scrollArrow").addClass("dspnone");
						dspflg1 = false;
						clearTimeout(tm2);
						clearInterval(tm1);
					}
				},0);
			},300);
		},function(){
			clearTimeout(tm2);
			clearInterval(tm1);
		});

		//scrollArrowが消えているときにscrollMAXと値が変わったら再表示
		$("#myGrid1 div.ag-body-viewport").scroll(function(){

			scrollMax = $("#myGrid1 div.ag-body-container").width() - $("#myGrid1 div.ag-body-viewport").width();

			if(scrollMax <= $("#myGrid1 div.ag-body-viewport").scrollLeft()){
				$("#table1 div.scrollArrow").addClass("dspnone");
				dspflg1 = false;
			}

			if(scrollMax > $("#myGrid1 div.ag-body-viewport").scrollLeft() && !dspflg1){
				$("#table1 div.scrollArrow").removeClass("dspnone");
				dspflg1 = true;
			}
		});	
	}
}


// JavaScript Document
function selectSelected(id){
	//未選択
	if($("#" + id).val() == "0"){
		$("#" + id).removeClass("active");
		$("#" + id).removeClass("error");
		$("#" + id).removeClass("disabled");
	//active
	}else if($("#" + id).val() == "1"){
		$("#" + id).addClass("active");
		$("#" + id).removeClass("error");
		$("#" + id).removeClass("disabled");
	//error
	}else if($("#" + id).val() == "2"){
		$("#" + id).removeClass("active");
		$("#" + id).addClass("error");
		$("#" + id).removeClass("disabled");
	//disabled
	}else if($("#" + id).val() == "3"){
		$("#" + id).removeClass("active");
		$("#" + id).removeClass("error");
		$("#" + id).addClass("disabled");
	}
}



function checkInput(){
	var radioCheck = $('input[name=modalradio]:eq(0)').prop('checked');
	if(radioCheck){
		$(".modal-header a").removeClass("disabled");
	}
	
}

//入荷実績登録のopen、close
function openOtherWindow_wa6020(id){
	$("div#" + id).removeClass("dspnone");
	$("div#" + id).addClass("openotherwindow");
}
function closeOtherWindow_wa6020(id,obj,idx){
	if(obj.length > 0 && !$("#" + obj).hasClass("disabled")){
		$("div#" + id).removeClass("openotherwindow");
		//display:noneでアニメーションやtransitionを使うと機能しないため
		//○○秒後にdisplay:noneを発動させる
		setTimeout(function(){
			$("div#" + id).addClass("dspnone");		
		}, 300);
		//登録、修正の場合は下記を実行、グリッドの指定行を青色にし2秒後にトランジションで消す
		$(".ag-pinned-left-cols-container .ag-row").each(function(){
			var index = $(".ag-pinned-left-cols-container .ag-row").index(this)
			if(index == idx){
				$(this).addClass("row-update");

				var obj = $(this);
				setTimeout(function(){
					obj.addClass("row-default");
					obj.removeClass("row-update");
				},2000);
			}
		});
		$(".ag-body-container .ag-row").each(function(){
			var index = $(".ag-body-container .ag-row").index(this)
			if(index == idx){
				$(this).addClass("row-update");
				var obj = $(this);
				setTimeout(function(){
					obj.addClass("row-default");
					obj.removeClass("row-update");
				},2000);
			}
		});
	}else if(obj.length == 0){
		$("div#" + id).removeClass("openotherwindow");
		//display:noneでアニメーションやtransitionを使うと機能しないため
		//○○秒後にdisplay:noneを発動させる
		setTimeout(function(){
			$("div#" + id).addClass("dspnone");		
		}, 300);
	}

}