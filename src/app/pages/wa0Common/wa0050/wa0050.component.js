$(function(){
	$(".ninushiSelect").addClass("dspnone");
	//アップロード（input=file）クリックし、ファイルを選択したときにファイル名を表示するスクリプト
	if($("#upload")){
		setInterval(function(){

			if($("#upload").val().length == 0){
				$("#filename").html("ファイルが選択されていません。");
			}else{
				$("#filename").html("<span><img class='deletebtn' src='../../../../assets/images/delete.png'></span>ファイル：" + $("#upload")[0].files[0].name);
				
				$("#upload_btn").removeClass("disabled");
			}
		},1000);
	}

	$("input[name='radio1-1']").click(function(){
		if($("#radio1-1-2").prop("checked")){
		   $("#select2").removeClass("disabled");
		   $("#select2").prop("disabled",false);
			
		}else{
		   $("#select2").addClass("disabled");
		   $("#select2").prop("disabled",true);
		}
	});
	
	$("#select1,#radio1-1-1,#radio1-1-2,#select2").change(function(){
		
		
		
		if($("#select1").val().length > 0 && $("#radio1-1-1").prop("checked")){
			$(".btnarea a.btn_primary").removeClass("disabled");
		}else if($("#select1").val().length > 0 && $("#radio1-1-2").prop("checked") && $("#select2").val().length > 0){
			$(".btnarea a.btn_primary").removeClass("disabled");
		}else{
			$(".btnarea a.btn_primary").addClass("disabled");
		}
	});
	
	$("#otherwindow1 input,#otherwindow1 select").change(function(){
		var idx = 0;
		var cnt = 0;
		$("#otherwindow1 input,#otherwindow1 select").each(function(){

			if(cnt == 0 || cnt == 1){

				if($(this).val().length > 0){
					idx++;
				}
			}
			cnt++;
		});
		if(idx == 2){

			$("#regist1").removeClass("disabled");
		}
	});	
	
	
	$(".otherwindow-contentsScroll").scroll(function(e){
		
		if($(".otherwindow-contentsScroll").scrollTop() > 550){
			for(var i = 1;i <= 8;i++){
				if($("#desc" + i)[0]){
					$("#desc" + i).addClass("dspnone");
				}
			}

			if($("#select1").val() == "1"){
				$("#desc1").removeClass("dspnone");
			}else if($("#select1").val() == "2"){
				$("#desc2").removeClass("dspnone");
			}else if($("#select1").val() == "3"){
				$("#desc3").removeClass("dspnone");
			}else if($("#select1").val() == "5"){
				$("#desc5").removeClass("dspnone");
			}
		}else{
			$(".otherwindow-desc").addClass("dspnone");
		}
		console.log($(".otherwindow-contentsScroll").scrollTop());
	});
});

function openOtherWindow_wa0050(id){
	$("div#otherwindow" + id).removeClass("dspnone");
	$("div#otherwindow" + id).addClass("openotherwindow");
	
	for(var i = 1;i <= 8;i++){
		$("#mapping" + i).addClass("dspnone");
	}

	if($("#select1").val() == "1"){
		$("#data1").html("出荷データ");
		$("#mapping1").removeClass("dspnone");
	}else if($("#select1").val() == "2"){
		$("#data1").html("入荷データ");
		$("#mapping2").removeClass("dspnone");
	}else if($("#select1").val() == "3"){
		$("#data1").html("商品データ");
		$("#mapping3").removeClass("dspnone");
	}else if($("#select1").val() == "4"){
		$("#data1").html("届先マスタ");
		$("#mapping4").removeClass("dspnone");
	}else if($("#select1").val() == "5"){
		$("#data1").html("ロケーションマスタ");
		$("#mapping5").removeClass("dspnone");
	}else if($("#select1").val() == "6"){
		$("#data1").html("荷主マスタ");
		$("#mapping6").removeClass("dspnone");

	}else if($("#select1").val() == "7"){
		$("#data1").html("引当可能在庫データ");
		$("#mapping7").removeClass("dspnone");
	}else if($("#select1").val() == "8"){
		$("#data1").html("期首在庫データ");
		$("#mapping8").removeClass("dspnone");
	}
}
function closeOtherWindow_wa0050(){
	$(".btnarea a.btn_primary").addClass("disabled");

	closeOtherWindow();
	showMessageBox('データマッピング定義を作成しました。');
}
// JavaScript Document
function selectSelected(id){
	//未選択
	if($("#" + id).val() == "0"){
		$("#" + id).removeClass("active");
		$("#" + id).removeClass("error");
		$("#" + id).removeClass("disabled");
	//active
	}else if($("#" + id).val() == "1"){
		$("#" + id).addClass("active");
		$("#" + id).removeClass("error");
		$("#" + id).removeClass("disabled");
	//error
	}else if($("#" + id).val() == "2"){
		$("#" + id).removeClass("active");
		$("#" + id).addClass("error");
		$("#" + id).removeClass("disabled");
	//disabled
	}else if($("#" + id).val() == "3"){
		$("#" + id).removeClass("active");
		$("#" + id).removeClass("error");
		$("#" + id).addClass("disabled");
	}
}
