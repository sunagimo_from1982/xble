$(function(){

	//各tab内のモーダル生成
	setGridData1("1",7);
	


});


var columnDefs1;
var gridOptions1;
var rowData1 = [];
function setGridData1(divid,data){

	// ヘッダー

	//headerName=列の題名,field=列のID,width=幅、cellClas=各セルに適用するクラス
	//headerCheckboxSelection=ヘッダーにチェックボックスを付けるか？
	//checkboxSelection=各行にチェックボックスを表示するか？
	//pinned=列固定,suppressMenu=ヘッダー一列目に検索フィールドを出すか？
	//hide=表示、非表示、sortable=ソートができるかできないか
	//filter=ヘッダー2列目の検索フィールドを何にするか？
	
	/*ヘッダーデータをタイプで入れ替える*/
	
	columnDefs1 = [
					{headerName: "出荷番号", field: "col1", filter:'text',unSortIcon: true,lockPosition:true,
					 suppressMenu: true,width:210,cellClass: ['tl','cellclass'],pinned: 'left',
					 cellRenderer: (params) => {
							const element = document.createElement('div');
							element.innerHTML = "<div class='itemcode'><a class='btn_secondary' href='javascript:openOtherWindow_wa9144("+ params.rowIndex+ ");'>修正</a><p>" + params.value + "</p></div>";
							return element;
						}},
					{headerName: "出荷指示日", field: "col2", filter:'text',unSortIcon: true,
					 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
					{headerName: "荷主名", field: "col3", filter:'text',unSortIcon: true,
					 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
					{headerName: "届先・名前", field: "col4", filter:'text',unSortIcon: true,
					 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
					{headerName: "届先コード", field: "col5", filter:'text',unSortIcon: true,
					 suppressMenu: true,width:130,cellClass: ['tl','cellclass']},
					{headerName: "届先・郵便番号", field: "col6", filter:'text',unSortIcon: true,
					 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
					{headerName: "住所1", field: "col7", filter:'text',unSortIcon: true,
					 suppressMenu: true,width:300,cellClass: ['tl','cellclass']},
					{headerName: "住所2", field: "col8", filter:'text',unSortIcon: true,
					 suppressMenu: true,width:250,cellClass: ['tl','cellclass']},
					{headerName: "住所3", field: "col9", filter:'text',unSortIcon: true,
					 suppressMenu: true,width:200,cellClass: ['tl','cellclass']},
					{headerName: "届先・電話番号", field: "col10", filter:'text',unSortIcon: true,
					 suppressMenu: true,width:160,cellClass: ['tl','cellclass']}
				];


	// 行のテストデータ

	for(var i = 0; i < data;i++){
		
		rowData1.push({col0:i,col1: "20181227-00034" , col2: '2019-04-21',col3: '荷主・SEAOS (B)',col4: 'ﾏﾘｱﾏﾘｱ　津南店', col5: '1301021-4060', col6: '514-0817', col7: '三重県津市高茶屋小森町145', col8: 'ｲｵﾝ津南ｼｮｯﾋﾟﾝｸﾞｾﾝﾀｰｻﾝﾊﾞﾚｰ1F', col9: '', col10: '059-238-6101'});
	}

			
	// オプションの設定
	gridOptions1 = {
		enableColResize:true,
		enableSorting: true,
		enableFilter: true,
		headerHeight: 30,
		floatingFiltersHeight:30,
		columnDefs: columnDefs1,/*headedrデータ*/
		rowData: rowData1,/*行データ*/
		floatingFilter: true,/*2列目のフィルターのon/off*/
		suppressRowClickSelection:true,/*チェックボックス以外のクリックを認めるか？*/
		rowSelection :'multiple',/*複数選択できるように*/
		localeText: getLocaleText(),/*データ０の時のメッセージ*/
		rowHeight : 40,
		pagination:true,
		paginationPageSize:rowData1.length
	};
	


	//   対象となる要素の指定
	var eGridDiv = document.querySelector('#myGrid' + divid);

	//   グリッドの生成
	new agGrid.Grid(eGridDiv, gridOptions1);

	setFunctions(gridOptions1,divid,rowData1);
	
	/*今回の場合はテーブル4*/
	var chkflg4 = false;
	//横スクロールでステータス領域が消えた時に左はじにカラー領域を表示
	$("#myGrid" + divid + " div.ag-body-viewport").scroll(function(e){
		checkboxChange();
	});
	
	

	getAllCnt(rowData1,divid,gridOptions1);
	
	
	spredScrollArrowChange(divid);
	
	gridWidth(".tableitem",columnDefs1,"#myGrid" + divid, "#table" + divid);

	gridHeight(".tableitem",rowData1,"#myGrid" + divid, "#table" + divid);			
	
}
function setGrid1(divid,del){
	rowData1.splice(del, 1);
	gridHeight(".tableitem",rowData1,"#myGrid1", "#table1");				
	if(rowData1.length == 0){
		gridOptions1.api.setRowData([]);
		$(".tableitem__tablearea .nodata").html("修正が必要なデータはありません。");
		$(".notice-desc").html("");
		hiddenPagerControl(divid,0);

		$("#table" + divid + " div.scrollArrow").addClass("dspnone");
	}else{
		gridOptions1.api.updateRowData({add: rowData1});
		gridOptions1.api.paginationSetPageSize(rowData1.length);

		getAllCnt(rowData1,divid,gridOptions1);


		spredScrollArrowChange(divid);
	}
	
}


function setFunctions(gridOptions,divid,rowData){
		//セルクリック時に行を選択状態にする
	gridOptions.onCellClicked = function(param){
		/*クリックされたセルの取得*/
		var cols = param.colDef.field;
		var index = param.node.childIndex;
		//セルがチェックボックスセルだったら
		if(cols == "col0"){
			//状況により行のセレクトをon/offする
			var node = gridOptions.api.getRowNode(index);
			if(node.isSelected()){
				node.setSelected(false);

			}else{
				node.setSelected(true);
			}
		}
	}
	//ag-gridの仕様電話再描画されるためソートするとチェックボックスがデフォルトデザインに戻ってしまうためそれを防ぐ
	gridOptions.onSortChanged = function(param){
		checkboxChange();
	}

	gridOptions.onFilterChanged = function(param){
		//モーダル内の検索インプットへのアクティブクラス付与
		$(".ag-floating-filter-input").each(function(){
			if($(this).val().length == 0){
				$(this).removeClass("active");
			}else{
				$(this).addClass("active");
			}
		});
		checkboxChange();
		
	}
	gridOptions.onSelectionChanged= function(param){
		getAllCnt(rowData,divid,gridOptions);
		
	}
	
}

//スプレッドの矢印の構築・再構築
function spredScrollArrowChange(divid){
	//初期表示しているすべて（TAB4）のみロード時に横スクロールの場所設定を行う
	var parentArea = $("#myGrid" + divid + " div.ag-body-container").height();

	var tbarea = $("#myGrid" + divid + " div.ag-body-viewport").scrollTop() + $("#myGrid" + divid + " div.ag-body-viewport").height();
	if(parentArea - tbarea > 0){
		//スクロールバーがある場合の処理
		$("#table" + divid + " div.scrollArrow").css("right","16px");
	}else{
		//スクロールバーがない場合の処理
		$("#table" + divid + " div.scrollArrow").css("right","0px");

	}

	//初期表示しているすべて（TAB4）のみロード時に横スクロールの表示設定を行う
	var parentArea = $("#myGrid" + divid + " div.ag-body-container").width();

	var tbarea = $("#myGrid" + divid + " div.ag-body-viewport").scrollTop() + $("#myGrid" + divid + " div.ag-body-viewport").width();

	if(parentArea - tbarea > 0){
		//スクロールバーがある場合の処理
		$("#table" + divid + " div.scrollArrow").removeClass("dspnone");
		var height = $("#myGrid" + divid + "").innerHeight() - 111;
		var dspheight = $("#myGrid" + divid + " .ag-body-container").innerHeight() ;

		var gh = 0;
		if(height < dspheight){
			gh = height;
		}else{
			gh = dspheight;
		}

		$("#table" + divid + " div.scrollArrow").css("height",gh + "px");

	}else{
		//スクロールバーがない場合の処理
		$("#table" + divid + " div.scrollArrow").addClass("dspnone");

	}



	/*TAB4のテーブル内の→マウスオーバー挙動*/
	var tm1 = 0;
	var tm2 = 0;
	var scrollMax = 0;
	var dspflg1 = true;
	//スクロールイベント
	$("#table" + divid + " div.scrollArrow").hover(function(){
		tm2 = setTimeout(function(){
			tm1 = setInterval(function(){
				//スクロールがMAXになったら表示を消すためにここで値を取得
				scrollMax = $("#myGrid" + divid + " div.ag-body-container").width() - $("#myGrid" + divid + " div.ag-body-viewport").width();

				$("#myGrid" + divid + " div.ag-body-viewport").scrollLeft( $("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() + 2);

				if(scrollMax <= $("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() && dspflg1){
					$("#table" + divid + " div.scrollArrow").addClass("dspnone");
					dspflg1 = false;
					clearTimeout(tm2);
					clearInterval(tm1);
				}
			},0);
		},300);
	},function(){
		clearTimeout(tm2);
		clearInterval(tm1);
	});

	//scrollArrowが消えているときにscrollMAXと値が変わったら再表示
	$("#myGrid" + divid + " div.ag-body-viewport").scroll(function(){

		scrollMax = $("#myGrid" + divid + " div.ag-body-container").width() - $("#myGrid" + divid + " div.ag-body-viewport").width();

		if(scrollMax <= $("#myGrid" + divid + " div.ag-body-viewport").scrollLeft()){
			$("#table" + divid + " div.scrollArrow").addClass("dspnone");
			dspflg1 = false;
		}

		if(scrollMax > $("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() && !dspflg1){
			$("#table" + divid + " div.scrollArrow").removeClass("dspnone");
			dspflg1 = true;
		}
	});	
}



//pagerの件数取得
function getAllCnt(rowData,divid,gridOptions){
	$("#allcount" + divid).html(rowData.length);
	if($("#selectcount" + divid)[0]){
		$("#selectcount" + divid).html(gridOptions.api.getSelectedRows().length);
	}
}

function setGridData_CheckBox(divid,data){
	var r = getRowData(divid,30);
	if(divid == 1){
		gridOptions1_1.api.setRowData([]);
		gridOptions1_1.api.updateRowData({add: r});
		getAllCnt(r,divid,gridOptions1_1);	
	}else if(divid == 3){
		gridOptions1_3.api.setRowData([]);
		gridOptions1_3.api.updateRowData({add: r});
		getAllCnt(r,divid,gridOptions1_3);
		setPageSep(gridOptions1_3,divid);
	}else if(divid == 4){
		gridOptions1_4.api.setRowData([]);
		gridOptions1_4.api.updateRowData({add: r});
		getAllCnt(r,divid,gridOptions1_4);
		setPageSep(gridOptions1_4,divid)
	}
	
	checkboxChange();

	
}
//modalはラジオボタンにする
function checkboxChange_otherwindow(){
	$(".otherwindow .ag-row .ag-selection-checkbox img:nth-child(1)").attr("src","../../../../assets/images/radio_checked.png");
	$(".otherwindow .ag-row .ag-selection-checkbox img:nth-child(2)").attr("src","../../../../assets/images/radio_body.png");
	$(".otherwindow .ag-row .ag-selection-checkbox img:nth-child(1)").attr("src","../../../../assets/images/radio_checked.png");
	$(".otherwindow .ag-row .ag-selection-checkbox img:nth-child(2)").attr("src","../../../../assets/images/radio_body.png");

}

//上からせりあがるウィンドウを閉じる
//グリッドに追加トランジションをかける
var deleteIndex = -1;
function closeOtherWindow_wa9144(){
	$("div.otherwindow").removeClass("openotherwindow");
	
	setTimeout(function(){
		$("div.otherwindow").addClass("dspnone");		
	}, 300);
	
	if(deleteIndex != -1){

		//登録、修正の場合は下記を実行、グリッドの指定行を青色にし2秒後にトランジションで消す
		
		$("#myGrid1 .ag-body-container .ag-row").each(function(){
			var index = $(this).attr("row");
			if(index == deleteIndex){
				setGrid1(1,deleteIndex);
			}
		});
		
		
		/*$("#myGrid1 .ag-pinned-left-cols-container .ag-row").each(function(){
			var index = $(this).attr("row");
			console.log(index)
			if(index == deleteIndex){
				$(this).addClass("row-update");

				var obj = $(this);
				setTimeout(function(){
					obj.addClass("row-default");
					obj.removeClass("row-update");
				},2000);
			}
		});

		$("#myGrid1 .ag-body-container .ag-row").each(function(){
			var index = $(this).attr("row");
			if(index == deleteIndex){
				$(this).addClass("row-update");
				var obj = $(this);
				setTimeout(function(){
					obj.addClass("row-default");
					obj.removeClass("row-update");
					
					var res = gridOptions1.api.setRowData([]);
					
					setGrid1(1,deleteIndex);
					

				},2000);
			}
		});*/
		
		
		
	}
	
}


//上にせりあがるwindowのdisabled解除
function openOtherWindow_wa9144(index){
	openOtherWindow();
	deleteIndex = index;
	setGridData2("2",30)
}


function changeTab(tnum){
	var cnt = 1;
	$(".otherwindow-search__tab--list").each(function() {
		$("#ot-tab"+ cnt + "-item").addClass("dspnone");
		if($(this)[0].id == "ot-tab" + tnum){
			$("#ot-tab" + cnt).addClass("active");
			$("#ot-tab"+ cnt + "-item").removeClass("dspnone");
			

		}else{
			$("#ot-tab" + cnt).removeClass("active");
		}
		cnt++;
	});

	
}