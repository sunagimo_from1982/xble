$(function(){
	$(".ninushiSelect").addClass("dspnone");

	//各tab内のモーダル生成
	setGridData1("1",30);
	//補充候補リスト
	//setGridData2("5",30);
	$("#otherwindow1 input,#otherwindow1 select").change(function(){
		var idx = 0;
		var cnt = 0;
		$("#otherwindow1 input,#otherwindow1 select").each(function(){

			if(cnt == 0 || cnt == 1 || cnt == 2|| cnt == 3 || cnt == 4){

				if($(this).val().length > 0){
					idx++;
				}
			}
			cnt++;
		});
		if(idx == 5){

			$("#regist1").removeClass("disabled");
		}
	});	
	$("#otherwindow2 input,#otherwindow2 select").change(function(){
		var idx = 0;
		var cnt = 0;
		$("#otherwindow2 input,#otherwindow2 select").each(function(){

			if(cnt == 0 || cnt == 1 || cnt == 2|| cnt == 3 || cnt == 4){

				if($(this).val().length > 0){
					idx++;
				}
			}
			cnt++;
		});
		if(idx == 5){

			$("#regist2").removeClass("disabled");
		}
	});	
	
});


var columnDefs1_1;
var gridOptions1_1;
var rowData1_1 = [];
function setGridData1_1(divid,data){

	// ヘッダー

	//headerName=列の題名,field=列のID,width=幅、cellClas=各セルに適用するクラス
	//headerCheckboxSelection=ヘッダーにチェックボックスを付けるか？
	//checkboxSelection=各行にチェックボックスを表示するか？
	//pinned=列固定,suppressMenu=ヘッダー一列目に検索フィールドを出すか？
	//hide=表示、非表示、sortable=ソートができるかできないか
	//filter=ヘッダー2列目の検索フィールドを何にするか？
	
	/*ヘッダーデータをタイプで入れ替える*/
	
	columnDefs1_1 = getColmnDefs(divid);


	// 行のテストデータ

	rowData1_1 = getRowData(divid,data);


			
	// オプションの設定
	gridOptions1_1 = {
		enableColResize:true,
		enableSorting: true,
		enableFilter: true,
		headerHeight: 30,
		floatingFiltersHeight:30,
		columnDefs: columnDefs1_1,/*headedrデータ*/
		rowData: rowData1_1,/*行データ*/
		floatingFilter: true,/*2列目のフィルターのon/off*/
		suppressRowClickSelection:true,/*チェックボックス以外のクリックを認めるか？*/
		localeText: getLocaleText(),/*データ０の時のメッセージ*/
		rowHeight : 40,
		pagination:true,
		paginationPageSize:20
	};
	


	//   対象となる要素の指定
	var eGridDiv = document.querySelector('#myGrid' + divid);

	//   グリッドの生成
	new agGrid.Grid(eGridDiv, gridOptions1_1);

	setFunctions(gridOptions1_1,divid,rowData1_1);
	
	/*今回の場合はテーブル4*/
	var chkflg4 = false;
	//横スクロールでステータス領域が消えた時に左はじにカラー領域を表示
	$("#myGrid" + divid + " div.ag-body-viewport").scroll(function(e){
		//今回のステータス位置が185付近なので今回はそこに設定
		if($("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() > 135 && !chkflg4 ){
			chkflg4 = true;

			showcol(true);
		}else if($("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() <= 135 && chkflg4){
			chkflg4 = false;
			showcol(false);

		}
		checkboxChange_wa9100();
	});
	//列の表示非表示切り替え
	function showcol(show) {
		gridOptions1_1.columnApi.setColumnVisible('col99', show);
	}
	
	gridOptions1_1.onRowSelected = function(param){
		$(".tableitem__btnarea--rightarea span a").removeClass("disabled");
	}
	var sort = [
		{colId: 'col1', sort: 'asc'}
	];
	gridOptions1_1.api.setSortModel(sort);
	

	getAllCnt(rowData1_1,divid,gridOptions1_1);
	
	
	//headerCheckboxChange();
	checkboxChange_wa9100();
	spredScrollArrowChange(divid);
	

	
}


function setFunctions(gridOptions,divid,rowData){
		//セルクリック時に行を選択状態にする
	gridOptions.onCellClicked = function(param){
		/*クリックされたセルの取得*/
		var cols = param.colDef.field;
		var index = param.node.childIndex;
		//セルがチェックボックスセルだったら
		if(cols == "col0"){
			//状況により行のセレクトをon/offする
			var node = gridOptions.api.getRowNode(index);
			if(node.isSelected()){
				node.setSelected(false);

			}else{
				node.setSelected(true);
			}
		}
	}
	//ag-gridの仕様電話再描画されるためソートするとチェックボックスがデフォルトデザインに戻ってしまうためそれを防ぐ
	gridOptions.onSortChanged = function(param){
		checkboxChange_wa9100();
	}
	gridOptions.onPaginationChanged = function(param){
		checkboxChange_wa9100();
	}
	
	gridOptions.onFilterChanged = function(param){
		//モーダル内の検索インプットへのアクティブクラス付与
		$(".ag-floating-filter-input").each(function(){
			if($(this).val().length == 0){
				$(this).removeClass("active");
			}else{
				$(this).addClass("active");
			}
		});
		checkboxChange_wa9100();
		
	}
	gridOptions.onSelectionChanged= function(param){
		getAllCnt(rowData,divid,gridOptions);
		
	}
	$("#pagerItem" + divid + ".tableitem__tablearea__pagerItem .disabled").removeClass("disabled");
	//各矢印クリックでページ移動
	$("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i").click(function(){
		if($(this).prop("id") == "first" && !$(this).hasClass("disabled")){
			gridOptions.api.paginationGoToFirstPage();
		}else if($(this).prop("id") == "prev" && !$(this).hasClass("disabled")){
			gridOptions.api.paginationGoToPreviousPage();
		}else if($(this).prop("id") == "next" && !$(this).hasClass("disabled")){
			gridOptions.api.paginationGoToNextPage();
		}else if($(this).prop("id") == "last" && !$(this).hasClass("disabled")){
			gridOptions.api.paginationGoToLastPage()
		}
		setPageSep(gridOptions,divid);
	});
	setPageSep(gridOptions,divid);
	var coldef = columnDefs1_1;
	var rd = rowData1_1;
	if(divid == "2"){
		coldef = columnDefs1_2;
		rd = rowData1_2;
	}else if(divid == "3"){
		coldef = columnDefs1_3;
		rd = rowData1_3;
	}else if(divid == "4"){
		coldef = columnDefs1_4;
		rd = rowData1_4;
	}
	
	gridWidth_wa9001(".tableitem",coldef,"#myGrid" + divid, "#table" + divid)
	gridHeight(".tableitem",rd,"#myGrid" + divid, "#table" + divid);	
	
}

function gridWidth_wa9001(owname,coldef,gridname,tablename){
	var ow = $(owname).innerWidth() - 40;
	//Gridの横幅が画面幅以下なら調整する
	var w = 20;
	for(var i = 0 ; i < coldef.length ;i++){
		w += coldef[i].width;
	}

	if(ow > w){
		$(gridname ).css("width","calc(100%)");
		$(tablename).css("width","calc(100% - 60px)");
		
		
	}
	
}
//pagerの表示件数を変更
function updatePager1(){
	var value = $("#select1").val();
    gridOptions1_1.api.paginationSetPageSize(Number(value));
	checkboxChange_wa9100();
	setPageSep(gridOptions1_1,1);
}
function updatePager2(){
	var value = $("#select2").val();
    gridOptions1_2.api.paginationSetPageSize(Number(value));
	checkboxChange_wa9100();
	setPageSep(gridOptions1_2,2);
}
//pagerの真ん中の構築 
function setPageSep(gridOptions,divid){

	var h = "<span class='tableitem__tablearea__pagerItem__nowpage--desc--big'>" + (gridOptions.api.paginationGetCurrentPage() + 1) + "</span> /" + gridOptions.api.paginationGetTotalPages() + " ";
	$("#pagerItem" + divid + " .tableitem__tablearea__pagerItem__nowpage--desc").html(h);
	//1ページ目では前に戻れないようにdisabled
	if(gridOptions.api.paginationGetCurrentPage() + 1 == 1){
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#first").addClass("disabled");
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#prev").addClass("disabled");
	}else{
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#first").removeClass("disabled");
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#prev").removeClass("disabled");
	}
	//LASTページ目では次にいけないようにdisabled
	if(gridOptions.api.paginationGetCurrentPage() + 1 == gridOptions.api.paginationGetTotalPages()){
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#next").addClass("disabled");
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#last").addClass("disabled");
	}else{
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#next").removeClass("disabled");
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#last").removeClass("disabled");
	}
}
//スプレッドの矢印の構築・再構築
function spredScrollArrowChange(divid){
	//初期表示しているすべて（TAB4）のみロード時に横スクロールの場所設定を行う
	var parentArea = $("#myGrid" + divid + " div.ag-body-container").height();

	var tbarea = $("#myGrid" + divid + " div.ag-body-viewport").scrollTop() + $("#myGrid" + divid + " div.ag-body-viewport").height();
	if(parentArea - tbarea > 0){
		//スクロールバーがある場合の処理
		$("#table" + divid + " div.scrollArrow").css("right","16px");
	}else{
		//スクロールバーがない場合の処理
		$("#table" + divid + " div.scrollArrow").css("right","0px");

	}

	//初期表示しているすべて（TAB4）のみロード時に横スクロールの表示設定を行う
	var parentArea = $("#myGrid" + divid + " div.ag-body-container").width();

	var tbarea = $("#myGrid" + divid + " div.ag-body-viewport").scrollTop() + $("#myGrid" + divid + " div.ag-body-viewport").width();

	if(parentArea - tbarea > 0){
		//スクロールバーがある場合の処理
		$("#table" + divid + " div.scrollArrow").removeClass("dspnone");
		var height = $("#myGrid" + divid + "").innerHeight() - 121;
		var dspheight = $("#myGrid" + divid + " .ag-body-container").innerHeight() ;

		var gh = 0;
		if(height < dspheight){
			gh = height;
		}else{
			gh = dspheight;
		}

		$("#table" + divid + " div.scrollArrow").css("height",gh + "px");

	}else{
		//スクロールバーがない場合の処理
		$("#table" + divid + " div.scrollArrow").addClass("dspnone");

	}



	/*TAB4のテーブル内の→マウスオーバー挙動*/
	var tm1 = 0;
	var tm2 = 0;
	var scrollMax = 0;
	var dspflg1 = true;
	//スクロールイベント
	$("#table" + divid + " div.scrollArrow").hover(function(){
		tm2 = setTimeout(function(){
			tm1 = setInterval(function(){
				//スクロールがMAXになったら表示を消すためにここで値を取得
				scrollMax = $("#myGrid" + divid + " div.ag-body-container").width() - $("#myGrid" + divid + " div.ag-body-viewport").width();

				$("#myGrid" + divid + " div.ag-body-viewport").scrollLeft( $("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() + 2);

				if(scrollMax <= $("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() && dspflg1){
					$("#table" + divid + " div.scrollArrow").addClass("dspnone");
					dspflg1 = false;
					clearTimeout(tm2);
					clearInterval(tm1);
				}
			},0);
		},300);
	},function(){
		clearTimeout(tm2);
		clearInterval(tm1);
	});

	//scrollArrowが消えているときにscrollMAXと値が変わったら再表示
	$("#myGrid" + divid + " div.ag-body-viewport").scroll(function(){

		scrollMax = $("#myGrid" + divid + " div.ag-body-container").width() - $("#myGrid" + divid + " div.ag-body-viewport").width();

		if(scrollMax <= $("#myGrid" + divid + " div.ag-body-viewport").scrollLeft()){
			$("#table" + divid + " div.scrollArrow").addClass("dspnone");
			dspflg1 = false;
		}

		if(scrollMax > $("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() && !dspflg1){
			$("#table" + divid + " div.scrollArrow").removeClass("dspnone");
			dspflg1 = true;
		}
	});	
}


//テーブル作成用ファンクション
function setGridData1(divid,data){
	setGridData1_1(divid,data);	
}
//setGridData1系のヘッダーデータ構築
function getColmnDefs(divid){
	return columnDefs1 = [
		{headerName: "", field: "col0", width: 30,cellClass: ['tc','cl_w','noCellFocus'],
		 headerCheckboxSelection: false,checkboxSelection: true,suppressMenu: true,
		 lockPosition:true,pinned: 'left'
		},
		{headerName: "ユーザコード", field: "col1", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
		{headerName: "ユーザ名", field: "col2", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
		{headerName: "論理倉庫", field: "col3", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
		{headerName: "権限", field: "col4", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
		{headerName: "更新日時", field: "col5", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
		{headerName: "登録日時", field: "col6", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:160,cellClass: ['tl','cellclass']}
	];		

}
//setGridData1系のデータ構築
function getRowData(divid,data){
	var rowData = [];
	for(var i = 0; i < data;i++){
		rowData.push({col0: i,col1: "00000" + i, col2: 'テストユーザー名1',col3: '倉庫(接続先00000)',col4: 'システム管理者', col5: '2019/5/15  9:00:00', col6: '2019/5/15  9:00:00'});

	}
	return rowData;
}
//pagerの件数取得
function getAllCnt(rowData,divid,gridOptions){
	$("#allcount" + divid).html(rowData.length);
	if($("#selectcount" + divid)[0]){
		$("#selectcount" + divid).html(gridOptions.api.getSelectedRows().length);
	}
}

//tab切替
function changeTab(tnum){
	var cnt = 1;
	$("section.tabs > ul.tabs-nav > li.tabs-nav__item").each(function() {
		$("#tab"+ cnt + "-item").addClass("dspnone");
		if($(this)[0].id == "t" + tnum){
			$("#t" + cnt).addClass("tabs-nav__item--active");
			$("#tab"+ cnt + "-item").removeClass("dspnone");
			
			dt = 30;
			setGridData1(tnum,dt);
			checkboxChange_wa9100();

		}else{
			$("#t" + cnt).removeClass("tabs-nav__item--active");
		}
		cnt++;
	});

	
}
function openOtherWindow_wa9001(id){
	$("div#otherwindow" + id).removeClass("dspnone");
	$("div#otherwindow" + id).addClass("openotherwindow");

}
var updateid = "0";

function closeOtherWindow_wa9001(divid,type){
	closeOtherWindow();
	var rd = rowData1_1;
	var go = gridOptions1_1;

	if(type == 1){
		updateid = "0";
	}else{
		$("#myGrid"+ divid + " .ag-body-container .ag-row").each(function(){
			var index = $(this).attr("row");
			var node = go.api.getRowNode(index);
			if(node.isSelected()){
				node.setSelected(false);
				updateid = index;
			}
		});
	}
	
	


	$("#myGrid" + divid + " .ag-pinned-left-cols-container .ag-row").each(function(){
		var index = $(this).attr("row");
		console.log(index);
		if(index == updateid){
			$(this).addClass("row-update");

			var obj = $(this);
			setTimeout(function(){
				obj.addClass("row-default");
				obj.removeClass("row-update");
			},2000);
		}
	});
	$("#myGrid" + divid + " .ag-body-container .ag-row").each(function(){
		var index = $(this).attr("row");
		if(index == updateid){
			$(this).addClass("row-update");
			var obj = $(this);
			setTimeout(function(){
				obj.addClass("row-default");
				obj.removeClass("row-update");
			},2000);
		}
	});


	
}


function openModalWindow_wa9001(divid){
	openModalWindow();
}

function closeModalWindow_wa9001(){
	var divid = "1";
	
	
	closeModalWindow();
	
	var rd = rowData1_1;
	var go = gridOptions1_1;
	$("#myGrid"+ divid + " .ag-body-container .ag-row").each(function(){
		var index = $(this).attr("row");
		var node = go.api.getRowNode(index);
		if(node.isSelected()){
			node.setSelected(false);
			setGrid1(divid,index);
		}
	});
	
			
			
}
function setGrid1(divid,del){
	var rd = rowData1_1;
	var go = gridOptions1_1;
	
	
	rd.splice(del, 1);
	
	if(rd.length == 0){
		
		go.api.setRowData([]);
		$(".tableitem .nodata").html("データがありません。");
		$(".notice-desc").html("");
		
		hiddenPagerControl(divid,0);
		gridHeight(".tableitem",rd,"#myGrid" + divid, "#table" + divid,1);	
	}else{
		go.api.updateRowData({add: rd});
		go.api.paginationSetPageSize(rd.length);

		getAllCnt(rd,divid,go);

		gridHeight(".tableitem",rd,"#myGrid" + divid, "#table" + divid);	

		spredScrollArrowChange(divid);
	}

	
}

//modalはラジオボタンにする
function checkboxChange_wa9100(){
	$("#myGrid1 .ag-row .ag-selection-checkbox img:nth-child(1)").attr("src","../../../../assets/images/radio_checked.png");
	$("#myGrid1 .ag-row .ag-selection-checkbox img:nth-child(2)").attr("src","../../../../assets/images/radio_body.png");
	$("#myGrid1 .ag-row .ag-selection-checkbox img:nth-child(1)").attr("src","../../../../assets/images/radio_checked.png");
	$("#myGrid1 .ag-row .ag-selection-checkbox img:nth-child(2)").attr("src","../../../../assets/images/radio_body.png");

}
