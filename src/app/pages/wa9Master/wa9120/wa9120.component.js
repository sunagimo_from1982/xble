$(function(){
	$(".ninushiSelect").addClass("dspnone");
	//各tab内のモーダル生成
	setGridData1("1",30);
	//補充候補リスト
	//setGridData2("5",30);
	//アップロード（input=file）クリックし、ファイルを選択したときにファイル名を表示するスクリプト
	if($("#upload")){
		setInterval(function(){

			if($("#upload").val().length == 0){
				$("#filename").html("ファイルが選択されていません。");
			}else{
				$("#filename").html("<span><img class='deletebtn' src='../../../../assets/images/delete.png'></span>" + $("#upload")[0].files[0].name);
			}
		},1000);
	}
	
	$("#otherwindow1 input,#otherwindow1 select").change(function(){
		var idx = 0;
		var cnt = 0;
		$("#otherwindow1 input,#otherwindow1 select").each(function(){

			if(cnt == 0 || cnt == 1 || cnt == 2 || cnt == 5 || cnt == 7 ||cnt == 8 ){

				if($(this).val().length > 0){
					idx++;
				}
			}
			cnt++;
		});
		if(idx == 6){

			$("#regist1").removeClass("disabled");
		}
	});
	$("#otherwindow2 input,#otherwindow2 select").change(function(){
		var idx = 0;
		var cnt = 0;
		$("#otherwindow2 input,#otherwindow2 select").each(function(){

			if(cnt == 0 || cnt == 1 || cnt == 2 || cnt == 5 || cnt == 7 ||cnt == 8 ){

				if($(this).val().length > 0){
					idx++;
				}
			}
			cnt++;
		});
		if(idx == 6){
			$("#regist2").removeClass("disabled");
		}
	});
	$("#otherwindow3 input,#otherwindow3 select").change(function(){
		var idx = 0;
		var cnt = 0;
		$("#otherwindow3 input,#otherwindow3 select").each(function(){

			if(cnt == 5 || cnt == 6 || cnt == 9 || cnt == 10 || cnt == 11 ||cnt == 12 ){

				if($(this).val().length > 0){
					idx++;
				}
			}
			cnt++;
		});

		if(idx == 6){

			$("#regist3").removeClass("disabled");
		}
	});
	$("#otherwindow4 input,#otherwindow4 select").change(function(){
		var idx = 0;
		var cnt = 0;
		$("#otherwindow4 input,#otherwindow4 select").each(function(){

			if(cnt == 0 || cnt == 1 || cnt == 4 || cnt == 5 || cnt == 6 ||cnt == 7 ){

				if($(this).val().length > 0){
					idx++;
				}
			}
			cnt++;
		});
		if(idx == 6){

			$("#regist4").removeClass("disabled");
		}
	});
	
	$("#otherwindow5 input,#otherwindow5 select").change(function(){
		var idx = 0;
		var cnt = 0;
		$("#otherwindow5 input,#otherwindow5 select").each(function(){

			if(cnt == 0 || cnt == 1){

				if($(this).val().length > 0){
					idx++;
				}
			}
			cnt++;
		});
		if(idx == 2){

			$("#regist5").removeClass("disabled");
		}
	});

});


var columnDefs1_1;
var gridOptions1_1;
var rowData1_1 = [];
function setGridData1_1(divid,data){

	// ヘッダー

	//headerName=列の題名,field=列のID,width=幅、cellClas=各セルに適用するクラス
	//headerCheckboxSelection=ヘッダーにチェックボックスを付けるか？
	//checkboxSelection=各行にチェックボックスを表示するか？
	//pinned=列固定,suppressMenu=ヘッダー一列目に検索フィールドを出すか？
	//hide=表示、非表示、sortable=ソートができるかできないか
	//filter=ヘッダー2列目の検索フィールドを何にするか？
	
	/*ヘッダーデータをタイプで入れ替える*/
	
	columnDefs1_1 = getColmnDefs(divid);


	// 行のテストデータ

	rowData1_1 = getRowData(divid,data);


			
	// オプションの設定
	gridOptions1_1 = {
		enableColResize:true,
		enableSorting: true,
		enableFilter: true,
		headerHeight: 30,
		floatingFiltersHeight:30,
		columnDefs: columnDefs1_1,/*headedrデータ*/
		rowData: rowData1_1,/*行データ*/
		floatingFilter: true,/*2列目のフィルターのon/off*/
		suppressRowClickSelection:true,/*チェックボックス以外のクリックを認めるか？*/
		rowSelection :'multiple',/*複数選択できるように*/
		localeText: getLocaleText(),/*データ０の時のメッセージ*/
		rowHeight : 40,
		pagination:true,
		paginationPageSize:20
	};
	


	//   対象となる要素の指定
	var eGridDiv = document.querySelector('#myGrid' + divid);

	//   グリッドの生成
	new agGrid.Grid(eGridDiv, gridOptions1_1);

	setFunctions(gridOptions1_1,divid,rowData1_1);
	
	/*今回の場合はテーブル4*/
	var chkflg4 = false;
	//横スクロールでステータス領域が消えた時に左はじにカラー領域を表示
	$("#myGrid" + divid + " div.ag-body-viewport").scroll(function(e){
		//今回のステータス位置が185付近なので今回はそこに設定
		if($("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() > 135 && !chkflg4 ){
			chkflg4 = true;

			showcol(true);
		}else if($("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() <= 135 && chkflg4){
			chkflg4 = false;
			showcol(false);

		}
		checkboxChange();
	});
	//列の表示非表示切り替え
	function showcol(show) {
		gridOptions1_1.columnApi.setColumnVisible('col99', show);
	}
	
	gridOptions1_1.onRowSelected = function(param){
		$(".tableitem__btnarea--rightarea span a").removeClass("disabled");
	}
	var sort = [
		{colId: 'col2', sort: 'asc'}
	];
	gridOptions1_1.api.setSortModel(sort);
	

	getAllCnt(rowData1_1,divid,gridOptions1_1);
	
	
	headerCheckboxChange();
	checkboxChange();
	spredScrollArrowChange(divid);
	

	
}

var columnDefs1_2;
var gridOptions1_2;
var rowData1_2 = [];
function setGridData1_2(divid,data){

	// ヘッダー

	//headerName=列の題名,field=列のID,width=幅、cellClas=各セルに適用するクラス
	//headerCheckboxSelection=ヘッダーにチェックボックスを付けるか？
	//checkboxSelection=各行にチェックボックスを表示するか？
	//pinned=列固定,suppressMenu=ヘッダー一列目に検索フィールドを出すか？
	//hide=表示、非表示、sortable=ソートができるかできないか
	//filter=ヘッダー2列目の検索フィールドを何にするか？
	
	/*ヘッダーデータをタイプで入れ替える*/
	
	columnDefs1_2 = getColmnDefs(divid);


	// 行のテストデータ

	rowData1_2 = getRowData(divid,data);


			
	// オプションの設定
	gridOptions1_2 = {
		enableColResize:true,
		enableSorting: true,
		enableFilter: true,
		headerHeight: 30,
		floatingFiltersHeight:30,
		columnDefs: columnDefs1_2,/*headedrデータ*/
		rowData: rowData1_2,/*行データ*/
		floatingFilter: true,/*2列目のフィルターのon/off*/
		suppressRowClickSelection:true,/*チェックボックス以外のクリックを認めるか？*/
		rowSelection :'multiple',/*複数選択できるように*/
		localeText: getLocaleText(),/*データ０の時のメッセージ*/
		rowHeight : 40,
		pagination:true,
		paginationPageSize:20
	};
	


	//   対象となる要素の指定
	var eGridDiv = document.querySelector('#myGrid' + divid);

	//   グリッドの生成
	new agGrid.Grid(eGridDiv, gridOptions1_2);

	setFunctions(gridOptions1_2,divid,rowData1_2);
	
	/*今回の場合はテーブル4*/
	var chkflg4 = false;
	//横スクロールでステータス領域が消えた時に左はじにカラー領域を表示
	$("#myGrid" + divid + " div.ag-body-viewport").scroll(function(e){
		//今回のステータス位置が185付近なので今回はそこに設定
		if($("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() > 135 && !chkflg4 ){
			chkflg4 = true;

			showcol(true);
		}else if($("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() <= 135 && chkflg4){
			chkflg4 = false;
			showcol(false);

		}
		checkboxChange();
	});
	//列の表示非表示切り替え
	function showcol(show) {
		gridOptions1_2.columnApi.setColumnVisible('col99', show);
	}
	
	gridOptions1_2.onRowSelected = function(param){
		$(".tableitem__btnarea--rightarea span a").removeClass("disabled");
	}
	

	getAllCnt(rowData1_2,divid,gridOptions1_2);
	
	
	headerCheckboxChange();
	checkboxChange();
	spredScrollArrowChange(divid);
}


function setFunctions(gridOptions,divid,rowData){
		//セルクリック時に行を選択状態にする
	gridOptions.onCellClicked = function(param){
		/*クリックされたセルの取得*/
		var cols = param.colDef.field;
		var index = param.node.childIndex;
		//セルがチェックボックスセルだったら
		if(cols == "col0"){
			//状況により行のセレクトをon/offする
			var node = gridOptions.api.getRowNode(index);
			if(node.isSelected()){
				node.setSelected(false);

			}else{
				node.setSelected(true);
			}
		}
	}
	//ag-gridの仕様電話再描画されるためソートするとチェックボックスがデフォルトデザインに戻ってしまうためそれを防ぐ
	gridOptions.onSortChanged = function(param){
		checkboxChange();
	}
	gridOptions.onPaginationChanged = function(param){
		checkboxChange();
	}
	
	gridOptions.onFilterChanged = function(param){
		//モーダル内の検索インプットへのアクティブクラス付与
		$(".ag-floating-filter-input").each(function(){
			if($(this).val().length == 0){
				$(this).removeClass("active");
			}else{
				$(this).addClass("active");
			}
		});
		checkboxChange();
		
	}
	gridOptions.onSelectionChanged= function(param){
		getAllCnt(rowData,divid,gridOptions);
		
	}
	$("#pagerItem" + divid + ".tableitem__tablearea__pagerItem .disabled").removeClass("disabled");
	//各矢印クリックでページ移動
	$("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i").click(function(){
		if($(this).prop("id") == "first" && !$(this).hasClass("disabled")){
			gridOptions.api.paginationGoToFirstPage();
		}else if($(this).prop("id") == "prev" && !$(this).hasClass("disabled")){
			gridOptions.api.paginationGoToPreviousPage();
		}else if($(this).prop("id") == "next" && !$(this).hasClass("disabled")){
			gridOptions.api.paginationGoToNextPage();
		}else if($(this).prop("id") == "last" && !$(this).hasClass("disabled")){
			gridOptions.api.paginationGoToLastPage()
		}
		setPageSep(gridOptions,divid);
	});
	setPageSep(gridOptions,divid);
	var coldef = columnDefs1_1;
	var rd = rowData1_1;
	if(divid == "2"){
		coldef = columnDefs1_2;
		rd = rowData1_2;
	}else if(divid == "3"){
		coldef = columnDefs1_3;
		rd = rowData1_3;
	}else if(divid == "4"){
		coldef = columnDefs1_4;
		rd = rowData1_4;
	}
	
	gridWidth(".tableitem",coldef,"#myGrid" + divid, "#table" + divid)
	gridHeight(".tableitem",rd,"#myGrid" + divid, "#table" + divid);	
	
}

//pagerの表示件数を変更
function updatePager1(){
	var value = $("#select1").val();
    gridOptions1_1.api.paginationSetPageSize(Number(value));
	checkboxChange();
	setPageSep(gridOptions1_1,1);
}
function updatePager2(){
	var value = $("#select2").val();
    gridOptions1_2.api.paginationSetPageSize(Number(value));
	checkboxChange();
	setPageSep(gridOptions1_2,2);
}
//pagerの真ん中の構築 
function setPageSep(gridOptions,divid){

	var h = "<span class='tableitem__tablearea__pagerItem__nowpage--desc--big'>" + (gridOptions.api.paginationGetCurrentPage() + 1) + "</span> /" + gridOptions.api.paginationGetTotalPages() + " ";
	$("#pagerItem" + divid + " .tableitem__tablearea__pagerItem__nowpage--desc").html(h);
	//1ページ目では前に戻れないようにdisabled
	if(gridOptions.api.paginationGetCurrentPage() + 1 == 1){
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#first").addClass("disabled");
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#prev").addClass("disabled");
	}else{
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#first").removeClass("disabled");
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#prev").removeClass("disabled");
	}
	//LASTページ目では次にいけないようにdisabled
	if(gridOptions.api.paginationGetCurrentPage() + 1 == gridOptions.api.paginationGetTotalPages()){
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#next").addClass("disabled");
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#last").addClass("disabled");
	}else{
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#next").removeClass("disabled");
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#last").removeClass("disabled");
	}
}
//スプレッドの矢印の構築・再構築
function spredScrollArrowChange(divid){
	//初期表示しているすべて（TAB4）のみロード時に横スクロールの場所設定を行う
	var parentArea = $("#myGrid" + divid + " div.ag-body-container").height();

	var tbarea = $("#myGrid" + divid + " div.ag-body-viewport").scrollTop() + $("#myGrid" + divid + " div.ag-body-viewport").height();
	if(parentArea - tbarea > 0){
		//スクロールバーがある場合の処理
		$("#table" + divid + " div.scrollArrow").css("right","16px");
	}else{
		//スクロールバーがない場合の処理
		$("#table" + divid + " div.scrollArrow").css("right","0px");

	}

	//初期表示しているすべて（TAB4）のみロード時に横スクロールの表示設定を行う
	var parentArea = $("#myGrid" + divid + " div.ag-body-container").width();

	var tbarea = $("#myGrid" + divid + " div.ag-body-viewport").scrollTop() + $("#myGrid" + divid + " div.ag-body-viewport").width();

	if(parentArea - tbarea > 0){
		//スクロールバーがある場合の処理
		$("#table" + divid + " div.scrollArrow").removeClass("dspnone");
		var height = $("#myGrid" + divid + "").innerHeight() - 121;
		var dspheight = $("#myGrid" + divid + " .ag-body-container").innerHeight() ;

		var gh = 0;
		if(height < dspheight){
			gh = height;
		}else{
			gh = dspheight;
		}

		$("#table" + divid + " div.scrollArrow").css("height",gh + "px");

	}else{
		//スクロールバーがない場合の処理
		$("#table" + divid + " div.scrollArrow").addClass("dspnone");

	}



	/*TAB4のテーブル内の→マウスオーバー挙動*/
	var tm1 = 0;
	var tm2 = 0;
	var scrollMax = 0;
	var dspflg1 = true;
	//スクロールイベント
	$("#table" + divid + " div.scrollArrow").hover(function(){
		tm2 = setTimeout(function(){
			tm1 = setInterval(function(){
				//スクロールがMAXになったら表示を消すためにここで値を取得
				scrollMax = $("#myGrid" + divid + " div.ag-body-container").width() - $("#myGrid" + divid + " div.ag-body-viewport").width();

				$("#myGrid" + divid + " div.ag-body-viewport").scrollLeft( $("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() + 2);

				if(scrollMax <= $("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() && dspflg1){
					$("#table" + divid + " div.scrollArrow").addClass("dspnone");
					dspflg1 = false;
					clearTimeout(tm2);
					clearInterval(tm1);
				}
			},0);
		},300);
	},function(){
		clearTimeout(tm2);
		clearInterval(tm1);
	});

	//scrollArrowが消えているときにscrollMAXと値が変わったら再表示
	$("#myGrid" + divid + " div.ag-body-viewport").scroll(function(){

		scrollMax = $("#myGrid" + divid + " div.ag-body-container").width() - $("#myGrid" + divid + " div.ag-body-viewport").width();

		if(scrollMax <= $("#myGrid" + divid + " div.ag-body-viewport").scrollLeft()){
			$("#table" + divid + " div.scrollArrow").addClass("dspnone");
			dspflg1 = false;
		}

		if(scrollMax > $("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() && !dspflg1){
			$("#table" + divid + " div.scrollArrow").removeClass("dspnone");
			dspflg1 = true;
		}
	});	
}


//テーブル作成用ファンクション
function setGridData1(divid,data){
	if(divid == "1" && rowData1_1.length == 0){
		setGridData1_1(divid,data);
	}else if(divid == "2" && rowData1_2.length == 0){
		setGridData1_2(divid,data);
	}
	
}
//setGridData1系のヘッダーデータ構築
function getColmnDefs(divid){
	if(divid == "1"){
		return columnDefs1 = [
			{headerName: "", field: "col0", width: 30,cellClass: ['tc','cl_w','noCellFocus'],
			 headerCheckboxSelection: true,checkboxSelection: true,suppressMenu: true,
			 lockPosition:true,pinned: 'left'
			},
			{headerName: "", field: "col1", filter:'text',unSortIcon: false,lockPosition:true,
			 suppressMenu: true,width:100,cellClass: ['tc','cellclass'],pinned: 'left',
			 cellRenderer: (params) => {
					const element = document.createElement('div');
					element.innerHTML = "<div class='itemcode'><a class='btn_secondary' href='javascript:openOtherWindow_wa9120_update(" + params.value + ");'>修正</a></div>";

					return element;
			}},
			{headerName: "ロケーション", field: "col2", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
			{headerName: "ロケーション名", field: "col3", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "ロケーションバーコード", field: "col4", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "倉庫コード", field: "col5", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "フロアコード", field: "col6", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "エリアコード", field: "col7", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "列コード", field: "col8", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:80,cellClass: ['tl','cellclass']},
			{headerName: "棚コード", field: "col9", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:80,cellClass: ['tl','cellclass']},
			{headerName: "段コード", field: "col10", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:80,cellClass: ['tl','cellclass']},
			{headerName: "番コード", field: "col11", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:80,cellClass: ['tl','cellclass']},
			{headerName: "荷主", field: "col12", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "在庫区分", field: "col13", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "温度帯区分", field: "col14", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "ロケブロック", field: "col15", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "出荷頻度ABC", field: "col16", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "格納什器ケース名", field: "col17", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
			{headerName: "入荷優先順位", field: "col18", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "出荷優先順位", field: "col19", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "入荷不可フラグ", field: "col20", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "出荷不可フラグ", field: "col21", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "商品混載可フラグ", field: "col22", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "ロット混載可フラグ", field: "col23", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "備考", field: "col24", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:260,cellClass: ['tl','cellclass']},
			{headerName: "更新日時", field: "col25", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "更新者名", field: "col26", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "登録日時", field: "col27", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "登録者名", field: "col28", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']}
		];		
	}else if(divid = "2"){
		return columnDefs1 = [
			{headerName: "", field: "col0", width: 30,cellClass: ['tc','cl_w','noCellFocus'],
			 headerCheckboxSelection: true,checkboxSelection: true,suppressMenu: true,
			 lockPosition:true,pinned: 'left'
			},
			{headerName: "", field: "col1", filter:'text',unSortIcon: false,lockPosition:true,
			 suppressMenu: true,width:100,cellClass: ['tc','cellclass'],pinned: 'left',
			 cellRenderer: (params) => {
					const element = document.createElement('div');
					element.innerHTML = "<div class='itemcode'><a class='btn_secondary' href='javascript:openOtherWindow_wa9120_update(" + params.value + ");'>修正</a></div>";

					return element;
			}},
			{headerName: "倉庫コード", field: "col2", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "フロアコード", field: "col3", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "エリアコード", field: "col4", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "列コード", field: "col5", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:80,cellClass: ['tl','cellclass']},
			{headerName: "棚コード", field: "col6", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:80,cellClass: ['tl','cellclass']},
			{headerName: "荷主", field: "col7", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "在庫区分", field: "col8", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "温度帯区分", field: "col9", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "ロケブロック", field: "col0", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "出荷頻度ABC", field: "col11", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "格納什器ケース名", field: "col12", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
			{headerName: "入荷不可フラグ", field: "col13", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "出荷不可フラグ", field: "col14", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "更新日時", field: "col15", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "更新者名", field: "col16", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "登録日時", field: "col17", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "登録者名", field: "col18", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']}
		];		
	}

}
//setGridData1系のデータ構築
function getRowData(divid,data){
	var rowData = [];
	if(divid == "1"){
		for(var i = 0; i < data;i++){
			rowData.push({col0: i,col1: i, col2: '0012223445561',col3: '00-1-222-3-44-55-61',col4: '0012223445561', col5: '00', col6: '1', col7: '222', col8: '3', col9: '44', col10: '55', col11: '61', col12: 'ダミーテキスト荷主A', col13: '良品', col14: '常温', col15: 'バラ(通常)', col16: 'F', col17: '軽量棚S箱', col18: '999999', col19: '999999', col20: '入荷可', col21: '出荷可', col22: '商品混載可', col23: 'ロット混載可', col24: 'ダミーテキストダミ…', col25: '2019-05-15  9:00:00', col26: 'テストユーザA', col27: '2019-05-15  9:00:00', col28: 'テストユーザA'});
			
		}
	}else if(divid == "2"){
		for(var i = 0; i < data;i++){
			rowData.push({col0: i,col1: i, col2: '00',col3: '1',col4: '222', col5: '3', col6: '44', col7: 'ダミーテキスト荷主A', col8: '良品', col9: '常温', col10: 'バラ(通常)', col11: 'F', col12: '軽量棚S箱', col13: '入荷可', col14: '出荷可',col15: '2019-05-15  9:00:00', col16: 'テストユーザA', col17: '2019-05-15  9:00:00', col18: 'テストユーザA'});		}
	}
	return rowData;
}
//pagerの件数取得
function getAllCnt(rowData,divid,gridOptions){
	$("#allcount" + divid).html(rowData.length);
	if($("#selectcount" + divid)[0]){
		$("#selectcount" + divid).html(gridOptions.api.getSelectedRows().length);
	}
}

//tab切替
function changeTab(tnum){
	var cnt = 1;
	$("section.tabs > ul.tabs-nav > li.tabs-nav__item").each(function() {
		$("#tab"+ cnt + "-item").addClass("dspnone");
		if($(this)[0].id == "t" + tnum){
			$("#t" + cnt).addClass("tabs-nav__item--active");
			$("#tab"+ cnt + "-item").removeClass("dspnone");
			
			dt = 30;
			setGridData1(tnum,dt);
			checkboxChange();

		}else{
			$("#t" + cnt).removeClass("tabs-nav__item--active");
		}
		cnt++;
	});

	
}

function openOtherWindow_wa9120(){
	if($("#t1").hasClass("tabs-nav__item--active")){
		$("div#otherwindow1").removeClass("dspnone");
		$("div#otherwindow1").addClass("openotherwindow");
	}else if($("#t2").hasClass("tabs-nav__item--active")){
		$("div#otherwindow3").removeClass("dspnone");
		$("div#otherwindow3").addClass("openotherwindow");
	}
}

var updateid = "0";

function openOtherWindow_wa9120_update(idx){
	updateid = idx;
	if($("#t1").hasClass("tabs-nav__item--active")){
		$("div#otherwindow2").removeClass("dspnone");
		$("div#otherwindow2").addClass("openotherwindow");
	}else if($("#t2").hasClass("tabs-nav__item--active")){
		$("div#otherwindow4").removeClass("dspnone");
		$("div#otherwindow4").addClass("openotherwindow");
	}
}
function openOtherWindow_wa9120_master(){
	$("div#otherwindow5").removeClass("dspnone");
	$("div#otherwindow5").addClass("openotherwindow");
}
function openModalWindow_wa9120(id){
	$("#modal" + id ).removeClass("dspnone");
}
function closeOtherWindow_wa9120_master(){
	closeOtherWindow();
	
	var divid = "1";
	if($("#t1").hasClass("tabs-nav__item--active")){
		divid = "1";
	}else if($("#t2").hasClass("tabs-nav__item--active")){
		divid = "2";
	}
	var rd = rowData1_1;
	var go = gridOptions1_1;
	
	if(divid == "2"){
		rd = rowData1_2;
		go = gridOptions1_2;
	}
	
	for(var i = 0; i < rd.length;i++){
		var node = go.api.getRowNode(i);

		$("#myGrid" + divid + " .ag-pinned-left-cols-container .ag-row").each(function(){
			var index = $(this).attr("row");
			$(this).addClass("row-update");

			var obj = $(this);
			setTimeout(function(){
				obj.addClass("row-default");
				obj.removeClass("row-update");
			},2000);
		});
		$("#myGrid" + divid + " .ag-body-container .ag-row").each(function(){
			var index = $(this).attr("row");
			$(this).addClass("row-update");
			var obj = $(this);
			setTimeout(function(){
				obj.addClass("row-default");
				obj.removeClass("row-update");
			},2000);
		});


	}		
}
function closeOtherWindow_wa9120(divid,type){
	closeOtherWindow();
	if(type == 1){
		updateid = "0";
	}
	
	var rd = rowData1_1;
	var go = gridOptions1_1;
	
	if(divid == "2"){
		rd = rowData1_2;
		go = gridOptions1_2;
	}
	
	for(var i = 0; i < rd.length;i++){
		var node = go.api.getRowNode(i);

		$("#myGrid" + divid + " .ag-pinned-left-cols-container .ag-row").each(function(){
			var index = $(this).attr("row");
			if(index == updateid){
				$(this).addClass("row-update");

				var obj = $(this);
				setTimeout(function(){
					obj.addClass("row-default");
					obj.removeClass("row-update");
				},2000);
			}
		});
		$("#myGrid" + divid + " .ag-body-container .ag-row").each(function(){
			var index = $(this).attr("row");
			if(index == updateid){
				$(this).addClass("row-update");
				var obj = $(this);
				setTimeout(function(){
					obj.addClass("row-default");
					obj.removeClass("row-update");
				},2000);
			}
		});


	}		
	
}

function closeModalWindow_wa9120(){
	var divid = "1";
	if($("#t1").hasClass("tabs-nav__item--active")){
		divid = "1";
	}else if($("#t2").hasClass("tabs-nav__item--active")){
		divid = "2";
	}
	
	
	closeModalWindow();
	
	var rd = rowData1_1;
	var go = gridOptions1_1;
	if(divid == "2"){
		rd = rowData1_2;
		go = gridOptions1_2;
	}
	$("#myGrid"+ divid + " .ag-body-container .ag-row").each(function(){
		var index = $(this).attr("row");
		var node = go.api.getRowNode(index);
		if(node.isSelected()){
			node.setSelected(false);
			setGrid1(divid,index);
		}
	});
	
			
			
}
function closeModalWindow_wa9120_label(){
	
	var divid = "1";
	if($("#t1").hasClass("tabs-nav__item--active")){
		divid = "1";
	}else if($("#t2").hasClass("tabs-nav__item--active")){
		divid = "2";
	}
	
	closeModalWindow();
	
	var rd = rowData1_1;
	var go = gridOptions1_1;
	if(divid == "2"){
		rd = rowData1_2;
		go = gridOptions1_2;
	}
	
	
	$("#myGrid" + divid + " .ag-pinned-left-cols-container .ag-row").each(function(){
		var index = $(this).attr("row");
		var node = go.api.getRowNode(index);
		if(node.isSelected()){
			$(this).addClass("row-update");

			var obj = $(this);
			setTimeout(function(){
				obj.addClass("row-default");
				obj.removeClass("row-update");
			},2000);
		}
	});
	$("#myGrid" + divid + " .ag-body-container .ag-row").each(function(){
		var index = $(this).attr("row");
		var node = go.api.getRowNode(index);
		if(node.isSelected()){
			node.setSelected(false);
			$(this).addClass("row-update");
			var obj = $(this);
			setTimeout(function(){
				obj.addClass("row-default");
				obj.removeClass("row-update");
			},2000);
		}
	});
	
			
			
}

function setGrid1(divid,del){
	var rd = rowData1_1;
	var go = gridOptions1_1;
	
	if(divid == 2){
		rd = rowData1_2;
	}
	
	rd.splice(del, 1);
	
	if(rd.length == 0){
		
		go.api.setRowData([]);
		$(".tableitem .nodata").html("データがありません。");
		$(".notice-desc").html("");
		
		hiddenPagerControl(divid,0);
		gridHeight(".tableitem",rd,"#myGrid" + divid, "#table" + divid,1);	
	}else{
		go.api.updateRowData({add: rd});
		go.api.paginationSetPageSize(rd.length);

		getAllCnt(rd,divid,go);

		gridHeight(".tableitem",rd,"#myGrid" + divid, "#table" + divid);	

		spredScrollArrowChange(divid);
	}

	
}
