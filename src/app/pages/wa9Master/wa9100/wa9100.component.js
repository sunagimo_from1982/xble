var anchors = [0,550,1900];
//座標の取得を試みたがdisplay:none?だからうまく座標が取れない。そのため固定とする

$(function(){
	//各tab内のモーダル生成
	setGridData1("1",30);
	//補充候補リスト
	//setGridData2("5",30);
	var _click = (window.ontouchstart === undefined)? 'click' : 'touchstart';
	$(".otherwindow-status__block1").on(_click, function() {
		id = $(this).attr("id");
		t = "";
		if(id == "block1-1"){
			$("#block1-1").addClass("active");
			$("#block1-2").removeClass("active");
			$("#block1-3").removeClass("active");
			
			t = anchors[0];
		}else if(id == "block1-2"){
			$("#block1-1").removeClass("active");
			$("#block1-2").addClass("active");
			$("#block1-3").removeClass("active");
			t = anchors[1];
		}else if(id == "block1-3"){
			$("#block1-1").removeClass("active");
			$("#block1-2").removeClass("active");
			$("#block1-3").addClass("active");
			t = anchors[2];
		}
		
		
		var speed = 300;
		var href= $(this).attr("href");
		//var target = $("#" + t);
		var position = t;

		$("#otherwindow1 .otherwindow-contentsScroll").animate({scrollTop:position}, speed, "swing");
		return false;
	});	
	
	
	$("#otherwindow1 .otherwindow-contentsScroll").scroll(function(e){
		
		if(anchors[1] > $(this).scrollTop()){
			$("#block1-1").addClass("active");
			$("#block1-2").removeClass("active");
			$("#block1-3").removeClass("active");
		}else if(anchors[1] <= $(this).scrollTop() && anchors[2] > $(this).scrollTop()){
			$("#block1-1").removeClass("active");
			$("#block1-2").addClass("active");
			$("#block1-3").removeClass("active");
		}else{
			$("#block1-1").removeClass("active");
			$("#block1-2").removeClass("active");
			$("#block1-3").addClass("active");
		}
		
	});
	
	$(".otherwindow-status__block2").on(_click, function() {
		id = $(this).attr("id");
		t = "";
		if(id == "block2-1"){
			$("#block2-1").addClass("active");
			$("#block2-2").removeClass("active");
			$("#block2-3").removeClass("active");
			
			t = anchors[0];
		}else if(id == "block2-2"){
			$("#block2-1").removeClass("active");
			$("#block2-2").addClass("active");
			$("#block2-3").removeClass("active");
			t = anchors[1];
		}else if(id == "block2-3"){
			$("#block2-1").removeClass("active");
			$("#block2-2").removeClass("active");
			$("#block2-3").addClass("active");
			t = anchors[2];
		}
		
		
		var speed = 300;
		var href= $(this).attr("href");
		//var target = $("#" + t);
		var position = t;

		$("#otherwindow2 .otherwindow-contentsScroll").animate({scrollTop:position}, speed, "swing");
		return false;
	});		
	

	$("#otherwindow2 .otherwindow-contentsScroll").scroll(function(e){
		
		if(anchors[1] > $(this).scrollTop()){
			$("#block2-1").addClass("active");
			$("#block2-2").removeClass("active");
			$("#block2-3").removeClass("active");
		}else if(anchors[1] <= $(this).scrollTop() && anchors[2] > $(this).scrollTop()){
			$("#block2-1").removeClass("active");
			$("#block2-2").addClass("active");
			$("#block2-3").removeClass("active");
		}else{
			$("#block2-1").removeClass("active");
			$("#block2-2").removeClass("active");
			$("#block2-3").addClass("active");
		}
	});

	$("#otherwindow1 input,#otherwindow1 select").change(function(){
		var idx = 0;
		var cnt = 0;
		$("#otherwindow1 input,#otherwindow1 select").each(function(){

			/*if(cnt == 0 || cnt == 1 || cnt == 2 || cnt == 3 || cnt == 4 ||cnt == 5 ||cnt == 6 ||cnt == 7 ||cnt == 30 ||cnt == 32 ||cnt == 33 ||cnt == 35 ||cnt == 37 ||cnt == 38 ||cnt == 39 ||cnt == 41||cnt == 42 ||cnt == 43 ){*/
			if(cnt == 0 || cnt == 1 || cnt == 2 || cnt == 3){

				if($(this).val().length > 0){
					idx++;
				}
			}
			cnt++;
		});
		if(idx == 4){

			$("#regist1").removeClass("disabled");
		}
	});
	
	$("#otherwindow2 input,#otherwindow2 select").change(function(){
		var idx = 0;
		var cnt = 0;
		$("#otherwindow2 input,#otherwindow2 select").each(function(){

			/*if(cnt == 0 || cnt == 1 || cnt == 2 || cnt == 3 || cnt == 4 ||cnt == 5 ||cnt == 6 ||cnt == 7 ||cnt == 30 ||cnt == 32 ||cnt == 33 ||cnt == 35 ||cnt == 37 ||cnt == 38 ||cnt == 39 ||cnt == 41||cnt == 42 ||cnt == 43 ){*/
			if(cnt == 0 || cnt == 1 || cnt == 2 || cnt == 3){


				if($(this).val().length > 0){
					idx++;
				}
			}
			cnt++;
		});
		if(idx == 4){

			$("#regist2").removeClass("disabled");
		}
	});
	$("#otherwindow3 input,#otherwindow3 select").change(function(){
		var idx = 0;
		var cnt = 0;
		$("#otherwindow3 input,#otherwindow3 select").each(function(){

			/*if(cnt == 0 || cnt == 1 || cnt == 2 || cnt == 3 || cnt == 4 ||cnt == 5 ||cnt == 6 ||cnt == 7 ||cnt == 30 ||cnt == 32 ||cnt == 33 ||cnt == 35 ||cnt == 37 ||cnt == 38 ||cnt == 39 ||cnt == 41||cnt == 42 ||cnt == 43 ){*/
			if(cnt == 1){


				if($(this).val().length > 0){
					idx++;
				}
			}
			cnt++;
		});
		if(idx == 1){

			$("#regist3").removeClass("disabled");
		}
	});
});

var columnDefs1_1;
var gridOptions1_1;
var rowData1_1 = [];
function setGridData1_1(divid,data){

	// ヘッダー

	//headerName=列の題名,field=列のID,width=幅、cellClas=各セルに適用するクラス
	//headerCheckboxSelection=ヘッダーにチェックボックスを付けるか？
	//checkboxSelection=各行にチェックボックスを表示するか？
	//pinned=列固定,suppressMenu=ヘッダー一列目に検索フィールドを出すか？
	//hide=表示、非表示、sortable=ソートができるかできないか
	//filter=ヘッダー2列目の検索フィールドを何にするか？
	
	/*ヘッダーデータをタイプで入れ替える*/
	
	columnDefs1_1 = getColmnDefs(divid);


	// 行のテストデータ

	rowData1_1 = getRowData(divid,data);


			
	// オプションの設定
	gridOptions1_1 = {
		enableColResize:true,
		enableSorting: true,
		enableFilter: true,
		headerHeight: 30,
		floatingFiltersHeight:30,
		columnDefs: columnDefs1_1,/*headedrデータ*/
		rowData: rowData1_1,/*行データ*/
		floatingFilter: true,/*2列目のフィルターのon/off*/
		suppressRowClickSelection:true,/*チェックボックス以外のクリックを認めるか？*/
		localeText: getLocaleText(),/*データ０の時のメッセージ*/
		rowHeight : 40,
		pagination:true,
		paginationPageSize:20
	};
	


	//   対象となる要素の指定
	var eGridDiv = document.querySelector('#myGrid' + divid);

	//   グリッドの生成
	new agGrid.Grid(eGridDiv, gridOptions1_1);

	setFunctions(gridOptions1_1,divid,rowData1_1);
	
	/*今回の場合はテーブル4*/
	var chkflg4 = false;
	//横スクロールでステータス領域が消えた時に左はじにカラー領域を表示
	$("#myGrid" + divid + " div.ag-body-viewport").scroll(function(e){
		//今回のステータス位置が185付近なので今回はそこに設定
		if($("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() > 135 && !chkflg4 ){
			chkflg4 = true;

			showcol(true);
		}else if($("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() <= 135 && chkflg4){
			chkflg4 = false;
			showcol(false);

		}
		checkboxChange_wa9100();
	});
	//列の表示非表示切り替え
	function showcol(show) {
		gridOptions1_1.columnApi.setColumnVisible('col99', show);
	}
	
	gridOptions1_1.onRowSelected = function(param){
		$(".tableitem__btnarea--rightarea span a").removeClass("disabled");
	}
	var sort = [
		{colId: 'col1', sort: 'asc'}
	];
	gridOptions1_1.api.setSortModel(sort);
	

	getAllCnt(rowData1_1,divid,gridOptions1_1);
	
	
	//headerCheckboxChange();
	checkboxChange_wa9100();
	spredScrollArrowChange(divid);
	

	
}


function setFunctions(gridOptions,divid,rowData){
		//セルクリック時に行を選択状態にする
	gridOptions.onCellClicked = function(param){
		/*クリックされたセルの取得*/
		var cols = param.colDef.field;
		var index = param.node.childIndex;
		//セルがチェックボックスセルだったら
		if(cols == "col0"){
			//状況により行のセレクトをon/offする
			var node = gridOptions.api.getRowNode(index);
			if(node.isSelected()){
				node.setSelected(false);

			}else{
				node.setSelected(true);
			}
		}
	}
	//ag-gridの仕様電話再描画されるためソートするとチェックボックスがデフォルトデザインに戻ってしまうためそれを防ぐ
	gridOptions.onSortChanged = function(param){
		checkboxChange_wa9100();
	}
	gridOptions.onPaginationChanged = function(param){
		checkboxChange_wa9100();
	}
	
	gridOptions.onFilterChanged = function(param){
		//モーダル内の検索インプットへのアクティブクラス付与
		$(".ag-floating-filter-input").each(function(){
			if($(this).val().length == 0){
				$(this).removeClass("active");
			}else{
				$(this).addClass("active");
			}
		});
		checkboxChange_wa9100();
		
	}
	gridOptions.onSelectionChanged= function(param){
		getAllCnt(rowData,divid,gridOptions);
		
	}
	$("#pagerItem" + divid + ".tableitem__tablearea__pagerItem .disabled").removeClass("disabled");
	//各矢印クリックでページ移動
	$("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i").click(function(){
		if($(this).prop("id") == "first" && !$(this).hasClass("disabled")){
			gridOptions.api.paginationGoToFirstPage();
		}else if($(this).prop("id") == "prev" && !$(this).hasClass("disabled")){
			gridOptions.api.paginationGoToPreviousPage();
		}else if($(this).prop("id") == "next" && !$(this).hasClass("disabled")){
			gridOptions.api.paginationGoToNextPage();
		}else if($(this).prop("id") == "last" && !$(this).hasClass("disabled")){
			gridOptions.api.paginationGoToLastPage()
		}
		setPageSep(gridOptions,divid);
	});
	setPageSep(gridOptions,divid);
	var coldef = columnDefs1_1;
	var rd = rowData1_1;
	if(divid == "2"){
		coldef = columnDefs1_2;
		rd = rowData1_2;
	}else if(divid == "3"){
		coldef = columnDefs1_3;
		rd = rowData1_3;
	}else if(divid == "4"){
		coldef = columnDefs1_4;
		rd = rowData1_4;
	}
	
	gridWidth(".tableitem",coldef,"#myGrid" + divid, "#table" + divid)
	gridHeight(".tableitem",rd,"#myGrid" + divid, "#table" + divid);	
	
}

//pagerの表示件数を変更
function updatePager1(){
	var value = $("#select1").val();
    gridOptions1_1.api.paginationSetPageSize(Number(value));
	checkboxChange_wa9100();
	setPageSep(gridOptions1_1,1);
}
function updatePager2(){
	var value = $("#select2").val();
    gridOptions1_2.api.paginationSetPageSize(Number(value));
	checkboxChange_wa9100();
	setPageSep(gridOptions1_2,2);
}
//pagerの真ん中の構築 
function setPageSep(gridOptions,divid){

	var h = "<span class='tableitem__tablearea__pagerItem__nowpage--desc--big'>" + (gridOptions.api.paginationGetCurrentPage() + 1) + "</span> /" + gridOptions.api.paginationGetTotalPages() + " ";
	$("#pagerItem" + divid + " .tableitem__tablearea__pagerItem__nowpage--desc").html(h);
	//1ページ目では前に戻れないようにdisabled
	if(gridOptions.api.paginationGetCurrentPage() + 1 == 1){
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#first").addClass("disabled");
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#prev").addClass("disabled");
	}else{
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#first").removeClass("disabled");
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#prev").removeClass("disabled");
	}
	//LASTページ目では次にいけないようにdisabled
	if(gridOptions.api.paginationGetCurrentPage() + 1 == gridOptions.api.paginationGetTotalPages()){
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#next").addClass("disabled");
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#last").addClass("disabled");
	}else{
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#next").removeClass("disabled");
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#last").removeClass("disabled");
	}
}
//スプレッドの矢印の構築・再構築
function spredScrollArrowChange(divid){
	//初期表示しているすべて（TAB4）のみロード時に横スクロールの場所設定を行う
	var parentArea = $("#myGrid" + divid + " div.ag-body-container").height();

	var tbarea = $("#myGrid" + divid + " div.ag-body-viewport").scrollTop() + $("#myGrid" + divid + " div.ag-body-viewport").height();
	if(parentArea - tbarea > 0){
		//スクロールバーがある場合の処理
		$("#table" + divid + " div.scrollArrow").css("right","16px");
	}else{
		//スクロールバーがない場合の処理
		$("#table" + divid + " div.scrollArrow").css("right","0px");

	}

	//初期表示しているすべて（TAB4）のみロード時に横スクロールの表示設定を行う
	var parentArea = $("#myGrid" + divid + " div.ag-body-container").width();

	var tbarea = $("#myGrid" + divid + " div.ag-body-viewport").scrollTop() + $("#myGrid" + divid + " div.ag-body-viewport").width();

	if(parentArea - tbarea > 0){
		//スクロールバーがある場合の処理
		$("#table" + divid + " div.scrollArrow").removeClass("dspnone");
		var height = $("#myGrid" + divid + "").innerHeight() - 121;
		var dspheight = $("#myGrid" + divid + " .ag-body-container").innerHeight() ;

		var gh = 0;
		if(height < dspheight){
			gh = height;
		}else{
			gh = dspheight;
		}

		$("#table" + divid + " div.scrollArrow").css("height",gh + "px");

	}else{
		//スクロールバーがない場合の処理
		$("#table" + divid + " div.scrollArrow").addClass("dspnone");

	}



	/*TAB4のテーブル内の→マウスオーバー挙動*/
	var tm1 = 0;
	var tm2 = 0;
	var scrollMax = 0;
	var dspflg1 = true;
	//スクロールイベント
	$("#table" + divid + " div.scrollArrow").hover(function(){
		tm2 = setTimeout(function(){
			tm1 = setInterval(function(){
				//スクロールがMAXになったら表示を消すためにここで値を取得
				scrollMax = $("#myGrid" + divid + " div.ag-body-container").width() - $("#myGrid" + divid + " div.ag-body-viewport").width();

				$("#myGrid" + divid + " div.ag-body-viewport").scrollLeft( $("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() + 2);

				if(scrollMax <= $("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() && dspflg1){
					$("#table" + divid + " div.scrollArrow").addClass("dspnone");
					dspflg1 = false;
					clearTimeout(tm2);
					clearInterval(tm1);
				}
			},0);
		},300);
	},function(){
		clearTimeout(tm2);
		clearInterval(tm1);
	});

	//scrollArrowが消えているときにscrollMAXと値が変わったら再表示
	$("#myGrid" + divid + " div.ag-body-viewport").scroll(function(){

		scrollMax = $("#myGrid" + divid + " div.ag-body-container").width() - $("#myGrid" + divid + " div.ag-body-viewport").width();

		if(scrollMax <= $("#myGrid" + divid + " div.ag-body-viewport").scrollLeft()){
			$("#table" + divid + " div.scrollArrow").addClass("dspnone");
			dspflg1 = false;
		}

		if(scrollMax > $("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() && !dspflg1){
			$("#table" + divid + " div.scrollArrow").removeClass("dspnone");
			dspflg1 = true;
		}
	});	
}


//テーブル作成用ファンクション
function setGridData1(divid,data){
	setGridData1_1(divid,data);	
}
//setGridData1系のヘッダーデータ構築
function getColmnDefs(divid){
	return columnDefs1 = [
		{headerName: "", field: "col0", width: 30,cellClass: ['tc','cl_w','noCellFocus'],
		 headerCheckboxSelection: false,checkboxSelection: true,suppressMenu: true,
		 lockPosition:true,pinned: 'left'
		},
		{headerName: "商品コード", field: "col1", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
		{headerName: "商品名", field: "col2", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:200,cellClass: ['tl','cellclass']},
		{headerName: "ロット管理", field: "col3", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "期限管理", field: "col4", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "温度帯区分", field: "col5", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "入荷時・使用期限アラート日数", field: "col6", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:210,cellClass: ['tr','cellclass']},
		{headerName: "出荷時・使用期限アラート日数", field: "col7", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:210,cellClass: ['tr','cellclass']},
		{headerName: '単位名', field: "col8", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:80,cellClass: ['tl','cellclass']},
		{headerName: "商品ABC", field: "col9", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:80,cellClass: ['tl','cellclass']},
		{headerName: "標準ケース", field: "col10", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "標準ケース入数", field: "col11", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:120,cellClass: ['tr','cellclass']},
		{headerName: "ピックヤード・バラ・入庫上限数", field: "col12", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:210,cellClass: ['tr','cellclass']},
		{headerName: "インナーケース入数", field: "col13", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:140,cellClass: ['tr','cellclass']},
		{headerName: "インナーケース数", field: "col14", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:140,cellClass: ['tr','cellclass']},
		{headerName: "ケース入数", field: "col15", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tr','cellclass']},
		{headerName: "パレット巻数", field: "col16", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tr','cellclass']},
		{headerName: "パレット段数", field: "col17", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tr','cellclass']},
		{headerName: "バーコード", field: "col18", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
		{headerName: "ケースバーコード", field: "col19", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
		{headerName: "縦(cm)", field: "col20", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:80,cellClass: ['tr','cellclass']},
		{headerName: "横(cm)", field: "col21", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:80,cellClass: ['tr','cellclass']},
		{headerName: "高(cm)", field: "col22", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:80,cellClass: ['tr','cellclass']},
		{headerName: "体積(m3)", field: "col23", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:80,cellClass: ['tr','cellclass']},
		{headerName: "重量(kg)", field: "col24", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:120,cellClass: ['tr','cellclass']},
		{headerName: "インターケースサイズ・縦(cm)", field: "col25", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:210,cellClass: ['tr','cellclass']},
		{headerName: "インターケースサイズ・横(cm)", field: "col26", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:210,cellClass: ['tr','cellclass']},
		{headerName: "インターケースサイズ・高さ(cm)", field: "col27", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:210,cellClass: ['tr','cellclass']},
		{headerName: "ケースサイズ・縦(cm)", field: "col28", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:160,cellClass: ['tr','cellclass']},
		{headerName: "ケースサイズ・横(cm)", field: "col29", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:160,cellClass: ['tr','cellclass']},
		{headerName: "ケースサイズ・高さ(cm)", field: "col30", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:160,cellClass: ['tr','cellclass']},
		{headerName: "単価区分", field: "col31", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "保管料単価(円)", field: "col32", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:120,cellClass: ['tr','cellclass']},
		{headerName: "入庫単価(円)", field: "col33", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:120,cellClass: ['tr','cellclass']},
		{headerName: "出庫単価(円)", field: "col34", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:120,cellClass: ['tr','cellclass']},
		{headerName: "寄託金額(円)", field: "col35", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:120,cellClass: ['tr','cellclass']},
		{headerName: "補充点", field: "col36", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tr','cellclass']},
		{headerName: "上限値", field: "col37", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tr','cellclass']},
		{headerName: "バリエーション名1", field: "col38", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
		{headerName: "バリエーション名2", field: "col39", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
		{headerName: "バリエーション名3", field: "col40", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
		{headerName: "表示用金額", field: "col41", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tr','cellclass']},
		{headerName: "荷主コード", field: "col42", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
		{headerName: "荷主名", field: "col43", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
		{headerName: "更新日時", field: "col44", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
		{headerName: "更新者名", field: "col45", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
		{headerName: "登録日時", field: "col46", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
		{headerName: "登録者名", field: "col47", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:160,cellClass: ['tl','cellclass']}
	];		

}
//setGridData1系のデータ構築
function getRowData(divid,data){
	var rowData = [];
	for(var i = 0; i < data;i++){
		rowData.push({col0: i,col1: "0-000-012", col2: 'ﾊｯﾄｽﾄﾗｯﾌﾟ　ﾎﾞｰﾀﾞｰ　RD/NV',col3: 'あり',col4: 'あり', col5: '常温', col6: '1', col7: '1', col8: 'ｶｰﾄﾝ', col9: 'A', col10: 'メーカ箱', col11: '50', col12: '99,999', col13: '10', col14: '1', col15: '5', col16: '9', col17: '5', col18: 'test20190515-001', col19: 'btest20190515-001', col20: '5', col21: '5', col22: '5', col23: '125', col24: '1', col25: '0', col26: '50', col27: '50', col28: '5', col29: '5', col30: '5', col31: '数量', col32: '24', col33: '20', col34: '20', col35: '1,000', col36: '0', col37: '9999', col38: '赤色', col39: '青色', col40: '黄色', col41: '0', col42: '0000111', col43: 'ダミーテキスト荷主A', col44: '2019-05-15  9:00:00', col45: 'テストユーザA', col46: '2019-05-15  9:00:00', col47: 'テストユーザA'});

	}
	return rowData;
}
//pagerの件数取得
function getAllCnt(rowData,divid,gridOptions){
	$("#allcount" + divid).html(rowData.length);
	if($("#selectcount" + divid)[0]){
		$("#selectcount" + divid).html(gridOptions.api.getSelectedRows().length);
	}
}

//tab切替
function changeTab(tnum){
	var cnt = 1;
	$("section.tabs > ul.tabs-nav > li.tabs-nav__item").each(function() {
		$("#tab"+ cnt + "-item").addClass("dspnone");
		if($(this)[0].id == "t" + tnum){
			$("#t" + cnt).addClass("tabs-nav__item--active");
			$("#tab"+ cnt + "-item").removeClass("dspnone");
			
			dt = 30;
			setGridData1(tnum,dt);
			checkboxChange_wa9100();

		}else{
			$("#t" + cnt).removeClass("tabs-nav__item--active");
		}
		cnt++;
	});

	
}




function openOtherWindow_wa9100(id){
	$("div#otherwindow" + id).removeClass("dspnone");
	$("div#otherwindow" + id).addClass("openotherwindow");
	$(".ninushiSelect").addClass("dspnone");



}
function openOtherWindow_wa9100_master(){
	$("div#otherwindow3").removeClass("dspnone");
	$("div#otherwindow3").addClass("openotherwindow");
	$(".ninushiSelect").addClass("dspnone");
	
}


function closeOtherWindow_wa9100_master(){
	$(".ninushiSelect").removeClass("dspnone");
	closeOtherWindow();
	
	var divid = "1";

	var rd = rowData1_1;
	var go = gridOptions1_1;
	
	
	for(var i = 0; i < rd.length;i++){
		var node = go.api.getRowNode(i);

		$("#myGrid" + divid + " .ag-pinned-left-cols-container .ag-row").each(function(){
			var index = $(this).attr("row");
			$(this).addClass("row-update");

			var obj = $(this);
			setTimeout(function(){
				obj.addClass("row-default");
				obj.removeClass("row-update");
			},2000);
		});
		$("#myGrid" + divid + " .ag-body-container .ag-row").each(function(){
			var index = $(this).attr("row");
			$(this).addClass("row-update");
			var obj = $(this);
			setTimeout(function(){
				obj.addClass("row-default");
				obj.removeClass("row-update");
			},2000);
		});


	}		
}
var updateid = "0";
function closeOtherWindow(){
	$(".ninushiSelect").removeClass("dspnone");
	$("div.otherwindow").removeClass("openotherwindow");
	//display:noneでアニメーションやtransitionを使うと機能しないため
	//○○秒後にdisplay:noneを発動させる
	setTimeout(function(){
		$("div.otherwindow").addClass("dspnone");		
	}, 300);
}
function closeOtherWindow_wa9100(divid,type){
	$(".ninushiSelect").removeClass("dspnone");
	closeOtherWindow();
	var rd = rowData1_1;
	var go = gridOptions1_1;
	if(type == 1){
		updateid = "0";
	}else{
		$("#myGrid"+ divid + " .ag-body-container .ag-row").each(function(){
			var index = $(this).attr("row");
			var node = go.api.getRowNode(index);
			if(node.isSelected()){
				node.setSelected(false);
				updateid = index;
			}
		});
	}
	
	
	for(var i = 0; i < rd.length;i++){

		$("#myGrid" + divid + " .ag-pinned-left-cols-container .ag-row").each(function(){
			var index = $(this).attr("row");
			if(index == updateid){
				$(this).addClass("row-update");

				var obj = $(this);
				setTimeout(function(){
					obj.addClass("row-default");
					obj.removeClass("row-update");
				},2000);
			}
		});
		$("#myGrid" + divid + " .ag-body-container .ag-row").each(function(){
			var index = $(this).attr("row");
			if(index == updateid){
				$(this).addClass("row-update");
				var obj = $(this);
				setTimeout(function(){
					obj.addClass("row-default");
					obj.removeClass("row-update");
				},2000);
			}
		});


	}		
	
}


//modalはラジオボタンにする
function checkboxChange_wa9100(){
	$("#myGrid1 .ag-row .ag-selection-checkbox img:nth-child(1)").attr("src","../../../../assets/images/radio_checked.png");
	$("#myGrid1 .ag-row .ag-selection-checkbox img:nth-child(2)").attr("src","../../../../assets/images/radio_body.png");
	$("#myGrid1 .ag-row .ag-selection-checkbox img:nth-child(1)").attr("src","../../../../assets/images/radio_checked.png");
	$("#myGrid1 .ag-row .ag-selection-checkbox img:nth-child(2)").attr("src","../../../../assets/images/radio_body.png");

}
