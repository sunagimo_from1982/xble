$(function(){

	//各tab内のモーダル生成
	setGridData1("1",30);


});


var columnDefs1_1;
var gridOptions1_1;
var rowData1_1 = [];
function setGridData1_1(divid,data){

	// ヘッダー

	//headerName=列の題名,field=列のID,width=幅、cellClas=各セルに適用するクラス
	//headerCheckboxSelection=ヘッダーにチェックボックスを付けるか？
	//checkboxSelection=各行にチェックボックスを表示するか？
	//pinned=列固定,suppressMenu=ヘッダー一列目に検索フィールドを出すか？
	//hide=表示、非表示、sortable=ソートができるかできないか
	//filter=ヘッダー2列目の検索フィールドを何にするか？
	
	/*ヘッダーデータをタイプで入れ替える*/
	
	columnDefs1_1 = getColmnDefs(divid);


	// 行のテストデータ

	rowData1_1 = getRowData(divid,data);


			
	// オプションの設定
	gridOptions1_1 = {
		enableColResize:true,
		enableSorting: true,
		enableFilter: true,
		headerHeight: 30,
		floatingFiltersHeight:30,
		columnDefs: columnDefs1_1,/*headedrデータ*/
		rowData: rowData1_1,/*行データ*/
		floatingFilter: true,/*2列目のフィルターのon/off*/
		suppressRowClickSelection:true,/*チェックボックス以外のクリックを認めるか？*/
		rowSelection :'multiple',/*複数選択できるように*/
		localeText: getLocaleText(),/*データ０の時のメッセージ*/
		rowHeight : 40,
		pagination:true,
		paginationPageSize:rowData1_1.length
	};
	


	//   対象となる要素の指定
	var eGridDiv = document.querySelector('#myGrid' + divid);

	//   グリッドの生成
	new agGrid.Grid(eGridDiv, gridOptions1_1);

	setFunctions(gridOptions1_1,divid,rowData1_1);
	
	/*今回の場合はテーブル4*/
	var chkflg4 = false;
	//横スクロールでステータス領域が消えた時に左はじにカラー領域を表示
	$("#myGrid" + divid + " div.ag-body-viewport").scroll(function(e){
		//今回のステータス位置が185付近なので今回はそこに設定
		if($("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() > 155 && !chkflg4 ){
			chkflg4 = true;

			showcol(true);
		}else if($("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() <= 155 && chkflg4){
			chkflg4 = false;
			showcol(false);

		}
		checkboxChange();
	});
	//列の表示非表示切り替え
	function showcol(show) {
		gridOptions1_1.columnApi.setColumnVisible('col99', show);
	}
	
	gridOptions1_1.onRowSelected = function(param){

		getAllCnt(rowData1_1,divid,gridOptions1_1);
	}
	

	getAllCnt(rowData1_1,divid,gridOptions1_1);
	
	
	headerCheckboxChange();
	checkboxChange();
	spredScrollArrowChange(divid);
}

var columnDefs1_2;
var gridOptions1_2;
var rowData1_2 = [];
function setGridData1_2(divid,data){

	// ヘッダー

	//headerName=列の題名,field=列のID,width=幅、cellClas=各セルに適用するクラス
	//headerCheckboxSelection=ヘッダーにチェックボックスを付けるか？
	//checkboxSelection=各行にチェックボックスを表示するか？
	//pinned=列固定,suppressMenu=ヘッダー一列目に検索フィールドを出すか？
	//hide=表示、非表示、sortable=ソートができるかできないか
	//filter=ヘッダー2列目の検索フィールドを何にするか？
	
	/*ヘッダーデータをタイプで入れ替える*/
	
	columnDefs1_2 = getColmnDefs(divid);


	// 行のテストデータ

	rowData1_2 = getRowData(divid,data);


			
	// オプションの設定
	gridOptions1_2 = {
		enableColResize:true,
		enableSorting: true,
		enableFilter: true,
		headerHeight: 30,
		floatingFiltersHeight:30,
		columnDefs: columnDefs1_2,/*headedrデータ*/
		rowData: rowData1_2,/*行データ*/
		floatingFilter: true,/*2列目のフィルターのon/off*/
		suppressRowClickSelection:true,/*チェックボックス以外のクリックを認めるか？*/
		rowSelection :'multiple',/*複数選択できるように*/
		localeText: getLocaleText(),/*データ０の時のメッセージ*/
		rowHeight : 40,
		pagination:true,
		paginationPageSize:rowData1_2.length
	};
	


	//   対象となる要素の指定
	var eGridDiv = document.querySelector('#myGrid' + divid);

	//   グリッドの生成
	new agGrid.Grid(eGridDiv, gridOptions1_2);

	setFunctions(gridOptions1_2,divid,rowData1_2);
	
	/*今回の場合はテーブル4*/
	var chkflg4 = false;
	//横スクロールでステータス領域が消えた時に左はじにカラー領域を表示
	$("#myGrid" + divid + " div.ag-body-viewport").scroll(function(e){
		//今回のステータス位置が185付近なので今回はそこに設定
		if($("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() > 155 && !chkflg4 ){
			chkflg4 = true;

			showcol(true);
		}else if($("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() <= 155 && chkflg4){
			chkflg4 = false;
			showcol(false);

		}
		checkboxChange();
	});
	//列の表示非表示切り替え
	function showcol(show) {
		gridOptions1_2.columnApi.setColumnVisible('col99', show);
	}
	
	gridOptions1_2.onRowSelected = function(param){
		getAllCnt(rowData1_2,divid,gridOptions1_2);
	}
	

	getAllCnt(rowData1_2,divid,gridOptions1_2);
	
	
	headerCheckboxChange();
	checkboxChange();
	spredScrollArrowChange(divid);
}


var columnDefs1_3;
var gridOptions1_3;
var rowData1_3 = [];
function setGridData1_3(divid,data){

	// ヘッダー

	//headerName=列の題名,field=列のID,width=幅、cellClas=各セルに適用するクラス
	//headerCheckboxSelection=ヘッダーにチェックボックスを付けるか？
	//checkboxSelection=各行にチェックボックスを表示するか？
	//pinned=列固定,suppressMenu=ヘッダー一列目に検索フィールドを出すか？
	//hide=表示、非表示、sortable=ソートができるかできないか
	//filter=ヘッダー2列目の検索フィールドを何にするか？
	
	/*ヘッダーデータをタイプで入れ替える*/
	
	columnDefs1_3 = getColmnDefs(divid);


	// 行のテストデータ

	rowData1_3 = getRowData(divid,data);


			
	// オプションの設定
	gridOptions1_3 = {
		enableColResize:true,
		enableSorting: true,
		enableFilter: true,
		headerHeight: 30,
		floatingFiltersHeight:30,
		columnDefs: columnDefs1_3,/*headedrデータ*/
		rowData: rowData1_3,/*行データ*/
		floatingFilter: true,/*2列目のフィルターのon/off*/
		suppressRowClickSelection:true,/*チェックボックス以外のクリックを認めるか？*/
		rowSelection :'multiple',/*複数選択できるように*/
		localeText: getLocaleText(),/*データ０の時のメッセージ*/
		rowHeight : 40,
		pagination:true,
		paginationPageSize:20
	};
	


	//   対象となる要素の指定
	var eGridDiv = document.querySelector('#myGrid' + divid);

	//   グリッドの生成
	new agGrid.Grid(eGridDiv, gridOptions1_3);

	setFunctions(gridOptions1_3,divid,rowData1_3);
	
	/*今回の場合はテーブル4*/
	var chkflg4 = false;
	//横スクロールでステータス領域が消えた時に左はじにカラー領域を表示
	$("#myGrid" + divid + " div.ag-body-viewport").scroll(function(e){
		//今回のステータス位置が185付近なので今回はそこに設定
		if($("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() > 155 && !chkflg4 ){
			chkflg4 = true;

			showcol(true);
		}else if($("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() <= 155 && chkflg4){
			chkflg4 = false;
			showcol(false);

		}
		checkboxChange();
	});
	//列の表示非表示切り替え
	function showcol(show) {
		gridOptions1_3.columnApi.setColumnVisible('col99', show);
	}
	
	

	getAllCnt(rowData1_3,divid,gridOptions1_3);
	
	
	headerCheckboxChange();
	checkboxChange();
	spredScrollArrowChange(divid);
}


var columnDefs1_4;
var gridOptions1_4;
var rowData1_4 = [];
function setGridData1_4(divid,data){

	// ヘッダー

	//headerName=列の題名,field=列のID,width=幅、cellClas=各セルに適用するクラス
	//headerCheckboxSelection=ヘッダーにチェックボックスを付けるか？
	//checkboxSelection=各行にチェックボックスを表示するか？
	//pinned=列固定,suppressMenu=ヘッダー一列目に検索フィールドを出すか？
	//hide=表示、非表示、sortable=ソートができるかできないか
	//filter=ヘッダー2列目の検索フィールドを何にするか？
	
	/*ヘッダーデータをタイプで入れ替える*/
	
	columnDefs1_4 = getColmnDefs(divid);


	// 行のテストデータ

	rowData1_4 = getRowData(divid,data);


			
	// オプションの設定
	gridOptions1_4 = {
		enableColResize:true,
		enableSorting: true,
		enableFilter: true,
		headerHeight: 30,
		floatingFiltersHeight:30,
		columnDefs: columnDefs1_4,/*headedrデータ*/
		rowData: rowData1_4,/*行データ*/
		floatingFilter: true,/*2列目のフィルターのon/off*/
		suppressRowClickSelection:true,/*チェックボックス以外のクリックを認めるか？*/
		rowSelection :'multiple',/*複数選択できるように*/
		localeText: getLocaleText(),/*データ０の時のメッセージ*/
		rowHeight : 40,
		pagination:true,
		paginationPageSize:20
	};
	


	//   対象となる要素の指定
	var eGridDiv = document.querySelector('#myGrid' + divid);

	//   グリッドの生成
	new agGrid.Grid(eGridDiv, gridOptions1_4);

	setFunctions(gridOptions1_4,divid,rowData1_4);
	
	/*今回の場合はテーブル4*/
	var chkflg4 = false;
	//横スクロールでステータス領域が消えた時に左はじにカラー領域を表示
	$("#myGrid" + divid + " div.ag-body-viewport").scroll(function(e){
		//今回のステータス位置が185付近なので今回はそこに設定
		if($("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() > 135 && !chkflg4 ){
			chkflg4 = true;

			showcol(true);
		}else if($("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() <= 135 && chkflg4){
			chkflg4 = false;
			showcol(false);

		}
		checkboxChange();
	});
	//列の表示非表示切り替え
	function showcol(show) {
		gridOptions1_4.columnApi.setColumnVisible('col99', show);
	}
	
	

	getAllCnt(rowData1_4,divid,gridOptions1_4);
	
	
	headerCheckboxChange();
	checkboxChange();
	spredScrollArrowChange(divid);
}

function setFunctions(gridOptions,divid,rowData){
		//セルクリック時に行を選択状態にする
	gridOptions.onCellClicked = function(param){
		/*クリックされたセルの取得*/
		var cols = param.colDef.field;
		var index = param.node.childIndex;
		//セルがチェックボックスセルだったら
		if(cols == "col0"){
			//状況により行のセレクトをon/offする
			var node = gridOptions.api.getRowNode(index);
			if(node.isSelected()){
				node.setSelected(false);

			}else{
				node.setSelected(true);
			}
		}
	}
	//ag-gridの仕様電話再描画されるためソートするとチェックボックスがデフォルトデザインに戻ってしまうためそれを防ぐ
	gridOptions.onSortChanged = function(param){
		checkboxChange();
	}

	gridOptions.onFilterChanged = function(param){
		//モーダル内の検索インプットへのアクティブクラス付与
		$(".ag-floating-filter-input").each(function(){
			if($(this).val().length == 0){
				$(this).removeClass("active");
			}else{
				$(this).addClass("active");
			}
		});
		checkboxChange();
		
	}
	gridOptions.onSelectionChanged= function(param){

		if(divid == "1"){
			if(gridOptions.api.getSelectedRows().length == 0){
				$("#tab" + divid + "-item .tableitem__btnarea a.btn_secondary").addClass("disabled");
			}else{
				$("#tab" + divid + "-item .tableitem__btnarea a.btn_secondary").removeClass("disabled");
			}

		}else{
			if(gridOptions.api.getSelectedRows().length == 0){
				$("#tab" + divid + "-item .tableitem__btnarea a.btn_secondary").addClass("disabled");
				$("#tab" + divid + "-item .tableitem__btnarea a.btn_primary").addClass("disabled");
			}else{

				$("#tab" + divid + "-item .tableitem__btnarea a.btn_secondary").addClass("disabled");
				$("#tab" + divid + "-item .tableitem__btnarea a.btn_primary").addClass("disabled");


				for(var i = 0; i < gridOptions.api.getSelectedRows().length;i++){
					 $("#btn2_1").removeClass("disabled");

					if(gridOptions.api.getSelectedRows()[i].col101 == 0){
						$("#btn2_2").removeClass("disabled");
					}
					if(gridOptions.api.getSelectedRows()[i].col102 == 0){
						$("#btn2_3").removeClass("disabled");
					}
					if(gridOptions.api.getSelectedRows()[i].col103 == 0){
						$("#btn2_4").removeClass("disabled");
					}

					if(gridOptions.api.getSelectedRows()[i].col101 == 1 &&
					  gridOptions.api.getSelectedRows()[i].col102 == 1 &&
					  gridOptions.api.getSelectedRows()[i].col103 == 1){
						$("#btn2_5").removeClass("disabled");
					}
				}


			}
		}
		
	}
	
	
	if(rowData.length > 0 && (divid == 3 || divid == 4)){
		$("#pagerItem" + divid + ".tableitem__tablearea__pagerItem .disabled").removeClass("disabled");
		//各矢印クリックでページ移動
		$("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i").click(function(){
			if($(this).prop("id") == "first" + divid && !$(this).hasClass("disabled")){
				gridOptions.api.paginationGoToFirstPage();
			}else if($(this).prop("id") == "prev" + divid && !$(this).hasClass("disabled")){
				gridOptions.api.paginationGoToPreviousPage();
			}else if($(this).prop("id") == "next" + divid && !$(this).hasClass("disabled")){
				gridOptions.api.paginationGoToNextPage();
			}else if($(this).prop("id") == "last" + divid && !$(this).hasClass("disabled")){
				gridOptions.api.paginationGoToLastPage()
			}
			setPageSep(gridOptions,divid);
		});
		setPageSep(gridOptions,divid);
	}
	
	var sort = [
		{colId: 'col2', sort: 'desc'}
	];
	gridOptions.api.setSortModel(sort);
	
	
	var ow = $(".tableitem").innerWidth() - 40;
	//Gridの横幅が画面幅以下なら調整する
	var w = 20;
	
	var coldef = columnDefs1_1;
	var rd = rowData1_1;
	if(divid == "2"){
		coldef = columnDefs1_2;
		rd = rowData1_2;
	}else if(divid == "3"){
		coldef = columnDefs1_3;
		rd = rowData1_3;
	}else if(divid == "4"){
		coldef = columnDefs1_4;
		rd = rowData1_4;
	}
	
	gridWidth(".tableitem",coldef,"#myGrid" + divid, "#table" + divid)
	
	gridHeight(".tableitem",rd,"#myGrid" + divid, "#table" + divid);			
	
}
//pagerの表示件数を変更
function updatePager1(){
	var value = $("#select5").val();
    gridOptions1_3.api.paginationSetPageSize(Number(value));
	checkboxChange();
	setPageSep(gridOptions1_3,3);
}
function updatePager2(){
	var value = $("#select6").val();
    gridOptions1_4.api.paginationSetPageSize(Number(value));
	checkboxChange();
	setPageSep(gridOptions1_4,4);
}
function updatePager3(){
	var value = $("#select7").val();
    gridOptions2.api.paginationSetPageSize(Number(value));
	setPageSep(gridOptions2,5);
	checkboxChange_modal();
}
//pagerの真ん中の構築 
function setPageSep(gridOptions,divid){

	var h = "<span class='tableitem__tablearea__pagerItem__nowpage--desc--big'>" + (gridOptions.api.paginationGetCurrentPage() + 1) + "</span> /" + gridOptions.api.paginationGetTotalPages() + " ";
	$("#pagerItem" + divid + " .tableitem__tablearea__pagerItem__nowpage--desc").html(h);
	//1ページ目では前に戻れないようにdisabled
	if(gridOptions.api.paginationGetCurrentPage() + 1 == 1){
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#first" + divid).addClass("disabled");
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#prev" + divid).addClass("disabled");
	}else{
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#first" + divid).removeClass("disabled");
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#prev" + divid).removeClass("disabled");
	}

	//LASTページ目では次にいけないようにdisabled
	if(gridOptions.api.paginationGetCurrentPage() + 1 == gridOptions.api.paginationGetTotalPages()){
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#next" + divid).addClass("disabled");
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#last" + divid).addClass("disabled");
	}else{
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#next" + divid).removeClass("disabled");
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#last" + divid).removeClass("disabled");
	}
	
}

//スプレッドの矢印の構築・再構築
function spredScrollArrowChange(divid){
	//初期表示しているすべて（TAB4）のみロード時に横スクロールの場所設定を行う
	var parentArea = $("#myGrid" + divid + " div.ag-body-container").height();

	var tbarea = $("#myGrid" + divid + " div.ag-body-viewport").scrollTop() + $("#myGrid" + divid + " div.ag-body-viewport").height();
	if(parentArea - tbarea > 0){
		//スクロールバーがある場合の処理
		$("#table" + divid + " div.scrollArrow").css("right","16px");
	}else{
		//スクロールバーがない場合の処理
		$("#table" + divid + " div.scrollArrow").css("right","0px");

	}

	//初期表示しているすべて（TAB4）のみロード時に横スクロールの表示設定を行う
	var parentArea = $("#myGrid" + divid + " div.ag-body-container").width();

	var tbarea = $("#myGrid" + divid + " div.ag-body-viewport").scrollTop() + $("#myGrid" + divid + " div.ag-body-viewport").width();

	if(parentArea - tbarea > 0){
		//スクロールバーがある場合の処理
		$("#table" + divid + " div.scrollArrow").removeClass("dspnone");
		var height = $("#myGrid" + divid + "").innerHeight() - 111;
		var dspheight = $("#myGrid" + divid + " .ag-body-container").innerHeight() ;

		var gh = 0;
		if(height < dspheight){
			gh = height;
		}else{
			gh = dspheight;
		}

		$("#table" + divid + " div.scrollArrow").css("height",gh + "px");

	}else{
		//スクロールバーがない場合の処理
		$("#table" + divid + " div.scrollArrow").addClass("dspnone");

	}



	/*TAB4のテーブル内の→マウスオーバー挙動*/
	var tm1 = 0;
	var tm2 = 0;
	var scrollMax = 0;
	var dspflg1 = true;
	//スクロールイベント
	$("#table" + divid + " div.scrollArrow").hover(function(){
		tm2 = setTimeout(function(){
			tm1 = setInterval(function(){
				//スクロールがMAXになったら表示を消すためにここで値を取得
				scrollMax = $("#myGrid" + divid + " div.ag-body-container").width() - $("#myGrid" + divid + " div.ag-body-viewport").width();

				$("#myGrid" + divid + " div.ag-body-viewport").scrollLeft( $("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() + 2);

				if(scrollMax <= $("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() && dspflg1){
					$("#table" + divid + " div.scrollArrow").addClass("dspnone");
					dspflg1 = false;
					clearTimeout(tm2);
					clearInterval(tm1);
				}
			},0);
		},300);
	},function(){
		clearTimeout(tm2);
		clearInterval(tm1);
	});

	//scrollArrowが消えているときにscrollMAXと値が変わったら再表示
	$("#myGrid" + divid + " div.ag-body-viewport").scroll(function(){

		scrollMax = $("#myGrid" + divid + " div.ag-body-container").width() - $("#myGrid" + divid + " div.ag-body-viewport").width();

		if(scrollMax <= $("#myGrid" + divid + " div.ag-body-viewport").scrollLeft()){
			$("#table" + divid + " div.scrollArrow").addClass("dspnone");
			dspflg1 = false;
		}

		if(scrollMax > $("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() && !dspflg1){
			$("#table" + divid + " div.scrollArrow").removeClass("dspnone");
			dspflg1 = true;
		}
	});	
}


//テーブル作成用ファンクション
function setGridData1(divid,data){
	if(divid == "1" && rowData1_1.length == 0){
		setGridData1_1(divid,data);
	}else if(divid == "2" && rowData1_2.length == 0){
		setGridData1_2(divid,data);
	}else if(divid == "3" && rowData1_3.length == 0){
		setGridData1_3(divid,data);
	}else if(divid == "4" && rowData1_4.length == 0){
		setGridData1_4(divid,data);
	}
	
}


//setGridData1系のヘッダーデータ構築
function getColmnDefs(divid){
	if(divid == "1"){
		return columnDefs1 = [
			{headerName: "", field: "col0", width: 30,cellClass: ['tc','cl_w','noCellFocus'],
			 headerCheckboxSelection: true,checkboxSelection: true,suppressMenu: true,
			 lockPosition:true,pinned: 'left'
			},
			{headerName: "", field: "col1", filter:'text',unSortIcon: false,lockPosition:true,
			 suppressMenu: true,width:180,cellClass: ['tl','cellclass'],pinned: 'left',
			 cellRenderer: (params) => {
					const element = document.createElement('div');
					element.innerHTML = "<div class='itemcode'><a class='btn_secondary' href='javascript:openModalWindow_wa8005(1);'>明細書</a><a class='btn_secondary' href='javascript:openModalWindow_wa8005(2);'>請求書</a></div>";

					return element;
			}},
			{headerName: "締日", field: "col2", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "計上年月", field: "col3", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:80,cellClass: ['tl','cellclass']},
			{headerName: "荷主コード", field: "col4", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "荷主名", field: "col5", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
			{headerName: "小計", field: "col6", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:80,cellClass: ['tr','cellclass']},
			{headerName: "課税金額", field: "col7", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tr','cellclass']},
			{headerName: "消費税", field: "col8", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:80,cellClass: ['tr','cellclass']},
			{headerName: "請求金額", field: "col9", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tr','cellclass']},
			{headerName: "請求ステータス", field: "col10", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "集計日時", field: "col11", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "確定日時", field: "col12", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "確定者名", field: "col13", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "保管荷役料", field: "col14", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tr','cellclass']},
			{headerName: "保管荷役料ステータス", field: "col15", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "保管荷役料承認日時", field: "col16", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "諸掛費", field: "col17", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:80,cellClass: ['tr','cellclass']},
			{headerName: "諸掛ステータス", field: "col18", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "諸掛承認日時", field: "col19", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "諸掛承認者名", field: "col20", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "運送費", field: "col21", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:80,cellClass: ['tr','cellclass']},
			{headerName: "運送費ステータス", field: "col22", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
			{headerName: "運送費承認日時", field: "col23", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "運送費承認者名", field: "col24", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "更新日時", field: "col25", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "更新者名", field: "col26", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "登録日時", field: "col27", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "登録者名", field: "col28", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']}
		];
	}else if(divid == "2"){
		return columnDefs1 = [
			{headerName: "", field: "col0", width: 30,cellClass: ['tc','cl_w','noCellFocus'],
			 headerCheckboxSelection: true,checkboxSelection: true,suppressMenu: true,
			 lockPosition:true,pinned: 'left'
			},
			{headerName: "", field: "col1", filter:'text',unSortIcon: false,lockPosition:true,
			 suppressMenu: true,width:180,cellClass: ['tc','cellclass'],pinned: 'left',
			 cellRenderer: (params) => {
					const element = document.createElement('div');
					element.innerHTML = "<div class='itemcode'><a class='btn_secondary' href='javascript:openModalWindow_wa8005(1);'>明細書</a><a class='btn_secondary' href='javascript:openModalWindow_wa8005(2);'>請求書</a></div>";

					return element;
			}},
			{headerName: "保", field: "col101", filter:'text',unSortIcon: false,lockPosition:true,
			 suppressMenu: true,width:40,cellClass: ['tc','cellclass'],pinned: 'left',sortable:false,
			 cellRenderer: (params) => {
				 
				 var img = "";
				 if(params.value == "1"){
					 img = "<img src='../../../../assets/images/check.png'>";
				 }
				const element = document.createElement('div');
				element.innerHTML = "<div class='chkGrid'>" + img + "</div>";

				return element;
			}},
			{headerName: "諸", field: "col102", filter:'text',unSortIcon: false,lockPosition:true,
			 suppressMenu: true,width:40,cellClass: ['tc','cellclass'],pinned: 'left',sortable:false,
			 cellRenderer: (params) => {
				 
				 var img = "";
				 if(params.value == "1"){
					 img = "<img src='../../../../assets/images/check.png'>";
				 }
				const element = document.createElement('div');
				element.innerHTML = "<div class='chkGrid'>" + img + "</div>";

				return element;
			}},
			{headerName: "運", field: "col103", filter:'text',unSortIcon: false,lockPosition:true,
			 suppressMenu: true,width:40,cellClass: ['tc','cellclass'],pinned: 'left',sortable:false,
			 cellRenderer: (params) => {
				 
				 var img = "";
				 if(params.value == "1"){
					 img = "<img src='../../../../assets/images/check.png'>";
				 }
				const element = document.createElement('div');
				element.innerHTML = "<div class='chkGrid'>" + img + "</div>";

				return element;
			}},
			{headerName: "締日", field: "col2", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "計上年月", field: "col3", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:80,cellClass: ['tl','cellclass']},
			{headerName: "荷主コード", field: "col4", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "荷主名", field: "col5", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
			{headerName: "小計", field: "col6", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:80,cellClass: ['tr','cellclass']},
			{headerName: "課税金額", field: "col7", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tr','cellclass']},
			{headerName: "消費税", field: "col8", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:80,cellClass: ['tr','cellclass']},
			{headerName: "請求金額", field: "col9", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tr','cellclass']},
			{headerName: "請求ステータス", field: "col10", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "集計日時", field: "col11", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "確定日時", field: "col12", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "確定者名", field: "col13", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "保管荷役料", field: "col14", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tr','cellclass']},
			{headerName: "保管荷役料ステータス", field: "col15", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "保管荷役料承認日時", field: "col16", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "諸掛費", field: "col17", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:80,cellClass: ['tr','cellclass']},
			{headerName: "諸掛ステータス", field: "col18", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "諸掛承認日時", field: "col19", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "諸掛承認者名", field: "col20", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "運送費", field: "col21", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:80,cellClass: ['tr','cellclass']},
			{headerName: "運送費ステータス", field: "col22", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
			{headerName: "運送費承認日時", field: "col23", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "運送費承認者名", field: "col24", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "更新日時", field: "col25", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "更新者名", field: "col26", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "登録日時", field: "col27", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "登録者名", field: "col28", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']}
		];		
	}else{
		return columnDefs1 = [
			{headerName: "", field: "col1", filter:'text',unSortIcon: false,lockPosition:true,
			 suppressMenu: true,width:180,cellClass: ['tl','cellclass'],pinned: 'left',
			 cellRenderer: (params) => {
					const element = document.createElement('div');
					element.innerHTML = "<div class='itemcode'><a class='btn_secondary' href='javascript:openModalWindow_wa8005(1);'>明細書</a><a class='btn_secondary' href='javascript:openModalWindow_wa8005(2);'>請求書</a></div>";

					return element;
			}},
			{headerName: "締日", field: "col2", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "計上年月", field: "col3", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:80,cellClass: ['tl','cellclass']},
			{headerName: "荷主コード", field: "col4", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "荷主名", field: "col5", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
			{headerName: "小計", field: "col6", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:80,cellClass: ['tr','cellclass']},
			{headerName: "課税金額", field: "col7", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tr','cellclass']},
			{headerName: "消費税", field: "col8", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:80,cellClass: ['tr','cellclass']},
			{headerName: "請求金額", field: "col9", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tr','cellclass']},
			{headerName: "請求ステータス", field: "col10", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "集計日時", field: "col11", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "確定日時", field: "col12", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "確定者名", field: "col13", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "保管荷役料", field: "col14", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tr','cellclass']},
			{headerName: "保管荷役料ステータス", field: "col15", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "保管荷役料承認日時", field: "col16", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "諸掛費", field: "col17", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:80,cellClass: ['tr','cellclass']},
			{headerName: "諸掛ステータス", field: "col18", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "諸掛承認日時", field: "col19", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "諸掛承認者名", field: "col20", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "運送費", field: "col21", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:80,cellClass: ['tr','cellclass']},
			{headerName: "運送費ステータス", field: "col22", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
			{headerName: "運送費承認日時", field: "col23", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "運送費承認者名", field: "col24", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "更新日時", field: "col25", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "更新者名", field: "col26", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "登録日時", field: "col27", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "登録者名", field: "col28", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']}
		];		
	}
}
//setGridData1系のデータ構築
function getRowData(divid,data){
	var rowData = [];
	
	
	if(divid == "1"){
		for(var i = 0; i < data;i++){
			rowData.push({col0: i,col1: '', col2: '2019-04-30', col3: '201904', col4: '000011', col5: '荷主・SEAOS (B)', col6: '5,200', col7: '5,200', col8: '416', col9: '5,616', col10: '未承認', col11: '2019-10-21 15:47:51', col12: '2019-10-21 15:47:51', col13: 'sasaba', col14: '0', col15: '未承認', col16: '2019-10-21 15:47:51', col17: '0', col18: '未承認', col19: '2019-10-21 15:47:51', col20: 'sasaba', col21: '0', col22: '未承認', col23: '2019-10-21 15:47:51', col24: 'sasaba', col25: '2019-10-21 15:47:51', col26: 'sasaba', col27: '2019-10-21 15:47:51', col28: 'sasaba'});			
			
		}
	}else if(divid == "2"){
		for(var i = 0; i < data;i++){
			
			var item1 = "0";
			var item2 = "0";
			var item3 = "0";
			
			if((data - i) % 2 == 0){
				item1 = "1";
			}
			
			if((data - i) % 3 == 0){
				item2 = "1";
			}
			
			if((data - i) % 4 == 0){
				item3 = "1";
			}
			
			
			
			rowData.push({col0: i,col101: item1,col102: item2,col103: item3,col1: '',col100: '1', col2: '2019-04-30', col3: '201904', col4: '000011', col5: '荷主・SEAOS (B)', col6: '5,200', col7: '5,200', col8: '416', col9: '5,616', col10: '未承認', col11: '2019-10-21 15:47:51', col12: '2019-10-21 15:47:51', col13: 'sasaba', col14: '0', col15: '未承認', col16: '2019-10-21 15:47:51', col17: '0', col18: '未承認', col19: '2019-10-21 15:47:51', col20: 'sasaba', col21: '0', col22: '未承認', col23: '2019-10-21 15:47:51', col24: 'sasaba', col25: '2019-10-21 15:47:51', col26: 'sasaba', col27: '2019-10-21 15:47:51', col28: 'sasaba'});			
			
		}

	}else{
		for(var i = 0; i < data;i++){
			rowData.push({col0: i,col1: '',col100: '1', col2: '2019-04-30', col3: '201904', col4: '000011', col5: '荷主・SEAOS (B)', col6: '5,200', col7: '5,200', col8: '416', col9: '5,616', col10: '未承認', col11: '2019-10-21 15:47:51', col12: '2019-10-21 15:47:51', col13: 'sasaba', col14: '0', col15: '未承認', col16: '2019-10-21 15:47:51', col17: '0', col18: '未承認', col19: '2019-10-21 15:47:51', col20: 'sasaba', col21: '0', col22: '未承認', col23: '2019-10-21 15:47:51', col24: 'sasaba', col25: '2019-10-21 15:47:51', col26: 'sasaba', col27: '2019-10-21 15:47:51', col28: 'sasaba'});			
			
		}

	}
	return rowData;
}
//pagerの件数取得
function getAllCnt(rowData,divid,gridOptions){
	$("#allcount" + divid).html(rowData.length);
	if($("#selectcount" + divid)[0]){
		$("#selectcount" + divid).html(gridOptions.api.getSelectedRows().length);
	}
}

function setGrid(divid,del){
	var	gridOptions = gridOptions1_1;
	var	rowData = rowData1_1;
	
	if(divid == "1"){
		gridOptions = gridOptions1_1;
		rowData = rowData1_1;
	}else if(divid == "2"){
		gridOptions = gridOptions1_2;
		rowData = rowData1_2;
	}

	
	
	
	for(var i = rowData.length; i >= 0;i--){
		for(var j = del.length; j >= 0;j--){
			if(i == del[j]){
				rowData.splice(del, 1);
			}
		}
	}

	
	if(rowData.length == 0){
		
		gridOptions.api.setRowData([]);
		$("#tab" + divid + "-item .nodata").html("表示するデータがありません。");
		
		hiddenPagerControl(divid,0);
		gridHeight("#table" + divid,rowData,"#myGrid" + divid, "#table" + divid,1);	
	}else{
		gridOptions.api.updateRowData({add: rowData});
		gridOptions.api.paginationSetPageSize(rowData.length);

		getAllCnt(rowData,divid,gridOptions);

		gridHeight("#table" + divid,rowData,"#myGrid" + divid, "#table" + divid);	

		spredScrollArrowChange(divid);
	}

	checkboxChange();
	
}
//保管荷役料承認
function showMoveMessage(divid){
	var	gridOptions = gridOptions1_1;
	
	if(divid == "1"){
		gridOptions = gridOptions1_1;
	}else if(divid == "2"){
		gridOptions = gridOptions1_2;
	}
	
	
	if(divid == "1"){
		gridOptions = gridOptions1_1;
		var cnt = gridOptions.api.getSelectedRows().length;

		if(cnt > 0){
			var indexes = [];
			$("#myGrid1 .ag-body-container .ag-row").each(function(){
				var index = $(this).attr("row");
				var node = gridOptions.api.getRowNode(index);
				if(node.isSelected()){
					node.setSelected(false);
					indexes.push(index);
				}

			});
		}
		setGrid(divid,indexes);
	}else if(divid == "2"){
		gridOptions = gridOptions1_2;
		var cnt = gridOptions.api.getSelectedRows().length;

		if(cnt > 0){
			selectOffAction(divid,gridOptions)	
		
			
		}
		
	

	}	
	showMessageBox("保管荷役料を承認しました。");
}
//請求金額再作成
function showRequestMessage(divid){
	
	if(divid == "1"){
		gridOptions = gridOptions1_1;
	}else if(divid == "2"){
		gridOptions = gridOptions1_2;
	}
	
	var cnt = gridOptions.api.getSelectedRows().length;
	
	if(cnt > 0){
		var indexes = [];
		selectOffAction(divid,gridOptions)	

		showMessageBox("請求データから請求金額を再作成しました。");
	}

}
//諸掛承認
function showMorokakeMessage(divid){
	
	if(divid == "1"){
		gridOptions = gridOptions1_1;
		var cnt = gridOptions.api.getSelectedRows().length;

		if(cnt > 0){
			var indexes = [];
			$("#myGrid1 .ag-body-container .ag-row").each(function(){
				var index = $(this).attr("row");
				var node = gridOptions.api.getRowNode(index);
				if(node.isSelected()){
					node.setSelected(false);
					indexes.push(index);
				}

			});
		}
		setGrid(divid,indexes);
	}else if(divid == "2"){
		gridOptions = gridOptions1_2;
		var cnt = gridOptions.api.getSelectedRows().length;

		if(cnt > 0){
			selectOffAction(divid,gridOptions)	
		
			
		}
		
	

	}
	showMessageBox("諸掛を承認しました。");
}
//諸掛承認
function showUnsouhiMessage(divid){
	
	if(divid == "1"){
		gridOptions = gridOptions1_1;
		var cnt = gridOptions.api.getSelectedRows().length;

		if(cnt > 0){
			var indexes = [];
			$("#myGrid1 .ag-body-container .ag-row").each(function(){
				var index = $(this).attr("row");
				var node = gridOptions.api.getRowNode(index);
				if(node.isSelected()){
					node.setSelected(false);
					indexes.push(index);
				}

			});
		}
		setGrid(divid,indexes);
	}else if(divid == "2"){
		gridOptions = gridOptions1_2;
		var cnt = gridOptions.api.getSelectedRows().length;

		if(cnt > 0){
			selectOffAction(divid,gridOptions)	
		
			
		}
		
	

	}
	showMessageBox("運送費を承認しました。");
}


function selectOffAction(divid,gridOptions){
	
	$("#myGrid" + divid + " .ag-pinned-left-cols-container .ag-row").each(function(){
		var index = $(this).attr("row");
		var node = gridOptions.api.getRowNode(index);
		if(node.isSelected()){
			$(this).addClass("row-update");

			var obj = $(this);
			setTimeout(function(){
				obj.addClass("row-default");
				obj.removeClass("row-update");
			},2000);
		}

	});

	$("#myGrid" + divid + " .ag-body-container .ag-row").each(function(){
		var index = $(this).attr("row");
		var node = gridOptions.api.getRowNode(index);
		if(node.isSelected()){
			node.setSelected(false);
			$(this).addClass("row-update");
			var obj = $(this);
			setTimeout(function(){
				obj.addClass("row-default");
				obj.removeClass("row-update");
			},2000);
		}
	});			
}
//請求金額確定
function showDecidedMessage(divid){
	var	gridOptions = gridOptions1_1;
	
	if(divid == "1"){
		gridOptions = gridOptions1_1;
	}else if(divid == "2"){
		gridOptions = gridOptions1_2;
	}
	var cnt = gridOptions.api.getSelectedRows().length;
	
	if(cnt > 0){
		var indexes = [];
		$("#myGrid" + divid+ " .ag-body-container .ag-row").each(function(){
			var index = $(this).attr("row");
			var node = gridOptions.api.getRowNode(index);
			if(node.isSelected()){
				node.setSelected(false);
				indexes.push(index);
			}
			
		});
		if(divid == 2){
			setGrid(divid,indexes);
		}

		showMessageBox("請求金額を確定しました。");
	}

}
//tab切替
function changeTab(tnum){
	var cnt = 1;
	$("section.tabs#tab1 > ul.tabs-nav > li.tabs-nav__item").each(function() {
		$("#tab"+ cnt + "-item").addClass("dspnone");
		if($(this)[0].id == "t" + tnum){
			$("#t" + cnt).addClass("tabs-nav__item--active");
			$("#tab"+ cnt + "-item").removeClass("dspnone");
			
			dt = 30;
			setGridData1(tnum,dt);
			checkboxChange();

		}else{
			$("#t" + cnt).removeClass("tabs-nav__item--active");
		}
		cnt++;
	});

	
}

function openModalWindow_wa8005(id){
	$("#modal" + id).removeClass("dspnone");
}

