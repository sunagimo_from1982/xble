$(function(){

	//各tab内のモーダル生成
	setGridData1("1",30);
	
	setGridData3("3",10);
	//補充候補リスト
	//setGridData2("5",30);
	$("#otherwindow1 #details1 input,#otherwindow1 #details1 select,#otherwindow1 #details1 textarea").change(function(){
		var chk = true;
		$("#otherwindow1 #details1 input,#details1 select,#details1 textarea").each(function(){
			if($(this).val().length == 0){
				chk = false;
			}
		});

		if(chk){
			$("#otherwindow1 #details1 .otherwindow-details__location__btns a.btn_secondary").removeClass("disabled");
			$("#otherwindow1 .otherwindow-header a.btn_primary").removeClass("disabled");
			
		}else{
			$("#otherwindow1 #details1 .otherwindow-details__location__btns a.btn_secondary").addClass("disabled");

		}
	});
	$("#otherwindow2 #details2 input,#otherwindow2 #details2 select,#otherwindow2 #details2 textarea").change(function(){
		var chk = true;
		$("#otherwindow2 #details2 input,#details2 select,#details2 textarea").each(function(){
			if($(this).val().length == 0){
				chk = false;
			}
		});

		if(chk){
			$("#otherwindow2 #details2 .otherwindow-details__location__btns a.btn_secondary").removeClass("disabled");
			$("#otherwindow2 .otherwindow-header a.btn_primary").removeClass("disabled");
			
		}else{
			$("#otherwindow1 #details2 .otherwindow-details__location__btns a.btn_secondary").addClass("disabled");

		}
	});
	
	var today = new Date();

	today = today.getFullYear() + "-" + dateZeroAd((today.getMonth() + 1)) + "-" + dateZeroAd(today.getDate());
	$("#date1").val(today);
	
});


var columnDefs1_1;
var gridOptions1_1;
var rowData1_1 = [];
function setGridData1_1(divid,data){

	// ヘッダー

	//headerName=列の題名,field=列のID,width=幅、cellClas=各セルに適用するクラス
	//headerCheckboxSelection=ヘッダーにチェックボックスを付けるか？
	//checkboxSelection=各行にチェックボックスを表示するか？
	//pinned=列固定,suppressMenu=ヘッダー一列目に検索フィールドを出すか？
	//hide=表示、非表示、sortable=ソートができるかできないか
	//filter=ヘッダー2列目の検索フィールドを何にするか？
	
	/*ヘッダーデータをタイプで入れ替える*/
	
	columnDefs1_1 = getColmnDefs(divid);


	// 行のテストデータ

	rowData1_1 = getRowData(divid,data);


			
	// オプションの設定
	gridOptions1_1 = {
		enableColResize:true,
		enableSorting: true,
		enableFilter: true,
		headerHeight: 30,
		floatingFiltersHeight:30,
		columnDefs: columnDefs1_1,/*headedrデータ*/
		rowData: rowData1_1,/*行データ*/
		floatingFilter: true,/*2列目のフィルターのon/off*/
		suppressRowClickSelection:true,/*チェックボックス以外のクリックを認めるか？*/
		rowSelection :'multiple',/*複数選択できるように*/
		localeText: getLocaleText(),/*データ０の時のメッセージ*/
		rowHeight : 40,
		pagination:true,
		paginationPageSize:rowData1_1.length
	};
	


	//   対象となる要素の指定
	var eGridDiv = document.querySelector('#myGrid' + divid);

	//   グリッドの生成
	new agGrid.Grid(eGridDiv, gridOptions1_1);

	setFunctions(gridOptions1_1,divid,rowData1_1);
	
	/*今回の場合はテーブル4*/
	var chkflg4 = false;
	//横スクロールでステータス領域が消えた時に左はじにカラー領域を表示
	$("#myGrid" + divid + " div.ag-body-viewport").scroll(function(e){
		//今回のステータス位置が185付近なので今回はそこに設定
		if($("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() > 135 && !chkflg4 ){
			chkflg4 = true;

			showcol(true);
		}else if($("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() <= 135 && chkflg4){
			chkflg4 = false;
			showcol(false);

		}
		checkboxChange();
	});
	//列の表示非表示切り替え
	function showcol(show) {
		gridOptions1_1.columnApi.setColumnVisible('col99', show);
	}
	
	gridOptions1_1.onRowSelected = function(param){
		$(".tableitem__btnarea--rightarea span a").removeClass("disabled");
	}
	

	getAllCnt(rowData1_1,divid,gridOptions1_1);
	
	
	headerCheckboxChange();
	checkboxChange();
	spredScrollArrowChange(divid);
	

	
}

var columnDefs1_2;
var gridOptions1_2;
var rowData1_2 = [];
function setGridData1_2(divid,data){

	// ヘッダー

	//headerName=列の題名,field=列のID,width=幅、cellClas=各セルに適用するクラス
	//headerCheckboxSelection=ヘッダーにチェックボックスを付けるか？
	//checkboxSelection=各行にチェックボックスを表示するか？
	//pinned=列固定,suppressMenu=ヘッダー一列目に検索フィールドを出すか？
	//hide=表示、非表示、sortable=ソートができるかできないか
	//filter=ヘッダー2列目の検索フィールドを何にするか？
	
	/*ヘッダーデータをタイプで入れ替える*/
	
	columnDefs1_2 = getColmnDefs(divid);


	// 行のテストデータ

	rowData1_2 = getRowData(divid,data);


			
	// オプションの設定
	gridOptions1_2 = {
		enableColResize:true,
		enableSorting: true,
		enableFilter: true,
		headerHeight: 30,
		floatingFiltersHeight:30,
		columnDefs: columnDefs1_2,/*headedrデータ*/
		rowData: rowData1_2,/*行データ*/
		floatingFilter: true,/*2列目のフィルターのon/off*/
		suppressRowClickSelection:true,/*チェックボックス以外のクリックを認めるか？*/
		rowSelection :'multiple',/*複数選択できるように*/
		localeText: getLocaleText(),/*データ０の時のメッセージ*/
		rowHeight : 40,
		pagination:true,
		paginationPageSize:20
	};
	


	//   対象となる要素の指定
	var eGridDiv = document.querySelector('#myGrid' + divid);

	//   グリッドの生成
	new agGrid.Grid(eGridDiv, gridOptions1_2);

	setFunctions(gridOptions1_2,divid,rowData1_2);
	
	/*今回の場合はテーブル4*/
	var chkflg4 = false;
	//横スクロールでステータス領域が消えた時に左はじにカラー領域を表示
	$("#myGrid" + divid + " div.ag-body-viewport").scroll(function(e){
		//今回のステータス位置が185付近なので今回はそこに設定
		if($("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() > 135 && !chkflg4 ){
			chkflg4 = true;

			showcol(true);
		}else if($("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() <= 135 && chkflg4){
			chkflg4 = false;
			showcol(false);

		}
		checkboxChange();
	});
	//列の表示非表示切り替え
	function showcol(show) {
		gridOptions1_2.columnApi.setColumnVisible('col99', show);
	}
	
	gridOptions1_2.onRowSelected = function(param){
		$(".tableitem__btnarea--rightarea span a").removeClass("disabled");
	}
	

	getAllCnt(rowData1_2,divid,gridOptions1_2);
	
	
	headerCheckboxChange();
	checkboxChange();
	spredScrollArrowChange(divid);
}


function setFunctions(gridOptions,divid,rowData){
		//セルクリック時に行を選択状態にする
	gridOptions.onCellClicked = function(param){
		/*クリックされたセルの取得*/
		var cols = param.colDef.field;
		var index = param.node.childIndex;
		//セルがチェックボックスセルだったら
		if(cols == "col0"){
			//状況により行のセレクトをon/offする
			var node = gridOptions.api.getRowNode(index);
			if(node.isSelected()){
				node.setSelected(false);

			}else{
				node.setSelected(true);
			}
		}
	}
	//ag-gridの仕様電話再描画されるためソートするとチェックボックスがデフォルトデザインに戻ってしまうためそれを防ぐ
	gridOptions.onSortChanged = function(param){
		checkboxChange();
	}
	gridOptions.onPaginationChanged = function(param){
		checkboxChange();
	}
	
	gridOptions.onFilterChanged = function(param){
		//モーダル内の検索インプットへのアクティブクラス付与
		$(".ag-floating-filter-input").each(function(){
			if($(this).val().length == 0){
				$(this).removeClass("active");
			}else{
				$(this).addClass("active");
			}
		});
		checkboxChange();
		
	}
	gridOptions.onSelectionChanged= function(param){
		getAllCnt(rowData,divid,gridOptions);
		
	}
	var sort = [
		{colId: 'col2', sort: 'desc'}
	];
	gridOptions.api.setSortModel(sort);
	
	
	
	
	$("#pagerItem" + divid + ".tableitem__tablearea__pagerItem .disabled").removeClass("disabled");
	//各矢印クリックでページ移動
	$("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i").click(function(){
		if($(this).prop("id") == "first" && !$(this).hasClass("disabled")){
			gridOptions.api.paginationGoToFirstPage();
		}else if($(this).prop("id") == "prev" && !$(this).hasClass("disabled")){
			gridOptions.api.paginationGoToPreviousPage();
		}else if($(this).prop("id") == "next" && !$(this).hasClass("disabled")){
			gridOptions.api.paginationGoToNextPage();
		}else if($(this).prop("id") == "last" && !$(this).hasClass("disabled")){
			gridOptions.api.paginationGoToLastPage()
		}
		setPageSep(gridOptions,divid);
	});
	setPageSep(gridOptions,divid);
	var coldef = columnDefs1_1;
	var rd = rowData1_1;
	if(divid == "2"){
		coldef = columnDefs1_2;
		rd = rowData1_2;
	}else if(divid == "3"){

		coldef = columnDefs3;
		rd = rowData3;
	}

	gridWidth(".tableitem",coldef,"#myGrid" + divid, "#table" + divid)
	gridHeight(".tableitem",rd,"#myGrid" + divid, "#table" + divid);	
	
}

//pagerの表示件数を変更
function updatePager1(){
	var value = $("#select1").val();
    gridOptions1_1.api.paginationSetPageSize(Number(value));
	checkboxChange();
	setPageSep(gridOptions1_1,1);
}
function updatePager2(){
	var value = $("#select2").val();
    gridOptions1_2.api.paginationSetPageSize(Number(value));
	checkboxChange();
	setPageSep(gridOptions1_2,2);
}
//pagerの真ん中の構築 
function setPageSep(gridOptions,divid){

	var h = "<span class='tableitem__tablearea__pagerItem__nowpage--desc--big'>" + (gridOptions.api.paginationGetCurrentPage() + 1) + "</span> /" + gridOptions.api.paginationGetTotalPages() + " ";
	$("#pagerItem" + divid + " .tableitem__tablearea__pagerItem__nowpage--desc").html(h);
	//1ページ目では前に戻れないようにdisabled
	if(gridOptions.api.paginationGetCurrentPage() + 1 == 1){
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#first").addClass("disabled");
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#prev").addClass("disabled");
	}else{
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#first").removeClass("disabled");
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#prev").removeClass("disabled");
	}
	//LASTページ目では次にいけないようにdisabled
	if(gridOptions.api.paginationGetCurrentPage() + 1 == gridOptions.api.paginationGetTotalPages()){
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#next").addClass("disabled");
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#last").addClass("disabled");
	}else{
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#next").removeClass("disabled");
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#last").removeClass("disabled");
	}
}
//スプレッドの矢印の構築・再構築
function spredScrollArrowChange(divid){
	//初期表示しているすべて（TAB4）のみロード時に横スクロールの場所設定を行う
	var parentArea = $("#myGrid" + divid + " div.ag-body-container").height();

	var tbarea = $("#myGrid" + divid + " div.ag-body-viewport").scrollTop() + $("#myGrid" + divid + " div.ag-body-viewport").height();
	if(parentArea - tbarea > 0){
		//スクロールバーがある場合の処理
		$("#table" + divid + " div.scrollArrow").css("right","16px");
	}else{
		//スクロールバーがない場合の処理
		$("#table" + divid + " div.scrollArrow").css("right","0px");

	}

	//初期表示しているすべて（TAB4）のみロード時に横スクロールの表示設定を行う
	var parentArea = $("#myGrid" + divid + " div.ag-body-container").width();

	var tbarea = $("#myGrid" + divid + " div.ag-body-viewport").scrollTop() + $("#myGrid" + divid + " div.ag-body-viewport").width();

	if(parentArea - tbarea > 0){
		//スクロールバーがある場合の処理
		$("#table" + divid + " div.scrollArrow").removeClass("dspnone");
		var height = $("#myGrid" + divid + "").innerHeight() - 111;
		var dspheight = $("#myGrid" + divid + " .ag-body-container").innerHeight() ;

		var gh = 0;
		if(height < dspheight){
			gh = height;
		}else{
			gh = dspheight;
		}

		$("#table" + divid + " div.scrollArrow").css("height",gh + "px");

	}else{
		//スクロールバーがない場合の処理
		$("#table" + divid + " div.scrollArrow").addClass("dspnone");

	}



	/*TAB4のテーブル内の→マウスオーバー挙動*/
	var tm1 = 0;
	var tm2 = 0;
	var scrollMax = 0;
	var dspflg1 = true;
	//スクロールイベント
	$("#table" + divid + " div.scrollArrow").hover(function(){
		tm2 = setTimeout(function(){
			tm1 = setInterval(function(){
				//スクロールがMAXになったら表示を消すためにここで値を取得
				scrollMax = $("#myGrid" + divid + " div.ag-body-container").width() - $("#myGrid" + divid + " div.ag-body-viewport").width();

				$("#myGrid" + divid + " div.ag-body-viewport").scrollLeft( $("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() + 2);

				if(scrollMax <= $("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() && dspflg1){
					$("#table" + divid + " div.scrollArrow").addClass("dspnone");
					dspflg1 = false;
					clearTimeout(tm2);
					clearInterval(tm1);
				}
			},0);
		},300);
	},function(){
		clearTimeout(tm2);
		clearInterval(tm1);
	});

	//scrollArrowが消えているときにscrollMAXと値が変わったら再表示
	$("#myGrid" + divid + " div.ag-body-viewport").scroll(function(){

		scrollMax = $("#myGrid" + divid + " div.ag-body-container").width() - $("#myGrid" + divid + " div.ag-body-viewport").width();

		if(scrollMax <= $("#myGrid" + divid + " div.ag-body-viewport").scrollLeft()){
			$("#table" + divid + " div.scrollArrow").addClass("dspnone");
			dspflg1 = false;
		}

		if(scrollMax > $("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() && !dspflg1){
			$("#table" + divid + " div.scrollArrow").removeClass("dspnone");
			dspflg1 = true;
		}
	});	
}


//テーブル作成用ファンクション
function setGridData1(divid,data){
	if(divid == "1" && rowData1_1.length == 0){
		setGridData1_1(divid,data);
	}else if(divid == "2" && rowData1_2.length == 0){
		setGridData1_2(divid,data);
	}
	
}
//setGridData1系のヘッダーデータ構築
function getColmnDefs(divid){
	if(divid == "1"){
		return columnDefs1 = [
			{headerName: "", field: "col1", filter:'text',unSortIcon: false,lockPosition:true,
			 suppressMenu: true,width:100,cellClass: ['tc','cellclass'],pinned: 'left',
			 cellRenderer: (params) => {
					const element = document.createElement('div');
					element.innerHTML = "<div class='itemcode'><a class='btn_secondary' href='javascript:openOtherWindow_wa8010(2," + params.value + ");'>修正</a></div>";

					return element;
			}},
			{headerName: "発生日", field: "col2", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "荷主コード", field: "col3", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "荷主名", field: "col4", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
			{headerName: "コード", field: "col5", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:80,cellClass: ['tl','cellclass']},
			{headerName: "項目", field: "col6", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "数量", field: "col7", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:80,cellClass: ['tr','cellclass']},
			{headerName: "単位", field: "col8", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:80,cellClass: ['tl','cellclass']},
			{headerName: "単価", field: "col9", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:80,cellClass: ['tr','cellclass']},
			{headerName: "税区分", field: "col10", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:80,cellClass: ['tl','cellclass']},
			{headerName: "金額", field: "col11", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:80,cellClass: ['tr','cellclass']},
			{headerName: "備考", field: "col12", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
			{headerName: "取消", field: "col13", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "取消日時", field: "col14", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "取消者名", field: "col15", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
			{headerName: "請求対象外", field: "col16", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "請求対象外日時", field: "col17", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "請求対象外者名", field: "col18", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
			{headerName: "更新日時", field: "col19", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "更新者名", field: "col20", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "登録日時", field: "col21", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "登録者名", field: "col22", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']}
		];		
	}else if(divid == "2"){
		return columnDefs1 = [
			{headerName: "発生日", field: "col2", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "荷主コード", field: "col3", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "荷主名", field: "col4", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
			{headerName: "コード", field: "col5", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:80,cellClass: ['tl','cellclass']},
			{headerName: "項目", field: "col6", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "数量", field: "col7", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:80,cellClass: ['tr','cellclass']},
			{headerName: "単位", field: "col8", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:80,cellClass: ['tl','cellclass']},
			{headerName: "単価", field: "col9", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:80,cellClass: ['tr','cellclass']},
			{headerName: "税区分", field: "col10", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:80,cellClass: ['tl','cellclass']},
			{headerName: "金額", field: "col11", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:80,cellClass: ['tr','cellclass']},
			{headerName: "備考", field: "col12", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
			{headerName: "取消", field: "col13", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "取消日時", field: "col14", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "取消者名", field: "col15", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
			{headerName: "請求対象外", field: "col16", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "請求対象外日時", field: "col17", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "請求対象外者名", field: "col18", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
			{headerName: "更新日時", field: "col19", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "更新者名", field: "col20", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "登録日時", field: "col21", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "登録者名", field: "col22", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']}
		];		
	}

}
//setGridData1系のデータ構築
function getRowData(divid,data){
	var rowData = [];
	


	
	if(divid == "1"){
		for(var i = 0; i < data;i++){
			rowData.push({col1: i,col2: "2019-04-30", col3: '000011',col4: '荷主・SEAOS (B)',col5: 'A1', col6: '送り状作成料', col7: '12', col8: '枚', col9: '12', col10: '外税', col11: '144', col12: '', col13: '', col14: '2019-10-21 15:47:51', col15: 'sasaba', col16: '', col17: '2019-10-21 15:47:51', col18: 'sasaba', col19: '2019-10-21 15:47:51', col20: 'sasaba', col21: '2019-10-21 15:47:51', col22: 'sasaba'});
			
		}
	}else if(divid == "2"){
		for(var i = 0; i < data;i++){
			rowData.push({col1: i,col2: "2019-04-30", col3: '000011',col4: '荷主・SEAOS (B)',col5: 'A1', col6: '送り状作成料', col7: '12', col8: '枚', col9: '12', col10: '外税', col11: '144', col12: '', col13: '', col14: '2019-10-21 15:47:51', col15: 'sasaba', col16: '', col17: '2019-10-21 15:47:51', col18: 'sasaba', col19: '2019-10-21 15:47:51', col20: 'sasaba', col21: '2019-10-21 15:47:51', col22: 'sasaba'});
			
		}
	}
	return rowData;
}

//テーブル作成用ファンクション-入荷実績登録用
var columnDefs3;
var gridOptions3;
var rowData3;
function setGridData3(divid,data){

	// ヘッダー

	//headerName=列の題名,field=列のID,width=幅、cellClas=各セルに適用するクラス
	//headerCheckboxSelection=ヘッダーにチェックボックスを付けるか？
	//checkboxSelection=各行にチェックボックスを表示するか？
	//pinned=列固定,suppressMenu=ヘッダー一列目に検索フィールドを出すか？
	//hide=表示、非表示、sortable=ソートができるかできないか
	//filter=ヘッダー2列目の検索フィールドを何にするか？
	
	/*ヘッダーデータをタイプで入れ替える*/
	columnDefs3 = [
		{headerName: "発生日", field: "col1", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
		{headerName: "項目", field: "col2", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:160,cellClass: ['tl','cellclass'],
		 cellRenderer: (params) => {
				const element = document.createElement('div');
				element.innerHTML = "<select class='content' onchange='noDisabledButton();'>"
			 					  + "<option value=''>項目を選択してください。</option>"
			 					  + "<option value='1'>ピッキング料金</option>"
			 					  + "<option value='2' selected>試験成績書印刷</option>"
			 					  + "</select>";

				return element;
			}
		},
		{headerName: "数量", field: "col3", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:80,cellClass: ['tl','cellclass'],editable: true
		 ,cellClass: 
		function(params) {
			//値によって追加するクラスを可変させる
			if(params.value.length != 0){
				return ['tr','cellclass','editClass','noCellFocus'];
			}
			return ['tl','cellclass','editClass','noCellFocus'];
		}},
		{headerName: "単価", field: "col4", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:80,editable: true
		 ,cellClass: 
		function(params) {
			return ['tr','cellclass','editClass','noCellFocus'];
		}},
		{headerName: "金額", field: "col5", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:80,editable: true
		 ,cellClass: 
		function(params) {
			return ['tr','cellclass','editClass','noCellFocus'];
		}},
		{headerName: "備考", field: "col6", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:280,editable: true
		 ,cellClass: 
		function(params) {
			//値によって追加するクラスを可変させる
			return ['tl','cellclass','editClass','noCellFocus'];
		}},
		{headerName: "請求対象外", field: "col7", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:160,cellClass: ['tl','cellclass'],
		 cellRenderer: (params) => {
				const element = document.createElement('div');
				element.innerHTML = "<select class='content' onchange='noDisabledButton();'>"
			 					  + "<option value=''>請求対象外を選択してください。</option>"
			 					  + "<option value='1' selected>請求対象外</option>"
			 					  + "</select>";

				return element;
			}
		}
	];			
		

	// 行のテストデータ

	rowData3 = [];

	for(var i = 0; i < data;i++){
		
		rowData3.push({col1: '2019-04-08', col2: 'ピッキング料金', col3: '80', 
					  col4: '20', col5: '1,600', col6: '', 
					  col7: '請求対象外'});
	}
			

	// オプションの設定
	gridOptions3 = {
		enableColResize:true,
		enableSorting: true,
		enableFilter: true,
		headerHeight: 30,
		floatingFiltersHeight:30,
		columnDefs: columnDefs3,/*headedrデータ*/
		rowData: rowData3,/*行データ*/
		floatingFilter: true,/*2列目のフィルターのon/off*/
		suppressRowClickSelection:true,/*チェックボックス以外のクリックを認めるか？*/
		rowSelection :'multiple',/*複数選択できるように*/
		localeText: getLocaleText(),/*データ０の時のメッセージ*/
		rowHeight : 40,
		singleClickEdit:true,
		pagination:true,
		paginationPageSize:rowData3.length
	};

	//   対象となる要素の指定
	var eGridDiv = document.querySelector('#myGrid' + divid);

	//   グリッドの生成
	new agGrid.Grid(eGridDiv, gridOptions3);

	//setFunctions(gridOptions3,divid,rowData3);
	
	/*今回の場合はテーブル4*/
	var chkflg4 = false;
	//横スクロールでステータス領域が消えた時に左はじにカラー領域を表示
	$("#myGrid" + divid + " div.ag-body-viewport").scroll(function(e){
		//checkboxChange();
	});
	
	gridOptions3.onRowSelected = function(param){
		$(".tableitem__btnarea--rightarea span a").removeClass("disabled");
	}
	

	getAllCnt(rowData3,divid,gridOptions3);
	
	gridOptions3.onCellValueChanged = function(param){

		$("#registbtn3").removeClass("disabled");
	}

}
function gridWidth_wa8010(owname,coldef,gridname,tablename){
	var ow = $(owname).innerWidth() - 40;
	//Gridの横幅が画面幅以下なら調整する
	var w = 20;
	for(var i = 0 ; i < coldef.length ;i++){
		w += coldef[i].width;
	}

	if(ow > w){
		$(gridname ).css("width","calc(100%)");
		$(tablename).css("width","calc(100% - 60px)");
		
		
	}
	
}

//pagerの件数取得
function getAllCnt(rowData,divid,gridOptions){
	$("#allcount" + divid).html(rowData.length);
	if($("#selectcount" + divid)[0]){
		$("#selectcount" + divid).html(gridOptions.api.getSelectedRows().length);
	}
}

//tab切替
function changeTab(tnum){
	var cnt = 1;
	$("section.tabs > ul.tabs-nav > li.tabs-nav__item").each(function() {
		$("#tab"+ cnt + "-item").addClass("dspnone");
		if($(this)[0].id == "t" + tnum){
			$("#t" + cnt).addClass("tabs-nav__item--active");
			$("#tab"+ cnt + "-item").removeClass("dspnone");
			
			dt = 30;
			setGridData1(tnum,dt);
			checkboxChange();

		}else{
			$("#t" + cnt).removeClass("tabs-nav__item--active");
		}
		cnt++;
	});

	
}
var updateid = "0";
function openOtherWindow_wa8010(ids,index = 0){
	updateid = index;
	$("#otherwindow" + ids).removeClass("dspnone");
	$("#otherwindow" + ids).addClass("openotherwindow");
	
	if(ids == "3"){
		gridWidth_wa8010(".otherwindow-tablearea",columnDefs3,"#myGrid3", ".otherwindow-tablearea")
		gridHeight(".otherwindow-tablearea",rowData3,"#myGrid3", ".otherwindow-tablearea");	
	}
	
}

function closeOtherWindow_wa8010(ids){
	var obj = $("#registbtn1");
	if(ids == "2"){
		obj = $("#registbtn2");
	}else if(ids == "3"){
		obj = $("#registbtn3");
	}
	if(!obj.hasClass("disabled")){
		closeOtherWindow();
		if(ids == "1" ){
			var rd = rowData1_1;
			var go = gridOptions1_1;

			$("#myGrid1 .ag-pinned-left-cols-container .ag-row").each(function(){
				var index = $(this).attr("row");
				if(index == 0 || index == 1 ){
					$(this).addClass("row-update");

					var obj = $(this);
					setTimeout(function(){
						obj.addClass("row-default");
						obj.removeClass("row-update");
					},2000);
				}
			});
			$("#myGrid1 .ag-body-container .ag-row").each(function(){
				var index = $(this).attr("row");
				if(index == 0 || index == 1 ){
					$(this).addClass("row-update");
					var obj = $(this);
					setTimeout(function(){
						obj.addClass("row-default");
						obj.removeClass("row-update");
					},2000);
				}
			});		

		}else if(ids == "2"){
			var rd = rowData1_1;
			var go = gridOptions1_1;

			$("#myGrid1 .ag-pinned-left-cols-container .ag-row").each(function(){
				var index = $(this).attr("row");


				if(index == updateid){
					$(this).addClass("row-update");

					var obj = $(this);
					setTimeout(function(){
						obj.addClass("row-default");
						obj.removeClass("row-update");
					},2000);
				}
			});
			$("#myGrid1 .ag-body-container .ag-row").each(function(){
				var index = $(this).attr("row");
				if(index == updateid){
					$(this).addClass("row-update");
					var obj = $(this);
					setTimeout(function(){
						obj.addClass("row-default");
						obj.removeClass("row-update");
					},2000);
				}
			});


		}else if(ids == "3" ){
			var rd = rowData1_1;
			var go = gridOptions1_1;

			$("#myGrid1 .ag-pinned-left-cols-container .ag-row").each(function(){
				var index = $(this).attr("row");
				if(index == 0 || index == 5 ){
					$(this).addClass("row-update");

					var obj = $(this);
					setTimeout(function(){
						obj.addClass("row-default");
						obj.removeClass("row-update");
					},2000);
				}
			});
			$("#myGrid1 .ag-body-container .ag-row").each(function(){
				var index = $(this).attr("row");
				if(index == 0 || index == 5 ){
					$(this).addClass("row-update");
					var obj = $(this);
					setTimeout(function(){
						obj.addClass("row-default");
						obj.removeClass("row-update");
					},2000);
				}
			});		

		}
	}
	
	
	updateid = "0";
}
//明細書の追加と追加した明細の初期化
var cnt = 2;
function addMeisai(){
	var h = $("#details1").html();
	h = h.replace( "明細1", "明細" + cnt );
	h = h.replace( "deletebtn1", "deletebtn" + cnt );
	h = h.replace( "date1", "date" + cnt );
	h = h.replace( "javascript:deleteMeisai(1);", "javascript:deleteMeisai(" + cnt + ");");
	
	$("#detailsMeisai").append("<div class=\"otherwindow-details__location\" id=\"details" + cnt + "\">" + h + "</section>");
	
	idcnt = 0;
	$("#details" + cnt + " input").each(function(){
		$(this).val("");
		/*
		if(idcnt == "2"){
			$(this).val("枚");
		}
			
		if(idcnt == "3"){
			$(this).val("12");
		}
			
		if(idcnt == "4"){
			$(this).val("外税");
		}
			
		if(idcnt == "5"){
			$(this).val("213,100");
		}*/
			
		idcnt++;
	});
	$("#details" + cnt + " select").val("");
	$("#details" + cnt + " textarea").val("");
	
	var today = new Date();

	today = today.getFullYear() + "-" + dateZeroAd((today.getMonth() + 1)) + "-" + dateZeroAd(today.getDate());
	
	$("#date" + cnt ).val(today);
	
	idcnt = 0;
	$("#details" + cnt + " select").each(function(){
		$(this).val("");
		
		if(idcnt == "1"){
			$(this).prop("disabled",true);
			$(this).addClass("disabled");
		}
			
			
		idcnt++;
	});
	$("#details" + cnt + " .otherwindow-details__location__btns a.btn_secondary").addClass("disabled");
	
	/*$("#details" + cnt + " input,#details" + cnt + " select,#details" + cnt + " textarea").change(function(){
		var chk = true;
		$("#details" + cnt + " input,#details" + cnt + " select,#details" + cnt + " textarea").each(function(){
			if($(this).val().length == 0){
				chk = false;
			}
		});

		if(chk){
			$("#details" + cnt + " .otherwindow-details__location__btns a.btn_secondary").removeClass("disabled");
			$(".otherwindow-header a.btn_primary").removeClass("disabled");

		}else{
			$("#details" + cnt + " .otherwindow-details__location__btns a.btn_secondary").addClass("disabled");
		}
	});*/
	
	$(".deletebtn").removeClass("disabled");
	cnt++;
}
function deleteMeisai(oid,meisaiid,obj){

	if(!$("#deletebtn" + meisaiid).hasClass("disabled")){
		$("#otherwindow" + oid + " #details" + meisaiid).remove();
	}
}
function changeData(obj){
	console.log(obj.parent().parent().next().find("select"));
	if(obj.val() == ""){
		obj.parent().parent().next().find("select").prop("disabled",true);
		obj.parent().parent().next().find("select").addClass("disabled");
	}else{
		obj.parent().parent().next().find("select").prop("disabled",false);
		obj.parent().parent().next().find("select").removeClass("disabled");
	}
}

function setInputData(obj){
	if(obj.val() == ""){
		obj.parent().parent().next().next().find("input").val("");
		obj.parent().parent().next().next().next().find("input").val("");
		obj.parent().parent().next().next().next().next().find("input").val("");
		obj.parent().parent().next().next().next().next().next().find("input").val("");
	}else{
		obj.parent().parent().next().next().find("input").val("枚");
		obj.parent().parent().next().next().next().find("input").val("12");
		obj.parent().parent().next().next().next().next().find("input").val("外税");
		obj.parent().parent().next().next().next().next().next().find("input").val("0");
	}
	
}