

$(function(){

	$("#text1").keyup(function(){
		changeText(1);
	});
	$("#text2").keyup(function(){
		changeText(1);
	});
	$("#text3").keyup(function(){
		changeText(2);
	});

});

function changeTab(tnum){
	var cnt = 1;
	$("section.tabs > ul.tabs-nav > li.tabs-nav__item").each(function() {
		$("#tab"+ cnt + "-item").addClass("dspnone");
		if($(this)[0].id == "t" + tnum){
			$("#t" + cnt).addClass("tabs-nav__item--active");
			$("#tab"+ cnt + "-item").removeClass("dspnone");
			
			var dt = 0;
			if(tnum == 4){
				dt = 30;
			}

		}else{
			$("#t" + cnt).removeClass("tabs-nav__item--active");
		}
		cnt++;
	});

	
}
function changeText(type){
	if(type == "1"){
		if($("#text1").val().length > 0 || $("#text2").val().length > 0){
			$("#tab1-item .btn_primary").removeClass("disabled");
		}else{
			$("#tab1-item .btn_primary").addClass("disabled");
		}
	}else{
		if($("#text3").val().length > 0){
			$("#tab2-item .btn_primary").removeClass("disabled");
		}else{
			$("#tab2-item .btn_primary").addClass("disabled");
		}
	}
}

function dspLogss(type){
	
	
	
	var h = "<p class=\"pickitem-logarea-log--item\"><span class=\"pickitem-logarea-log--item--time\">18:23:07</span>&nbsp;&nbsp;ピック番号=[20190110-00008]</p>"
		  + "<p class=\"pickitem-logarea-log--error\">ピック指示書が存在しません。</p>"
		  + "<p class=\"pickitem-logarea-log--item\"><span class=\"pickitem-logarea-log--item--time\">18:20:12</span>&nbsp;&nbsp;ピック番号=[20190110-00008]</p>"
		  + "<p class=\"pickitem-logarea-log--item\"><span class=\"pickitem-logarea-log--item--time\">18:15:33</span>&nbsp;&nbsp;ピック番号=[20190110-00008]</p>"
		  + "<p class=\"pickitem-logarea-log--item\"><span class=\"pickitem-logarea-log--item--time\">18:10:29</span>&nbsp;&nbsp;ピック番号=[20190110-00008]</p>"
		  + "<p class=\"pickitem-logarea-log--item\"><span class=\"pickitem-logarea-log--item--time\">18:10:29</span>&nbsp;&nbsp;ピック番号=[20190110-00008]</p>"
		  + "<p class=\"pickitem-logarea-log--item\"><span class=\"pickitem-logarea-log--item--time\">18:10:29</span>&nbsp;&nbsp;ピック番号=[20190110-00008]</p>"
		  + "<p class=\"pickitem-logarea-log--item\"><span class=\"pickitem-logarea-log--item--time\">18:10:29</span>&nbsp;&nbsp;ピック番号=[20190110-00008]</p>";
	var erCnt = 0;
	$(".pickitem-input--error").html("");
	$("input[type=\"text\"]").removeClass("error");
	
	if(type == "1"){
		
		if($("#text1").val().length > 20){
			$("#error1").html("文字数の上限を超えています。最大文字数：20");
			$("#text1").addClass("error");
			erCnt++;
		}
		
		if($("#text2").val().length > 10){
			$("#error2").html("文字数の上限を超えています。最大文字数：10");
			$("#text2").addClass("error");
			erCnt++;
		}
		if(erCnt == 0){
			$("#log1").html(h);
		}
	}else{
		if($("#text3").val().length > 20){
			$("#error3").html("文字数の上限を超えています。最大文字数：20");
			$("#text3").addClass("error");
			erCnt++;
		}
		if(erCnt == 0){
			$("#log2").html(h);
		}
	}
}