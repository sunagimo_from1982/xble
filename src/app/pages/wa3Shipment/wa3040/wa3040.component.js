

$(function(){

	setGridData1("1",30);
	
	setGridData2("2",30);
	
});

var columnDefs1_1;
var gridOptions1_1;
var rowData1_1 = [];
function setGridData1_1(divid,data){

	// ヘッダー

	//headerName=列の題名,field=列のID,width=幅、cellClas=各セルに適用するクラス
	//headerCheckboxSelection=ヘッダーにチェックボックスを付けるか？
	//checkboxSelection=各行にチェックボックスを表示するか？
	//pinned=列固定,suppressMenu=ヘッダー一列目に検索フィールドを出すか？
	//hide=表示、非表示、sortable=ソートができるかできないか
	//filter=ヘッダー2列目の検索フィールドを何にするか？
	
	/*ヘッダーデータをタイプで入れ替える*/
	
	columnDefs1_1 = getColmnDefs(divid);


	// 行のテストデータ

	rowData1_1 = getRowData(divid,data);


			
	// オプションの設定
	gridOptions1_1 = {
		enableColResize:true,
		enableSorting: true,
		enableFilter: true,
		headerHeight: 30,
		floatingFiltersHeight:30,
		columnDefs: columnDefs1_1,/*headedrデータ*/
		rowData: rowData1_1,/*行データ*/
		floatingFilter: true,/*2列目のフィルターのon/off*/
		suppressRowClickSelection:true,/*チェックボックス以外のクリックを認めるか？*/
		rowSelection :'multiple',/*複数選択できるように*/
		localeText: getLocaleText(),/*データ０の時のメッセージ*/
		rowHeight : 40,
		pagination:true,
		paginationPageSize:20
	};
	


	//   対象となる要素の指定
	var eGridDiv = document.querySelector('#myGrid' + divid);

	//   グリッドの生成
	new agGrid.Grid(eGridDiv, gridOptions1_1);

	setFunctions(gridOptions1_1,divid,rowData1_1);
	
	/*今回の場合はテーブル4*/
	var chkflg4 = false;
	//横スクロールでステータス領域が消えた時に左はじにカラー領域を表示
	$("#myGrid" + divid + " div.ag-body-viewport").scroll(function(e){
		checkboxChange();
	});
	
	gridOptions1_1.onRowSelected = function(param){
		$(".tableitem__btnarea--rightarea span a").removeClass("disabled");
	}
	

	getAllCnt(rowData1_1,divid,gridOptions1_1);
	
	
	headerCheckboxChange();
	checkboxChange();
	spredScrollArrowChange(divid);
	
	
	gridWidth(".tableitem",columnDefs1_1,"#myGrid" + divid, "#table" + divid)
	gridHeight(".tableitem",rowData1_1,"#myGrid" + divid, "#table" + divid);	
	
}

function setFunctions(gridOptions,divid,rowData){
		//セルクリック時に行を選択状態にする
	gridOptions.onCellClicked = function(param){
		/*クリックされたセルの取得*/
		var cols = param.colDef.field;
		var index = param.node.childIndex;
		//セルがチェックボックスセルだったら
		if(cols == "col0"){
			//状況により行のセレクトをon/offする
			var node = gridOptions.api.getRowNode(index);
			if(node.isSelected()){
				node.setSelected(false);

			}else{
				node.setSelected(true);
			}
		}
	}
	//ag-gridの仕様電話再描画されるためソートするとチェックボックスがデフォルトデザインに戻ってしまうためそれを防ぐ
	gridOptions.onSortChanged = function(param){
		checkboxChange();
	}
	gridOptions.onPaginationChanged = function(param){
		checkboxChange();
	}
	

	gridOptions.onFilterChanged = function(param){
		//モーダル内の検索インプットへのアクティブクラス付与
		$(".ag-floating-filter-input").each(function(){
			if($(this).val().length == 0){
				$(this).removeClass("active");
			}else{
				$(this).addClass("active");
			}
		});
		checkboxChange();
		
	}
	gridOptions.onSelectionChanged= function(param){
		getAllCnt(rowData,divid,gridOptions);
		
	}
	$("#pagerItem" + divid + ".tableitem__tablearea__pagerItem .disabled").removeClass("disabled");
	//各矢印クリックでページ移動
	$("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i").click(function(){
		if($(this).prop("id") == "first" && !$(this).hasClass("disabled")){
			gridOptions.api.paginationGoToFirstPage();
		}else if($(this).prop("id") == "prev" && !$(this).hasClass("disabled")){
			gridOptions.api.paginationGoToPreviousPage();
		}else if($(this).prop("id") == "next" && !$(this).hasClass("disabled")){
			gridOptions.api.paginationGoToNextPage();
		}else if($(this).prop("id") == "last" && !$(this).hasClass("disabled")){
			gridOptions.api.paginationGoToLastPage()
		}
		setPageSep(gridOptions,divid);
	});
	setPageSep(gridOptions,divid);
	
}
//pagerの表示件数を変更
function updatePager1(){
	var value = $("#select1").val();
    gridOptions1_1.api.paginationSetPageSize(Number(value));
	checkboxChange();
	setPageSep(gridOptions1_1,1);
}

//pagerの表示件数を変更
function updatePager2(){
	var value = $("#select3").val();
    gridOptions2.api.paginationSetPageSize(Number(value));
	setPageSep(gridOptions2,2);
}

//pagerの真ん中の構築 
function setPageSep(gridOptions,divid){

	var h = "<span class='tableitem__tablearea__pagerItem__nowpage--desc--big'>" + (gridOptions.api.paginationGetCurrentPage() + 1) + "</span> /" + gridOptions.api.paginationGetTotalPages() + " ";
	$("#pagerItem" + divid + " .tableitem__tablearea__pagerItem__nowpage--desc").html(h);
	//1ページ目では前に戻れないようにdisabled
	if(gridOptions.api.paginationGetCurrentPage() + 1 == 1){
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#first").addClass("disabled");
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#prev").addClass("disabled");
	}else{
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#first").removeClass("disabled");
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#prev").removeClass("disabled");
	}
	//LASTページ目では次にいけないようにdisabled
	if(gridOptions.api.paginationGetCurrentPage() + 1 == gridOptions.api.paginationGetTotalPages()){
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#next").addClass("disabled");
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#last").addClass("disabled");
	}else{
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#next").removeClass("disabled");
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#last").removeClass("disabled");
	}
	checkboxChange();	
}
//スプレッドの矢印の構築・再構築
function spredScrollArrowChange(divid){
	//初期表示しているすべて（TAB4）のみロード時に横スクロールの場所設定を行う
	var parentArea = $("#myGrid" + divid + " div.ag-body-container").height();

	var tbarea = $("#myGrid" + divid + " div.ag-body-viewport").scrollTop() + $("#myGrid" + divid + " div.ag-body-viewport").height();
	if(parentArea - tbarea > 0){
		//スクロールバーがある場合の処理
		$("#table" + divid + " div.scrollArrow").css("right","16px");
	}else{
		//スクロールバーがない場合の処理
		$("#table" + divid + " div.scrollArrow").css("right","0px");

	}

	//初期表示しているすべて（TAB4）のみロード時に横スクロールの表示設定を行う
	var parentArea = $("#myGrid" + divid + " div.ag-body-container").width();

	var tbarea = $("#myGrid" + divid + " div.ag-body-viewport").scrollTop() + $("#myGrid" + divid + " div.ag-body-viewport").width();

	if(parentArea - tbarea > 0){
		//スクロールバーがある場合の処理
		$("#table" + divid + " div.scrollArrow").removeClass("dspnone");
		var height = $("#myGrid" + divid + "").innerHeight() - 121;
		var dspheight = $("#myGrid" + divid + " .ag-body-container").innerHeight() ;

		var gh = 0;
		if(height < dspheight){
			gh = height;
		}else{
			gh = dspheight;
		}

		$("#table" + divid + " div.scrollArrow").css("height",gh + "px");

	}else{
		//スクロールバーがない場合の処理
		$("#table" + divid + " div.scrollArrow").addClass("dspnone");

	}



	/*TAB4のテーブル内の→マウスオーバー挙動*/
	var tm1 = 0;
	var tm2 = 0;
	var scrollMax = 0;
	var dspflg1 = true;
	//スクロールイベント
	$("#table" + divid + " div.scrollArrow").hover(function(){
		tm2 = setTimeout(function(){
			tm1 = setInterval(function(){
				//スクロールがMAXになったら表示を消すためにここで値を取得
				scrollMax = $("#myGrid" + divid + " div.ag-body-container").width() - $("#myGrid" + divid + " div.ag-body-viewport").width();

				$("#myGrid" + divid + " div.ag-body-viewport").scrollLeft( $("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() + 2);

				if(scrollMax <= $("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() && dspflg1){
					$("#table" + divid + " div.scrollArrow").addClass("dspnone");
					dspflg1 = false;
					clearTimeout(tm2);
					clearInterval(tm1);
				}
			},0);
		},300);
	},function(){
		clearTimeout(tm2);
		clearInterval(tm1);
	});

	//scrollArrowが消えているときにscrollMAXと値が変わったら再表示
	$("#myGrid" + divid + " div.ag-body-viewport").scroll(function(){

		scrollMax = $("#myGrid" + divid + " div.ag-body-container").width() - $("#myGrid" + divid + " div.ag-body-viewport").width();

		if(scrollMax <= $("#myGrid" + divid + " div.ag-body-viewport").scrollLeft()){
			$("#table" + divid + " div.scrollArrow").addClass("dspnone");
			dspflg1 = false;
		}

		if(scrollMax > $("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() && !dspflg1){
			$("#table" + divid + " div.scrollArrow").removeClass("dspnone");
			dspflg1 = true;
		}
	});	
}


//テーブル作成用ファンクション
function setGridData1(divid,data){
	if(divid == "1" && rowData1_1.length == 0){
		setGridData1_1(divid,data);
	}else if(divid == "2" && rowData1_2.length == 0){
		setGridData1_2(divid,data);
	}
	
}
//setGridData1系のヘッダーデータ構築
function getColmnDefs(divid){
	return columnDefs1 = [
		{headerName: "", field: "col0", width: 30,cellClass: ['tc','cl_w','noCellFocus'],
		 headerCheckboxSelection: true,checkboxSelection: true,suppressMenu: true,
		 lockPosition:true,pinned: 'left'
		},
		{headerName: "", field: "col1",pinned: 'left',lockPosition:true,
		 suppressMenu: true,sortable:true,width:95,cellClass: ['tc','cellclass'],suppressMovable:true,
		 cellRenderer: (params) => {
				const element = document.createElement('div');
				element.innerHTML = "<a class='btn_secondary' href='javascript:openOtherWindow_wa3040(\"otherwindow_update\",\"" + params.rowIndex + "\");'>修正</a>";

				return element;
			}
		},
		{headerName: "出荷番号", field: "col2", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
		{headerName: "ピック指示番号", field: "col3", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
		{headerName: "出荷個口連番", field: "col4", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:120,cellClass: ['tr','cellclass']},
		{headerName: "出荷個口数", field: "col5", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tr','cellclass']},
		{headerName: "確定", field: "col6", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
		{headerName: "荷主ケース区分", field: "col7", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
		{headerName: "荷姿", field: "col8", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:80,cellClass: ['tl','cellclass']},
		{headerName: "配送方法", field: "col9", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "配送区分", field: "col10", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "問合せNo", field: "col11", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
		{headerName: "単価", field: "col12", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:80,cellClass: ['tr','cellclass']},
		{headerName: "金額", field: "col13", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:80,cellClass: ['tr','cellclass']},
		{headerName: "荷主コード", field: "col14", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
		{headerName: "荷主名", field: "col15", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
		{headerName: "受注番号", field: "col16", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "届先コード", field: "col17", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "届先名", field: "col18", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
		{headerName: "届先県名", field: "col19", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:80,cellClass: ['tl','cellclass']},
		{headerName: "配送伝票印刷日時", field: "col20", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
		{headerName: "出荷完了日時", field: "col21", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
		{headerName: "備考", field: "col22", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "取消", field: "col23", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "取消日時", field: "col24", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
		{headerName: "取消者名", field: "col25", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "請求対象外", field: "col26", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "請求対象外日時", field: "col27", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
		{headerName: "請求対象者名", field: "col28", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
		{headerName: "出荷ケースID", field: "col29", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:120,cellClass: ['tr','cellclass']},
		{headerName: "出荷指示ID", field: "col30", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:120,cellClass: ['tr','cellclass']}
	];
}
//setGridData1系のデータ構築
function getRowData(divid,data){
	var rowData = [];
	for(var i = 0; i < data;i++){
		rowData.push({col0: i,col1: "",col2: "20181227-00034", col3: 'P20190408-00001',col4: '1',col5: '1', col6: '', col7: 'オーダーピック ', col8: 'バラ', col9: '佐川急便', col10: '陸便', col11: ' 129000000161', col12: '1', col13: '1', col14: '000011', col15: '荷主・SEAOS (B)', col16: 'TEST001', col17: 'DT001', col18: '(資)村瀬商会', col19: '東京都', col20: '2018-12-21 15:47:51', col21: '2018-12-21 15:47:51', col22: '', col23: '', col24: '', col25: '', col26: '', col27: '', col28: '',col29: '150', col30: '1690'});

	}

	return rowData;
}
//pagerの件数取得
function getAllCnt(rowData,divid,gridOptions){
	$("#allcount" + divid).html(rowData.length);
	if($("#selectcount" + divid)[0]){
		$("#selectcount" + divid).html(gridOptions.api.getSelectedRows().length);
	}
}


//テーブル作成用ファンクション-入荷実績登録用
var columnDefs2;
var gridOptions2;
var rowData2;
function setGridData2(divid,data){

	// ヘッダー

	//headerName=列の題名,field=列のID,width=幅、cellClas=各セルに適用するクラス
	//headerCheckboxSelection=ヘッダーにチェックボックスを付けるか？
	//checkboxSelection=各行にチェックボックスを表示するか？
	//pinned=列固定,suppressMenu=ヘッダー一列目に検索フィールドを出すか？
	//hide=表示、非表示、sortable=ソートができるかできないか
	//filter=ヘッダー2列目の検索フィールドを何にするか？
	
	/*ヘッダーデータをタイプで入れ替える*/
	columnDefs2 = [
		{headerName: "出荷日", field: "col1", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
		{headerName: "届先名", field: "col2", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
		{headerName: "配送方法", field: "col3", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:150,cellClass: ['tl','cellclass'],
		 cellRenderer: (params) => {
				const element = document.createElement('div');
				element.innerHTML = "<select class='content' onchange='noDisabledButton();'>"
			 					  + "<option value=''>配送会社を選択してください。</option>"
			 					  + "<option value='1'>ヤマト運輸</option>"
			 					  + "<option value='2' selected>佐川急便</option>"
			 					  + "<option value='3'>日本郵政</option>"
			 					  + "</select>";

				return element;
			}
		},
		{headerName: "問合せNo", field: "col4", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:140,cellClass: ['tl','cellclass'],editable: true
		 ,cellClass: 
		function(params) {
			//値によって追加するクラスを可変させる
			if(params.value.length != 0){
				return ['tl','cellclass','editClass','noCellFocus'];
			}
			return ['tl','cellclass','editClass','noCellFocus'];
		}},
		{headerName: "出荷個口数", field: "col5", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:120,editable: true
		 ,cellClass: 
		function(params) {
			return ['tr','cellclass','editClass','noCellFocus'];
		}},
		{headerName: "単価", field: "col6", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,editable: true
		 ,cellClass: 
		function(params) {
			return ['tr','cellclass','editClass','noCellFocus'];
		}},
		{headerName: "金額", field: "col7", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,editable: true
		 ,cellClass: 
		function(params) {
			//値によって追加するクラスを可変させる
			return ['tr','cellclass','editClass','noCellFocus'];
		}},
		{headerName: "備考", field: "col8", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:200,cellClass: ['tl','cellclass'],editable: true
		 ,cellClass: 
		function(params) {
			//値によって追加するクラスを可変させる
			if(params.value.length != 0){
				return ['tl','cellclass','editClass','noCellFocus'];
			}
			return ['tl','cellclass','editClass','noCellFocus'];
		}}
	];			
		

	// 行のテストデータ

	rowData2 = [];

	for(var i = 0; i < data;i++){
		
		rowData2.push({col1: '2019-04-08', col2: '(資)村瀬商会', col3: '佐川急便', 
					  col4: '129000000161', col5: '1', col6: '1', 
					  col7: '1', col8: 'テキスト変更テキスト変更'});
	}
			

	// オプションの設定
	gridOptions2 = {
		enableColResize:true,
		enableSorting: true,
		enableFilter: true,
		headerHeight: 30,
		floatingFiltersHeight:30,
		columnDefs: columnDefs2,/*headedrデータ*/
		rowData: rowData2,/*行データ*/
		floatingFilter: true,/*2列目のフィルターのon/off*/
		suppressRowClickSelection:true,/*チェックボックス以外のクリックを認めるか？*/
		rowSelection :'multiple',/*複数選択できるように*/
		localeText: getLocaleText(),/*データ０の時のメッセージ*/
		rowHeight : 40,
		singleClickEdit:true,
		pagination:true,
		paginationPageSize:20
	};

	//   対象となる要素の指定
	var eGridDiv = document.querySelector('#myGrid' + divid);

	//   グリッドの生成
	new agGrid.Grid(eGridDiv, gridOptions2);

	setFunctions(gridOptions2,divid,rowData2);
	
	/*今回の場合はテーブル4*/
	var chkflg4 = false;
	//横スクロールでステータス領域が消えた時に左はじにカラー領域を表示
	$("#myGrid" + divid + " div.ag-body-viewport").scroll(function(e){
		checkboxChange();
	});
	
	gridOptions2.onRowSelected = function(param){
		$(".tableitem__btnarea--rightarea span a").removeClass("disabled");
	}
	

	getAllCnt(rowData2,divid,gridOptions2);
	
	gridOptions2.onCellValueChanged = function(param){
		$("#regist2").removeClass("disabled");
	}

}
function gridWidth_wa3040(owname,coldef,gridname,tablename){
	var ow = $(owname).innerWidth() - 40;
	//Gridの横幅が画面幅以下なら調整する
	var w = 20;
	for(var i = 0 ; i < coldef.length ;i++){
		w += coldef[i].width;
	}

	if(ow > w){
		$(gridname ).css("width","calc(100%)");
		$(tablename).css("width","calc(100% - 60px)");
		
		
	}
	
}

//テーブル作成用ファンクション-入荷実績送信用（モーダル）
function noDisabledButton(){
	$("#regist2").removeClass("disabled");
}

//tab切替
function changeTab(tnum){
	var cnt = 1;
	$("section.tabs > ul.tabs-nav > li.tabs-nav__item").each(function() {
		$("#tab"+ cnt + "-item").addClass("dspnone");
		if($(this)[0].id == "t" + tnum){
			$("#t" + cnt).addClass("tabs-nav__item--active");
			$("#tab"+ cnt + "-item").removeClass("dspnone");
			
			var dt = 0;
			if(tnum == 4){
				dt = 30;
			}
			setGridData1("myGrid" + tnum,dt);

		}else{
			$("#t" + cnt).removeClass("tabs-nav__item--active");
		}
		cnt++;
	});
}


// JavaScript Document
function selectSelected(id){
	//未選択
	if($("#" + id).val() == ""){
		$("#" + id).removeClass("active");
		$("#" + id).removeClass("error");
		$("#" + id).removeClass("disabled");
	//active
	}else if($("#" + id).val() == "1"){
		$("#" + id).addClass("active");
		$("#" + id).removeClass("error");
		$("#" + id).removeClass("disabled");
	//error
	}else if($("#" + id).val() == "2"){
		$("#" + id).removeClass("active");
		$("#" + id).addClass("error");
		$("#" + id).removeClass("disabled");
	//disabled
	}else if($("#" + id).val() == "3"){
		$("#" + id).removeClass("active");
		$("#" + id).removeClass("error");
		$("#" + id).addClass("disabled");
	}
}

var delIndex = "";
//最古登録/修正のopen、close
function openOtherWindow_wa3040(id,idx){
	delIndex = idx;
	$("div#" + id).removeClass("dspnone");
	$("div#" + id).addClass("openotherwindow");
	
	if(id == "otherwindow_all"){
		gridWidth_wa3040(".otherwindow-tablearea",columnDefs2,"#myGrid2", ".otherwindow-tablearea")
		gridHeight(".otherwindow-tablearea",rowData2,"#myGrid2", ".otherwindow-tablearea");	
	}
}
function closeOtherWindow_wa3040(id,obj,idx){
	if(obj.length > 0 && !$("#" + obj).hasClass("disabled")){
		$("div#" + id).removeClass("openotherwindow");
		//display:noneでアニメーションやtransitionを使うと機能しないため
		//○○秒後にdisplay:noneを発動させる
		setTimeout(function(){
			$("div#" + id).addClass("dspnone");		
		}, 300);
		//登録、修正の場合は下記を実行、グリッドの指定行を青色にし2秒後にトランジションで消す
		$(".ag-pinned-left-cols-container .ag-row").each(function(){
			var index = $(this).attr("row");
			if(index == delIndex){
				$(this).addClass("row-update");

				var obj = $(this);
				setTimeout(function(){
					obj.addClass("row-default");
					obj.removeClass("row-update");
				},2000);
			}
		});
		$(".ag-body-container .ag-row").each(function(){
			var index = $(this).attr("row");
			if(index == delIndex){
				$(this).addClass("row-update");
				var obj = $(this);
				setTimeout(function(){
					obj.addClass("row-default");
					obj.removeClass("row-update");
				},2000);
			}
		});
	}else if(obj.length == 0){
		$("div#" + id).removeClass("openotherwindow");
		//display:noneでアニメーションやtransitionを使うと機能しないため
		//○○秒後にdisplay:noneを発動させる
		setTimeout(function(){
			$("div#" + id).addClass("dspnone");		
		}, 300);
	}

}
function checkVal(){
	if($("#select2").val() == ""){
		$("#regist1").addClass("disabled");
	}else{
		$("#regist1").removeClass("disabled");

	}
}