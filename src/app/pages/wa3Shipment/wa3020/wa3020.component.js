

$(function(){

	setGridData1("myGrid1",0);
	
	var sh = $(".search__searcharea").height();
	$(".search__searcharea--title").click(function(){
		if($(".search__searcharea__items").hasClass("dspnone")){
			$(".tableitem__tablearea").css("height","calc(100% - 295px)");
			$(".search__searcharea__items").removeClass("dspnone");
			$(this).removeClass("closed");
		}else{
			$(".search__searcharea__items").addClass("dspnone");
			$(this).addClass("closed");
			$(".tableitem__tablearea").css("height","calc(100% - 135px)");
		}
		spredScrollArrowChange();
	});
	
	$(".arrowdiv").click(function(e){
		//サイドメニューオープン時にスプレッドの高さを調節
		setTimeout(function(){
			/*サイドメニューオープン時*/
			if($(".arrowdiv").hasClass("openarrow")){
				if(sh != $(".search__searcharea").height()){
					if(!$(".search__searcharea__items").hasClass("dspnone")){
						$(".tableitem__tablearea").css("height","calc(100% - 365px)");
					}			
				}			
			/*サイドメニュークローズ時*/
			}else{
				if(!$(".search__searcharea__items").hasClass("dspnone")){
					$(".tableitem__tablearea").css("height","calc(100% - 295px)");
				}else{
					$(".tableitem__tablearea").css("height","calc(100% - 140px)");
				}
			}
			
			spredScrollArrowChange();
		},200);
		
	});	

	//form要素に変更がったらactiveにする
	$("#otherwindow_update input,#otherwindow_update select,#otherwindow_update textarea").change(function(){

		if($(this).prop("type") == "text" || $(this).prop("type") == "date" || $(this)[0].tagName.toLowerCase() == "textarea"){
			$(this).addClass("active");
		}

		$("#regist2").removeClass("disabled");
	});



});


//テーブル作成用ファンクション
var columnDefs1;
var gridOptions1;
var rowData = [];
function setGridData1(divid,data){

	// ヘッダー

	//headerName=列の題名,field=列のID,width=幅、cellClas=各セルに適用するクラス
	//headerCheckboxSelection=ヘッダーにチェックボックスを付けるか？
	//checkboxSelection=各行にチェックボックスを表示するか？
	//pinned=列固定,suppressMenu=ヘッダー一列目に検索フィールドを出すか？
	//hide=表示、非表示、sortable=ソートができるかできないか
	//filter=ヘッダー2列目の検索フィールドを何にするか？
	
	/*ヘッダーデータをタイプで入れ替える*/
	columnDefs1 = [
		/*スクロール時にステータスフィールドが消えたら表示するフィールド*/
		{headerName: "", field: "col99",suppressMenu: true,width:8,
		 cellClass: ['tc','cellclass'],hide:true,pinned: 'left',
			cellStyle: function(params) {
				//ステータス色変化
				if (params.value=='0') {
					return {backgroundColor: 'rgba(255,255,255,0.2)',color: 'rgba(255,255,255,0.2)'};
				}else if (params.value=='1') {
					return {backgroundColor: 'rgba(235,153,175,0.2)',color: 'rgba(235,153,175,0.2)'};
				}else if (params.value=='2') {
					return {backgroundColor: 'rgba(227,230,87,0.2)',color: 'rgba(227,230,87,0.2)'};
				}else if (params.value=='3') {
					return {backgroundColor: 'rgba(201,202,202,0.2)',color: 'rgba(201,202,202,0.2)'};
				} else {
					return null;
				}
			}
		},
		{headerName: "出荷番号", field: "col1", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
		{headerName: "出荷ステータス", field: "col2", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:140,cellClass: ['tc','cellclass'],
			cellStyle: function(params) {
				//ステータス色変化
				if (params.value=='欠品') {
					return {backgroundColor: 'rgba(255,255,255,0.2)',color: 'rgba(217,59,59,1)'};
				}else if (params.value=='未引当') {
					return {backgroundColor: 'rgba(235,153,175,0.2)'};
				}else if (params.value=='引当完了') {
					return {backgroundColor: 'rgba(227,230,87,0.2)'};
				}else if (params.value=='出荷完了') {
					return {backgroundColor: 'rgba(201,202,202,0.2)'};
				} else {
					return null;
				}
			}
		},
		{headerName: "荷主名", field: "col3", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
		{headerName: "出荷指示日", field: "col4", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "届先名", field: "col5", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
		{headerName: "届先郵便番号", field: "col6", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "届先住所1", field: "col7", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:300,cellClass: ['tl','cellclass']},
		{headerName: "届先住所2", field: "col8", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:250,cellClass: ['tl','cellclass']},
		{headerName: "届先電話番号", field: "col9", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
		{headerName: "商品コード", field: "col10", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
		{headerName: "商品名", field: "col11", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
		{headerName: "ロット", field: "col12", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "単位名", field: "col13", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:80,cellClass: ['tl','cellclass']},
		{headerName: "ケース数", field: "col14", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tr','cellclass']},
		{headerName: "入数", field: "col15", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:80,cellClass: ['tr','cellclass']},
		{headerName: "バラ数", field: "col16", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:100,cellClass: ['tr','cellclass']},
		{headerName: "数量", field: "col17", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:80,cellClass: ['tr','cellclass']},
		{headerName: "出荷時摘要", field: "col18", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
		{headerName: "伝票色/入数", field: "col19", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
		{headerName: "伝票サイズ/ケース", field: "col20", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
		{headerName: "トランザクション区分枝", field: "col21", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
		{headerName: "伝票単位", field: "col22", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
		{headerName: "伝票引合", field: "col23", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
		{headerName: "伝票引合区分", field: "col24", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
		{headerName: "伝票原単価", field: "col25", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:120,cellClass: ['tr','cellclass']},
		{headerName: "伝票売単価", field: "col26", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:120,cellClass: ['tr','cellclass']},
		{headerName: "更新日時", field: "col27", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
		{headerName: "登録日時", field: "col28", filter:'text',unSortIcon: true,
		 suppressMenu: true,width:160,cellClass: ['tl','cellclass']}
	];	

	// 行のテストデータ


			

	// オプションの設定
	gridOptions1 = {
		enableColResize:true,
		enableSorting: true,
		enableFilter: true,
		headerHeight: 30,
		floatingFiltersHeight:30,
		columnDefs: columnDefs1,/*headedrデータ*/
		rowData: rowData,/*行データ*/
		floatingFilter: true,/*2列目のフィルターのon/off*/
		suppressRowClickSelection:true,/*チェックボックス以外のクリックを認めるか？*/
		rowSelection :'multiple',/*複数選択できるように*/
		localeText: getLocaleText(),/*データ０の時のメッセージ*/
		rowHeight : 40,
		pagination:true,
		paginationPageSize:20
	};
	


	//   対象となる要素の指定
	var eGridDiv = document.querySelector('#' + divid);

	//   グリッドの生成
	new agGrid.Grid(eGridDiv, gridOptions1);

	//セルクリック時に行を選択状態にする
	gridOptions1.onCellClicked = function(param){
		/*クリックされたセルの取得*/
		var cols = param.colDef.field;
		var index = param.node.childIndex;
		//セルがチェックボックスセルだったら
		if(cols == "col0"){
			//状況により行のセレクトをon/offする
			var node = gridOptions1.api.getRowNode(index);
			if(node.isSelected()){
				node.setSelected(false);

			}else{
				node.setSelected(true);
			}
		}
	}

	gridOptions1.onFilterChanged = function(param){
		//モーダル内の検索インプットへのアクティブクラス付与
		$(".ag-floating-filter-input").each(function(){
			if($(this).val().length == 0){
				$(this).removeClass("active");
			}else{
				$(this).addClass("active");
			}
		});
		
	}
	gridOptions1.onPaginationChanged = function(param){
		spredScrollArrowChange();
	}
	/*今回の場合はテーブル4*/
	var chkflg4 = false;
	//横スクロールでステータス領域が消えた時に左はじにカラー領域を表示
	$("#myGrid1 div.ag-body-viewport").scroll(function(e){
		//今回のステータス位置が185付近なので今回はそこに設定
		if($("#myGrid1 div.ag-body-viewport").scrollLeft() > 255 && !chkflg4 ){
			chkflg4 = true;

			showcol(true);
		}else if($("#myGrid1 div.ag-body-viewport").scrollLeft() <= 255 && chkflg4){
			chkflg4 = false;
			showcol(false);

		}
	});
	//列の表示非表示切り替え
	function showcol(show) {
		gridOptions1.columnApi.setColumnVisible('col99', show);
	}
	

	headerCheckboxChange();
	checkboxChange();
	
	gridWidth(".tableitem",columnDefs1,"#myGrid1", "#table1");
	
	hiddenPagerControl("1",0);
	gridHeight(".tableitem",rowData,"#myGrid1", "#table1");	

}
//pagerの表示件数を変更
function updatePager(){
	var value = $("#select2").val();
    gridOptions1.api.paginationSetPageSize(Number(value));
	checkboxChange();
	setPageSep();
}
//pagerの件数取得
function getAllCnt(){
	$("#allcount").html(rowData.length);
}
var nowPage = 0;
function setGrid(){
	$("p.nodata").html("");
	
	

	for(var i = 0; i < 100;i++){
		rowData.push({col0: i,col99: 1,col1: '20190409-00011', col2: '未引当', col3: '荷主・SEAOS (B)', col4: '2019-04-21',col5:'(資)村瀬商会', col6: '150-0011', col7: '東京都渋谷区恵比寿１丁目１８−１８', col8: '東急不動産恵比寿ビル1F', col9: '03-5791-1171', col10: '18303003-041', col11: 'MERE・ﾜｯﾁ NV', col12: '', col13: 'ｶｰﾄﾝ', col14: '1', col15: '1', col16: '1', col17: '2', col18: '', col19: '', col20: '', col21: '', col22: '', col23: '', col24: '', col25: '', col26: '', col27: '2018-12-21 15:47:51', col28: '2018-12-21 15:47:51'});
		
		rowData.push({col0: i,col99: 2,col1: '20190409-00011', col2: '引当完了', col3: '荷主・SEAOS (B)', col4: '2019-04-21',col5:'(資)村瀬商会', col6: '150-0011', col7: '東京都渋谷区恵比寿１丁目１８−１８', col8: '東急不動産恵比寿ビル1F', col9: '03-5791-1171', col10: '18303003-041', col11: 'MERE・ﾜｯﾁ NV', col12: '', col13: 'ｶｰﾄﾝ', col14: '1', col15: '1', col16: '1', col17: '2', col18: '', col19: '', col20: '', col21: '', col22: '', col23: '', col24: '', col25: '', col26: '', col27: '2018-12-21 15:47:51', col28: '2018-12-21 15:47:51'});		
		
		rowData.push({col0: i,col99: 3,col1: '20190409-00011', col2: '出荷完了', col3: '荷主・SEAOS (B)', col4: '2019-04-21',col5:'(資)村瀬商会', col6: '150-0011', col7: '東京都渋谷区恵比寿１丁目１８−１８', col8: '東急不動産恵比寿ビル1F', col9: '03-5791-1171', col10: '18303003-041', col11: 'MERE・ﾜｯﾁ NV', col12: '', col13: 'ｶｰﾄﾝ', col14: '1', col15: '1', col16: '1', col17: '2', col18: '', col19: '', col20: '', col21: '', col22: '', col23: '', col24: '', col25: '', col26: '', col27: '2018-12-21 15:47:51', col28: '2018-12-21 15:47:51'});
	
	
		rowData.push({col0: i,col99: 0,col1: '20190409-00011', col2: '欠品', col3: '荷主・SEAOS (B)', col4: '2019-04-21',col5:'(資)村瀬商会', col6: '150-0011', col7: '東京都渋谷区恵比寿１丁目１８−１８', col8: '東急不動産恵比寿ビル1F', col9: '03-5791-1171', col10: '18303003-041', col11: 'MERE・ﾜｯﾁ NV', col12: '', col13: 'ｶｰﾄﾝ', col14: '1', col15: '1', col16: '1', col17: '2', col18: '', col19: '', col20: '', col21: '', col22: '', col23: '', col24: '', col25: '', col26: '', col27: '2018-12-21 15:47:51', col28: '2018-12-21 15:47:51'});	
	}	
	
	gridOptions1.api.updateRowData({add: rowData});
	checkboxChange();
	gridHeight(".tableitem",rowData,"#myGrid1", "#table1");	

	spredScrollArrowChange();
	getAllCnt();
	
	if(rowData.length > 0){
		$(".tableitem__tablearea__pagerItem .disabled").removeClass("disabled");
		//各矢印クリックでページ移動
		$(".tableitem__tablearea__pagerItem i").click(function(){
			if($(this).prop("id") == "first" && !$(this).hasClass("disabled")){
				gridOptions1.api.paginationGoToFirstPage();
			}else if($(this).prop("id") == "prev" && !$(this).hasClass("disabled")){
				gridOptions1.api.paginationGoToPreviousPage();
			}else if($(this).prop("id") == "next" && !$(this).hasClass("disabled")){
				gridOptions1.api.paginationGoToNextPage();
			}else if($(this).prop("id") == "last" && !$(this).hasClass("disabled")){
				gridOptions1.api.paginationGoToLastPage()
			}
			setPageSep();
		});
		setPageSep();
	}
	hiddenPagerControl("1",1);

	
}

//pagerの真ん中の構築 
function setPageSep(){
	
	var h = "<span class='tableitem__tablearea__pagerItem__nowpage--desc--big'>" + (gridOptions1.api.paginationGetCurrentPage() + 1) + "</span> /" + gridOptions1.api.paginationGetTotalPages() + " ";
	$(".tableitem__tablearea__pagerItem__nowpage--desc").html(h);
	//1ページ目では前に戻れないようにdisabled
	if(gridOptions1.api.paginationGetCurrentPage() + 1 == 1){
	   $(".tableitem__tablearea__pagerItem i#first").addClass("disabled");
	   $(".tableitem__tablearea__pagerItem i#prev").addClass("disabled");
	}else{
	   $(".tableitem__tablearea__pagerItem i#first").removeClass("disabled");
	   $(".tableitem__tablearea__pagerItem i#prev").removeClass("disabled");
	}
	
	//LASTページ目では次にいけないようにdisabled
	if(gridOptions1.api.paginationGetCurrentPage() + 1 == gridOptions1.api.paginationGetTotalPages()){
	   $(".tableitem__tablearea__pagerItem i#next").addClass("disabled");
	   $(".tableitem__tablearea__pagerItem i#last").addClass("disabled");
	}else{
	   $(".tableitem__tablearea__pagerItem i#next").removeClass("disabled");
	   $(".tableitem__tablearea__pagerItem i#last").removeClass("disabled");
	}
	
}
//スプレッドの矢印の構築・再構築
function spredScrollArrowChange(){
	if(rowData.length > 0){
		//初期表示しているすべて（TAB4）のみロード時に横スクロールの場所設定を行う
		var parentArea = $("#myGrid1 div.ag-body-container").height();

		var tbarea = $("#myGrid1 div.ag-body-viewport").scrollTop() + $("#myGrid1 div.ag-body-viewport").height();
		if(parentArea - tbarea > 0){
			//スクロールバーがある場合の処理
			$("#table1 div.scrollArrow").css("right","16px");
		}else{
			//スクロールバーがない場合の処理
			$("#table1 div.scrollArrow").css("right","0px");

		}

		//初期表示しているすべて（TAB4）のみロード時に横スクロールの表示設定を行う
		var parentArea = $("#myGrid1 div.ag-body-container").width();

		var tbarea = $("#myGrid1 div.ag-body-viewport").scrollTop() + $("#myGrid1 div.ag-body-viewport").width();

		if(parentArea - tbarea > 0){
			//スクロールバーがある場合の処理
			$("#table1 div.scrollArrow").removeClass("dspnone");
			var height = $("#myGrid1").innerHeight() - 113;
			var dspheight = $("#myGrid1 .ag-body-container").innerHeight() ;

			var gh = 0;
			if(height < dspheight){
				gh = height;
			}else{
				gh = dspheight;
			}

			$("#table1 div.scrollArrow").css("height",gh + "px");

		}else{
			//スクロールバーがない場合の処理
			$("#table1 div.scrollArrow").addClass("dspnone");

		}



		/*TAB4のテーブル内の→マウスオーバー挙動*/
		var tm1 = 0;
		var tm2 = 0;
		var scrollMax = 0;
		var dspflg1 = true;
		//スクロールイベント
		$("#table1 div.scrollArrow").hover(function(){
			tm2 = setTimeout(function(){
				tm1 = setInterval(function(){
					//スクロールがMAXになったら表示を消すためにここで値を取得
					scrollMax = $("#myGrid1 div.ag-body-container").width() - $("#myGrid1 div.ag-body-viewport").width();

					$("#myGrid1 div.ag-body-viewport").scrollLeft( $("#myGrid1 div.ag-body-viewport").scrollLeft() + 1);

					if(scrollMax <= $("#myGrid1 div.ag-body-viewport").scrollLeft() && dspflg1){
						$("#table1 div.scrollArrow").addClass("dspnone");
						dspflg1 = false;
						clearTimeout(tm2);
						clearInterval(tm1);
					}
				},0);
			},300);
		},function(){
			clearTimeout(tm2);
			clearInterval(tm1);
		});

		//scrollArrowが消えているときにscrollMAXと値が変わったら再表示
		$("#myGrid1 div.ag-body-viewport").scroll(function(){

			scrollMax = $("#myGrid1 div.ag-body-container").width() - $("#myGrid1 div.ag-body-viewport").width();

			if(scrollMax <= $("#myGrid1 div.ag-body-viewport").scrollLeft()){
				$("#table1 div.scrollArrow").addClass("dspnone");
				dspflg1 = false;
			}

			if(scrollMax > $("#myGrid1 div.ag-body-viewport").scrollLeft() && !dspflg1){
				$("#table1 div.scrollArrow").removeClass("dspnone");
				dspflg1 = true;
			}
		});	
	}
}
