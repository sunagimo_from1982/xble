$(function(){

	//各tab内のモーダル生成
	setGridData1("1",7);
	
	setGridData2("2",30)


});


var columnDefs1;
var gridOptions1;
var rowData1 = [];
function setGridData1(divid,data){

	// ヘッダー

	//headerName=列の題名,field=列のID,width=幅、cellClas=各セルに適用するクラス
	//headerCheckboxSelection=ヘッダーにチェックボックスを付けるか？
	//checkboxSelection=各行にチェックボックスを表示するか？
	//pinned=列固定,suppressMenu=ヘッダー一列目に検索フィールドを出すか？
	//hide=表示、非表示、sortable=ソートができるかできないか
	//filter=ヘッダー2列目の検索フィールドを何にするか？
	
	/*ヘッダーデータをタイプで入れ替える*/
	
	columnDefs1 = [
					{headerName: "郵便番号", field: "col1", filter:'text',unSortIcon: true,lockPosition:true,
					 suppressMenu: true,width:170,cellClass: ['tl','cellclass'],pinned: 'left',
					 cellRenderer: (params) => {
							const element = document.createElement('div');
							element.innerHTML = "<div class='itemcode'><a class='btn_secondary' href='javascript:openOtherWindow_wa3016("+ params.rowIndex+ ");'>修正</a><p>" + params.value + "</p></div>";
							return element;
						}},
					{headerName: "住所1", field: "col2", filter:'text',unSortIcon: true,
					 suppressMenu: true,width:300,cellClass: ['tl','cellclass']},
					{headerName: "住所2", field: "col3", filter:'text',unSortIcon: true,
					 suppressMenu: true,width:250,cellClass: ['tl','cellclass']},
					{headerName: "住所3", field: "col4", filter:'text',unSortIcon: true,
					 suppressMenu: true,width:200,cellClass: ['tl','cellclass']},
					{headerName: "配送会社", field: "col5", filter:'text',unSortIcon: true,
					 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
					{headerName: "荷主ID", field: "col6", filter:'text',unSortIcon: true,
					 suppressMenu: true,width:80,cellClass: ['tl','cellclass']},
					{headerName: "荷主コード", field: "col7", filter:'text',unSortIcon: true,
					 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
					{headerName: "出荷番号", field: "col8", filter:'text',unSortIcon: true,
					 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
					{headerName: "受注番号", field: "col9", filter:'text',unSortIcon: true,
					 suppressMenu: true,width:140,cellClass: ['tl','cellclass']}
				];


	// 行のテストデータ

	for(var i = 0; i < data;i++){
		
		rowData1.push({col0:i,col1: "151-0222" , col2: '東京都渋谷区恵比寿１丁目１８−１８-' + i,col3: '東急不動産恵比寿ビル3F',col4: '', col5: '日本郵便', col6: '10', col7: '000011', col8: '20181227-00034', col9: 'S3-BARA'});
	}

			
	// オプションの設定
	gridOptions1 = {
		enableColResize:true,
		enableSorting: true,
		enableFilter: true,
		headerHeight: 30,
		floatingFiltersHeight:30,
		columnDefs: columnDefs1,/*headedrデータ*/
		rowData: rowData1,/*行データ*/
		floatingFilter: true,/*2列目のフィルターのon/off*/
		suppressRowClickSelection:true,/*チェックボックス以外のクリックを認めるか？*/
		rowSelection :'multiple',/*複数選択できるように*/
		localeText: getLocaleText(),/*データ０の時のメッセージ*/
		rowHeight : 40,
		pagination:true,
		paginationPageSize:rowData1.length
	};
	


	//   対象となる要素の指定
	var eGridDiv = document.querySelector('#myGrid' + divid);

	//   グリッドの生成
	new agGrid.Grid(eGridDiv, gridOptions1);

	setFunctions(gridOptions1,divid,rowData1);
	
	/*今回の場合はテーブル4*/
	var chkflg4 = false;
	//横スクロールでステータス領域が消えた時に左はじにカラー領域を表示
	$("#myGrid" + divid + " div.ag-body-viewport").scroll(function(e){
		checkboxChange();
	});
	
	

	getAllCnt(rowData1,divid,gridOptions1);
	
	
	spredScrollArrowChange(divid);
	
	gridWidth(".tableitem",columnDefs1,"#myGrid" + divid, "#table" + divid);
	gridHeight(".tableitem",rowData1,"#myGrid" + divid, "#table" + divid);	
	
}
function setGrid1(divid,del){
	rowData1.splice(del, 1);
	
	if(rowData1.length == 0){
		
		gridOptions1.api.setRowData([]);
		$(".tableitem .nodata").html("修正が必要なデータはありません。");
		$(".notice-desc").html("");
		
		hiddenPagerControl(divid,0);
		gridHeight(".tableitem",rowData1,"#myGrid" + divid, "#table" + divid,1);	
	}else{
		gridOptions1.api.updateRowData({add: rowData1});
		gridOptions1.api.paginationSetPageSize(rowData1.length);

		getAllCnt(rowData1,divid,gridOptions1);

		gridHeight(".tableitem",rowData1,"#myGrid" + divid, "#table" + divid);	

		spredScrollArrowChange(divid);
	}

	
}


function setFunctions(gridOptions,divid,rowData){
		//セルクリック時に行を選択状態にする
	gridOptions.onCellClicked = function(param){
		/*クリックされたセルの取得*/
		var cols = param.colDef.field;
		var index = param.node.childIndex;
		//セルがチェックボックスセルだったら
		if(cols == "col0"){
			//状況により行のセレクトをon/offする
			var node = gridOptions.api.getRowNode(index);
			if(node.isSelected()){
				node.setSelected(false);

			}else{
				node.setSelected(true);
			}
		}
	}
	//ag-gridの仕様電話再描画されるためソートするとチェックボックスがデフォルトデザインに戻ってしまうためそれを防ぐ
	gridOptions.onSortChanged = function(param){
		checkboxChange();
	}

	gridOptions.onFilterChanged = function(param){
		//モーダル内の検索インプットへのアクティブクラス付与
		$(".ag-floating-filter-input").each(function(){
			if($(this).val().length == 0){
				$(this).removeClass("active");
			}else{
				$(this).addClass("active");
			}
		});
		checkboxChange();
		
	}
	gridOptions.onSelectionChanged= function(param){
		getAllCnt(rowData,divid,gridOptions);
		
	}
}

//スプレッドの矢印の構築・再構築
function spredScrollArrowChange(divid){
	//初期表示しているすべて（TAB4）のみロード時に横スクロールの場所設定を行う
	var parentArea = $("#myGrid" + divid + " div.ag-body-container").height();

	var tbarea = $("#myGrid" + divid + " div.ag-body-viewport").scrollTop() + $("#myGrid" + divid + " div.ag-body-viewport").height();
	if(parentArea - tbarea > 0){
		//スクロールバーがある場合の処理
		$("#table" + divid + " div.scrollArrow").css("right","16px");
	}else{
		//スクロールバーがない場合の処理
		$("#table" + divid + " div.scrollArrow").css("right","0px");

	}

	//初期表示しているすべて（TAB4）のみロード時に横スクロールの表示設定を行う
	var parentArea = $("#myGrid" + divid + " div.ag-body-container").width();

	var tbarea = $("#myGrid" + divid + " div.ag-body-viewport").scrollTop() + $("#myGrid" + divid + " div.ag-body-viewport").width();

	if(parentArea - tbarea > 0){
		//スクロールバーがある場合の処理
		$("#table" + divid + " div.scrollArrow").removeClass("dspnone");
		var height = $("#myGrid" + divid + "").innerHeight() - 111;
		var dspheight = $("#myGrid" + divid + " .ag-body-container").innerHeight() ;

		var gh = 0;
		if(height < dspheight){
			gh = height;
		}else{
			gh = dspheight;
		}

		$("#table" + divid + " div.scrollArrow").css("height",gh + "px");

	}else{
		//スクロールバーがない場合の処理
		$("#table" + divid + " div.scrollArrow").addClass("dspnone");

	}



	/*TAB4のテーブル内の→マウスオーバー挙動*/
	var tm1 = 0;
	var tm2 = 0;
	var scrollMax = 0;
	var dspflg1 = true;
	//スクロールイベント
	$("#table" + divid + " div.scrollArrow").hover(function(){
		tm2 = setTimeout(function(){
			tm1 = setInterval(function(){
				//スクロールがMAXになったら表示を消すためにここで値を取得
				scrollMax = $("#myGrid" + divid + " div.ag-body-container").width() - $("#myGrid" + divid + " div.ag-body-viewport").width();

				$("#myGrid" + divid + " div.ag-body-viewport").scrollLeft( $("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() + 2);

				if(scrollMax <= $("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() && dspflg1){
					$("#table" + divid + " div.scrollArrow").addClass("dspnone");
					dspflg1 = false;
					clearTimeout(tm2);
					clearInterval(tm1);
				}
			},0);
		},300);
	},function(){
		clearTimeout(tm2);
		clearInterval(tm1);
	});

	//scrollArrowが消えているときにscrollMAXと値が変わったら再表示
	$("#myGrid" + divid + " div.ag-body-viewport").scroll(function(){

		scrollMax = $("#myGrid" + divid + " div.ag-body-container").width() - $("#myGrid" + divid + " div.ag-body-viewport").width();

		if(scrollMax <= $("#myGrid" + divid + " div.ag-body-viewport").scrollLeft()){
			$("#table" + divid + " div.scrollArrow").addClass("dspnone");
			dspflg1 = false;
		}

		if(scrollMax > $("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() && !dspflg1){
			$("#table" + divid + " div.scrollArrow").removeClass("dspnone");
			dspflg1 = true;
		}
	});	
}
//テーブル作成用ファンクション-入荷実績登録用
var columnDefs2;
var gridOptions2;
var rowData2;
function setGridData2(divid,data){

	// ヘッダー

	//headerName=列の題名,field=列のID,width=幅、cellClas=各セルに適用するクラス
	//headerCheckboxSelection=ヘッダーにチェックボックスを付けるか？
	//checkboxSelection=各行にチェックボックスを表示するか？
	//pinned=列固定,suppressMenu=ヘッダー一列目に検索フィールドを出すか？
	//hide=表示、非表示、sortable=ソートができるかできないか
	//filter=ヘッダー2列目の検索フィールドを何にするか？
	
	/*ヘッダーデータをタイプで入れ替える*/
	columnDefs2 = [
		{headerName: "", field: "col0", width: 30,cellClass: ['tc','cl_w','noCellFocus'],
		 checkboxSelection: true,suppressMenu: true,
		 lockPosition:true,pinned: 'left'
		},
		{headerName: "郵便番号", field: "col1",unSortIcon: true,
		 suppressMenu: true,sortable:true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "都道府県", field: "col2",unSortIcon: true,
		 suppressMenu: true,sortable:true,width:150,cellClass: ['tl','cellclass']},
		{headerName: "市区町村名", field: "col3",unSortIcon: true,
		 suppressMenu: true,sortable:true,width:150,cellClass: ['tl','cellclass']},
		{headerName: "町域", field: "col4",unSortIcon: true,
		 suppressMenu: true,sortable:true,width:150,cellClass: ['tl','cellclass']},
		{headerName: "仕分けコード", field: "col5",unSortIcon: true,
		 suppressMenu: true,sortable:true,width:150,cellClass: ['tl','cellclass']}
	];			
		

	// 行のテストデータ

	rowData2 = [];

	/*for(var i = 0; i < data;i++){
		
		rowData2.push({col0:i,col1: "00000005" , col2: '村瀬商会 熱田DC', col3: '東京都渋谷区1', 
					  col4: '恵比寿AAS007', col5: 'TEST001', col6: 'TEST001', col7: '086-234-2125', col8: "086-234-2125",col9: "2019-04-01 19:21:56", col10: 'hama001', col11: '2019-04-01 19:21:56', col12: 'hama001'});
	}*/
			

	// オプションの設定
	gridOptions2 = {
		enableColResize:true,
		enableSorting: true,
		enableFilter: true,
		headerHeight: 30,
		floatingFiltersHeight:30,
		columnDefs: columnDefs2,/*headedrデータ*/
		rowData: rowData2,/*行データ*/
		floatingFilter: true,/*2列目のフィルターのon/off*/
		suppressRowClickSelection:true,/*チェックボックス以外のクリックを認めるか？*/
		localeText: getLocaleText(),/*データ０の時のメッセージ*/
		rowHeight : 40,
		pagination:true,
		paginationPageSize:20
	};

	//   対象となる要素の指定
	var eGridDiv = document.querySelector('#myGrid' + divid);

	//   グリッドの生成
	new agGrid.Grid(eGridDiv, gridOptions2);


	//セルクリック時に行を選択状態にする
	gridOptions2.onCellClicked = function(param){
		/*クリックされたセルの取得*/
		var cols = param.colDef.field;
		var index = param.rowIndex;
		//セルがチェックボックスセルだったら
		if(cols == "col0"){
			//状況により行のセレクトをon/offする
			var node = gridOptions2.api.getRowNode(index);
			if(node.isSelected()){
				node.setSelected(false);

			}else{
				node.setSelected(true);
			}
		}
	}
	gridOptions2.onRowSelected = function(param){
		$(".otherwindow .otherwindow-header a").removeClass("disabled");
	}
	//ag-gridの仕様電話再描画されるためソートするとチェックボックスがデフォルトデザインに戻ってしまうためそれを防ぐ
	gridOptions2.onSortChanged = function(param){
		checkboxChange_otherwindow();
	}
	gridOptions2.onFilterChanged = function(param){
		//モーダル内の検索インプットへのアクティブクラス付与
		$(".ag-floating-filter-input").each(function(){
			if($(this).val().length == 0){
				$(this).removeClass("active");
			}else{
				$(this).addClass("active");
			}
		});
		checkboxChange_otherwindow();
	}
	/*今回の場合はテーブル4*/
	var chkflg4 = false;
	//横スクロールでステータス領域が消えた時に左はじにカラー領域を表示
	$("#myGrid" + divid + " div.ag-body-viewport").scroll(function(e){
		checkboxChange_otherwindow();
	});
	gridOptions2.onSelectionChanged= function(param){
		getAllCnt(rowData2,2,gridOptions2);
		
	}
	
	getAllCnt(rowData2,2,gridOptions2);
	
	checkboxChange_otherwindow();

	gridWidth(".otherwindow",columnDefs2,"#myGrid" + divid, ".otherwindow-tablearea")

	hiddenPagerControl(2,0);
	gridHeight_wa3016(".otherwindow-tablearea",rowData2,"#myGrid" + divid, ".otherwindow-tablearea");			
}


function setGrid2(data){
	
	
	rowData2 = [];

	for(var i = 0; i < data;i++){
		rowData2.push({col0: i,col1: '151-0001', col2: '東京都', col3: '渋谷区',col4:'恵比寿1-18-18', col5: '7-011-7011'});
	}	
	
	gridOptions2.api.updateRowData({add: rowData2});
    gridOptions2.api.paginationSetPageSize(rowData2.length);
	
	
	checkboxChange_otherwindow();
	$(".otherwindow p.nodata").html("");
	getAllCnt(rowData2,2,gridOptions2);
	
	hiddenPagerControl(2,1);	
	gridHeight_wa3016(".otherwindow-tablearea",rowData2,"#myGrid2", ".otherwindow-tablearea");		
}



//pagerの件数取得
function getAllCnt(rowData,divid,gridOptions){
	$("#allcount" + divid).html(rowData.length);
	if($("#selectcount" + divid)[0]){
		$("#selectcount" + divid).html(gridOptions.api.getSelectedRows().length);
	}
}

function setGridData_CheckBox(divid,data){
	var r = getRowData(divid,30);
	if(divid == 1){
		gridOptions1_1.api.setRowData([]);
		gridOptions1_1.api.updateRowData({add: r});
		getAllCnt(r,divid,gridOptions1_1);	
	}else if(divid == 3){
		gridOptions1_3.api.setRowData([]);
		gridOptions1_3.api.updateRowData({add: r});
		getAllCnt(r,divid,gridOptions1_3);
		setPageSep(gridOptions1_3,divid);
	}else if(divid == 4){
		gridOptions1_4.api.setRowData([]);
		gridOptions1_4.api.updateRowData({add: r});
		getAllCnt(r,divid,gridOptions1_4);
		setPageSep(gridOptions1_4,divid)
	}
	
	checkboxChange();

	
}
//modalはラジオボタンにする
function checkboxChange_otherwindow(){
	$(".otherwindow .ag-row .ag-selection-checkbox img:nth-child(1)").attr("src","../../../../assets/images/radio_checked.png");
	$(".otherwindow .ag-row .ag-selection-checkbox img:nth-child(2)").attr("src","../../../../assets/images/radio_body.png");
	$(".otherwindow .ag-row .ag-selection-checkbox img:nth-child(1)").attr("src","../../../../assets/images/radio_checked.png");
	$(".otherwindow .ag-row .ag-selection-checkbox img:nth-child(2)").attr("src","../../../../assets/images/radio_body.png");

}

//上からせりあがるウィンドウを閉じる
//グリッドに追加トランジションをかける
var deleteIndex = -1;
function closeOtherWindow_wa3016(){
	$("div.otherwindow").removeClass("openotherwindow");
	
	setTimeout(function(){
		$("div.otherwindow").addClass("dspnone");		
	}, 300);
	
	if(deleteIndex != -1){
		$("#myGrid1 .ag-body-container .ag-row").each(function(){
			var index = $(this).attr("row");
			if(index == deleteIndex){
				setGrid1(1,deleteIndex);
			}
		});
		//登録、修正の場合は下記を実行、グリッドの指定行を青色にし2秒後にトランジションで消す
/*		$("#myGrid1 .ag-pinned-left-cols-container .ag-row").each(function(){
			var index = $(this).attr("row");

			if(index == deleteIndex){
				$(this).addClass("row-update");

				var obj = $(this);
				setTimeout(function(){
					obj.addClass("row-default");
					obj.removeClass("row-update");
				},2000);
			}
		});

		$("#myGrid1 .ag-body-container .ag-row").each(function(){
			var index = $(this).attr("row");
			if(index == deleteIndex){
				$(this).addClass("row-update");
				var obj = $(this);
				setTimeout(function(){
					obj.addClass("row-default");
					obj.removeClass("row-update");
					
					var res = gridOptions1.api.setRowData([]);
					
					setGrid1(1,deleteIndex);
					

				},2000);
			}
		});*/
		
		
		
	}
	
}

//上にせりあがるwindowのdisabled解除
function openOtherWindow_wa3016(index){
	openOtherWindow();
	deleteIndex = index;
	gridWidth(".otherwindow",columnDefs2,"#myGrid2", ".otherwindow-tablearea")
	gridHeight_wa3016(".otherwindow-tablearea",rowData2,"#myGrid2", ".otherwindow-tablearea");			
}

function gridHeight_wa3016(owname,rowdata,gridname,tablename,flg1 = 0,flg2 = 0){
	$(tablename).css("height","calc(100vh - 310px)");
	var oh = $(owname).innerHeight();
	//Gridの横幅が画面幅以下なら調整する
	var h = 95;

	
	if ($(gridname + " .ag-paging-panel").css('display') == 'none' || flg1 == 1) {
		h = 55;
	}	
	for(var i = 0 ; i < rowdata.length ;i++){
		h += 40;
	}

	//初期表示しているすべて（TAB4）のみロード時に横スクロールの表示設定を行う
	var parentArea = $(gridname + " div.ag-body-container").width();

	var tbarea = $(gridname + " div.ag-body-viewport").scrollTop() + $(gridname + " div.ag-body-viewport").width();

	if(parentArea - tbarea > 0 && rowdata.length != 0){

		h += 17;
	}

	if(oh > h){
		if(rowdata.length != 0){

			h += 7;
		}else{
			h += 5;
		}
		$(gridname ).css("height",h + "px");
		$(tablename).css("height",h + "px");
	}else{
		$(gridname ).css("height","");
		$(tablename).css("height","");
	}
}

function changeTab(tnum){
	var cnt = 1;
	$(".otherwindow-search__tab--list").each(function() {
		$("#ot-tab"+ cnt + "-item").addClass("dspnone");
		if($(this)[0].id == "ot-tab" + tnum){
			$("#ot-tab" + cnt).addClass("active");
			$("#ot-tab"+ cnt + "-item").removeClass("dspnone");
			

		}else{
			$("#ot-tab" + cnt).removeClass("active");
		}
		cnt++;
	});

	
}