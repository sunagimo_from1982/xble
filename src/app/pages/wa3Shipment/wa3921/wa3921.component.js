$(function(){

	//アップロード（input=file）クリックし、ファイルを選択したときにファイル名を表示するスクリプト
	if($("#upload")){
		setInterval(function(){

			if($("#upload").val().length == 0){
				$("#filename").html("ファイルが選択されていません。");
			}else{
				$("#filename").html("<span><img class='deletebtn' src='../../../../assets/images/delete.png'></span>ファイル：" + $("#upload")[0].files[0].name);
				
				$("#upload_btn").removeClass("disabled");
			}
		},1000);
	}
	
});
function changeTab(tnum){
	var cnt = 1;
	$("section.tabs > ul.tabs-nav > li.tabs-nav__item").each(function() {
		$("#tab"+ cnt + "-item").addClass("dspnone");
		if($(this)[0].id == "t" + tnum){
			$("#t" + cnt).addClass("tabs-nav__item--active");
			$("#tab"+ cnt + "-item").removeClass("dspnone");
			
			var dt = 0;
			if(tnum == 4){
				dt = 30;
			}

		}else{
			$("#t" + cnt).removeClass("tabs-nav__item--active");
		}
		cnt++;
	});

	
}

function showMessage(){
	if(!$("#upload_btn").hasClass("disabled")){
		showMessageBox('ファイルのアップロードが完了しました。');
		$("#upload").val("");
		$("#upload_btn").addClass("disabled")
	}
}