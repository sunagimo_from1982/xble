$(function(){

	//各tab内のモーダル生成
	setGridData1("1",30);
	//modalgrid
	setGridData2("5",30);

	
	$(".otherwindow-exclamation__items").click(function(){
		if($(this).prop("id") == "error1" || $(this).prop("id") == "error3"){
			location.href = "#";
		}else{
			location.href = "#";
		}
	});


});


var columnDefs1_1;
var gridOptions1_1;
var rowData1_1 = [];
function setGridData1_1(divid,data){

	// ヘッダー

	//headerName=列の題名,field=列のID,width=幅、cellClas=各セルに適用するクラス
	//headerCheckboxSelection=ヘッダーにチェックボックスを付けるか？
	//checkboxSelection=各行にチェックボックスを表示するか？
	//pinned=列固定,suppressMenu=ヘッダー一列目に検索フィールドを出すか？
	//hide=表示、非表示、sortable=ソートができるかできないか
	//filter=ヘッダー2列目の検索フィールドを何にするか？
	
	/*ヘッダーデータをタイプで入れ替える*/
	
	columnDefs1_1 = getColmnDefs(divid);


	// 行のテストデータ

	rowData1_1 = getRowData(divid,data);


			
	// オプションの設定
	gridOptions1_1 = {
		enableColResize:true,
		enableSorting: true,
		enableFilter: true,
		headerHeight: 30,
		floatingFiltersHeight:30,
		columnDefs: columnDefs1_1,/*headedrデータ*/
		rowData: rowData1_1,/*行データ*/
		floatingFilter: true,/*2列目のフィルターのon/off*/
		suppressRowClickSelection:true,/*チェックボックス以外のクリックを認めるか？*/
		rowSelection :'multiple',/*複数選択できるように*/
		localeText: getLocaleText(),/*データ０の時のメッセージ*/
		rowHeight : 40,
		pagination:true,
		paginationPageSize:rowData1_1.length
	};
	


	//   対象となる要素の指定
	var eGridDiv = document.querySelector('#myGrid' + divid);

	//   グリッドの生成
	new agGrid.Grid(eGridDiv, gridOptions1_1);

	setFunctions(gridOptions1_1,divid,rowData1_1);
	
	/*今回の場合はテーブル4*/
	var chkflg4 = false;
	//横スクロールでステータス領域が消えた時に左はじにカラー領域を表示
	$("#myGrid" + divid + " div.ag-body-viewport").scroll(function(e){
		//今回のステータス位置が185付近なので今回はそこに設定
		if($("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() > 155 && !chkflg4 ){
			chkflg4 = true;

			showcol(true);
		}else if($("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() <= 155 && chkflg4){
			chkflg4 = false;
			showcol(false);

		}
		checkboxChange();
	});
	//列の表示非表示切り替え
	function showcol(show) {
		gridOptions1_1.columnApi.setColumnVisible('col99', show);
	}
	gridOptions1_1.onRowSelected = function(param){
		$("#tab1-item .tableitem__btnarea--rightarea span a").removeClass("disabled");
		$("#tab1-item .tableitem__btnarea--rightarea__select span").removeClass("disabled");
		
	}
	
	

	getAllCnt(rowData1_1,divid,gridOptions1_1);
	
	
	headerCheckboxChange();
	checkboxChange();
	spredScrollArrowChange(divid);
}



var columnDefs1_3;
var gridOptions1_3;
var rowData1_3 = [];
function setGridData1_3(divid,data){

	// ヘッダー

	//headerName=列の題名,field=列のID,width=幅、cellClas=各セルに適用するクラス
	//headerCheckboxSelection=ヘッダーにチェックボックスを付けるか？
	//checkboxSelection=各行にチェックボックスを表示するか？
	//pinned=列固定,suppressMenu=ヘッダー一列目に検索フィールドを出すか？
	//hide=表示、非表示、sortable=ソートができるかできないか
	//filter=ヘッダー2列目の検索フィールドを何にするか？
	
	/*ヘッダーデータをタイプで入れ替える*/
	
	columnDefs1_3 = getColmnDefs(divid);


	// 行のテストデータ

	rowData1_3 = getRowData(divid,data);


			
	// オプションの設定
	gridOptions1_3 = {
		enableColResize:true,
		enableSorting: true,
		enableFilter: true,
		headerHeight: 30,
		floatingFiltersHeight:30,
		columnDefs: columnDefs1_3,/*headedrデータ*/
		rowData: rowData1_3,/*行データ*/
		floatingFilter: true,/*2列目のフィルターのon/off*/
		suppressRowClickSelection:true,/*チェックボックス以外のクリックを認めるか？*/
		rowSelection :'multiple',/*複数選択できるように*/
		localeText: getLocaleText(),/*データ０の時のメッセージ*/
		rowHeight : 40,
		pagination:true,
		paginationPageSize:20
	};
	


	//   対象となる要素の指定
	var eGridDiv = document.querySelector('#myGrid' + divid);

	//   グリッドの生成
	new agGrid.Grid(eGridDiv, gridOptions1_3);

	setFunctions(gridOptions1_3,divid,rowData1_3);
	
	/*今回の場合はテーブル4*/
	var chkflg4 = false;
	//横スクロールでステータス領域が消えた時に左はじにカラー領域を表示
	$("#myGrid" + divid + " div.ag-body-viewport").scroll(function(e){
		//今回のステータス位置が185付近なので今回はそこに設定
		if($("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() > 155 && !chkflg4 ){
			chkflg4 = true;

			showcol(true);
		}else if($("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() <= 155 && chkflg4){
			chkflg4 = false;
			showcol(false);

		}
		checkboxChange();
	});
	//列の表示非表示切り替え
	function showcol(show) {
		gridOptions1_3.columnApi.setColumnVisible('col99', show);
	}
	
	gridOptions1_3.onRowSelected = function(param){
		$("#tab3-item .tableitem__btnarea--rightarea span a").removeClass("disabled");		
		$("#tab3-item .tableitem__btnarea--rightarea__select span").removeClass("disabled");
	}
	

	getAllCnt(rowData1_3,divid,gridOptions1_3);
	
	
	headerCheckboxChange();
	checkboxChange();
	spredScrollArrowChange(divid);
}


var columnDefs1_4;
var gridOptions1_4;
var rowData1_4 = [];
function setGridData1_4(divid,data){

	// ヘッダー

	//headerName=列の題名,field=列のID,width=幅、cellClas=各セルに適用するクラス
	//headerCheckboxSelection=ヘッダーにチェックボックスを付けるか？
	//checkboxSelection=各行にチェックボックスを表示するか？
	//pinned=列固定,suppressMenu=ヘッダー一列目に検索フィールドを出すか？
	//hide=表示、非表示、sortable=ソートができるかできないか
	//filter=ヘッダー2列目の検索フィールドを何にするか？
	
	/*ヘッダーデータをタイプで入れ替える*/
	
	columnDefs1_4 = getColmnDefs(divid);


	// 行のテストデータ

	rowData1_4 = getRowData(divid,data);


			
	// オプションの設定
	gridOptions1_4 = {
		enableColResize:true,
		enableSorting: true,
		enableFilter: true,
		headerHeight: 30,
		floatingFiltersHeight:30,
		columnDefs: columnDefs1_4,/*headedrデータ*/
		rowData: rowData1_4,/*行データ*/
		floatingFilter: true,/*2列目のフィルターのon/off*/
		suppressRowClickSelection:true,/*チェックボックス以外のクリックを認めるか？*/
		rowSelection :'multiple',/*複数選択できるように*/
		localeText: getLocaleText(),/*データ０の時のメッセージ*/
		rowHeight : 40,
		pagination:true,
		paginationPageSize:20
	};
	


	//   対象となる要素の指定
	var eGridDiv = document.querySelector('#myGrid' + divid);

	//   グリッドの生成
	new agGrid.Grid(eGridDiv, gridOptions1_4);

	setFunctions(gridOptions1_4,divid,rowData1_4);
	
	/*今回の場合はテーブル4*/
	var chkflg4 = false;
	//横スクロールでステータス領域が消えた時に左はじにカラー領域を表示
	$("#myGrid" + divid + " div.ag-body-viewport").scroll(function(e){
		//今回のステータス位置が185付近なので今回はそこに設定
		if($("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() > 135 && !chkflg4 ){
			chkflg4 = true;

			showcol(true);
		}else if($("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() <= 135 && chkflg4){
			chkflg4 = false;
			showcol(false);

		}
		checkboxChange();
	});
	//列の表示非表示切り替え
	function showcol(show) {
		gridOptions1_4.columnApi.setColumnVisible('col99', show);
	}
	
	

	getAllCnt(rowData1_4,divid,gridOptions1_4);
	
	
	headerCheckboxChange();
	checkboxChange();
	spredScrollArrowChange(divid);
}

function setFunctions(gridOptions,divid,rowData){
		//セルクリック時に行を選択状態にする
	gridOptions.onCellClicked = function(param){
		/*クリックされたセルの取得*/
		var cols = param.colDef.field;
		var index = param.node.childIndex;
		//セルがチェックボックスセルだったら
		if(cols == "col0"){
			//状況により行のセレクトをon/offする
			var node = gridOptions.api.getRowNode(index);
			if(node.isSelected()){
				node.setSelected(false);

			}else{
				node.setSelected(true);
			}
		}
	}
	//ag-gridの仕様電話再描画されるためソートするとチェックボックスがデフォルトデザインに戻ってしまうためそれを防ぐ
	gridOptions.onSortChanged = function(param){
		checkboxChange();
	}

	gridOptions.onFilterChanged = function(param){
		//モーダル内の検索インプットへのアクティブクラス付与
		$(".ag-floating-filter-input").each(function(){
			if($(this).val().length == 0){
				$(this).removeClass("active");
			}else{
				$(this).addClass("active");
			}
		});
		checkboxChange();
		
	}
	gridOptions.onSelectionChanged= function(param){
		getAllCnt(rowData,divid,gridOptions);
		
	}
	
	
	if(rowData.length > 0 && (divid == 3 || divid == 4)){
		$("#pagerItem" + divid + ".tableitem__tablearea__pagerItem .disabled").removeClass("disabled");
		//各矢印クリックでページ移動
		$("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i").click(function(){
			if($(this).prop("id") == "first" + divid && !$(this).hasClass("disabled")){
				gridOptions.api.paginationGoToFirstPage();
			}else if($(this).prop("id") == "prev" + divid && !$(this).hasClass("disabled")){
				gridOptions.api.paginationGoToPreviousPage();
			}else if($(this).prop("id") == "next" + divid && !$(this).hasClass("disabled")){
				gridOptions.api.paginationGoToNextPage();
			}else if($(this).prop("id") == "last" + divid && !$(this).hasClass("disabled")){
				gridOptions.api.paginationGoToLastPage()
			}
			setPageSep(gridOptions,divid);
		});
		setPageSep(gridOptions,divid);
	}
	var ow = $(".tableitem").innerWidth() - 40;
	//Gridの横幅が画面幅以下なら調整する
	var w = 20;
	
	var coldef = columnDefs1_1;
	var rd = rowData1_1;
	if(divid == "2"){
		coldef = columnDefs1_2;
		rd = rowData1_2;
	}else if(divid == "3"){
		coldef = columnDefs1_3;
		rd = rowData1_3;
	}else if(divid == "4"){
		coldef = columnDefs1_4;
		rd = rowData1_4;
	}
	
	gridWidth(".tableitem",coldef,"#myGrid" + divid, "#table" + divid)
	
	gridHeight(".tableitem",rd,"#myGrid" + divid, "#table" + divid);			
	
}
//pagerの表示件数を変更
function updatePager1(){
	var value = $("#select5").val();
    gridOptions1_3.api.paginationSetPageSize(Number(value));
	checkboxChange();
	setPageSep(gridOptions1_3,3);
}
function updatePager2(){
	var value = $("#select6").val();
    gridOptions1_4.api.paginationSetPageSize(Number(value));
	checkboxChange();
	setPageSep(gridOptions1_4,4);
}
function updatePager3(){
	var value = $("#select7").val();
    gridOptions2.api.paginationSetPageSize(Number(value));
	setPageSep(gridOptions2,5);
	checkboxChange_modal();
}
//pagerの真ん中の構築 
function setPageSep(gridOptions,divid){

	var h = "<span class='tableitem__tablearea__pagerItem__nowpage--desc--big'>" + (gridOptions.api.paginationGetCurrentPage() + 1) + "</span> /" + gridOptions.api.paginationGetTotalPages() + " ";
	$("#pagerItem" + divid + " .tableitem__tablearea__pagerItem__nowpage--desc").html(h);
	//1ページ目では前に戻れないようにdisabled
	if(gridOptions.api.paginationGetCurrentPage() + 1 == 1){
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#first" + divid).addClass("disabled");
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#prev" + divid).addClass("disabled");
	}else{
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#first" + divid).removeClass("disabled");
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#prev" + divid).removeClass("disabled");
	}

	//LASTページ目では次にいけないようにdisabled
	if(gridOptions.api.paginationGetCurrentPage() + 1 == gridOptions.api.paginationGetTotalPages()){
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#next" + divid).addClass("disabled");
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#last" + divid).addClass("disabled");
	}else{
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#next" + divid).removeClass("disabled");
	   $("#pagerItem" + divid + ".tableitem__tablearea__pagerItem i#last" + divid).removeClass("disabled");
	}
	
}

//スプレッドの矢印の構築・再構築
function spredScrollArrowChange(divid){
	//初期表示しているすべて（TAB4）のみロード時に横スクロールの場所設定を行う
	var parentArea = $("#myGrid" + divid + " div.ag-body-container").height();

	var tbarea = $("#myGrid" + divid + " div.ag-body-viewport").scrollTop() + $("#myGrid" + divid + " div.ag-body-viewport").height();
	if(parentArea - tbarea > 0){
		//スクロールバーがある場合の処理
		$("#table" + divid + " div.scrollArrow").css("right","16px");
	}else{
		//スクロールバーがない場合の処理
		$("#table" + divid + " div.scrollArrow").css("right","0px");

	}

	//初期表示しているすべて（TAB4）のみロード時に横スクロールの表示設定を行う
	var parentArea = $("#myGrid" + divid + " div.ag-body-container").width();

	var tbarea = $("#myGrid" + divid + " div.ag-body-viewport").scrollTop() + $("#myGrid" + divid + " div.ag-body-viewport").width();

	if(parentArea - tbarea > 0){
		//スクロールバーがある場合の処理
		$("#table" + divid + " div.scrollArrow").removeClass("dspnone");
		var height = $("#myGrid" + divid + "").innerHeight() - 111;
		var dspheight = $("#myGrid" + divid + " .ag-body-container").innerHeight() ;

		var gh = 0;
		if(height < dspheight){
			gh = height;
		}else{
			gh = dspheight;
		}

		$("#table" + divid + " div.scrollArrow").css("height",gh + "px");

	}else{
		//スクロールバーがない場合の処理
		$("#table" + divid + " div.scrollArrow").addClass("dspnone");

	}



	/*TAB4のテーブル内の→マウスオーバー挙動*/
	var tm1 = 0;
	var tm2 = 0;
	var scrollMax = 0;
	var dspflg1 = true;
	//スクロールイベント
	$("#table" + divid + " div.scrollArrow").hover(function(){
		tm2 = setTimeout(function(){
			tm1 = setInterval(function(){
				//スクロールがMAXになったら表示を消すためにここで値を取得
				scrollMax = $("#myGrid" + divid + " div.ag-body-container").width() - $("#myGrid" + divid + " div.ag-body-viewport").width();

				$("#myGrid" + divid + " div.ag-body-viewport").scrollLeft( $("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() + 2);

				if(scrollMax <= $("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() && dspflg1){
					$("#table" + divid + " div.scrollArrow").addClass("dspnone");
					dspflg1 = false;
					clearTimeout(tm2);
					clearInterval(tm1);
				}
			},0);
		},300);
	},function(){
		clearTimeout(tm2);
		clearInterval(tm1);
	});

	//scrollArrowが消えているときにscrollMAXと値が変わったら再表示
	$("#myGrid" + divid + " div.ag-body-viewport").scroll(function(){

		scrollMax = $("#myGrid" + divid + " div.ag-body-container").width() - $("#myGrid" + divid + " div.ag-body-viewport").width();

		if(scrollMax <= $("#myGrid" + divid + " div.ag-body-viewport").scrollLeft()){
			$("#table" + divid + " div.scrollArrow").addClass("dspnone");
			dspflg1 = false;
		}

		if(scrollMax > $("#myGrid" + divid + " div.ag-body-viewport").scrollLeft() && !dspflg1){
			$("#table" + divid + " div.scrollArrow").removeClass("dspnone");
			dspflg1 = true;
		}
	});	
}
//テーブル作成用ファンクション-入荷実績登録用
var columnDefs2;
var gridOptions2;
var rowData2;
function setGridData2(divid,data){

	// ヘッダー

	//headerName=列の題名,field=列のID,width=幅、cellClas=各セルに適用するクラス
	//headerCheckboxSelection=ヘッダーにチェックボックスを付けるか？
	//checkboxSelection=各行にチェックボックスを表示するか？
	//pinned=列固定,suppressMenu=ヘッダー一列目に検索フィールドを出すか？
	//hide=表示、非表示、sortable=ソートができるかできないか
	//filter=ヘッダー2列目の検索フィールドを何にするか？
	
	/*ヘッダーデータをタイプで入れ替える*/
	columnDefs2 = [
		{headerName: "", field: "col0", width: 30,cellClass: ['tc','cl_w','noCellFocus'],
		 checkboxSelection: true,suppressMenu: true,
		 lockPosition:true,pinned: 'left'
		},
		{headerName: "届先コード", field: "col1",unSortIcon: true,
		 suppressMenu: true,sortable:true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "届先名", field: "col2",unSortIcon: true,
		 suppressMenu: true,sortable:true,width:150,cellClass: ['tl','cellclass']},
		{headerName: "届先住所1", field: "col3",unSortIcon: true,
		 suppressMenu: true,sortable:true,width:150,cellClass: ['tl','cellclass']},
		{headerName: "届先住所2", field: "col4",unSortIcon: true,
		 suppressMenu: true,sortable:true,width:150,cellClass: ['tl','cellclass']},
		{headerName: "届先住所3", field: "col5",unSortIcon: true,
		 suppressMenu: true,sortable:true,width:150,cellClass: ['tl','cellclass']},
		{headerName: "届先住所4", field: "col6",unSortIcon: true,
		 suppressMenu: true,sortable:true,width:150,cellClass: ['tl','cellclass']},
		{headerName: "届先Tel", field: "col7",unSortIcon: true,
		 suppressMenu: true,sortable:true,width:130,cellClass: ['tl','cellclass']},
		{headerName: "届先Fax", field: "col8",unSortIcon: true,
		 suppressMenu: true,sortable:true,width:130,cellClass: ['tl','cellclass']},
		{headerName: "更新日時", field: "col9",unSortIcon: true,
		 suppressMenu: true,sortable:true,width:160,cellClass: ['tl','cellclass']},
		{headerName: "更新者名", field: "col10",unSortIcon: true,
		 suppressMenu: true,sortable:true,width:120,cellClass: ['tl','cellclass']},
		{headerName: "登録日時", field: "col11",unSortIcon: true,
		 suppressMenu: true,sortable:true,width:160,cellClass: ['tl','cellclass']},
		{headerName: "登録者名", field: "col12",unSortIcon: true,
		 suppressMenu: true,sortable:true,width:120,cellClass: ['tl','cellclass']}
	];			
		

	// 行のテストデータ

	rowData2 = [];

	for(var i = 0; i < data;i++){
		
		rowData2.push({col0:i,col1: "00000005" , col2: '村瀬商会 熱田DC', col3: '東京都渋谷区1', 
					  col4: '恵比寿AAS007', col5: 'TEST001', col6: 'TEST001', col7: '086-234-2125', col8: "086-234-2125",col9: "2019-04-01 19:21:56", col10: 'hama001', col11: '2019-04-01 19:21:56', col12: 'hama001'});
	}
			

	// オプションの設定
	gridOptions2 = {
		enableColResize:true,
		enableSorting: true,
		enableFilter: true,
		headerHeight: 30,
		floatingFiltersHeight:30,
		columnDefs: columnDefs2,/*headedrデータ*/
		rowData: rowData2,/*行データ*/
		floatingFilter: true,/*2列目のフィルターのon/off*/
		suppressRowClickSelection:true,/*チェックボックス以外のクリックを認めるか？*/
		localeText: getLocaleText(),/*データ０の時のメッセージ*/
		rowHeight : 40,
		domLayout: 'autoHeight',
		pagination:true,
		paginationPageSize:20
	};

	//   対象となる要素の指定
	var eGridDiv = document.querySelector('#myGrid' + divid);

	//   グリッドの生成
	new agGrid.Grid(eGridDiv, gridOptions2);


	//セルクリック時に行を選択状態にする
	gridOptions2.onCellClicked = function(param){
		/*クリックされたセルの取得*/
		var cols = param.colDef.field;
		var index = param.rowIndex;
		//セルがチェックボックスセルだったら
		if(cols == "col0"){
			//状況により行のセレクトをon/offする
			var node = gridOptions2.api.getRowNode(index);
			if(node.isSelected()){
				node.setSelected(false);

			}else{
				node.setSelected(true);
			}
		}
	}
	gridOptions2.onRowSelected = function(param){
		$("#modal1 .modal-header a").removeClass("disabled");
	}
	//ag-gridの仕様電話再描画されるためソートするとチェックボックスがデフォルトデザインに戻ってしまうためそれを防ぐ
	gridOptions2.onSortChanged = function(param){
		checkboxChange_modal();
	}
	gridOptions2.onFilterChanged = function(param){
		//モーダル内の検索インプットへのアクティブクラス付与
		$(".ag-floating-filter-input").each(function(){
			if($(this).val().length == 0){
				$(this).removeClass("active");
			}else{
				$(this).addClass("active");
			}
		});
		checkboxChange_modal();
	}
	/*今回の場合はテーブル4*/
	var chkflg4 = false;
	//横スクロールでステータス領域が消えた時に左はじにカラー領域を表示
	$("#myGrid" + divid + " div.ag-body-viewport").scroll(function(e){
		checkboxChange_modal();
	});
	gridOptions2.onSelectionChanged= function(param){
		getAllCnt(rowData2,5,gridOptions2);
		
	}
	if(rowData2.length > 0){
		$("#pagerItem5.tableitem__tablearea__pagerItem .disabled").removeClass("disabled");
		//各矢印クリックでページ移動
		$("#pagerItem5.tableitem__tablearea__pagerItem i").click(function(){
			if($(this).prop("id") == "first5" && !$(this).hasClass("disabled")){
				gridOptions2.api.paginationGoToFirstPage();
			}else if($(this).prop("id") == "prev5" && !$(this).hasClass("disabled")){
				gridOptions2.api.paginationGoToPreviousPage();
			}else if($(this).prop("id") == "next5" && !$(this).hasClass("disabled")){
				gridOptions2.api.paginationGoToNextPage();
			}else if($(this).prop("id") == "last5" && !$(this).hasClass("disabled")){
				gridOptions2.api.paginationGoToLastPage()
			}
			setPageSep(gridOptions2,5);
			checkboxChange_modal();
		});
		setPageSep(gridOptions2,5);
		checkboxChange_modal();
	}	
	
	getAllCnt(rowData2,5,gridOptions2);
	
	checkboxChange_modal();

}
//テーブル作成用ファンクション-入荷実績登録用
var columnDefs3;
var gridOptions3;
var rowData3;
function setGridData3(divid,data){

	// ヘッダー

	//headerName=列の題名,field=列のID,width=幅、cellClas=各セルに適用するクラス
	//headerCheckboxSelection=ヘッダーにチェックボックスを付けるか？
	//checkboxSelection=各行にチェックボックスを表示するか？
	//pinned=列固定,suppressMenu=ヘッダー一列目に検索フィールドを出すか？
	//hide=表示、非表示、sortable=ソートができるかできないか
	//filter=ヘッダー2列目の検索フィールドを何にするか？
	
	/*ヘッダーデータをタイプで入れ替える*/
	columnDefs3 = [
		{headerName: "荷主コード", field: "col1",unSortIcon: true,
		 suppressMenu: true,sortable:true,width:120,cellClass: ['tl','cellclass']},
		{headerName: "荷主名", field: "col2",unSortIcon: true,
		 suppressMenu: true,sortable:true,width:150,cellClass: ['tl','cellclass']},
		{headerName: "商品コード", field: "col3",unSortIcon: true,
		 suppressMenu: true,sortable:true,width:120,cellClass: ['tl','cellclass']},
		{headerName: "商品名", field: "col4",unSortIcon: true,
		 suppressMenu: true,sortable:true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "指示数", field: "col5",unSortIcon: true,
		 suppressMenu: true,sortable:true,width:100,cellClass: ['tr','cellclass']},
		{headerName: "在庫数", field: "col6",unSortIcon: true,
		 suppressMenu: true,sortable:true,width:100,cellClass: ['tr','cellclass']},
		{headerName: "欠品数", field: "col7",unSortIcon: true,
		 suppressMenu: true,sortable:true,width:100,cellClass: ['tr','cellclass']}
	];			
		

	// 行のテストデータ

	rowData3 = [];

	for(var i = 0; i < data;i++){
		
		rowData3.push({col0:i,col1: "0000222" , col2: '荷主・SEAOS (B)', col3: 'ITEM01', 
					  col4: '商品1', col5: '1,210', col6: '800', col7: '410'});
	}
			
	// オプションの設定
	gridOptions3 = {
		enableColResize:true,
		enableSorting: true,
		enableFilter: true,
		headerHeight: 30,
		floatingFiltersHeight:30,
		columnDefs: columnDefs3,/*headedrデータ*/
		rowData: rowData3,/*行データ*/
		floatingFilter: true,/*2列目のフィルターのon/off*/
		suppressRowClickSelection:true,/*チェックボックス以外のクリックを認めるか？*/
		localeText: getLocaleText(),/*データ０の時のメッセージ*/
		rowHeight : 40
	};

	//   対象となる要素の指定
	var eGridDiv = document.querySelector('#myGrid' + divid);

	//   グリッドの生成
	new agGrid.Grid(eGridDiv, gridOptions3);
	gridWidth("#otherwindow3",columnDefs3,"#myGrid" + divid, "#table" + divid)
	gridHeight("#otherwindow3",rowData3,"#myGrid" + divid, "#table" + divid,1,0);			
}


//テーブル作成用ファンクション-入荷実績登録用
var columnDefs4;
var gridOptions4;
var rowData4;
function setGridData4(divid,data){

	// ヘッダー

	//headerName=列の題名,field=列のID,width=幅、cellClas=各セルに適用するクラス
	//headerCheckboxSelection=ヘッダーにチェックボックスを付けるか？
	//checkboxSelection=各行にチェックボックスを表示するか？
	//pinned=列固定,suppressMenu=ヘッダー一列目に検索フィールドを出すか？
	//hide=表示、非表示、sortable=ソートができるかできないか
	//filter=ヘッダー2列目の検索フィールドを何にするか？
	
	/*ヘッダーデータをタイプで入れ替える*/
	columnDefs4 = [
		{headerName: "荷主コード", field: "col1",unSortIcon: true,
		 suppressMenu: true,sortable:true,width:120,cellClass: ['tl','cellclass']},
		{headerName: "荷主名", field: "col2",unSortIcon: true,
		 suppressMenu: true,sortable:true,width:150,cellClass: ['tl','cellclass']},
		{headerName: "商品コード", field: "col3",unSortIcon: true,
		 suppressMenu: true,sortable:true,width:120,cellClass: ['tl','cellclass']},
		{headerName: "商品名", field: "col4",unSortIcon: true,
		 suppressMenu: true,sortable:true,width:100,cellClass: ['tl','cellclass']},
		{headerName: "全指示数", field: "col5",unSortIcon: true,
		 suppressMenu: true,sortable:true,width:100,cellClass: ['tr','cellclass']},
		{headerName: "在庫数", field: "col6",unSortIcon: true,
		 suppressMenu: true,sortable:true,width:100,cellClass: ['tr','cellclass']},
		{headerName: "欠品数", field: "col7",unSortIcon: true,
		 suppressMenu: true,sortable:true,width:100,cellClass: ['tr','cellclass']},
		{headerName: "届先コード", field: "col8",unSortIcon: true,
		 suppressMenu: true,sortable:true,width:120,cellClass: ['tl','cellclass']},
		{headerName: "届先名", field: "col9",unSortIcon: true,
		 suppressMenu: true,sortable:true,width:150,cellClass: ['tl','cellclass']},
		{headerName: "届先単位の指示数", field: "col10",unSortIcon: true,
		 suppressMenu: true,sortable:true,width:140,cellClass: ['tl','cellclass']}
		
		
	];			
		

	// 行のテストデータ

	rowData4 = [];

	for(var i = 0; i < data;i++){
		
		rowData4.push({col0:i,col1: "0000222" , col2: '荷主・SEAOS (B)', col3: 'ITEM01', 
					  col4: '商品1', col5: '1,210', col6: '800', col7: '410', col8: "00000001",col9: "村瀬商会 熱田DC",col10: "41"});
	}
			
	// オプションの設定
	gridOptions4 = {
		enableColResize:true,
		enableSorting: true,
		enableFilter: true,
		headerHeight: 30,
		floatingFiltersHeight:30,
		columnDefs: columnDefs4,/*headedrデータ*/
		rowData: rowData4,/*行データ*/
		floatingFilter: true,/*2列目のフィルターのon/off*/
		suppressRowClickSelection:true,/*チェックボックス以外のクリックを認めるか？*/
		localeText: getLocaleText(),/*データ０の時のメッセージ*/
		rowHeight : 40
	};

	//   対象となる要素の指定
	var eGridDiv = document.querySelector('#myGrid' + divid);

	//   グリッドの生成
	new agGrid.Grid(eGridDiv, gridOptions4);
	gridWidth("#otherwindow3",columnDefs4,"#myGrid" + divid, "#table" + divid)
	gridHeight("#otherwindow3",rowData4,"#myGrid" + divid, "#table" + divid,1,0);			


}


//テーブル作成用ファンクション
function setGridData1(divid,data){
	if(divid == "1" && rowData1_1.length == 0){
		setGridData1_1(divid,data);
	}else if(divid == "2" && rowData1_2.length == 0){
		setGridData1_2(divid,data);
	}else if(divid == "3" && rowData1_3.length == 0){
		setGridData1_3(divid,data);
	}else if(divid == "4" && rowData1_4.length == 0){
		setGridData1_4(divid,data);
	}
	
}


//setGridData1系のヘッダーデータ構築
function getColmnDefs(divid){
	if(divid == "1" || divid =="3"){
		return columnDefs1 = [
			{headerName: "", field: "col0", width: 30,cellClass: ['tc','cl_w','noCellFocus'],
			 headerCheckboxSelection: true,checkboxSelection: true,suppressMenu: true,
			 lockPosition:true,pinned: 'left'
			},
			{headerName: "出荷番号", field: "col1", filter:'text',unSortIcon: true,lockPosition:true,
			 suppressMenu: true,width:220,cellClass: ['tl','cellclass'],pinned: 'left',
			 cellRenderer: (params) => {
				 	var ex = "";
				 	if(divid == "1"){
					   if(params.data.col99 == "1" && (params.data.col0 == "1" || params.data.col0 == "6" || params.data.col0 == "1")){
						   ex = "<div class='exc_relative'><span class='exc_red'>!</span></div> ";
					   }else if(params.data.col99 == "1" && (params.data.col0 == "4" || params.data.col0 == "7")){
						   ex = "<div class='exc_relative'><span class='exc_blue'>!</span></div> ";
					   }
				    }
					const element = document.createElement('div');
					element.innerHTML = "<div class='itemcode'><p>" + params.value + "</p><a class='btn_secondary' href='javascript:openOtherWindow_wa3010("+ params.data.col99+ ");'>詳細</a>" + ex + "</div>";

					return element;
				}},
			/*スクロール時にステータスフィールドが消えたら表示するフィールド*/
			{headerName: "", field: "col99",suppressMenu: true,width:8,
			 cellClass: ['tc','cellclass'],hide:true,pinned: 'left',
				cellStyle: function(params) {
					//ステータス色変化
					if (params.value=='0') {
						return {backgroundColor: 'rgba(255,255,255,0.2)',color: 'rgba(255,255,255,0.2)'};
					}else if (params.value=='1') {
						return {backgroundColor: 'rgba(235,153,175,0.2)',color: 'rgba(235,153,175,0.2)'};
					}else if (params.value=='2') {
						return {backgroundColor: 'rgba(227,230,87,0.2)',color: 'rgba(227,230,87,0.2)'};
					}else if (params.value=='3') {
						return {backgroundColor: 'rgba(201,202,202,0.2)',color: 'rgba(201,202,202,0.2)'};
					} else {
						return null;
					}
				}
			},
			{headerName: "出荷ステータス", field: "col2", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:130,cellClass: ['tc','cellclass'],
				cellStyle: function(params) {
					//ステータス色変化
					if (params.value=='欠品') {
						return {backgroundColor: 'rgba(255,255,255,0.2)',color: 'rgba(217,59,59,1)'};
					}else if (params.value=='未引当') {
						return {backgroundColor: 'rgba(235,153,175,0.2)'};
					}else if (params.value=='引当完了') {
						return {backgroundColor: 'rgba(227,230,87,0.2)'};
					}else if (params.value=='キャンセル') {
						return {backgroundColor: 'rgba(201,202,202,0.2)'};
					} else {
						return null;
					}
				}
			},
			{headerName: "受注番号", field: "col3", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:130,cellClass: ['tl','cellclass']},
			{headerName: "荷主名", field: "col4", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:130,cellClass: ['tl','cellclass']},
			{headerName: "出荷指示日", field: "col5", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "配送キャリア名", field: "col6", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tc','cellclass']},
			{headerName: "届先エラー", field: "col7", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tc','cellclass']},
			{headerName: "届先コード", field: "col8", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "届先名", field: "col9", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tr','cellclass']},
			{headerName: "届先郵便番号", field: "col10", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tr','cellclass']},
			{headerName: "届先住所1", field: "col11", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:300,cellClass: ['tl','cellclass']},
			{headerName: "届先住所2", field: "col12", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:180,cellClass: ['tl','cellclass']},
			{headerName: "届先電話番号", field: "col13", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "依頼住所1", field: "col14", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:180,cellClass: ['tl','cellclass']},
			{headerName: "依頼住所2", field: "col15", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:180,cellClass: ['tl','cellclass']},
			{headerName: "依頼住所3", field: "col16", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:180,cellClass: ['tl','cellclass']},
			{headerName: "依頼主名", field: "col17", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
			{headerName: "代行出荷", field: "col18", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "配送区分名", field: "col19", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "営業店止め", field: "col20", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "着荷指示日", field: "col21", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "着荷指定時間帯", field: "col22", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "着荷指定時刻", field: "col23", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "代引金額", field: "col24", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "代引税額", field: "col25", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "出荷備考", field: "col26", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "引当日時", field: "col27", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "引当ユーザー名", field: "col28", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:180,cellClass: ['tl','cellclass']},
			{headerName: "配送データダウンロード日時", field: "col29", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:200,cellClass: ['tl','cellclass']},
			{headerName: "納品書区分名", field: "col30", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "納品書番号", field: "col31", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "出荷完了日時", field: "col32", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "出荷完了ユーザー名", field: "col33", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:180,cellClass: ['tl','cellclass']},
			{headerName: "出荷保留", field: "col34", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "配送会社向け出荷実際送信日時", field: "col35", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:200,cellClass: ['tl','cellclass']},
			{headerName: "仕分コードエラー", field: "col36", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
			{headerName: "送信対象", field: "col37", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "送信日時", field: "col38", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:200,cellClass: ['tl','cellclass']},
			{headerName: "伝票A欄", field: "col39", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "伝票B欄", field: "col40", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "伝票C欄", field: "col41", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "伝票D欄", field: "col42", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "伝票E欄", field: "col43", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "伝票F欄", field: "col44", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "伝票G欄", field: "col45", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "伝票H欄", field: "col46", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "伝票I欄", field: "col47", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "伝票J欄", field: "col48", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "伝票K欄", field: "col49", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "伝票L欄", field: "col50", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "伝票社名", field: "col51", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "伝票店名", field: "col52", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "伝票社・店コード", field: "col53", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
			{headerName: "伝票分類コード", field: "col54", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "伝票区分", field: "col55", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "伝票番号", field: "col56", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "伝票取引先コード", field: "col57", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
			{headerName: "伝票取引先名", field: "col58", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "伝票発注日", field: "col59", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "伝票納品日", field: "col60", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "伝票便", field: "col61", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "更新日時", field: "col62", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "更新者名", field: "col63", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:180,cellClass: ['tl','cellclass']},
			{headerName: "登録日時", field: "col64", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "登録者名", field: "col65", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:180,cellClass: ['tl','cellclass']}
		];
	}else{
		return columnDefs1 = [
			{headerName: "出荷番号", field: "col1", filter:'text',unSortIcon: true,lockPosition:true,
			 suppressMenu: true,width:220,cellClass: ['tl','cellclass'],pinned: 'left',
			 cellRenderer: (params) => {
					const element = document.createElement('div');
					element.innerHTML = "<div class='itemcode'><p>" + params.value + "</p><a class='btn_secondary' href='javascript:openOtherWindow_wa3010("+ params.data.col99+ ");'>詳細</a></div>";

					return element;
				}},
			/*スクロール時にステータスフィールドが消えたら表示するフィールド*/
			{headerName: "", field: "col99",suppressMenu: true,width:8,
			 cellClass: ['tc','cellclass'],hide:true,pinned: 'left',
				cellStyle: function(params) {
					//ステータス色変化
					if (params.value=='0') {
						return {backgroundColor: 'rgba(255,255,255,0.2)',color: 'rgba(255,255,255,0.2)'};
					}else if (params.value=='1') {
						return {backgroundColor: 'rgba(235,153,175,0.2)',color: 'rgba(235,153,175,0.2)'};
					}else if (params.value=='2') {
						return {backgroundColor: 'rgba(227,230,87,0.2)',color: 'rgba(227,230,87,0.2)'};
					}else if (params.value=='3') {
						return {backgroundColor: 'rgba(201,202,202,0.2)',color: 'rgba(201,202,202,0.2)'};
					} else {
						return null;
					}
				}
			},
			{headerName: "出荷ステータス", field: "col2", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:130,cellClass: ['tc','cellclass'],
				cellStyle: function(params) {
					//ステータス色変化
					if (params.value=='欠品') {
						return {backgroundColor: 'rgba(255,255,255,0.2)',color: 'rgba(217,59,59,1)'};
					}else if (params.value=='未引当') {
						return {backgroundColor: 'rgba(235,153,175,0.2)'};
					}else if (params.value=='引当完了') {
						return {backgroundColor: 'rgba(227,230,87,0.2)'};
					}else if (params.value=='キャンセル') {
						return {backgroundColor: 'rgba(201,202,202,0.2)'};
					} else {
						return null;
					}
				}
			},
			{headerName: "受注番号", field: "col3", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:130,cellClass: ['tl','cellclass']},
			{headerName: "荷主名", field: "col4", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:130,cellClass: ['tl','cellclass']},
			{headerName: "出荷指示日", field: "col5", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "配送キャリア名", field: "col6", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tc','cellclass']},
			{headerName: "届先エラー", field: "col7", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tc','cellclass']},
			{headerName: "届先コード", field: "col8", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "届先名", field: "col9", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tr','cellclass']},
			{headerName: "届先郵便番号", field: "col10", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tr','cellclass']},
			{headerName: "届先住所1", field: "col11", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:300,cellClass: ['tl','cellclass']},
			{headerName: "届先住所2", field: "col12", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:180,cellClass: ['tl','cellclass']},
			{headerName: "届先電話番号", field: "col13", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "依頼住所1", field: "col14", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:180,cellClass: ['tl','cellclass']},
			{headerName: "依頼住所2", field: "col15", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:180,cellClass: ['tl','cellclass']},
			{headerName: "依頼住所3", field: "col16", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:180,cellClass: ['tl','cellclass']},
			{headerName: "依頼主名", field: "col17", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
			{headerName: "代行出荷", field: "col18", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "配送区分名", field: "col19", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "営業店止め", field: "col20", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "着荷指示日", field: "col21", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "着荷指定時間帯", field: "col22", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "着荷指定時刻", field: "col23", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "代引金額", field: "col24", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "代引税額", field: "col25", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "出荷備考", field: "col26", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "引当日時", field: "col27", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "引当ユーザー名", field: "col28", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:180,cellClass: ['tl','cellclass']},
			{headerName: "配送データダウンロード日時", field: "col29", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:200,cellClass: ['tl','cellclass']},
			{headerName: "納品書区分名", field: "col30", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "納品書番号", field: "col31", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "出荷完了日時", field: "col32", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "出荷完了ユーザー名", field: "col33", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:180,cellClass: ['tl','cellclass']},
			{headerName: "出荷保留", field: "col34", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "配送会社向け出荷実際送信日時", field: "col35", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:200,cellClass: ['tl','cellclass']},
			{headerName: "仕分コードエラー", field: "col36", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
			{headerName: "送信対象", field: "col37", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "送信日時", field: "col38", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:200,cellClass: ['tl','cellclass']},
			{headerName: "伝票A欄", field: "col39", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "伝票B欄", field: "col40", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "伝票C欄", field: "col41", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "伝票D欄", field: "col42", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "伝票E欄", field: "col43", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "伝票F欄", field: "col44", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "伝票G欄", field: "col45", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "伝票H欄", field: "col46", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "伝票I欄", field: "col47", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "伝票J欄", field: "col48", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "伝票K欄", field: "col49", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "伝票L欄", field: "col50", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "伝票社名", field: "col51", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "伝票店名", field: "col52", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "伝票社・店コード", field: "col53", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
			{headerName: "伝票分類コード", field: "col54", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "伝票区分", field: "col55", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "伝票番号", field: "col56", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "伝票取引先コード", field: "col57", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:140,cellClass: ['tl','cellclass']},
			{headerName: "伝票取引先名", field: "col58", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:120,cellClass: ['tl','cellclass']},
			{headerName: "伝票発注日", field: "col59", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "伝票納品日", field: "col60", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "伝票便", field: "col61", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:100,cellClass: ['tl','cellclass']},
			{headerName: "更新日時", field: "col62", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "更新者名", field: "col63", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:180,cellClass: ['tl','cellclass']},
			{headerName: "登録日時", field: "col64", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:160,cellClass: ['tl','cellclass']},
			{headerName: "登録者名", field: "col65", filter:'text',unSortIcon: true,
			 suppressMenu: true,width:180,cellClass: ['tl','cellclass']}
		];	}	
}
//setGridData1系のデータ構築
function getRowData(divid,data){
	var rowData = [];
	
	
	if(divid == "1"){
		for(var i = 0; i < data;i++){
			if(!$("#checkbox1").prop("checked")){
				rowData.push({col0: i,col99:0,col1: '20190409-0001' + i, col2: '欠品', col3: 'S1-BARA-COD', col4: '荷主・SEAOS (B)', col5: '2019-04-21', col6: '佐川急便', col7: '', col8: 'DT001', col9: 'テスト届先１', col10: '150-0011', col11: '東京都渋谷区恵比寿１丁目１８−１８', col12: '東急不動産恵比寿ビル1F', col13: '03-5791-1171', col14: '東京都新宿区XXXXXXX', col15: 'YYYYYYYYYYYYYYYY', col16: 'ZZZZZZZZZZZZZZZZZ', col17: '荷主・SEAOS (B)', col18: '', col19: '', col20: '', col21: '2019-04-21', col22: '指定なし', col23: '指定なし', col24: '0', col25: '0', col26: '', col27: '2018-12-21 15:47:51', col28: '平田01（seaos管理者）', col29: '2018-12-21 15:47:51', col30: '納品書_BtoB用 ', col31: '', col32: '2018-12-21 15:47:51', col33: '平田01（seaos管理者）', col34: '', col35: '2018-12-21 15:47:51', col36: '', col37: '送信対象', col38: '2019-04-04T09:25:50.000Z ', col39: '', col40: '', col41: '', col42: '', col43: '', col44: '', col45: '', col46: '', col47: '', col48: '', col49: '', col50: '', col51: '', col52: '', col53: '', col54: '', col55: '', col56: '', col57: '', col58: '', col59: '', col60: '', col61: '', col62: '2018-12-21 15:47:51', col63: '小林01', col64: '2018-12-21 15:47:51', col65: '平田01（seaos管理者）'});
			}
			rowData.push({col0: i,col99:1,col1: '20190409-1001' + i, col2: '未引当', col3: 'S1-BARA-COD', col4: '荷主・SEAOS (A)', col5: '2019-04-21', col6: '佐川急便', col7: '', col8: 'DT001', col9: 'テスト届先１', col10: '150-0011', col11: '東京都渋谷区恵比寿１丁目１８−１８', col12: '東急不動産恵比寿ビル1F', col13: '03-5791-1171', col14: '東京都新宿区XXXXXXX', col15: 'YYYYYYYYYYYYYYYY', col16: 'ZZZZZZZZZZZZZZZZZ', col17: '荷主・SEAOS (B)', col18: '', col19: '', col20: '', col21: '2019-04-21', col22: '指定なし', col23: '指定なし', col24: '0', col25: '0', col26: '', col27: '2018-12-21 15:47:51', col28: '平田01（seaos管理者）', col29: '2018-12-21 15:47:51', col30: '納品書_BtoB用 ', col31: '', col32: '2018-12-21 15:47:51', col33: '平田01（seaos管理者）', col34: '', col35: '2018-12-21 15:47:51', col36: '', col37: '送信対象', col38: '2019-04-04T09:25:50.000Z ', col39: '', col40: '', col41: '', col42: '', col43: '', col44: '', col45: '', col46: '', col47: '', col48: '', col49: '', col50: '', col51: '', col52: '', col53: '', col54: '', col55: '', col56: '', col57: '', col58: '', col59: '', col60: '', col61: '', col62: '2018-12-21 15:47:51', col63: '小林01', col64: '2018-12-21 15:47:51', col65: '平田01（seaos管理者）'});			
			
		}
	}else if(divid == "3"){
		for(var i = 0; i < data;i++){
			rowData.push({col0: i,col99:2,col1: '20190409-2001' + i, col2: '引当完了', col3: 'S1-BARA-COD', col4: '荷主・SEAOS (B)', col5: '2019-04-21', col6: '佐川急便', col7: '', col8: 'DT001', col9: 'テスト届先１', col10: '150-0011', col11: '東京都渋谷区恵比寿１丁目１８−１８', col12: '東急不動産恵比寿ビル1F', col13: '03-5791-1171', col14: '東京都新宿区XXXXXXX', col15: 'YYYYYYYYYYYYYYYY', col16: 'ZZZZZZZZZZZZZZZZZ', col17: '荷主・SEAOS (B)', col18: '', col19: '', col20: '', col21: '2019-04-21', col22: '指定なし', col23: '指定なし', col24: '0', col25: '0', col26: '', col27: '2018-12-21 15:47:51', col28: '平田01（seaos管理者）', col29: '2018-12-21 15:47:51', col30: '納品書_BtoB用 ', col31: '', col32: '2018-12-21 15:47:51', col33: '平田01（seaos管理者）', col34: '', col35: '2018-12-21 15:47:51', col36: '', col37: '送信対象', col38: '2019-04-04T09:25:50.000Z ', col39: '', col40: '', col41: '', col42: '', col43: '', col44: '', col45: '', col46: '', col47: '', col48: '', col49: '', col50: '', col51: '', col52: '', col53: '', col54: '', col55: '', col56: '', col57: '', col58: '', col59: '', col60: '', col61: '', col62: '2018-12-21 15:47:51', col63: '小林01', col64: '2018-12-21 15:47:51', col65: '平田01（seaos管理者）'});

			if(!$("#checkbox3").prop("checked")){
				rowData.push({col0: i,col99:3,col1: '20190409-3001' + i, col2: 'キャンセル', col3: 'S1-BARA-COD', col4: '荷主・SEAOS (B)', col5: '2019-04-21', col6: '佐川急便', col7: '', col8: 'DT001', col9: 'テスト届先１', col10: '150-0011', col11: '東京都渋谷区恵比寿１丁目１８−１８', col12: '東急不動産恵比寿ビル1F', col13: '03-5791-1171', col14: '東京都新宿区XXXXXXX', col15: 'YYYYYYYYYYYYYYYY', col16: 'ZZZZZZZZZZZZZZZZZ', col17: '荷主・SEAOS (B)', col18: '', col19: '', col20: '', col21: '2019-04-21', col22: '指定なし', col23: '指定なし', col24: '0', col25: '0', col26: '', col27: '2018-12-21 15:47:51', col28: '平田01（seaos管理者）', col29: '2018-12-21 15:47:51', col30: '納品書_BtoB用 ', col31: '', col32: '2018-12-21 15:47:51', col33: '平田01（seaos管理者）', col34: '', col35: '2018-12-21 15:47:51', col36: '', col37: '送信対象', col38: '2019-04-04T09:25:50.000Z ', col39: '', col40: '', col41: '', col42: '', col43: '', col44: '', col45: '', col46: '', col47: '', col48: '', col49: '', col50: '', col51: '', col52: '', col53: '', col54: '', col55: '', col56: '', col57: '', col58: '', col59: '', col60: '', col61: '', col62: '2018-12-21 15:47:51', col63: '小林01', col64: '2018-12-21 15:47:51', col65: '平田01（seaos管理者）'});
			}
		}
	}else if(divid == "4"){
		for(var i = 0; i < data;i++){
			if(!$("#checkbox4").prop("checked")){
				rowData.push({col0: i,col99:0,col1: '20190409-0001' + i, col2: '欠品', col3: 'S1-BARA-COD', col4: '荷主・SEAOS (B)', col5: '2019-04-21', col6: '佐川急便', col7: '', col8: 'DT001', col9: 'テスト届先１', col10: '150-0011', col11: '東京都渋谷区恵比寿１丁目１８−１８', col12: '東急不動産恵比寿ビル1F', col13: '03-5791-1171', col14: '東京都新宿区XXXXXXX', col15: 'YYYYYYYYYYYYYYYY', col16: 'ZZZZZZZZZZZZZZZZZ', col17: '荷主・SEAOS (B)', col18: '', col19: '', col20: '', col21: '2019-04-21', col22: '指定なし', col23: '指定なし', col24: '0', col25: '0', col26: '', col27: '2018-12-21 15:47:51', col28: '平田01（seaos管理者）', col29: '2018-12-21 15:47:51', col30: '納品書_BtoB用 ', col31: '', col32: '2018-12-21 15:47:51', col33: '平田01（seaos管理者）', col34: '', col35: '2018-12-21 15:47:51', col36: '', col37: '送信対象', col38: '2019-04-04T09:25:50.000Z ', col39: '', col40: '', col41: '', col42: '', col43: '', col44: '', col45: '', col46: '', col47: '', col48: '', col49: '', col50: '', col51: '', col52: '', col53: '', col54: '', col55: '', col56: '', col57: '', col58: '', col59: '', col60: '', col61: '', col62: '2018-12-21 15:47:51', col63: '小林01', col64: '2018-12-21 15:47:51', col65: '平田01（seaos管理者）'});
			}

			rowData.push({col0: i,col99:1,col1: '20190409-1001' + i, col2: '未引当', col3: 'S1-BARA-COD', col4: '荷主・SEAOS (B)', col5: '2019-04-21', col6: '佐川急便', col7: '', col8: 'DT001', col9: 'テスト届先１', col10: '150-0011', col11: '東京都渋谷区恵比寿１丁目１８−１８', col12: '東急不動産恵比寿ビル1F', col13: '03-5791-1171', col14: '東京都新宿区XXXXXXX', col15: 'YYYYYYYYYYYYYYYY', col16: 'ZZZZZZZZZZZZZZZZZ', col17: '荷主・SEAOS (B)', col18: '', col19: '', col20: '', col21: '2019-04-21', col22: '指定なし', col23: '指定なし', col24: '0', col25: '0', col26: '', col27: '2018-12-21 15:47:51', col28: '平田01（seaos管理者）', col29: '2018-12-21 15:47:51', col30: '納品書_BtoB用 ', col31: '', col32: '2018-12-21 15:47:51', col33: '平田01（seaos管理者）', col34: '', col35: '2018-12-21 15:47:51', col36: '', col37: '送信対象', col38: '2019-04-04T09:25:50.000Z ', col39: '', col40: '', col41: '', col42: '', col43: '', col44: '', col45: '', col46: '', col47: '', col48: '', col49: '', col50: '', col51: '', col52: '', col53: '', col54: '', col55: '', col56: '', col57: '', col58: '', col59: '', col60: '', col61: '', col62: '2018-12-21 15:47:51', col63: '小林01', col64: '2018-12-21 15:47:51', col65: '平田01（seaos管理者）'});			

			if(!$("#checkbox4").prop("checked")){
				rowData.push({col0: i,col99:2,col1: '20190409-2001' + i, col2: '引当完了', col3: 'S1-BARA-COD', col4: '荷主・SEAOS (B)', col5: '2019-04-21', col6: '佐川急便', col7: '', col8: 'DT001', col9: 'テスト届先１', col10: '150-0011', col11: '東京都渋谷区恵比寿１丁目１８−１８', col12: '東急不動産恵比寿ビル1F', col13: '03-5791-1171', col14: '東京都新宿区XXXXXXX', col15: 'YYYYYYYYYYYYYYYY', col16: 'ZZZZZZZZZZZZZZZZZ', col17: '荷主・SEAOS (B)', col18: '', col19: '', col20: '', col21: '2019-04-21', col22: '指定なし', col23: '指定なし', col24: '0', col25: '0', col26: '', col27: '2018-12-21 15:47:51', col28: '平田01（seaos管理者）', col29: '2018-12-21 15:47:51', col30: '納品書_BtoB用 ', col31: '', col32: '2018-12-21 15:47:51', col33: '平田01（seaos管理者）', col34: '', col35: '2018-12-21 15:47:51', col36: '', col37: '送信対象', col38: '2019-04-04T09:25:50.000Z ', col39: '', col40: '', col41: '', col42: '', col43: '', col44: '', col45: '', col46: '', col47: '', col48: '', col49: '', col50: '', col51: '', col52: '', col53: '', col54: '', col55: '', col56: '', col57: '', col58: '', col59: '', col60: '', col61: '', col62: '2018-12-21 15:47:51', col63: '小林01', col64: '2018-12-21 15:47:51', col65: '平田01（seaos管理者）'});

				rowData.push({col0: i,col99:3,col1: '20190409-3001' + i, col2: 'キャンセル', col3: 'S1-BARA-COD', col4: '荷主・SEAOS (B)', col5: '2019-04-21', col6: '佐川急便', col7: '', col8: 'DT001', col9: 'テスト届先１', col10: '150-0011', col11: '東京都渋谷区恵比寿１丁目１８−１８', col12: '東急不動産恵比寿ビル1F', col13: '03-5791-1171', col14: '東京都新宿区XXXXXXX', col15: 'YYYYYYYYYYYYYYYY', col16: 'ZZZZZZZZZZZZZZZZZ', col17: '荷主・SEAOS (B)', col18: '', col19: '', col20: '', col21: '2019-04-21', col22: '指定なし', col23: '指定なし', col24: '0', col25: '0', col26: '', col27: '2018-12-21 15:47:51', col28: '平田01（seaos管理者）', col29: '2018-12-21 15:47:51', col30: '納品書_BtoB用 ', col31: '', col32: '2018-12-21 15:47:51', col33: '平田01（seaos管理者）', col34: '', col35: '2018-12-21 15:47:51', col36: '', col37: '送信対象', col38: '2019-04-04T09:25:50.000Z ', col39: '', col40: '', col41: '', col42: '', col43: '', col44: '', col45: '', col46: '', col47: '', col48: '', col49: '', col50: '', col51: '', col52: '', col53: '', col54: '', col55: '', col56: '', col57: '', col58: '', col59: '', col60: '', col61: '', col62: '2018-12-21 15:47:51', col63: '小林01', col64: '2018-12-21 15:47:51', col65: '平田01（seaos管理者）'});			
			}
			
		}
	}
	return rowData;
}
//pagerの件数取得
function getAllCnt(rowData,divid,gridOptions){
	$("#allcount" + divid).html(rowData.length);
	if($("#selectcount" + divid)[0]){
		$("#selectcount" + divid).html(gridOptions.api.getSelectedRows().length);
	}
}

function setGridData_CheckBox(divid,data){
	var r = getRowData(divid,30);
	if(divid == 1){
		gridOptions1_1.api.setRowData([]);
		gridOptions1_1.api.updateRowData({add: r});
		getAllCnt(r,divid,gridOptions1_1);	
	}else if(divid == 3){
		gridOptions1_3.api.setRowData([]);
		gridOptions1_3.api.updateRowData({add: r});
		getAllCnt(r,divid,gridOptions1_3);
		setPageSep(gridOptions1_3,divid);
	}else if(divid == 4){
		gridOptions1_4.api.setRowData([]);
		gridOptions1_4.api.updateRowData({add: r});
		getAllCnt(r,divid,gridOptions1_4);
		setPageSep(gridOptions1_4,divid)
	}
	
	checkboxChange();

	
}

//移動指示キャンセル
function showCancelMessage(){
	
	var cnt = gridOptions1_1.api.getSelectedRows().length;
	if(cnt > 0){
		
		
		for(var i = 0;i < rowData1_1.length;i++){
			var index = i;
			//セルがチェックボックスセルだったら
			//状況により行のセレクトをon/offする
			var node = gridOptions1_1.api.getRowNode(index);
			if(node.isSelected()){
				node.setSelected(false);

			}
		}

		
		
		showMessageBox(cnt + "件の移動指示をキャンセルしました。");
	}
	
	
}
//移動指示作成
function setMessage(type){
	
	if(type == "1"){
		clearData();
		 $('.otherwindow-contentsScroll').animate({ scrollTop: 0 }, 500);
	}else{
		var cnt = gridOptions2.api.getSelectedRows().length;
		if(cnt > 0){


			for(var i = 0;i < rowData2.length;i++){
				var index = i;
				//セルがチェックボックスセルだったら
				//状況により行のセレクトをon/offする
				var node = gridOptions2.api.getRowNode(index);
				if(node.isSelected()){
					node.setSelected(false);

				}
			}

			var selectedData = gridOptions2.api.getSelectedRows();
			var res = gridOptions2.api.setRowData([]);


			showMessageBox(cnt + "件の移動指示を作成しました。");
		}
		//なぜかtimeoutをしかけないと最後のボタンまでdisabledされない。
		setTimeout(function(){
			$(".otherwindow .btn_primary,.otherwindow .btn_secondary").each(function(){
				var index = $(".otherwindow .btn_primary,.otherwindow .btn_secondary").index(this);

				if(index == 6){
					$(this).addClass("disabled");
				}
			});

		},100);

	}
	
}
//指示作業完了
function showMoveMessage(){
	var cnt = gridOptions1_2.api.getSelectedRows().length;
	if(cnt > 0){
		for(var i = 0;i < rowData1_2.length;i++){
			var index = i;
			//セルがチェックボックスセルだったら
			//状況により行のセレクトをon/offする
			var node = gridOptions1_2.api.getRowNode(index);
			if(node.isSelected()){
				node.setSelected(false);

			}
		}
		showMessageBox(cnt + "件の移動指示を完了しました。");
	}
}

//tab切替
function changeTab(tnum){
	var cnt = 1;
	$("section.tabs#tab1 > ul.tabs-nav > li.tabs-nav__item").each(function() {
		if(cnt == 2){
			cnt++;
		}
		$("#tab"+ cnt + "-item").addClass("dspnone");
		if($(this)[0].id == "t" + tnum){
			$("#t" + cnt).addClass("tabs-nav__item--active");
			$("#tab"+ cnt + "-item").removeClass("dspnone");
			
			dt = 30;
			setGridData1(tnum,dt);
			checkboxChange();

		}else{
			$("#t" + cnt).removeClass("tabs-nav__item--active");
		}
		cnt++;
	});

	
}
//tab切替
function changeTabOther(tnum){
	var cnt = 1;
	$("section.tabs#tab2 > ul.tabs-nav > li.tabs-nav__item").each(function() {
		$("#tab_other"+ cnt).addClass("dspnone");
		if($(this)[0].id == "ot" + tnum){
			$("#ot" + cnt).addClass("tabs-nav__item--active");
			$("#tab_other"+ cnt).removeClass("dspnone");
			

		}else{
			$("#ot" + cnt).removeClass("tabs-nav__item--active");
		}
		cnt++;
	});

	
}
//modal内のspredに謎のスクロールが発生
//親要素を少し大きくするスクリプト
function modalSpredHeightControl(){
	if($("#myGrid6")[0]){
		$("#myGrid6 div.ag-body-viewport").css("height",$("#myGrid6 div.ag-body-container").height() + 5 + "px");
	}
}

function openModalWindow_wa3010(id,obj){
	if(id == "1" && !$("#omw_btn1").hasClass("disabled")){
		$("#modal" + id).removeClass("dspnone");
	}else if(id == "2" && !$("#omw_btn2").hasClass("disabled")){

		$("#modal" + id).removeClass("dspnone");
		checkboxChange_modal();
		gridWidth(".modal-content__tablearea",columnDefs2,"#myGrid5", ".modal-content__tablearea");
		gridHeight(".modal-content__tablearea",rowData2,"#myGrid5", ".modal-content__tablearea");			
	}
}

function closeModalWindow_wa3010(id){
	
	for(var i = 0; i < rowData1_1.length;i++){
		//状況により行のセレクトをon/offする
		var node = gridOptions1_1.api.getRowNode(i);
		if(node.isSelected() && !(node.data.col99 == "1" && (node.data.col0 == "1" || node.data.col0 == "6" || node.data.col0 == "1" || node.data.col0 == "4" || node.data.col0 == "7"))){
			node.setSelected(false);

			//登録、修正の場合は下記を実行、グリッドの指定行を青色にし2秒後にトランジションで消す
			$(".ag-pinned-left-cols-container .ag-row").each(function(){
				var index = $(".ag-pinned-left-cols-container .ag-row").index(this)
				if(index == i){
					$(this).addClass("row-update");

					var obj = $(this);
					setTimeout(function(){
						obj.addClass("row-default");
						obj.removeClass("row-update");
					},2000);
				}
			});
			$(".ag-body-container .ag-row").each(function(){
				var index = $(".ag-body-container .ag-row").index(this)
				if(index == i){
					$(this).addClass("row-update");
					var obj = $(this);
					setTimeout(function(){
						obj.addClass("row-default");
						obj.removeClass("row-update");
					},2000);
				}
			});
		}
	}
	
	$("#modal" + id).addClass("dspnone");
}
//modalはラジオボタンにする
function checkboxChange_modal(){
	$("#modal1 .ag-row .ag-selection-checkbox img:nth-child(1)").attr("src","../../../../assets/images/radio_checked.png");
	$("#modal1 .ag-row .ag-selection-checkbox img:nth-child(2)").attr("src","../../../../assets/images/radio_body.png");
	$("#modal2 .ag-row .ag-selection-checkbox img:nth-child(1)").attr("src","../../../../assets/images/radio_checked.png");
	$("#modal2 .ag-row .ag-selection-checkbox img:nth-child(2)").attr("src","../../../../assets/images/radio_body.png");

}

//上からせりあがるウィンドウを閉じる
//グリッドに追加トランジションをかける
function closeOtherWindow_wa3010(){
	$("div.otherwindow").removeClass("openotherwindow");
	
	setTimeout(function(){
		$("div.otherwindow").addClass("dspnone");		
	}, 300);
	//登録、修正の場合は下記を実行、グリッドの指定行を青色にし2秒後にトランジションで消す
	$(".ag-pinned-left-cols-container .ag-row").each(function(){
		var index = $(".ag-pinned-left-cols-container .ag-row").index(this)
		if(index == 0 || index == 1){
			$(this).addClass("row-update");

			var obj = $(this);
			setTimeout(function(){
				obj.addClass("row-default");
				obj.removeClass("row-update");
			},2000);
		}
	});
	$(".ag-body-container .ag-row").each(function(){
		var index = $(".ag-body-container .ag-row").index(this)
		if(index == 0 || index == 1){
			$(this).addClass("row-update");
			var obj = $(this);
			setTimeout(function(){
				obj.addClass("row-default");
				obj.removeClass("row-update");
			},2000);
		}
	});
	
}


//上にせりあがるwindowのdisabled解除
function openOtherWindow_wa3010(status){
	if(status == "0" || status == "1"){
		$("div#otherwindow2").removeClass("dspnone");
		$("div#otherwindow2").addClass("openotherwindow");
	}else if(status == "99"){
		var c1 = false;
		var c2 = false;
		for(var i = 0; i < rowData1_1.length;i++){
			//状況により行のセレクトをon/offする
			var node = gridOptions1_1.api.getRowNode(i);
			if(node.isSelected()){
				if(node.data.col4 == "荷主・SEAOS (A)"){
					c1 = true
				}

				if(node.data.col4 == "荷主・SEAOS (B)"){
					c2 = true
				}

			}
		}
		if(c1 && c2){
			showMessageBox_error("複数荷主の出荷指示を一度に欠品確認することはできません。");
		}else{
			$("div#otherwindow3").removeClass("dspnone");
			$("div#otherwindow3").addClass("openotherwindow");
			//欠品確認
			setGridData3("6",5);
			setGridData4("7",15);

		}


	}else{
		$("div#otherwindow1").removeClass("dspnone");
		$("div#otherwindow1").addClass("openotherwindow");
	}
}

//otherwindow2のボタンコントローラー
function inputControl(ptn){
	if(ptn == 0){
		$("#inputfield1 input[type='text'],input[type='date'],select").removeClass("disabled");
		$("#inputfield1 input[type='text'],input[type='date'],select").prop("disabled",false);
		$("#inputfield1 a.btn_secondary").removeClass("disabled");
		
		
		$(".otherwindow-buttons a").each(function(){
			var index = $(".otherwindow-buttons a").index(this);
			if(index == 0 || index == 1){
				$(this).removeClass("dspnone");
			}else{
				$(this).addClass("dspnone");
			}
		});
	}else if(ptn == 1){
		$("#inputfield1 input[type='text'],input[type='date'],select").addClass("disabled");
		$("#inputfield1 input[type='text'],input[type='date'],select").prop("disabled",true);
		$("#inputfield1 a.btn_secondary").addClass("disabled");
		$(".otherwindow-buttons a").each(function(){
			var index = $(".otherwindow-buttons a").index(this);
			if(index == 2){
				$(this).removeClass("dspnone");
			}else{
				$(this).addClass("dspnone");
			}
		});		
	}else if(ptn == 2){
		$("#inputfield1 input[type='text'],input[type='date'],select").addClass("disabled");
		$("#inputfield1 input[type='text'],input[type='date'],select").prop("disabled",true);
		$("#inputfield1 a.btn_secondary").addClass("disabled");
		$(".otherwindow-buttons a").each(function(){
			var index = $(".otherwindow-buttons a").index(this);
			if(index == 2){
				$(this).removeClass("dspnone");
			}else{
				$(this).addClass("dspnone");
			}
		});		
		showMessageBox("届先情報を更新しました。");
	}
}
//出荷キャンセル・保留のセレクト時の挙動
//ひとまず6行目にエフェクトをかける
$(function(){
	$(".tableitem__btnarea--rightarea__select span").click(function(){
		if(!$(this).parent().hasClass("disabled")){
			$(this).parent().addClass("active");

			$(this).parent().find(".tableitem__btnarea--rightarea__select--list").removeClass("dspnone");
		}
	});
	//overlayをクリックしたら閉じる
	$("body").click(function(ev){
		$(".tableitem__btnarea--rightarea__select").each(function(){
			var select = $(this);
			
			if(!select.hasClass("dspnone")){
				var clickpoint_Y = ev.pageY;
				var clickpoint_X = ev.pageX;

				var selectleft = select.offset().left;
				var selecttop = select.offset().top;
				var selectright = select.offset().left +  select.width();
				var selectbottom = select.offset().top +  select.height();
				if(clickpoint_Y != 0 && clickpoint_X != 0){
					if(!((clickpoint_Y > selecttop && clickpoint_Y < selectbottom ) &&
					   (clickpoint_X > selectleft && clickpoint_X < selectright))){
						select.find(".tableitem__btnarea--rightarea__select--list").addClass("dspnone");
						select.removeClass("active");
					}
				}				
			}
		});
		
		

	});
	
	
	//出荷保留の際の挙動
	$(".tableitem__btnarea--rightarea__select--list--item").click(function(){

		$(this).parent().addClass("dspnone");
		$(this).parent().find(".tableitem__btnarea--rightarea__select--list--item").each(function(){
			$(this).removeClass("active");
		});
		$(this).addClass("active");
		$(this).parent().parent().removeClass("active");
		
		var gb = gridOptions1_1;
		var rd = rowData1_1;
		var grid = "#myGrid1";
		if($(this).parent().parent().parent().parent().parent().prop("id") == "tab3-item"){
			gb = gridOptions1_3;
			rd = rowData1_3;
			grid = "#myGrid3";
		}
		
		var h = $(this).html();
		
		for(var i = 0; i < rd.length;i++){
			//状況により行のセレクトをon/offする
			var node = gb.api.getRowNode(i);
			if(node.isSelected()){
				node.setSelected(false);
				//登録、修正の場合は下記を実行、グリッドの指定行を青色にし2秒後にトランジションで消す
				$(grid + " .ag-pinned-left-cols-container .ag-row").each(function(){
					var index = $(grid + " .ag-pinned-left-cols-container .ag-row").index(this)
					if(index == i){
						$(this).addClass("row-update");
						
						if(h == "出荷保留"){
							console.log($(this).find(".itemcode"));
							$(this).find(".itemcode").append("<div class='exc_relative'><span class='exc_blue'>!</span></div>");		
						}

						var obj = $(this);
						setTimeout(function(){
							obj.addClass("row-default");
							obj.removeClass("row-update");
						},2000);
					}
				});
				$(grid + " .ag-body-container .ag-row").each(function(){
					var index = $(grid + " .ag-body-container .ag-row").index(this)
					if(index == i){
						$(this).addClass("row-update");
						var obj = $(this);
						setTimeout(function(){
							obj.addClass("row-default");
							obj.removeClass("row-update");
						},2000);
					}
				});

				showMessageBox($(this).html() + "に設定しました。");
			}
		}
	});	
});

// JavaScript Document
function selectSelected(id){
	//未選択
	/*if($("#" + id).val().length > 0){
		$("#" + id).addClass("active");
		$("#" + id).removeClass("error");
		$("#" + id).removeClass("disabled");
	//active
	}else{
		$("#" + id).removeClass("active");
		$("#" + id).removeClass("error");
		$("#" + id).removeClass("disabled");
	}*/
}
